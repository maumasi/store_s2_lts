<?php
/**
 * Created by PhpStorm.
 * User: liumaumasi
 * Date: 9/5/16
 * Time: 5:13 PM
 */

namespace AppBundle\DoctrineListener;


use AppBundle\Entity\Customer;
use AppBundle\Entity\User;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

class HashPasswordListener implements EventSubscriber
{

    private $hash;


    /**
     * HashPasswordListener constructor.
     */
    public function __construct(UserPasswordEncoder $hash)
    {
        $this->hash = $hash;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $user = $args->getEntity();

        if ($user instanceof User){
            $this->hashNakedPassword($user);


        }elseif ($user instanceof Customer){
            $this->hashGuestNakedPassword($user);
        }

        return;



    }



    public function preUpdate(LifecycleEventArgs $args)
    {
        $user = $args->getEntity();

        if ($user instanceof User){
            $this->hashNakedPassword($user);
        }elseif ($user instanceof Customer){
            $this->hashGuestNakedPassword($user);
        }else{
            return;
        }



        $em = $args->getEntityManager();
        $meta = $em->getClassMetadata(get_class($user));
        $em->getUnitOfWork()->recomputeSingleEntityChangeSet($meta, $user);

    }

    
    
    public function getSubscribedEvents()
    {
        return ['prePersist', 'preUpdate'];
    }



    /**
     * @param User $entity
     */
    private function hashNakedPassword(User $user)
    {
        if (!empty($user->getNakedPassword())){

            $hashed = $this->hash->encodePassword($user, $user->getNakedPassword());
            $user->setPassword($hashed);
        }


    }


    /**
     * @param Customer $entity
     */
    private function hashGuestNakedPassword(Customer $user)
    {
        if (!empty($user->getNakedPassword())){

            $hashed = $this->hash->encodePassword($user, $user->getNakedPassword());
            $user->setPassword($hashed);
        }


    }


}