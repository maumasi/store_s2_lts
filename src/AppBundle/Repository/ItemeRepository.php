<?php
/**
 * Created by PhpStorm.
 * User: liumaumasi
 * Date: 9/2/16
 * Time: 3:55 PM
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Item;
use Doctrine\ORM\EntityRepository;

/**
 * Class ItemeRepository
 * @package AppBundle\Repository
 */
class ItemeRepository  extends EntityRepository
{

    /**
     * @param $id
     * @return object|null
     */
    public function findItemById($id)
    {
        return $this->createQueryBuilder('item')
            ->andWhere('item.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();


//            use this to get multiple records
//            ->execute();
    }

//    No good, it sucks
//    /**
//     * @param $id
//     * @return array
//     */
    public function findItemsByVendorById($id)
    {
        return $this->createQueryBuilder('item')
            ->leftJoin('item.user', 'user')
            ->andWhere('user.id = :id')
            ->setMaxResults(5)
//            ->setFirstResult(3)
            ->setParameter('id', $id)
            ->getQuery()
            ->execute();
    }


    public function findItemByVendor($user)
    {
        return $this->createQueryBuilder('item')
            ->andWhere('item.user = :user')
            ->setParameter(':user', $user)
            ->getQuery()
            ->execute();
    }



    public function findItemByVendorWithLimit($user, $limit)
    {
        return $this->createQueryBuilder('item')
            ->andWhere('item.user = :user')
            ->setParameter(':user', $user)
            ->setMaxResults($limit)
            ->getQuery()
            ->execute();
    }



    public function itemsPerPage($pageLimit)
    {
        return $this->createQueryBuilder('item')
            ->setMaxResults(16)
            ->setFirstResult($pageLimit)
            ->getQuery()
            ->execute();
    }



//
//
//    public function getPendingOrders()
//    {
//
//        return $this->createQueryBuilder('item')
////            ->leftJoin('orders.item', 'item')
////            ->leftJoin('orders.customer', 'customer')
//            ->leftJoin('item.user', 'user')
//            ->andWhere('user.id = :id')
//            ->setParameter('id', 21)
//            ->getQuery()
//            ->execute();
//    }
//




}