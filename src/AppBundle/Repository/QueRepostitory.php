<?php
/**
 * Created by PhpStorm.
 * User: liumaumasi
 * Date: 9/4/16
 * Time: 10:07 AM
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

/**
 * Class QueRepostitory
 * @package AppBundle\Repository
 */
class QueRepostitory extends EntityRepository
{

    /**
     * @param $vendor
     * @return mixed
     */
    public function findVendorById($vendor)
    {

        return $this->createQueryBuilder('vendorQue')
            ->leftJoin('vendorQue.vendor', 'user')
            ->andWhere('user.id = :id')
            ->orderBy('vendorQue.createdAt', 'DESC')
            ->setParameter('id', $vendor)
            ->getQuery()
            ->execute();
    }


    /**
     * @param $customer
     * @return mixed
     */
    public function findCustomerById($customer)
    {

        return $this->createQueryBuilder('vendorQue')
            ->leftJoin('vendorQue.customer', 'customer')
            ->andWhere('customer.id = :id')
            ->setParameter('id', $customer)
            ->getQuery()
            ->execute();
    }




    /**
     * @param $item
     * @return mixed
     */
    public function findItemById($item)
    {

        return $this->createQueryBuilder('vendorQue')
            ->leftJoin('vendorQue.item', 'item')
            ->andWhere('item.id = :id')
            ->setParameter('id', $item)
            ->getQuery()
            ->execute();
    }













}