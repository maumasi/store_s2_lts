<?php
/**
 * Created by PhpStorm.
 * User: liumaumasi
 * Date: 9/2/16
 * Time: 3:44 PM
 */

namespace AppBundle\Repository;


use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

/**
 * Class UserRepository
 * @package AppBundle\Repository
 */
class UserRepository extends EntityRepository
{

    /**
     * @param $id
     * @return object|null
     */
    public function findVendorById($id)
    {
        return $this->createQueryBuilder('user')
            ->andWhere('user.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();


//            use this to get multiple records
//            ->execute();
    }


    public function getPendingOrders()
    {



        return $this->createQueryBuilder('user')
//            ->leftJoin('orders.item', 'item')
//            ->leftJoin('orders.customer', 'customer')
            ->leftJoin('user.item', 'item')
            ->andWhere('user.id = :id')
            ->setParameter('id', 21)
            ->getQuery()
            ->execute();
    }



}



//            select u.first_name as 'Knife Maker', i.item_name as Item, c.first_name as Customer
//            from item i
//
//            join user u
//            on u.id = i.user_id
//
//            join orders o
//            on o.item_id = i.id
//
//            join customer c
//            on o.customer_id = c.id
//
//            where u.id = 20 <---- maker's ID

