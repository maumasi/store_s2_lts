<?php
/**
 * Created by PhpStorm.
 * User: liumaumasi
 * Date: 9/4/16
 * Time: 7:50 AM
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

/**
 * Class CustomerRepository
 * @package AppBundle\Repository
 */
class CustomerRepository extends EntityRepository
{
    /**
     * @param $id
     * @return mixed
     */
    public function findCustomerById($id)
    {


        return $this->createQueryBuilder('customer')
            ->andWhere('customer.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }



    public function findCustomerByEmail($email)
    {

        return $this->createQueryBuilder('customer')
            ->andWhere('customer.email = :email')
            ->setParameter('email', $email)
            ->getQuery()
            ->getOneOrNullResult();
    }


    /**
     * @param $orderCustomerId
     * @return mixed
     */
    public function findCustomerOrder($orderCustomerId)
    {

//       return $this->createQueryBuilder('orders')
//           ->leftJoin('')
//            ->andWhere('orders.customer = :id')
//            ->setParameter('id', $orderCustomerId)
//            ->getQuery()
//            ->execute();

//        $cart = $this->createQueryBuilder('item')
//            ->leftJoin('item.order', 'order_obj')
//            ->leftJoin('order_obj.customer', 'customer_obj')
//            ->andWhere('customer_obj.id = :customerId')
//            ->setParameter('customerId', $orderCustomerId)
//            ->getQuery()
//            ->execute();

//        return $this->createQueryBuilder('item')
//            ->leftJoin('item.order', 'order_obj')
//            ->leftJoin('order_obj.customer', 'customer_obj')
//            ->andWhere('customer_obj.id = :customerId')
//            ->setParameter('customerId', $orderCustomerId)
//            ->getQuery()
//            ->execute();

    }
}