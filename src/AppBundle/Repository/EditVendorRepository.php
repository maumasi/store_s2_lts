<?php
/**
 * Created by PhpStorm.
 * User: liumaumasi
 * Date: 9/2/16
 * Time: 3:44 PM
 */

namespace AppBundle\Repository;


use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;


class EditVendorRepository extends EntityRepository
{

    /**
     * @param $id
     * @return object|null
     */
    public function findEditByVendorById($id)
    {
        return $this->createQueryBuilder('user')
            ->andWhere('user.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();


//            use this to get multiple records
//            ->execute();
    }



}