<?php
/**
 * Created by PhpStorm.
 * User: liumaumasi
 * Date: 9/4/16
 * Time: 6:39 AM
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

/**
 * Class OrderRepository
 * @package AppBundle\Repository
 */
class OrderRepository extends EntityRepository
{

    /**
     * @param $customerId
     * @return mixed
     */
    public function findByCustomerId($customerId)
    {

        return $this->createQueryBuilder('orders')
            ->andWhere('orders.customer = :id')
            ->setParameter('id', $customerId)
            ->getQuery()
            ->execute();
    }


    public function findByCustomer($customer)
    {

        return $this->createQueryBuilder('orders')
            ->andWhere('orders.customer = :customer')
            ->setParameter('customer', $customer)
            ->getQuery()
            ->execute();
    }


    /**
     * @param $itemId
     * @return mixed
     */
    public function findCartItemsById($itemId)
    {

        return $this->createQueryBuilder('orders')
            ->andWhere('orders.item = :id')
            ->setParameter('id', $itemId)
            ->getQuery()
            ->execute();
    }







    public function getPendingOrders($id)
    {



        return $this->createQueryBuilder('orders')
            ->innerJoin('orders.item', 'item')
            ->innerJoin('orders.customer', 'customer')
            ->join('item.user', 'user')
            ->addOrderBy('orders.createdAt', 'DESC')
            ->andWhere('user.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->execute();
    }



//            select u.first_name as 'Knife Maker', i.item_name as Item, c.first_name as Customer
//            from item i
//
//            join user u
//            on u.id = i.user_id
//
//            join orders o
//            on o.item_id = i.id
//
//            join customer c
//            on o.customer_id = c.id
//
//            where u.id = 20 <---- maker's ID



















//    /**
//     * @param $itemId
//     * @param $customerId
//     * @return mixed
//     */
//    public function findCustomerOrder($itemId, $customerId)
//    {
//        return $this->createQueryBuilder('orders')
//            ->leftJoin('orders.customer', 'customer')
//            ->leftJoin('orders.item', 'item')
//            ->setParameters(['itemId:' => $itemId, ':customerId' => $customerId])
//            ->getQuery()
//            ->execute();
//    }
}