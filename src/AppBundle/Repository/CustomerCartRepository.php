<?php
/**
 * Created by PhpStorm.
 * User: liumaumasi
 * Date: 9/19/16
 * Time: 9:46 AM
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class CustomerCartRepository extends EntityRepository
{


    public function cartItems($userId)
    {

        return $this->createQueryBuilder('cart')
            ->leftJoin('cart.customer', 'customer')
            ->andWhere('customer.id = :id')
            ->setParameter(':id', $userId)
            ->getQuery()
            ->execute();
    }


    public function findCartItemById($item, $customer)
    {
        return $this->createQueryBuilder('cart')
            ->leftJoin('cart.item', 'item')
            ->leftJoin('cart.customer', 'customer')
            ->andWhere('item = :item')
            ->andWhere('customer = :customer')
            ->setParameter(':item', $item)
            ->setParameter(':customer', $customer)
            ->getQuery()
            ->getOneOrNullResult();

    }


    public function findItemToDelete($item, $customer)
    {
        return $this->createQueryBuilder('cart')
            ->andWhere('cart.customer = :customer')
            ->andWhere('cart.item = :item')
            ->setParameter(':customer', $customer)
            ->setParameter(':item', $item)
            ->getQuery()
            ->getOneOrNullResult();
    }
}