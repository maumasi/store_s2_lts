<?php
/**
 * Created by PhpStorm.
 * User: liumaumasi
 * Date: 9/9/16
 * Time: 3:53 AM
 */

namespace AppBundle\EventListener;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class EntitySubscriber implements EventSubscriberInterface
{

    public function onKernelRequest(GetResponseEvent $event)
    {
//        die('test');

        $request = $event->getRequest();

        $t = 'test';

//        $t = stripos($userAgent, 'mac') !== false;
        $request->attributes->set('t', $t);


//        note: change the controller
//        $request->attributes->set('_controller', function (){
//
//            return new Response("Hello Yall!!");
//        });
    }
    
    
    public static function getSubscribedEvents()
    {
       return array(
           'kernel.request' => 'onKernelRequest'
       );
    }

}