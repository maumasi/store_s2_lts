<?php


namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Item;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nelmio\Alice\Fixtures;

class Loadfixtures implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {


        Fixtures::load(__DIR__.
            '/fixtures.yml',
            $manager,
            [
                'providers' => [$this]
            ]
            );
        
    }

    public function knife()
    {
        $knives = [
            'Chefs knife',
            'Pocket knife',
            'Utility knife',
            'fishing knife',
            'Hunting knife',
            'Butterfly knife',
            'Fighter knife',
            'Cake knife',
            'Butter knife',
            'Throwing knife',
            'Wild knife',
            'Chizle tip knife'

        ];

        $key = array_rand($knives);

        return $knives[$key];
    }




}
