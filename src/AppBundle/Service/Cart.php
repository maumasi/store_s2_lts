<?php

namespace AppBundle\Service;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Item;
use AppBundle\Entity\CustomerCart;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;

class Cart
{

//    name for the session
    const CART_ITEM_KEY = '_cart.items';

    const MOVE_CART_AFTER_LOGIN = '_cart.afterLogin';

    private $session;
    private $em;
    private $items;
    private $customer;


//    inject sessions and the entity manager to help with this tool
    public function __construct(Session $session, EntityManager $em)
    {
        $this->session = $session;
        $this->em = $em;
    }


//
    public function addItem(Item $item, $user)
    {

//    if user is signed in make sure they are set up as a customer to use the cart

        if($user){
//
            $this->customer = $this->em->getRepository('AppBundle:Customer')
                ->findCustomerByEmail($user->getEmail());

            if($this->customer) {


                $items = $this->getItems($this->customer);
                $notInCart = true;

                //            dump($item->getId());
                //            die;

                foreach ($items as $cartItem) {

                    //                dump($cartItem);
                    //                die;
                    if ($cartItem->getItem()->getId() == $item->getId()) {
                        $notInCart = false;
                    }
                }

                if ($notInCart) {

                    $cart = new CustomerCart();

                    $cart->setCreatedAt(new \DateTime());
                    $cart->setCustomer($this->customer);
                    $cart->setItem($item);
                    $cart->setQuantity((int)$_POST['quantity']);
                    $this->em->persist($cart);
                    $this->em->flush();

                    $itemQ = $this->em->getRepository('AppBundle:Item')
                        ->findItemById($item->getId());
                    $itemQ->setQuantity($item->getQuantity() - (int)$_POST['quantity']);
                    $this->em->persist($itemQ);
                    $this->em->flush();

                    //                $items[] = $item;

                } else {

                    //                dump($item); die;
                    $cartItem = $this->em->getRepository('AppBundle:CustomerCart')
                        ->findCartItemById($item, $this->customer);

                    //                dump($cartItem); die;


                    $itemQ = $this->em->getRepository('AppBundle:Item')
                        ->findItemById($item->getId());

                    $itemQ->setQuantity($item->getQuantity() + $cartItem->getQuantity());
                    $this->em->persist($itemQ);
                    $this->em->flush();


                    $cartItem->setQuantity((int)$_POST['quantity']);
                    $this->em->persist($cartItem);
                    $this->em->flush();


                    $itemQ = $this->em->getRepository('AppBundle:Item')
                        ->findItemById($item->getId());
                    $itemQ->setQuantity($item->getQuantity() - (int)$_POST['quantity']);
                    $this->em->persist($itemQ);
                    $this->em->flush();


                }
            }

//            $this->updateCart($items);


        }else{


//        if user is not a customer or not signed in set cart up to use sessions
            $items = $this->getItems(null);

            if (!in_array($item, $items)) {


                $items[] = $item;

//            echo "hi";
//            die;
            }

//        dump($items); die;

            $this->updateCart($items);

        }
    }


//    get items with ids from the session
    /**
     * @return Item[]
     */
    public function getItems($user)
    {

        if ($user){


            //    if this is a vendor and they have a customer account
            $vendorIsNewCustomer = $this->em->getRepository('AppBundle:Customer')
                    ->findCustomerByEmail($user->getEmail()) == null;

//                this is for when the customer is an instance of a user (Vendor)

//            dump($vendorIsNewCustomer);
//            die;


            if($vendorIsNewCustomer) {

                $newCustomer = new Customer();

                $newCustomer->setEmail($user->getEmail());
                $newCustomer->setFirstName($user->getFirstName());
                $newCustomer->setLastName($user->getLastName());
                $newCustomer->setAddressLine1($user->getAddressLine1());
                $newCustomer->setAddressLine2($user->getAddressLine2());
                $newCustomer->setCity($user->getCity());
                $newCustomer->setState($user->getState());
                $newCustomer->setZip($user->getZip());
                $newCustomer->setPhone($user->getPhone());
                $newCustomer->setCreatedAt($user->getCreatedAt());

                $this->em->persist($newCustomer);
                $this->em->flush();


                $this->customer = $this->em->getRepository('AppBundle:Customer')
                    ->findCustomerByEmail($user->getEmail());

//                dump($user);
//                die;

            }else{

                $this->customer = $this->em->getRepository('AppBundle:Customer')
                    ->findCustomerByEmail($user->getEmail());
            }

            $cartItems = $this->em->getRepository('AppBundle:CustomerCart')
                ->cartItems($this->customer->getId());

            $cart = [];
            foreach ($cartItems as $itemInCart){

                $date = $itemInCart->getCreatedAt();
                $today = new \DateTime();

//                $newDate = $date->modify('+ 2 days');
//                dump($itemInCart->getCreatedAt());

//                dump($date->modify('+ 2 days')->format('Y-m-d') < $today->format('Y-m-d'));

//                die;

//                dump($itemInCart->getCreatedAt());

                $cart [] = $itemInCart;

                if ($itemInCart->getQuantity() == 0){


                    $this->em->remove($itemInCart);
                    $this->em->flush();
                }


//        if item has been in cart for 48hrs or more return it to the that item's quantity
                if ($date->modify('+ 2 days')->format('Y-m-d') < $today->format('Y-m-d')){

                    $this->removeItem($itemInCart, $this->customer);
                }
            }

//            die;

            return $cart;
//            return $this->em->getRepository('AppBundle:CustomerCart')
//                ->cartItems($this->customer->getId());

        }else{

            if ($this->items === null) {
                $itemRepo = $this->em->getRepository('AppBundle:Item');
                $ids = $this->session->get(self::CART_ITEM_KEY, []);

//            dump($ids); die;
                $items = [];

                foreach ($ids as $id) {
                    $item = $itemRepo->find($id);

                    // in case a item obj becomes deleted by the vendor
                    if ($item) {
                        $items[] = $item;
                    }
                }

                $this->items = $items;
            }

//        dump($this->items); die;
            return $this->items;
        }

    }




//    get the total price of all items in the cart
    public function getTotal($user)
    {
        $this->customer = $this->em->getRepository('AppBundle:Customer')
            ->findCustomerByEmail($user->getEmail());

        $total = 0;

        if ($this->customer){

            $cartItems = $this->em->getRepository('AppBundle:CustomerCart')
                ->cartItems($this->customer->getId());


            foreach ($cartItems as $itemInCart){

                $total += $itemInCart->getItem()->getPrice();
            }

            return $total;

        }else{

            foreach ($this->getItems($user) as $item) {
                $total += $item->getPrice();
            }
            return $total;
        }


    }













//    set cart to an empty array
    public function emptyCart($user)
    {
        $this->customer = $this->em->getRepository('AppBundle:Customer')
            ->findCustomerByEmail($user->getEmail());

        $cartItems = $this->getItems($user);

//        dump($cartItems);
//        die;

        if ($this->customer){


            foreach ($cartItems as $item){

                $itemInCart = $this->em->getRepository('AppBundle:CustomerCart')
                    ->findCartItemById($item->getItem()->getId(), $this->customer);
                $this->em->remove($itemInCart);
                $this->em->flush();
            }

        }else{

            $this->updateCart([]);
        }

    }












//    delete an item from the cart
    public function removeItem(Item $itemDelete, $user)
    {
        $this->customer = $this->em->getRepository('AppBundle:Customer')
            ->findCustomerByEmail($user->getEmail());

        if($this->customer){


            $cartItem = $this->em->getRepository('AppBundle:CustomerCart')
                ->findItemToDelete($itemDelete, $this->customer);


            $item = $this->em->getRepository('AppBundle:Item')
                ->findItemById($itemDelete->getId());

            $item->setQuantity($item->getQuantity() + $cartItem->getQuantity());
            $this->em->persist($item);
            $this->em->flush();


            $this->em->remove($cartItem);
            $this->em->flush();

        }else{
//
//            $items = $this->getItems($this->customer);
//
////        dump($items); die;
//            foreach ($items as $index => $item){
//                if ($item === $itemDelete){
//                    array_splice($items, $index, 1);
//                }
//            }
//
//            $this->updateCart($items);
        }





//        dump($items); die;
    }



















//
    /**
     * @param Item[] $items
     */
    private function updateCart(array $items)
    {
        $this->items = $items;

        $ids = array_map(function(Item $item) {
            return $item->getId();
        }, $items);

//        dump($ids); die;

        $this->session->set(self::CART_ITEM_KEY, $ids);
    }




}