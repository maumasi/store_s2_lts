<?php
/**
 * Created by PhpStorm.
 * User: liumaumasi
 * Date: 9/7/16
 * Time: 2:44 PM
 */

namespace AppBundle\Service;


use Symfony\Component\HttpFoundation\File\UploadedFile;

class Upload
{
    private $uploadDir;

    public function __construct($uploadDir)
    {
        $this->uploadDir = $uploadDir;
    }

    public function img(UploadedFile $img)
    {
        $itemImg = md5(uniqid()).'.'.$img->guessExtension();

        $img->move($this->uploadDir, $itemImg);

        return $itemImg;
    }
}