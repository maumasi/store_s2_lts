<?php
/**
 * Created by PhpStorm.
 * User: liumaumasi
 * Date: 9/10/16
 * Time: 5:05 PM
 */

namespace AppBundle\Service;


use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class Que
{

    private $em;
    private $controller;

    //    inject sessions and the entity manager to help with this tool
    public function __construct(EntityManager $em, Controller $controller)
    {
        $this->em = $em;
        $this->controller = $controller;
    }

    public function que(User $user)
    {

        $em = $this->em;

        $vendorId = $user->getId();

        $orderRepo = $em->getRepository('AppBundle:Orders');
        $customerRepo = $em->getRepository('AppBundle:Customer');
        $itemRepo = $em->getRepository('AppBundle:Item');

//        get orders for this vendor and prep item ids and customer ids
        $pendingOrders = $orderRepo->getPendingOrders($vendorId);

//        dump($pendingOrders);
//        die;
        $customerOrder = array();

        for($i = 0; $i < count($pendingOrders); $i++) {

//            dump($pendingOrders);
//            die;

            $customerId = $pendingOrders[$i]->getCustomer()->getId();

            if (!in_array($customerId, $customerOrder)){


//                dump($pendingOrders);
//                die;

                $customer = $customerRepo->findCustomerById($customerId);


//            I changed this, testing it out
                $customerOrder[$customerId] = array(
                    'customer' => $customer,
                    'order' => array()
                );
            }// if
        }// loop





//        loop over all the orders to get the customers and items they ordered
        for($i = 0; $i < count($pendingOrders); $i++){

            $itemId = $pendingOrders[$i]->getItem()->getId();
            $customerId = $pendingOrders[$i]->getCustomer()->getId();

//        get customer details
//            $customer = $customerRepo->findCustomerById($customerId);


//        get item details
            $item = $itemRepo->findItemById($itemId);

//            dump($pendingOrders[$i]);
//            die;
//            array_push($customerOrder[$customerId]['order'], $item);

            $customerOrder[$customerId]['order'] = [

                'que' => [
                    'item' => $item,
                    'quantity' => $pendingOrders[$i]->getQuantity()
                ]
            ];

        }// loop


//        dump($customerOrder);
//        die;

        return $customerOrder;
    }
}