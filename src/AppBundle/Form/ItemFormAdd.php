<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ItemFormAdd extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('itemName',TextType::class,[
                'trim' => true,
            ])

            ->add('description', TextareaType::class, [
                'trim' => true,
            ])

            ->add('price', MoneyType::class,[
                'trim' => true,
                'currency' => 'USD'
            ])

            ->add('quantity', IntegerType::class,[
                'trim' => true,
            ])

            ->add('itemImg', FileType::class, [
                'trim' => true,
////                'required' => false,
////                'empty_value' => false
//                'attr' =>[
//                    'required' => false
//                ]
            ]);
//            ->add('isInStock', CheckboxType::class,[
//                'trim' => true,
//                'attr' => [
//                    'checked' => true
//                ]
//            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Item',
            'validation_groups' => ['Default', 'Create']
        ]);
    }

    public function getName()
    {
        return 'app_bundle_item_create_form';
    }
}
