<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\EmailValidator;


class NewUserForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('companyName')
            ->add('phone')
            ->add('email', EmailType::class)
            ->add('addressLine1')
            ->add('addressLine2')
            ->add('city')
            ->add('state')
            ->add('zip')
            ->add('nakedPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Password didn\'t match. Please try again.',
                'first_options' => [
                    'label' => 'Password'
                ],

                'second_options' => [
                    'label' => 'Confirm Password'
                ]
            ]);


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\User',
            'validation_groups' => ['Default', 'Register']
        ]);
    }
}
