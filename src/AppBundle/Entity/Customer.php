<?php
/**
 * Created by PhpStorm.
 * User: liumaumasi
 * Date: 9/2/16
 * Time: 9:52 PM
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CustomerRepository")
 * @ORM\Table(name="customer")
 * @UniqueEntity(fields={"email"}, message="This email is already registered. Try logging in.")
 */
class Customer implements UserInterface
{





    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true, unique=true)
     */
    private $stripeId;


    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", nullable=true)
     */
    private $firstName;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", nullable=true)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $phone;

    /**
     *  @Assert\Email(
     *     message="{{ value }} is not a valid email.",
     *     checkMX = "true",
     *     checkHost = "true"
     * )
     *
     * @ORM\Column(type="string", nullable=true, unique=true)
     */
    private $email;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", nullable=true)
     */
    private $addressLine1;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $addressLine2;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", nullable=true)
     */
    private $city;



    /**
     * @Assert\NotBlank()
     * @Assert\Type("numeric")
     * @Assert\Length(
     *     min="5",
     *     max="5",
     *     minMessage="The zip code should be 5 digits.",
     *     maxMessage="The zip code should be 5 digits."
     * )
     * @ORM\Column(type="integer", nullable=true)
     */
    private $zip;

//
//    /**
//     * @Assert\NotBlank()
//     * @ORM\ManyToOne(targetEntity="State",  inversedBy="customer" )
//     * @ORM\JoinColumn(nullable=true)
//     */
//    private $state;


    /**
     * @ORM\Column(type="string" , nullable=true)
     */
    private $state;



    /**
     * @ORM\OneToMany(targetEntity="Orders",  mappedBy="customer" )
     * @ORM\JoinColumn(nullable=true)
     */
    private $order;

    

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="VendorQue", mappedBy="customer")
     * @ORM\JoinColumn(nullable=true)
     */
    private $que;



    /**
     * @ORM\Column(type="string", nullable=true)
     *
     */
    private $password;

    /**
     * @Assert\NotBlank(groups={"Register"})
     */
    private $nakedPassword;






    /**
     * @return ArrayCollection|Orders[]
     */
    public function __construct()
    {
        $this->order = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getQue()
    {
        return $this->que;
    }

    /**
     * @param mixed $que
     */
    public function setQue(VendorQue $que)
    {
        $this->que = $que;
    }


    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }


    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

//    /**
//     * @param mixed $order
//     */
//    public function setOrder(Orders $order)
//    {
//        $this->order = $order;
//    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }


    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getAddressLine1()
    {
        return $this->addressLine1;
    }

    /**
     * @param mixed $addressLine1
     */
    public function setAddressLine1($addressLine1)
    {
        $this->addressLine1 = $addressLine1;
    }

    /**
     * @return mixed
     */
    public function getAddressLine2()
    {
        return $this->addressLine2;
    }

    /**
     * @param mixed $addressLine2
     */
    public function setAddressLine2($addressLine2)
    {
        $this->addressLine2 = $addressLine2;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param mixed $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return mixed
     */
    public function getStripeId()
    {
        return $this->stripeId;
    }

    /**
     * @param mixed $stripeId
     */
    public function setStripeId($stripeId)
    {
        $this->stripeId = $stripeId;
    }




//    ============ below are needed for security

    public function getUsername()
    {
        return $this->email;
    }


    public function eraseCredentials()
    {
        $this->nakedPassword = null;
    }
//
//    public function setUsername($username)
//    {
//        $this->username = $username;
//    }


    public function getRoles()
    {
        return array('ROLE_CUSTOMER');
    }


    /**
     * @return mixed
     */
    public function getNakedPassword()
    {
        return $this->nakedPassword;
    }

    /**
     * @param mixed $nakedPassword
     */
    public function setNakedPassword($nakedPassword)
    {
        $this->nakedPassword = $nakedPassword;

        $this->password = null;
    }


    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }


    /**
     * @return mixed
     */
    public function getSalt()
    {
//        return $this->salt;
    }


}