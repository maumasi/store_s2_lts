<?php
/**
 * Created by PhpStorm.
 * User: liumaumasi
 * Date: 9/4/16
 * Time: 9:21 AM
 */

namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QueRepostitory")
 * @ORM\Table(name="vendorQue")
 */
class VendorQue
{



    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @Assert\Type("float")
     * @Assert\Range(
     *     min="0",
     *     minMessage="Set price must be 0.00 or higher."
     * )
     * @ORM\Column(type="decimal", nullable=true)
     */
    private $price;


    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $itemName;


    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;


    /**
     * @ORM\Column(type="string",  nullable=true)
     */
    private $itemImg;


    /**
     * @ORM\Column(type="datetime")
     *
     */
    private $createdAt;


    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isCompleted = false;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="que")
     * @ORM\JoinColumn(nullable=true)
     */private $vendor;


    /**
     * @ORM\ManyToOne(targetEntity="Customer", inversedBy="que")
     * @ORM\JoinColumn(nullable=true)
     *
     */
    private $customer;

//    /**
//     * @ORM\OneToMany(targetEntity="Item", mappedBy="que")
//     * @ORM\JoinColumn(nullable=true)
//     *
//     */
//    private $item;
//








    /**
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param mixed $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }


    /**
     * @param mixed $customer
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
    }


    /**
     * @return mixed
     */
    public function getCustomer()
    {
        return $this->customer;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * @param mixed $vendor
     */
    public function setVendor(User $vendor)
    {
        $this->vendor = $vendor;
    }


    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getItemName()
    {
        return $this->itemName;
    }

    /**
     * @param mixed $itemName
     */
    public function setItemName($itemName)
    {
        $this->itemName = $itemName;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }


    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }


    /**
     * @return mixed
     */
    public function getItemImg()
    {
        return $this->itemImg;
    }

    /**
     * @param mixed $itemImg
     */
    public function setItemImg($itemImg)
    {
        $this->itemImg = $itemImg;
    }

    /**
     * @return mixed
     */
    public function getIsCompleted()
    {
        return $this->isCompleted;
    }

    /**
     * @param mixed $isCompleted
     */
    public function setIsCompleted($isCompleted)
    {
        $this->isCompleted = $isCompleted;
    }




}