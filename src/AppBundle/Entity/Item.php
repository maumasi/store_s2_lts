<?php
/**
 * Created by PhpStorm.
 * User: liumaumasi
 * Date: 9/1/16
 * Time: 2:49 PM
 */


//
//use Doctrine\ORM\Mapping as ORM;
//use Symfony\Component\Security\Core\User\UserInterface;
//
///**
// * @ORM\Entity(repositoryClass="AppBundle\Repository\ItemeRepository")
// * @ORM\Table(name="item")
// */
//abstract class Item implements UserInterface


namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ItemeRepository")
 * @ORM\Table(name="item")
 */
class Item
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @Assert\NotBlank()
     * @Assert\Type("float")
     * @Assert\Range(
     *     min="0",
     *     minMessage="Set price must be 0.00 or higher."
     * )
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $price;


    /**
     * @Assert\NotBlank(
     *     message="Give your customers a name for this product."
     * )
     * @ORM\Column(type="string", nullable=true)
     */
    private $itemName;


    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

//
//    /**
//     * @ORM\Column(type="boolean")
//     */
//    private $isInStock;


    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isRemovedFromStore;


    /**
     *@Assert\NotNull(groups={"Create"})
     * @ORM\Column(type="string",  nullable=true)
     */
    private $itemImg;


    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;


    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="item")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     *
     */
    private $user;



    /**
     * @ORM\OneToMany(targetEntity="Orders",  mappedBy="item")
     * @ORM\JoinColumn(nullable=true)
     *
     */
    private $order;



    /**
     * @ORM\OneToMany(targetEntity="CustomerCart",  mappedBy="item")
     * @ORM\JoinColumn(nullable=true)
     *
     */
    private $cart;


    /**
     * @Assert\GreaterThan(0)
     * @Assert\NotBlank()
     * @ORM\Column(type="integer")
     */
    private $quantity;






    public function __construct()
    {

//        $this->que = new ArrayCollection();
        $this->order = new ArrayCollection();

    }



    /**
     * @return mixed
     */
    public function getQue()
    {
        return $this->que;
    }




    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }



//    /**
//     * @param mixed $order
//     */
//    public function setOrder($order)
//    {
//        $this->order = $order;
//    }




    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }




    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getItemName()
    {
        return $this->itemName;
    }

    /**
     * @param mixed $itemName
     */
    public function setItemName($itemName)
    {
        $this->itemName = $itemName;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }


    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }


    /**
     * @return mixed
     */
    public function getItemImg()
    {
        return $this->itemImg;
    }

    /**
     * @param mixed $itemImg
     */
    public function setItemImg($itemImg)
    {
        $this->itemImg = $itemImg;
    }


    /**
     * @return mixed
     */
    public function getIsInStock()
    {
        return $this->isInStock;
    }

    /**
     * @param mixed $isInStock
     */
    public function setIsInStock($isInStock)
    {
        $this->isInStock = $isInStock;
    }

    /**
     * @return mixed
     */
    public function getIsRemovedFromStore()
    {
        return $this->isRemovedFromStore;
    }

    /**
     * @param mixed $isRemovedFromStore
     */
    public function setIsRemovedFromStore($isRemovedFromStore)
    {
        $this->isRemovedFromStore = $isRemovedFromStore;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @param mixed $cart
     */
    public function setCart($cart)
    {
        $this->cart = $cart;
    }


}