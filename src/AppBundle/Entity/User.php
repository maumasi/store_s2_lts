<?php
/**
 * Created by PhpStorm.
 * User: liumaumasi
 * Date: 9/1/16
 * Time: 2:06 PM
 */


//
//use Doctrine\ORM\Mapping as ORM;
//use Symfony\Component\Security\Core\User\UserInterface;
//
///**
// * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
// * @ORM\Table(name="user")
// */
//abstract class User implements UserInterface
//{





namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\Role\Role;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="user")
 * @UniqueEntity(fields={"email"}, message="This email is already registered. Try logging in.")
 */
class User implements UserInterface
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @Assert\NotBlank(groups={"Register"})
     * @ORM\Column(type="string")
     */
    private $firstName;


    /**
     * @Assert\NotBlank(groups={"Register"})
     * @ORM\Column(type="string", nullable=true)
     */
    private $lastName;


//    unique=true
    /**
     * @Assert\NotBlank(groups={"Register"})
     * @Assert\Email(
     *     groups={"Register"},
     *     message="{{ value }} is not a valid email.",
     *     checkMX = "true",
     *     checkHost = "true"
     * )
     * @ORM\Column(type="string", nullable=true, unique=true)
     */
    private $email;


    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $phone;


    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $password;

    /**
     * @Assert\NotBlank(groups={"Register"})
     */
    private $nakedPassword;

//    /**
//     * @ORM\Column(type="string")
//     */
//    private $passwordHashed;

//    /**
//     * @ORM\Column(type="string")
//     */
//    private $salt;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $companyName;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isActive = true;


    /**
     * @ORM\OneToMany(targetEntity="Item",  mappedBy="user")
     * @ORM\JoinColumn(nullable=true)
     */
    private $item;

    /**
     * @ORM\OneToMany(targetEntity="EditUser" ,  mappedBy="user")
     * @ORM\JoinColumn(nullable=true)
     */
    private $editUser;


    /**
     * @ORM\OneToMany(targetEntity="VendorQue" ,  mappedBy="vendor")
     * @ORM\JoinColumn(nullable=true)
     */
    private $que;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    private $createdAt;


    /**
     * @ORM\Column(type="string", nullable=true, unique=true)
     */
    private $stripeId;


    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", nullable=true)
     */
    private $addressLine1;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $addressLine2;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", nullable=true)
     */
    private $city;



    /**
     * @Assert\NotBlank()
     * @Assert\Type("numeric")
     * @Assert\Length(
     *     min="5",
     *     max="5",
     *     minMessage="The zip code should be 5 digits.",
     *     maxMessage="The zip code should be 5 digits."
     * )
     * @ORM\Column(type="integer", nullable=true)
     */
    private $zip;

//
//    /**
//     * @Assert\NotBlank()
//     * @ORM\ManyToOne(targetEntity="State",  inversedBy="customer" )
//     * @ORM\JoinColumn(nullable=true)
//     */
//    private $state;


    /**
     * @ORM\Column(type="string" , nullable=true)
     */
    private $state;



//    /**
//     * @return ArrayCollection|User[]
//     */
//    public function __construct ()
//    {
////        $this->$editUser = new ArrayCollection();
//        $this->item = new ArrayCollection();
//        $this->que = new ArrayCollection();
//
//    }









    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }



//    /**
//     * @return mixed
//     */
//    public function getPasswordHashed()
//    {
//        return $this->passwordHashed;
//    }

//    /**
//     * @param mixed $passwordHashed
//     */
//    public function setPasswordHashed($passwordHashed)
//    {
//        $this->passwordHashed = $passwordHashed;
//    }



    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param mixed $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }


    /**
     * @return mixed
     */
    public function getEditUser()
    {
        return $this->editUser;
    }

    /**
     * @return mixed
     */
    public function getQue()
    {
        return $this->que;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }



    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

//
//    public function __construct($username = null)
//    {
//        $this->username = $username;
//    }

    public function getUsername()
    {
        return $this->email;
    }



    public function eraseCredentials()
    {
        $this->nakedPassword = null;
    }
//
//    public function setUsername($username)
//    {
//        $this->username = $username;
//    }


    public function getRoles()
    {
        return array('ROLE_VENDOR');
    }


    /**
     * @return mixed
     */
    public function getNakedPassword()
    {
        return $this->nakedPassword;
    }

    /**
     * @param mixed $nakedPassword
     */
    public function setNakedPassword($nakedPassword)
    {
        $this->nakedPassword = $nakedPassword;

        $this->password = null;
    }


    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }


    /**
     * @return mixed
     */
    public function getSalt()
    {
//        return $this->salt;
    }

    /**
     * @return mixed
     */
    public function getStripeId()
    {
        return $this->stripeId;
    }

    /**
     * @param mixed $stripeId
     */
    public function setStripeId($stripeId)
    {
        $this->stripeId = $stripeId;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getAddressLine1()
    {
        return $this->addressLine1;
    }

    /**
     * @param mixed $addressLine1
     */
    public function setAddressLine1($addressLine1)
    {
        $this->addressLine1 = $addressLine1;
    }

    /**
     * @return mixed
     */
    public function getAddressLine2()
    {
        return $this->addressLine2;
    }

    /**
     * @param mixed $addressLine2
     */
    public function setAddressLine2($addressLine2)
    {
        $this->addressLine2 = $addressLine2;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param mixed $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }






}