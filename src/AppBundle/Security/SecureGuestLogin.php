<?php
/**
 * Created by PhpStorm.
 * User: liumaumasi
 * Date: 9/5/16
 * Time: 4:47 AM
 */


namespace AppBundle\Security;


use AppBundle\Form\LoginForm;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;

class SecureGuestLogin extends AbstractFormLoginAuthenticator
{
    private $formFactory;
    private $em;
    private $router;
    private $hashedPassword;





//    inject some func tools to help get the job done

    /**
     * SecureGuestLogin constructor.
     */
    public function __construct(
        FormFactoryInterface $formFactory,
        EntityManager $em,
        RouterInterface $router,
        UserPasswordEncoder $hashedPassword)
    {

        $this->formFactory = $formFactory;
        $this->em = $em;
        $this->router = $router;
        $this->hashedPassword = $hashedPassword;
    }






    public function getCredentials(Request $request)
    {

//        set a bool to check for a form submit and that the current user's ulr is on the login page

        $loginSubmited = $request->getPathInfo() == '/guestLogin' && $request->isMethod('POST');

//      if the login form was not submitted and the current page is not the login page
//      return null and kill the rest of the authentication checks

        if(!$loginSubmited){

            return;
        }


//        create a version of the login form and grab the field values from it and set
//        the last tried email to the email field if the this is the only check that passes
//        so that the user doesn't have to re-type in their email.

        $loginForm = $this->formFactory->create(LoginForm::class);
        $loginForm->handleRequest($request);

        $data = $loginForm->getData();

        $request->getSession()->set(
            Security::LAST_USERNAME,
            $data['_email']
        );

        return $data;
    }





//    run the credentials from the login form against the DB to return the user if they exists

    public function getUser($credentials, UserProviderInterface $userProvider)
    {

        $email = $credentials['_email'];

        return $this->em->getRepository('AppBundle:Customer')
            ->findOneBy(['email' =>$email]);
    }



//  check the password against the one matching the user's email

    public function checkCredentials($credentials, UserInterface $user)
    {
        $password = $credentials['_password'];

        if($this->hashedPassword->isPasswordValid($user, $password)){

            return true;
        }
        return false;
    }



//  keep user on the login page if they fail to login

    protected function getLoginUrl()
    {
        return $this->router->generate('secure_guest_login');

    }

//  send guest to the store if they pass validation

    protected function getDefaultSuccessRedirectUrl()
    {
        return $this->router->generate('store');
    }
}