<?php
/**
 * Created by PhpStorm.
 * User: liumaumasi
 * Date: 9/3/16
 * Time: 1:08 AM
 */



namespace AppBundle\Controller;

use AppBundle\Entity\EditUser;
use AppBundle\Entity\User;
use AppBundle\Entity\VendorQue;
use AppBundle\Form\EditUserForm;
use AppBundle\Form\NewUserForm;
use AppBundle\Form\QueItemForm;
use AppBundle\Service\Salt;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;



/**
 * @Route("/vendor", schemes={"%secure_channel%"})
 * @Security("is_granted('ROLE_VENDOR')")
 */
class VendorController extends Controller
{




//      vender's dashboard
    /**
     * @Route("/dashboard", name="vendor_dashboard")
     */
    public function vendorDashboard()
    {
        return $this->render('vendor/vendorDashboard.html.twig',[
            'cartCount' => $this->get('cart')->getItems($this->getUser())
        ]);

    }








    /**
     * @Route("/que", name="que_list")
     */
    public function queList()
    {

        $queList = $this->get('que_list')->que($this->getUser());

//        $t = 0;
//        foreach ($queList as $key => $value){
//
//            $t = $queList[$key]['order']['que']['quantity'];
//
////            dump($t);
//        }
//
////        die;


//        dump($queList);
//        die;
        return $this->render('vendor/vendorQue/queList.html.twig',[
            'queList' => $queList,
            'cartCount' => $this->get('cart')->getItems($this->getUser())
        ]);
    }














    /**
     * @Route("/que/order/{customerId}", name="que_order")
     */
    public function queOrder(Request $request, $customerId)
    {

        $que = $queList = $this->get('que_list')->que($this->getUser());

        $order = [];

        foreach ($que as $key => $value){

            if($key === (int)$customerId){
                $order = $value;
            }
        }// foreach

//        dump($order);
////        dump($customerId);
//        die;

//        dump($order);
//        die;

        return $this->render('vendor/vendorQue/queOrder.html.twig',[
            'order' => $order,
            'cartCount' => $this->get('cart')->getItems($this->getUser())
        ]);
    }


















//      find what orders are in the vendor's que. still needs a lot of work
    /**
     * @Route("/vendorQue", name="vendor_item_list")
     */
    public function queItemList(Request $request)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $queSet = $em->getRepository('AppBundle:VendorQue')
            ->findVendorById($user->getId());

//        dump($queSet); die;
//
        $form = $this->createForm(QueItemForm::class);
        $form->handleRequest($request);
//
        if($form->isSubmitted() && $form->isValid()){



//            return $this->redirectToRoute();
        }


        return $this->render('vendor/queItemsList.html.twig',[
            'queSet' => $queSet,
            'queForm' => $form->createView(),
            'cartCount' => $this->get('cart')->getItems($this->getUser())
//            'items' => $itemsInQue
        ]);
    }








}






