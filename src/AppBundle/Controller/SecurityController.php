<?php
/**
 * Created by PhpStorm.
 * User: liumaumasi
 * Date: 9/5/16
 * Time: 4:47 AM
 */

namespace AppBundle\Controller;





use AppBundle\Entity\User;
use AppBundle\Form\EditUserForm;
use AppBundle\Form\GuestEditForm;
use AppBundle\Form\LoginForm;
use AppBundle\Form\NewGuestForm;
use AppBundle\Form\NewUserForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;



class SecurityController extends Controller
{



//      registration page
    /**
     * @Route("vendor/register", name="vendor_register", schemes={"%secure_channel%"})
     */
    public function registerVendor(Request $request)
    {
        $form = $this->createForm(NewUserForm::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $form->getData()->setCreatedAt(new \DateTime());
            $user = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();


            $this->get('security.authentication.guard_handler')
                ->authenticateUserAndHandleSuccess(
                    $user,
                    $request,
                    $this->get('app.security.login'),
                    'main'
                );
            return $this->redirectToRoute('vendor_dashboard');
        }

        return $this->render('vendor/vendorSignup.html.twig',[
            'registrationForm' => $form->createView(),
            'cartCount' => $this->get('cart')->getItems($this->getUser())
        ]);
    }







//     Login page
    /**
     * @Route("/login", name="secure_login", schemes={"%secure_channel%"})
     */
    public function login()
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        $form = $this->createForm(LoginForm::class,[
            '_email' => $lastUsername
        ]);

        return $this->render(
            'security/vendorLogin.html.twig',
            array(
                // last username entered by the user
                'form' => $form->createView(),
                'error'         => $error,
                'cartCount' => $this->get('cart')->getItems($this->getUser())
            )
        );
    }





//      vendor edit page
    /**
     * @Route("vendor/edit", name="vendor_edit", schemes={"%secure_channel%"})
     */
    public function newVendor(Request $request)
    {
        $form = $this->createForm(EditUserForm::class, $this->getUser());
        $form->handleRequest($request);


        if($form->isSubmitted() && $form->isValid()){



            $user = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();


            return $this->get('security.authentication.guard_handler')
                ->authenticateUserAndHandleSuccess(
                    $user,
                    $request,
                    $this->get('app.security.login'),
                    'main'
                );
        }

        return $this->render('vendor/vendorEdit.html.twig',[
            'registrationForm' => $form->createView(),
            'cartCount' => $this->get('cart')->getItems($this->getUser())
        ]);
    }


//    this just gives symfony a chance to redirect the user to the home page (the page I set for logging out)
//    to prevent a non-URL page from generating
    /**
     * @Route("/logout", name="secure_logout")
     */
    public function logout()
    {
//        only the developer will see this
        throw new \Exception('This should not be reached.');
    }



//    ========================================================      Guest login stuff







//      registration page
    /**
     * @Route("store/register", name="guest_register", schemes={"%secure_channel%"})
     */
    public function registerGuest(Request $request)
    {
        $form = $this->createForm(NewGuestForm::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $form->getData()->setCreatedAt(new \DateTime());
            $user = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();


            $this->get('security.authentication.guard_handler')
                ->authenticateUserAndHandleSuccess(
                    $user,
                    $request,
                    $this->get('app.security.guest_login'),
                    'main'
                );
            return $this->redirectToRoute('store');
        }

        return $this->render('store/guestSignup.html.twig',[
            'registrationForm' => $form->createView(),
            'cartCount' => $this->get('cart')->getItems($this->getUser())
        ]);
    }









//      customer edit page
    /**
     * @Route("store/guest_edit_profile", name="guest_edit", schemes={"%secure_channel%"})
     */
    public function editGuest(Request $request)
    {
        $form = $this->createForm(GuestEditForm::class, $this->getUser());
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $form->getData()->setCreatedAt(new \DateTime());
            $user = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();


            $this->get('security.authentication.guard_handler')
                ->authenticateUserAndHandleSuccess(
                    $user,
                    $request,
                    $this->get('app.security.guest_login'),
                    'main'
                );
            return $this->redirectToRoute('store');
        }

        return $this->render('store/guestEditProfile.html.twig',[
            'registrationForm' => $form->createView(),
            'cartCount' => $this->get('cart')->getItems($this->getUser())
        ]);
    }










//     Guest login page
    /**
     * @Route("/guestLogin", name="secure_guest_login", schemes={"%secure_channel%"})
     */
    public function guestLogin()
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        $form = $this->createForm(LoginForm::class,[
            '_email' => $lastUsername
        ]);

        return $this->render(
            'security/guestLogin.html.twig',
            array(
                // last username entered by the user
                'form' => $form->createView(),
                'error'         => $error,
                'cartCount' => $this->get('cart')->getItems($this->getUser())
            )
        );
    }









}


