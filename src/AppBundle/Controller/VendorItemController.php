<?php
/**
 * Created by PhpStorm.
 * User: liumaumasi
 * Date: 9/7/16
 * Time: 12:35 PM
 */

namespace AppBundle\Controller;


use AppBundle\Entity\User;
use AppBundle\Form\ItemForm;
use AppBundle\Form\ItemFormAdd;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Item;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/vendor", schemes={"%secure_channel%"})
 * @Security("is_granted('ROLE_VENDOR')"))
 */

class VendorItemController extends Controller
{

    //      vender's item create
    /**
     * @Route("/itemCreate", name="vendor_item_create")
     */
    public function itemCreate(Request $request)
    {

        $form = $this->createForm(ItemFormAdd::class);
        $form->handleRequest($request);



        if($form->isSubmitted() && $form->isValid()){

            $item = $form->getData();

            $item->setUser($this->getUser());
            $item->setCreatedAt(new \DateTime());

            $file = $item->getItemImg();
            $imgName = $this->get('app.service.upload')->img($file);
            $item->setItemImg($imgName);

//            dump( $item->getItemImg()->getMimeType() ); die;
            $em = $this->getDoctrine()->getManager();
            $em->persist($item);
            $em->flush();


            $items = $em->getRepository('AppBundle:Item')
                ->findItemsByVendorById($this->getUser()->getId());

            return $this->render('vendor/vendorItemViews/itemCollection.html.twig',[
                'items' => $items,
                'cartCount' => $this->get('cart')->getItems($this->getUser())
            ]);
        }



        return $this->render('vendor/vendorItemViews/itemCreate.html.twig',[
            'itemForm' => $form->createView(),
            'cartCount' => $this->get('cart')->getItems($this->getUser())
        ]);
    }





    //      vender's item collection (read)
    /**
     * @Route("/itemCollection", name="vendor_item_collection")
     */
    public function itemCollection()
    {
        $em = $this->getDoctrine()->getManager();
//        $items = $em->getRepository('AppBundle:Item')
//            ->findItemsByVendorById($this->getUser()->getId());

        $items = $em->getRepository('AppBundle:Item')
            ->findItemByVendor($this->getUser());



//        dump($items); die;
        return $this->render('vendor/vendorItemViews/itemCollection.html.twig',[
            'items' => $items,
            'cartCount' => $this->get('cart')->getItems($this->getUser())
        ]);

    }





    //      vender's item edit
    /**
     * @Route("/{id}/itemEdit", name="vendor_item_edit")
     */
    public function itemEdite(Request $request, Item $itemEdit)
    {
        $fileSys = new Filesystem();
        $lastImg = $itemEdit->getItemImg();
        $itemEdit->setItemImg(
            new File($this->getParameter('item_img_directory').'/'.$itemEdit->getItemImg())
        );

        $oldImg = $itemEdit->getItemImg()->getPathName();


        $form = $this->createForm(ItemForm::class, $itemEdit);
        $form->handleRequest($request);



        if($form->isSubmitted() && $form->isValid()){

            $item = $form->getData();


            $item->setUser($this->getUser());
            $item->setCreatedAt(new \DateTime());

//            delete old image if a new image is uploaded
            if ($item->getItemImg() !== null){
                $fileSys->remove($oldImg);

                $file = $item->getItemImg();
                $imgName = $this->get('app.service.upload')->img($file);
                $item->setItemImg($imgName);
            }else{

                $item->setItemImg($lastImg);
            }





//            dump( $item->getItemImg()->getMimeType() ); die;
            $em = $this->getDoctrine()->getManager();
            $em->persist($item);
            $em->flush();


            $items = $em->getRepository('AppBundle:Item')
                ->findItemByVendor($this->getUser());

            return $this->render('vendor/vendorItemViews/itemCollection.html.twig',[
                'items' => $items,
                'cartCount' => $this->get('cart')->getItems($this->getUser())
            ]);
        }




        return $this->render('vendor/vendorItemViews/itemEdit.html.twig',[
            'itemForm' => $form->createView(),
            'cartCount' => $this->get('cart')->getItems($this->getUser())
        ]);
    }





    //      vender's item collection (read)




//
//    //      vender's item edit
//    /**
//     * @Route("{id}/itemEdit/", name="vendor_item_edit")
//     */
//    public function itemUpdate(Request $request, Item $itemEdit)
//    {
////        $oldImg = $itemEdit->getItemImg();
//
////        $itemEdit->setItemImg(
////            new File($this->getParameter('item_img_directory').'/'.$itemEdit->getItemImg())
////        );
//
//
//
//        $form = $this->createForm(ItemForm::class, $itemEdit);
//        $form->handleRequest($request);
//
//
//
//        if($form->isSubmitted() && $form->isValid()){
//
//            $item = $form->getData();
//
//            $item->setUser($this->getUser());
//            $item->setCreatedAt(new \DateTime());
//
//
//            $file = $item->getItemImg();
//            $imgName = $this->get('app.service.upload')->img($file);
//            $item->setItemImg($imgName);
//
//
////            $oldImg = $itemEdit->getItemImg();
////
////            $item->setItemImg(
////                new File($oldImg)
////            );
//
//
////            dump( $item->getItemImg()->getMimeType() ); die;
//            $em = $this->getDoctrine()->getManager();
//            $em->persist($item);
//            $em->flush();
//
//
//            $items = $em->getRepository('AppBundle:Item')
//                ->findItemsByVendorById($this->getUser()->getId());
//
//            return $this->render('vendor/vendorItemViews/itemCollection.html.twig',[
//                'items' => $items
//            ]);
//        }
//
//
//
//        return $this->render('vendor/vendorItemViews/itemCreate.html.twig',[
//            'itemForm' => $form->createView()
//        ]);
//    }
//







//          vender's item delete
    /**
     * @Route("/{id}/itemDeleteConfirm", name="vendor_item_confirm_delete")
     */
    public function itemDeleteConfirm($id)
    {
        $em = $this->getDoctrine()->getManager();


        $item = $em->getRepository('AppBundle:Item')
            ->findItemById($id);

        return $this->render('vendor/vendorItemViews/itemDeleteConfirm.html.twig',[
            'item' => $item,
            'cartCount' => $this->get('cart')->getItems($this->getUser())
        ]);
    }
















//          vender's item confirm delete
    /**
     * @Route("/{id}/itemDelete", name="vendor_item_delete")
     */
    public function itemDelete($id)
    {

        $fileSys = new Filesystem();

        $em = $this->getDoctrine()->getManager();
        $item = $em->getRepository('AppBundle:Item')
            ->findItemById($id);


        $oldImg = new File($this->getParameter('item_img_directory').'/'.$item->getItemImg());


//        dump($oldImg->getPathname()); die;

        try{

//            delete form DB
            $em->remove($item);
            $em->flush();


//            delete image after item has successfully been deleted for the DB
            $fileSys->remove($oldImg->getPathname());

        }catch (ForeignKeyConstraintViolationException $e){
//            echo "Oops";

            $this->get('session')->getFlashBag()->add('stillHasOrders', 'This item still has pending orders. You can mark this item as "Removed From Store" to remove it from the store until this item no longer has pending orders.');

            return $this->render('vendor/vendorItemViews/itemDeleteConfirm.html.twig',[
                'item' => $item,
                'cartCount' => $this->get('cart')->getItems($this->getUser())
            ]);
        }


        $items = $em->getRepository('AppBundle:Item')
            ->findItemsByVendorById($this->getUser()->getId());

//        dump($items);
//        die;
        return $this->render('vendor/vendorItemViews/itemCollection.html.twig',[
            'items' => $items,
            'cartCount' => $this->get('cart')->getItems($this->getUser())
        ]);
    }





}