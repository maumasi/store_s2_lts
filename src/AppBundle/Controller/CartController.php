<?php
/**
 * Created by PhpStorm.
 * User: liumaumasi
 * Date: 9/8/16
 * Time: 3:28 PM
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Customer;
use AppBundle\Entity\Item;
use AppBundle\Entity\Orders;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Component\BrowserKit\Request;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;


/**
 * Class CartController
 * @package AppBundle\Controller
 * @Route("store/cart", schemes={"%secure_channel%"})
 * @Security("is_granted('ROLE_CUSTOMER') || is_granted('ROLE_VENDOR')")
 */
class CartController extends Controller
{

    /**
     * @Route("/checkout", name="store_checkout")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function checkout(Request $request)
    {
        $items = $this->get('cart')->getItems($this->getUser());
        $receiptItems = $items;
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $newCustomer = 'will become a new customer if need be.';
        $customerIsSet = false;


//        dump($items);
//        die;
//        checkout form was submitted
        if ($request->isMethod('POST')){

            $stripeToken = $request->get('stripeToken');

//            dump($stripeToken);
//            die;

            \Stripe\Stripe::setApiKey($this->getParameter('stripe_secret_key'));



//            Check if this customer has not bought from this store before
            if (!$user->getStripeId()){

                $customer = \Stripe\Customer::create(array(
                    "email" => $user->getEmail(),
                    "source" => $stripeToken, // obtained with Stripe.js
//                    "receipt_email" => $user->getEmail()
                ));


//                set customer up as a new customer or patron customer is using a new card
//                either way, set a new stripe id
                $user->setStripeId($customer->id);

//                save customer's new stripe id to the DB

                $em->persist($user);
                $em->flush();

            }else{

//                customer has already bought from this store before
                $customer = \Stripe\Customer::retrieve($user->getStripeId());
                $customer->source = $stripeToken;
                $customer->save();
            }


//            dump($this->get('cart')->getItems($this->getUser()));
//            die;

//            create an invoice for each item in the cart
            foreach ($this->get('cart')->getItems($this->getUser()) as $item){

                $cartItem = $em->getRepository('AppBundle:Item')
                    ->findItemById($item->getItem()->getId());

//                dump($cartItem);
//                die;

                \Stripe\InvoiceItem::create(array(
                    "amount" => ($cartItem->getPrice() * $item->getQuantity()) * 100,
                    "currency" => "usd",
                    "customer" => $user->getStripeId(), // obtained with Stripe.js
                    "description" => 'Item: '.$cartItem->getItemName().' ( Item ID: '.$cartItem->getId().' )'

                ));


//                create a new order for all the vendors this customer bought from
                $order = new Orders();

                $vendorIsNewCustomer = $em->getRepository('AppBundle:Customer')
                    ->findCustomerByEmail($user->getEmail()) == null;

//                this is for when the customer is an instance of a user (Vendor)

                if($vendorIsNewCustomer) {

                    if (!($user instanceof Customer)) {

                        if (!$customerIsSet) {
                            $newCustomer = new Customer();

                            $newCustomer->setStripeId($user->getStripeId());
                            $newCustomer->setEmail($user->getEmail());
                            $newCustomer->setFirstName($user->getFirstName());
                            $newCustomer->setLastName($user->getLastName());
                            $newCustomer->setAddressLine1($user->getAddressLine1());
                            $newCustomer->setAddressLine2($user->getAddressLine2());
                            $newCustomer->setCity($user->getCity());
                            $newCustomer->setState($user->getState());
                            $newCustomer->setZip($user->getZip());
                            $newCustomer->setPhone($user->getPhone());
                            $newCustomer->setCreatedAt($user->getCreatedAt());

                            $em->persist($newCustomer);
                            $em->flush();


                            $customerIsSet = true;
                        }

                        $order->setCustomer($newCustomer);
                        $order->setItem($cartItem);
                        $order->setQuantity($cartItem->getQuantity());
                        $order->setCreatedAt(new \DateTime());


                        $em->persist($order);
                        $em->flush();

                    }


                }else{

                    $customerForVendor = $em->getRepository('AppBundle:Customer')
                        ->findCustomerByEmail($this->getUser()->getEmail());

                    $order->setCustomer($customerForVendor);
                    $order->setItem($cartItem);
                    $order->setQuantity($item->getQuantity());

//                    dump($cartItem);
//                    die;
                    $order->setCreatedAt(new \DateTime());

                    $em->persist($order);
                    $em->flush();

                }
            }


            $invoice = \Stripe\Invoice::create([
                'customer' => $user->getStripeId(),

            ]);

            $invoiceId = json_decode($invoice->getLastResponse()->body)->id;

//            dump(json_decode($invoice->getLastResponse()->body)->id);
//            die;

            $invoice->pay();
//
//            dump($receiptItems);
//            die;

            $this->get('cart')->emptyCart($this->getUser());

            $this->addFlash('order_success', 'Your order is now being prepared to ship! Yay!!');

            $itemsBought = new Session();
//            $invoiceReceipt = new Session();

            $itemsBought->set('items', $receiptItems);


            return $this->redirectToRoute('receipt', [
                    'invoiceNo' => $invoiceId
                ]);
        }

//        dump($this->get('cart')->getItems($this->getUser()));
//        die;

        return $this->render('store/_checkout.html.twig',[
            'items' => $items,
            'cartCount' => $this->get('cart')->getItems($this->getUser()),
            'cart' => $this->get('cart'),
            'stripe_public_key' => $this->getParameter('stripe_public_key')
        ]);
    }


    /**
     * @Route("/checkout/receipt/{invoiceNo}", name="receipt")
     */
    public function receipt($invoiceNo)
    {

//        $this->get('session')->get('items');

        return $this->render('store/_printableReceipt.html.twig',[
                'cartCount' => $this->get('cart')->getItems($this->getUser()),
                'items' => $this->get('session')->get('items'),
                'invoiceId' => $invoiceNo,
            ]);
    }



//    only used to delete a single item while at checkout
    /**
     * @Route("/removeItemCheckout/{itemId}", name="remove_item_at_checkout")
     */
    public function removeItem($itemId)
    {
        $em = $this->getDoctrine()->getManager();

        $itemToDelete = $em->getRepository(Item::class)
            ->findItemById($itemId);

//        dump($itemToDelete); die;

        $this->get('cart')->removeItem($itemToDelete, $this->getUser());


        $items = $this->get('cart')->getItems($this->getUser());


        return $this->redirectToRoute('store_checkout',[
            'items' => $items,
            'cart' => $this->get('cart'),
            'cartCount' => $this->get('cart')->getItems($this->getUser()),
//            'stripe_public_key' => $this->getParameter('stripe_public_key')
        ]);





    }




}

