<?php
/**
 * Created by PhpStorm.
 * User: liumaumasi
 * Date: 9/1/16
 * Time: 11:15 AM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Orders;
use AppBundle\Entity\User;
use AppBundle\Form\NewUserForm;
use AppBundle\Repository\ItemeRepository;
use AppBundle\Repository\UserRepository;
use AppBundle\Repository\OrderRepository;
use AppBundle\Entity\Item;
use AppBundle\Service\Cart;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 *
 *
 */
class StoreController extends Controller
{

////      home page
//    /**
//     * @Route("/", name="home")
//     */
//    public function home()
//    {
//        $templating = $this->get('templating');
//        $html = $templating->render('store/home.html.twig');
//
//        return new Response($html);
//    }


//      item list page
    /**
     * @Route("/", name="store")
     */
    public function itemGallery(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $allItems = $em->getRepository('AppBundle:Item')
            ->findAll();

        $items = $em->getRepository('AppBundle:Item')
            ->itemsPerPage(0);

        $pages = count($allItems) / 16;

//        echo count($allItems);
//        dump($this->getUser()); die;

        return $this->render('store/gallery.html.twig',[
            'items' => $items,
            'cartCount' => $this->get('cart')->getItems($this->getUser()),
            'page' => $pages,
            'currentPage' => 1
        ]);
    }








//      item list page
    /**
     * @Route("/{pageNumber}", name="store_page")
     */
    public function padgination($pageNumber, Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $items = $em->getRepository('AppBundle:Item')
            ->itemsPerPage($pageNumber);

        $allItems = $em->getRepository('AppBundle:Item')
            ->findAll();

        $pages = count($allItems) / 16;

//        echo(count($allItems));

        return $this->render('store/gallery.html.twig',[
            'items' => $items,
            'cartCount' => $this->get('cart')->getItems($this->getUser()),
            'page' => $pages,
            'currentPage' => ($pageNumber / 16)
        ]);
    }


//      single item page
    /**
     * @Route("/store/{itemObj}", name="single_item")
     */
    public function showItem($itemObj)
    {

        $em = $this->getDoctrine()->getManager();

        $item = $em->getRepository('AppBundle:Item') // select an item  from item's table where id == id
        ->findOneBy(['id' => $itemObj]);

//        $vendor = $item->getUser();

//        dump($item->getUser()); die;

        $itemsByThisVendor = $em->getRepository('AppBundle:Item')
            ->findItemByVendorWithLimit($item->getUser(), 5);


//      404 page for this if statement / 500: item does not exists
        if(!$item){

            throw $this->createNotFoundException('Oops, This is not an item. This Obj is: NULL, Find this code in StoreController, line: 134');
        }

        return $this->render('store/item.html.twig',[
            'item' => $item,
            'itemsByThisVendor' => $itemsByThisVendor,
            'vendor' => $item->getUser(),
            'cartCount' => $this->get('cart')->getItems($this->getUser())
        ]);
    }





    /**
     * @Route("/store/item/{addItem}", name="add_to_cart")
     * @Method("POST")
     * @Security("is_granted('ROLE_CUSTOMER') || is_granted('ROLE_VENDOR')")
     */
    public function addItem(Item $addItem)
    {

        $em = $this->getDoctrine()->getManager();

        $item = $em->getRepository('AppBundle:Item') // select an item  from item's table where id == id
        ->findOneBy(['id' => $addItem]);

//        $cart = $this->get('cart')->addItem($addItem);

        $this->get('cart')->addItem($addItem, $this->getUser());

        $itemsByThisVendor = $em->getRepository('AppBundle:Item')->findItemByVendorWithLimit($item->getUser(), 5);


//        dump($this->get('cart')->getItems()); die;

        return $this->render('store/item.html.twig',[
            'item' => $item,
            'vendor' => $item->getUser(),
            'itemsByThisVendor' => $itemsByThisVendor,
            'cartCount' => $this->get('cart')->getItems($this->getUser())
        ]);
    }








}