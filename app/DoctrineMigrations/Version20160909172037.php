<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160909172037 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE customer ADD state VARCHAR(255) DEFAULT NULL, DROP state_id');
        $this->addSql('ALTER TABLE state CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE state state VARCHAR(255) NOT NULL, CHANGE state_name state_name VARCHAR(255) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE customer ADD state_id INT NOT NULL, DROP state');
        $this->addSql('ALTER TABLE state CHANGE id id INT UNSIGNED AUTO_INCREMENT NOT NULL, CHANGE state state CHAR(2) DEFAULT NULL COLLATE utf8_general_ci, CHANGE state_name state_name VARCHAR(50) DEFAULT NULL COLLATE utf8_general_ci');
    }
}
