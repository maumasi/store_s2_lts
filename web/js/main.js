(function($){


    var $cartIcon = $('li.customers-cart');
    var $cartList = $('div.cartList');
    var $closeCart = $('span.close-cart');

    var $page = $('li.page');






// =============================================    show / hide cart
    // show cart items
    $($cartIcon).on('mousemove', function () {

        $($cartList).slideDown();
    });

    // hide cart items
    $($closeCart).on('click', function () {

            $($cartList).slideUp();

    });



// =============================================    current page: add remove classes


    $($page).click(function () {


        $($page).removeClass('active');
        $(this).addClass('active');
    });



})(jQuery);