
requirejs.config({

    baseUrl: "js/dependencies",
    path: {

        jquery: [
            '//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min',
            'jquery-3.1.0.min'
        ],

        velocity: [
            '//cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min',
            'velocity'
        ]
    }
});