<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        if (0 === strpos($pathinfo, '/store/cart')) {
            if (0 === strpos($pathinfo, '/store/cart/checkout')) {
                // store_checkout
                if ($pathinfo === '/store/cart/checkout') {
                    $requiredSchemes = array (  'http' => 0,);
                    if (!isset($requiredSchemes[$this->context->getScheme()])) {
                        return $this->redirect($pathinfo, 'store_checkout', key($requiredSchemes));
                    }

                    return array (  '_controller' => 'AppBundle\\Controller\\CartController::checkout',  '_route' => 'store_checkout',);
                }

                // receipt
                if (0 === strpos($pathinfo, '/store/cart/checkout/receipt') && preg_match('#^/store/cart/checkout/receipt/(?P<invoiceNo>[^/]++)$#s', $pathinfo, $matches)) {
                    $requiredSchemes = array (  'http' => 0,);
                    if (!isset($requiredSchemes[$this->context->getScheme()])) {
                        return $this->redirect($pathinfo, 'receipt', key($requiredSchemes));
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'receipt')), array (  '_controller' => 'AppBundle\\Controller\\CartController::receipt',));
                }

            }

            // remove_item_at_checkout
            if (0 === strpos($pathinfo, '/store/cart/removeItemCheckout') && preg_match('#^/store/cart/removeItemCheckout/(?P<itemId>[^/]++)$#s', $pathinfo, $matches)) {
                $requiredSchemes = array (  'http' => 0,);
                if (!isset($requiredSchemes[$this->context->getScheme()])) {
                    return $this->redirect($pathinfo, 'remove_item_at_checkout', key($requiredSchemes));
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'remove_item_at_checkout')), array (  '_controller' => 'AppBundle\\Controller\\CartController::removeItem',));
            }

        }

        // vendor_register
        if ($pathinfo === '/vendor/register') {
            $requiredSchemes = array (  'http' => 0,);
            if (!isset($requiredSchemes[$this->context->getScheme()])) {
                return $this->redirect($pathinfo, 'vendor_register', key($requiredSchemes));
            }

            return array (  '_controller' => 'AppBundle\\Controller\\SecurityController::registerVendor',  '_route' => 'vendor_register',);
        }

        // secure_login
        if ($pathinfo === '/login') {
            $requiredSchemes = array (  'http' => 0,);
            if (!isset($requiredSchemes[$this->context->getScheme()])) {
                return $this->redirect($pathinfo, 'secure_login', key($requiredSchemes));
            }

            return array (  '_controller' => 'AppBundle\\Controller\\SecurityController::login',  '_route' => 'secure_login',);
        }

        // vendor_edit
        if ($pathinfo === '/vendor/edit') {
            $requiredSchemes = array (  'http' => 0,);
            if (!isset($requiredSchemes[$this->context->getScheme()])) {
                return $this->redirect($pathinfo, 'vendor_edit', key($requiredSchemes));
            }

            return array (  '_controller' => 'AppBundle\\Controller\\SecurityController::newVendor',  '_route' => 'vendor_edit',);
        }

        // secure_logout
        if ($pathinfo === '/logout') {
            return array (  '_controller' => 'AppBundle\\Controller\\SecurityController::logout',  '_route' => 'secure_logout',);
        }

        if (0 === strpos($pathinfo, '/store')) {
            // guest_register
            if ($pathinfo === '/store/register') {
                $requiredSchemes = array (  'http' => 0,);
                if (!isset($requiredSchemes[$this->context->getScheme()])) {
                    return $this->redirect($pathinfo, 'guest_register', key($requiredSchemes));
                }

                return array (  '_controller' => 'AppBundle\\Controller\\SecurityController::registerGuest',  '_route' => 'guest_register',);
            }

            // guest_edit
            if ($pathinfo === '/store/guest_edit_profile') {
                $requiredSchemes = array (  'http' => 0,);
                if (!isset($requiredSchemes[$this->context->getScheme()])) {
                    return $this->redirect($pathinfo, 'guest_edit', key($requiredSchemes));
                }

                return array (  '_controller' => 'AppBundle\\Controller\\SecurityController::editGuest',  '_route' => 'guest_edit',);
            }

        }

        // secure_guest_login
        if ($pathinfo === '/guestLogin') {
            $requiredSchemes = array (  'http' => 0,);
            if (!isset($requiredSchemes[$this->context->getScheme()])) {
                return $this->redirect($pathinfo, 'secure_guest_login', key($requiredSchemes));
            }

            return array (  '_controller' => 'AppBundle\\Controller\\SecurityController::guestLogin',  '_route' => 'secure_guest_login',);
        }

        // store
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'store');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\StoreController::itemGallery',  '_route' => 'store',);
        }

        // store_page
        if (preg_match('#^/(?P<pageNumber>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'store_page')), array (  '_controller' => 'AppBundle\\Controller\\StoreController::padgination',));
        }

        if (0 === strpos($pathinfo, '/store')) {
            // single_item
            if (preg_match('#^/store/(?P<itemObj>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'single_item')), array (  '_controller' => 'AppBundle\\Controller\\StoreController::showItem',));
            }

            // add_to_cart
            if (0 === strpos($pathinfo, '/store/item') && preg_match('#^/store/item/(?P<addItem>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_add_to_cart;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'add_to_cart')), array (  '_controller' => 'AppBundle\\Controller\\StoreController::addItem',));
            }
            not_add_to_cart:

        }

        // app_vendoraction_newvendorregister
        if ($pathinfo === '/register') {
            $requiredSchemes = array (  'http' => 0,);
            if (!isset($requiredSchemes[$this->context->getScheme()])) {
                return $this->redirect($pathinfo, 'app_vendoraction_newvendorregister', key($requiredSchemes));
            }

            return array (  '_controller' => 'AppBundle\\Controller\\VendorActionController::newVendorRegister',  '_route' => 'app_vendoraction_newvendorregister',);
        }

        if (0 === strpos($pathinfo, '/vendor')) {
            // vendor_dashboard
            if ($pathinfo === '/vendor/dashboard') {
                $requiredSchemes = array (  'http' => 0,);
                if (!isset($requiredSchemes[$this->context->getScheme()])) {
                    return $this->redirect($pathinfo, 'vendor_dashboard', key($requiredSchemes));
                }

                return array (  '_controller' => 'AppBundle\\Controller\\VendorController::vendorDashboard',  '_route' => 'vendor_dashboard',);
            }

            if (0 === strpos($pathinfo, '/vendor/que')) {
                // que_list
                if ($pathinfo === '/vendor/que') {
                    $requiredSchemes = array (  'http' => 0,);
                    if (!isset($requiredSchemes[$this->context->getScheme()])) {
                        return $this->redirect($pathinfo, 'que_list', key($requiredSchemes));
                    }

                    return array (  '_controller' => 'AppBundle\\Controller\\VendorController::queList',  '_route' => 'que_list',);
                }

                // que_order
                if (0 === strpos($pathinfo, '/vendor/que/order') && preg_match('#^/vendor/que/order/(?P<customerId>[^/]++)$#s', $pathinfo, $matches)) {
                    $requiredSchemes = array (  'http' => 0,);
                    if (!isset($requiredSchemes[$this->context->getScheme()])) {
                        return $this->redirect($pathinfo, 'que_order', key($requiredSchemes));
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'que_order')), array (  '_controller' => 'AppBundle\\Controller\\VendorController::queOrder',));
                }

            }

            // vendor_item_list
            if ($pathinfo === '/vendor/vendorQue') {
                $requiredSchemes = array (  'http' => 0,);
                if (!isset($requiredSchemes[$this->context->getScheme()])) {
                    return $this->redirect($pathinfo, 'vendor_item_list', key($requiredSchemes));
                }

                return array (  '_controller' => 'AppBundle\\Controller\\VendorController::queItemList',  '_route' => 'vendor_item_list',);
            }

            if (0 === strpos($pathinfo, '/vendor/itemC')) {
                // vendor_item_create
                if ($pathinfo === '/vendor/itemCreate') {
                    $requiredSchemes = array (  'http' => 0,);
                    if (!isset($requiredSchemes[$this->context->getScheme()])) {
                        return $this->redirect($pathinfo, 'vendor_item_create', key($requiredSchemes));
                    }

                    return array (  '_controller' => 'AppBundle\\Controller\\VendorItemController::itemCreate',  '_route' => 'vendor_item_create',);
                }

                // vendor_item_collection
                if ($pathinfo === '/vendor/itemCollection') {
                    $requiredSchemes = array (  'http' => 0,);
                    if (!isset($requiredSchemes[$this->context->getScheme()])) {
                        return $this->redirect($pathinfo, 'vendor_item_collection', key($requiredSchemes));
                    }

                    return array (  '_controller' => 'AppBundle\\Controller\\VendorItemController::itemCollection',  '_route' => 'vendor_item_collection',);
                }

            }

            // vendor_item_edit
            if (preg_match('#^/vendor/(?P<id>[^/]++)/itemEdit$#s', $pathinfo, $matches)) {
                $requiredSchemes = array (  'http' => 0,);
                if (!isset($requiredSchemes[$this->context->getScheme()])) {
                    return $this->redirect($pathinfo, 'vendor_item_edit', key($requiredSchemes));
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'vendor_item_edit')), array (  '_controller' => 'AppBundle\\Controller\\VendorItemController::itemEdite',));
            }

            // vendor_item_confirm_delete
            if (preg_match('#^/vendor/(?P<id>[^/]++)/itemDeleteConfirm$#s', $pathinfo, $matches)) {
                $requiredSchemes = array (  'http' => 0,);
                if (!isset($requiredSchemes[$this->context->getScheme()])) {
                    return $this->redirect($pathinfo, 'vendor_item_confirm_delete', key($requiredSchemes));
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'vendor_item_confirm_delete')), array (  '_controller' => 'AppBundle\\Controller\\VendorItemController::itemDeleteConfirm',));
            }

            // vendor_item_delete
            if (preg_match('#^/vendor/(?P<id>[^/]++)/itemDelete$#s', $pathinfo, $matches)) {
                $requiredSchemes = array (  'http' => 0,);
                if (!isset($requiredSchemes[$this->context->getScheme()])) {
                    return $this->redirect($pathinfo, 'vendor_item_delete', key($requiredSchemes));
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'vendor_item_delete')), array (  '_controller' => 'AppBundle\\Controller\\VendorItemController::itemDelete',));
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
