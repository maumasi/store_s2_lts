<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_59efe10f5985a897735c5dbacdbf933326680391b9cd56e359264e61af28a0ac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3deba985b9b3e2b8095217d95fd6b10b739634a81c6179b091fd1da3a4e4a8d7 = $this->env->getExtension("native_profiler");
        $__internal_3deba985b9b3e2b8095217d95fd6b10b739634a81c6179b091fd1da3a4e4a8d7->enter($__internal_3deba985b9b3e2b8095217d95fd6b10b739634a81c6179b091fd1da3a4e4a8d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_3deba985b9b3e2b8095217d95fd6b10b739634a81c6179b091fd1da3a4e4a8d7->leave($__internal_3deba985b9b3e2b8095217d95fd6b10b739634a81c6179b091fd1da3a4e4a8d7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'number')) ?>*/
/* */
