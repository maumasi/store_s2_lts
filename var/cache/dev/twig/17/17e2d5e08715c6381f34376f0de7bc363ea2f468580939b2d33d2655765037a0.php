<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_eef5a9e88749893aaba88d7167a12227655efb3ce3b4115458d3de4d1c8f84e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1c409cab6fd1fc4a1a88f3a2ffc5f2e54ad924d75c1a4be7dbb026446a19f632 = $this->env->getExtension("native_profiler");
        $__internal_1c409cab6fd1fc4a1a88f3a2ffc5f2e54ad924d75c1a4be7dbb026446a19f632->enter($__internal_1c409cab6fd1fc4a1a88f3a2ffc5f2e54ad924d75c1a4be7dbb026446a19f632_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_1c409cab6fd1fc4a1a88f3a2ffc5f2e54ad924d75c1a4be7dbb026446a19f632->leave($__internal_1c409cab6fd1fc4a1a88f3a2ffc5f2e54ad924d75c1a4be7dbb026446a19f632_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'email')) ?>*/
/* */
