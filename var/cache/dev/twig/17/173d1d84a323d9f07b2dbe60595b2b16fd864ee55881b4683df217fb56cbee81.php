<?php

/* vendor/vendorQue/queOrder.html.twig */
class __TwigTemplate_6f8321397da1fddf36b3070bd798583bce076bd465ced619082b036820e505d8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "vendor/vendorQue/queOrder.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascript' => array($this, 'block_javascript'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fe93c5c0e2dc13d9da984c9707609bb1566dc17edd2b3a678d3a639704d6cce9 = $this->env->getExtension("native_profiler");
        $__internal_fe93c5c0e2dc13d9da984c9707609bb1566dc17edd2b3a678d3a639704d6cce9->enter($__internal_fe93c5c0e2dc13d9da984c9707609bb1566dc17edd2b3a678d3a639704d6cce9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "vendor/vendorQue/queOrder.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fe93c5c0e2dc13d9da984c9707609bb1566dc17edd2b3a678d3a639704d6cce9->leave($__internal_fe93c5c0e2dc13d9da984c9707609bb1566dc17edd2b3a678d3a639704d6cce9_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_8e893b051ead5e80df8e532bc1e982ec1436074ab23015fd1771da4a9e7457bd = $this->env->getExtension("native_profiler");
        $__internal_8e893b051ead5e80df8e532bc1e982ec1436074ab23015fd1771da4a9e7457bd->enter($__internal_8e893b051ead5e80df8e532bc1e982ec1436074ab23015fd1771da4a9e7457bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo "
    - Que
";
        
        $__internal_8e893b051ead5e80df8e532bc1e982ec1436074ab23015fd1771da4a9e7457bd->leave($__internal_8e893b051ead5e80df8e532bc1e982ec1436074ab23015fd1771da4a9e7457bd_prof);

    }

    // line 8
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_82478f904212fa56efe5f7d045def138b3c7ad5c339c1a21dcd6e0de0a683759 = $this->env->getExtension("native_profiler");
        $__internal_82478f904212fa56efe5f7d045def138b3c7ad5c339c1a21dcd6e0de0a683759->enter($__internal_82478f904212fa56efe5f7d045def138b3c7ad5c339c1a21dcd6e0de0a683759_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 9
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
        
        $__internal_82478f904212fa56efe5f7d045def138b3c7ad5c339c1a21dcd6e0de0a683759->leave($__internal_82478f904212fa56efe5f7d045def138b3c7ad5c339c1a21dcd6e0de0a683759_prof);

    }

    // line 12
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_4e7b309c741095b91b5c3db8b0b4556bdb47568144162612d1fa6b47a9ec6052 = $this->env->getExtension("native_profiler");
        $__internal_4e7b309c741095b91b5c3db8b0b4556bdb47568144162612d1fa6b47a9ec6052->enter($__internal_4e7b309c741095b91b5c3db8b0b4556bdb47568144162612d1fa6b47a9ec6052_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        // line 13
        echo "    ";
        $this->displayParentBlock("javascript", $context, $blocks);
        echo "
";
        
        $__internal_4e7b309c741095b91b5c3db8b0b4556bdb47568144162612d1fa6b47a9ec6052->leave($__internal_4e7b309c741095b91b5c3db8b0b4556bdb47568144162612d1fa6b47a9ec6052_prof);

    }

    // line 21
    public function block_body($context, array $blocks = array())
    {
        $__internal_d508b0bdf1e6306db3befd8297a56e21f690a3f1d0836dbf063c7d6feb736b6d = $this->env->getExtension("native_profiler");
        $__internal_d508b0bdf1e6306db3befd8297a56e21f690a3f1d0836dbf063c7d6feb736b6d->enter($__internal_d508b0bdf1e6306db3befd8297a56e21f690a3f1d0836dbf063c7d6feb736b6d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 22
        echo "    ";
        $this->displayParentBlock("body", $context, $blocks);
        echo "


<main class=\"customer-order-que container\">


    <div class=\"row well well-sm\">


        ";
        // line 32
        echo "

        <div class=\"panel panel-default col-sm-4 col-xs-offset-1\">
            <div class=\"panel-heading\">
                <h3 class=\"panel-title\">Customer contact info</h3>
            </div>
            <div class=\"panel-body\">
                <p>";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : $this->getContext($context, "order")), "customer", array()), "firstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : $this->getContext($context, "order")), "customer", array()), "lastName", array()), "html", null, true);
        echo "</p>

                <p>Phone: ";
        // line 41
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : $this->getContext($context, "order")), "customer", array()), "phone", array()), "html", null, true);
        echo "</p>

                <p>Email: ";
        // line 43
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : $this->getContext($context, "order")), "customer", array()), "email", array()), "html", null, true);
        echo "</p>

            </div>
        </div>




        <div class=\"panel panel-default col-sm-4 col-xs-offset-2\">
            <div class=\"panel-heading\">
                <h3 class=\"panel-title\">Mailing address</h3>
            </div>
            <div class=\"panel-body\">
                <address class=\"customer-address\">
                    <p>";
        // line 57
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : $this->getContext($context, "order")), "customer", array()), "firstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : $this->getContext($context, "order")), "customer", array()), "lastName", array()), "html", null, true);
        echo "</p>

                    <p>";
        // line 59
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : $this->getContext($context, "order")), "customer", array()), "addressLine1", array()), "html", null, true);
        echo "</p>

                    <p>";
        // line 61
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : $this->getContext($context, "order")), "customer", array()), "addressLine2", array()), "html", null, true);
        echo "</p>

                    <p>";
        // line 63
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : $this->getContext($context, "order")), "customer", array()), "city", array()), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : $this->getContext($context, "order")), "customer", array()), "state", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : $this->getContext($context, "order")), "customer", array()), "zip", array()), "html", null, true);
        echo "</p>

                </address>
            </div>
        </div>




            ";
        // line 73
        echo "                ";
        // line 74
        echo "
                ";
        // line 76
        echo "
                ";
        // line 78
        echo "
            ";
        // line 80
        echo "



        ";
        // line 85
        echo "
    </div>



    <div class=\"row\">

        <table class=\"table\">
            <thead>
                <tr>
                    <th>Item</th>
                    <th>Qty.</th>
                    <th>Status</th>
                </tr>
            </thead>

        ";
        // line 102
        echo "            ";
        // line 103
        echo "




            <tbody>
            ";
        // line 109
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["order"]) ? $context["order"] : $this->getContext($context, "order")), "order", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["customerOrder"]) {
            // line 110
            echo "
                <tr>
                    <td>
                        <a href=\"";
            // line 113
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("single_item", array("itemObj" => $this->getAttribute($this->getAttribute($context["customerOrder"], "item", array()), "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["customerOrder"], "item", array()), "itemName", array()), "html", null, true);
            echo "</a>
                    </td>

                    <td>Count: ";
            // line 116
            echo twig_escape_filter($this->env, $this->getAttribute($context["customerOrder"], "quantity", array()), "html", null, true);
            echo "</td>
                    <td>
                        <input type=\"checkbox\" name=\"";
            // line 118
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["customerOrder"], "item", array()), "itemName", array()), "html", null, true);
            echo "\"/> Item is sent out
                    </td>
                </tr>

            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['customerOrder'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 123
        echo "            </tbody>
        </table>
    </div>

</main>
";
        
        $__internal_d508b0bdf1e6306db3befd8297a56e21f690a3f1d0836dbf063c7d6feb736b6d->leave($__internal_d508b0bdf1e6306db3befd8297a56e21f690a3f1d0836dbf063c7d6feb736b6d_prof);

    }

    public function getTemplateName()
    {
        return "vendor/vendorQue/queOrder.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  254 => 123,  243 => 118,  238 => 116,  230 => 113,  225 => 110,  221 => 109,  213 => 103,  211 => 102,  193 => 85,  187 => 80,  184 => 78,  181 => 76,  178 => 74,  176 => 73,  160 => 63,  155 => 61,  150 => 59,  143 => 57,  126 => 43,  121 => 41,  114 => 39,  105 => 32,  92 => 22,  86 => 21,  76 => 13,  70 => 12,  60 => 9,  54 => 8,  43 => 4,  37 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block title %}*/
/*     {{ parent() }}*/
/*     - Que*/
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/* {% endblock %}*/
/* */
/* {% block javascript %}*/
/*     {{ parent() }}*/
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* */
/* */
/* {% block body %}*/
/*     {{ parent() }}*/
/* */
/* */
/* <main class="customer-order-que container">*/
/* */
/* */
/*     <div class="row well well-sm">*/
/* */
/* */
/*         {#<div class="col-md- ">#}*/
/* */
/* */
/*         <div class="panel panel-default col-sm-4 col-xs-offset-1">*/
/*             <div class="panel-heading">*/
/*                 <h3 class="panel-title">Customer contact info</h3>*/
/*             </div>*/
/*             <div class="panel-body">*/
/*                 <p>{{ order.customer.firstName }} {{  order.customer.lastName }}</p>*/
/* */
/*                 <p>Phone: {{  order.customer.phone }}</p>*/
/* */
/*                 <p>Email: {{  order.customer.email }}</p>*/
/* */
/*             </div>*/
/*         </div>*/
/* */
/* */
/* */
/* */
/*         <div class="panel panel-default col-sm-4 col-xs-offset-2">*/
/*             <div class="panel-heading">*/
/*                 <h3 class="panel-title">Mailing address</h3>*/
/*             </div>*/
/*             <div class="panel-body">*/
/*                 <address class="customer-address">*/
/*                     <p>{{ order.customer.firstName }} {{  order.customer.lastName }}</p>*/
/* */
/*                     <p>{{  order.customer.addressLine1 }}</p>*/
/* */
/*                     <p>{{  order.customer.addressLine2 }}</p>*/
/* */
/*                     <p>{{  order.customer.city }}, {{  order.customer.state }} {{  order.customer.zip }}</p>*/
/* */
/*                 </address>*/
/*             </div>*/
/*         </div>*/
/* */
/* */
/* */
/* */
/*             {#<div class="col-sm-4">#}*/
/*                 {#<p>{{ order.customer.firstName }} {{  order.customer.lastName }}</p>#}*/
/* */
/*                 {#<p>Phone: {{  order.customer.phone }}</p>#}*/
/* */
/*                 {#<p>Email: {{  order.customer.email }}</p>#}*/
/* */
/*             {#</div>#}*/
/* */
/* */
/* */
/* */
/*         {#</div>#}*/
/* */
/*     </div>*/
/* */
/* */
/* */
/*     <div class="row">*/
/* */
/*         <table class="table">*/
/*             <thead>*/
/*                 <tr>*/
/*                     <th>Item</th>*/
/*                     <th>Qty.</th>*/
/*                     <th>Status</th>*/
/*                 </tr>*/
/*             </thead>*/
/* */
/*         {#<ul>#}*/
/*             {#{% for item in order.order %}#}*/
/* */
/* */
/* */
/* */
/* */
/*             <tbody>*/
/*             {% for customerOrder in order.order %}*/
/* */
/*                 <tr>*/
/*                     <td>*/
/*                         <a href="{{ path('single_item',{'itemObj' : customerOrder.item.id}) }}">{{ customerOrder.item.itemName }}</a>*/
/*                     </td>*/
/* */
/*                     <td>Count: {{ customerOrder.quantity }}</td>*/
/*                     <td>*/
/*                         <input type="checkbox" name="{{ customerOrder.item.itemName }}"/> Item is sent out*/
/*                     </td>*/
/*                 </tr>*/
/* */
/*             {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/*     </div>*/
/* */
/* </main>*/
/* {% endblock %}*/
/* */
/* */
/* */
