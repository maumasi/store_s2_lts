<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_8edc65598d7152dd976d78c9927c3fb991804a98e362b4b5fcfc2d706849870a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fd51b1eb1b35459f3d21562509993153a1eef7524006676cb789e0dfd1005dd3 = $this->env->getExtension("native_profiler");
        $__internal_fd51b1eb1b35459f3d21562509993153a1eef7524006676cb789e0dfd1005dd3->enter($__internal_fd51b1eb1b35459f3d21562509993153a1eef7524006676cb789e0dfd1005dd3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_fd51b1eb1b35459f3d21562509993153a1eef7524006676cb789e0dfd1005dd3->leave($__internal_fd51b1eb1b35459f3d21562509993153a1eef7524006676cb789e0dfd1005dd3_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}*/
/* */
