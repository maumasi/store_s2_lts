<?php

/* store/_guestRegistrationForm.html.twig */
class __TwigTemplate_46b70dc3ea82791f9f9fe5c761cf2ea280004fd6f33722f1b377e04b790ba471 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_789fe2595aff3877ecfbd1ff7c4236e5b185bd150fdd735cb0780a2f77d95b1f = $this->env->getExtension("native_profiler");
        $__internal_789fe2595aff3877ecfbd1ff7c4236e5b185bd150fdd735cb0780a2f77d95b1f->enter($__internal_789fe2595aff3877ecfbd1ff7c4236e5b185bd150fdd735cb0780a2f77d95b1f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "store/_guestRegistrationForm.html.twig"));

        // line 1
        echo "
";
        // line 3
        echo "

";
        // line 5
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), 'form_start', array("attr" => array("class" => "col-lg-offset-4 col-md-offset-3 col-sm-offset-3 col-lg-4 col-md-6 col-sm-6")));
        // line 9
        echo "

    ";
        // line 12
        echo "
<div class=\"col-md-6 col-sm-6\">
    ";
        // line 14
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "firstName", array()), 'row', array("attr" => array("placeholder" => "First Name"), "label_attr" => array("class" => "sr-only")));
        // line 21
        echo "
</div>



<div class=\"col-md-6 col-sm-6\">
    ";
        // line 27
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "lastName", array()), 'row', array("attr" => array("placeholder" => "Last Name"), "label_attr" => array("class" => "sr-only")));
        // line 34
        echo "
</div>



<div class=\"col-md-6 col-sm-6\">
    ";
        // line 40
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "phone", array()), 'row', array("attr" => array("placeholder" => "Phone No."), "label_attr" => array("class" => "sr-only")));
        // line 47
        echo "
</div>



<div class=\"col-sm-6 \">
    ";
        // line 53
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "email", array()), 'row', array("attr" => array("placeholder" => "Email"), "label_attr" => array("class" => "sr-only")));
        // line 60
        echo "
</div>




<div class=\"col-sm-6 \">
    ";
        // line 67
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "addressLine1", array()), 'row', array("attr" => array("placeholder" => "Street Address"), "label_attr" => array("class" => "sr-only")));
        // line 74
        echo "
</div>



<div class=\"col-sm-6 \">
    ";
        // line 80
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "addressLine2", array()), 'row', array("attr" => array("placeholder" => "Suite / Apt"), "label_attr" => array("class" => "sr-only")));
        // line 87
        echo "
</div>




<div class=\"col-sm-6 \">
    ";
        // line 94
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "city", array()), 'row', array("attr" => array("placeholder" => "City"), "label_attr" => array("class" => "sr-only")));
        // line 101
        echo "
</div>




<div class=\"col-sm-3 \">
    ";
        // line 108
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "state", array()), 'row', array("attr" => array("placeholder" => "State"), "label_attr" => array("class" => "sr-only")));
        // line 115
        echo "
</div>



<div class=\"col-sm-3 \">
    ";
        // line 121
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "zip", array()), 'row', array("attr" => array("placeholder" => "Zip"), "label_attr" => array("class" => "sr-only")));
        // line 128
        echo "
</div>



<div class=\"col-sm-6 \">
    ";
        // line 134
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "nakedPassword", array()), "first", array()), 'row', array("attr" => array("placeholder" => "New Password"), "label_attr" => array("class" => "sr-only")));
        // line 141
        echo "
</div>



<div class=\"col-sm-6 \">
    ";
        // line 147
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "nakedPassword", array()), "second", array()), 'row', array("attr" => array("placeholder" => "Confirm Password"), "label_attr" => array("class" => "sr-only")));
        // line 154
        echo "
</div>

<div class=\"col-sm-6 \">

</div>


<div class=\"col-sm-3 col-xs-3 \">
    <button type=\"submit\" class=\"btn btn-primary\" formnovalidate >";
        // line 163
        echo twig_escape_filter($this->env, (isset($context["submitBtn"]) ? $context["submitBtn"] : $this->getContext($context, "submitBtn")), "html", null, true);
        echo "</button>

</div>


    ";
        // line 169
        echo "    <a class=\"btn btn-cancel\" href=\"";
        echo twig_escape_filter($this->env, (isset($context["cancelPath"]) ? $context["cancelPath"] : $this->getContext($context, "cancelPath")), "html", null, true);
        echo "\">Cancel</a>


";
        // line 172
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), 'form_end');
        echo "








";
        // line 185
        echo "
";
        // line 192
        echo "
";
        
        $__internal_789fe2595aff3877ecfbd1ff7c4236e5b185bd150fdd735cb0780a2f77d95b1f->leave($__internal_789fe2595aff3877ecfbd1ff7c4236e5b185bd150fdd735cb0780a2f77d95b1f_prof);

    }

    public function getTemplateName()
    {
        return "store/_guestRegistrationForm.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  185 => 192,  182 => 185,  170 => 172,  163 => 169,  155 => 163,  144 => 154,  142 => 147,  134 => 141,  132 => 134,  124 => 128,  122 => 121,  114 => 115,  112 => 108,  103 => 101,  101 => 94,  92 => 87,  90 => 80,  82 => 74,  80 => 67,  71 => 60,  69 => 53,  61 => 47,  59 => 40,  51 => 34,  49 => 27,  41 => 21,  39 => 14,  35 => 12,  31 => 9,  29 => 5,  25 => 3,  22 => 1,);
    }
}
/* */
/* {#<div class="col-xs-12">#}*/
/* */
/* */
/* {{ form_start(registrationForm,{*/
/*     'attr': {*/
/*         'class': 'col-lg-offset-4 col-md-offset-3 col-sm-offset-3 col-lg-4 col-md-6 col-sm-6'*/
/*     }*/
/* }) }}*/
/* */
/*     {#{{ form_widget(registrationForm) }}#}*/
/* */
/* <div class="col-md-6 col-sm-6">*/
/*     {{ form_row(registrationForm.firstName,{*/
/*         'attr': {*/
/*             'placeholder': 'First Name',*/
/*         },*/
/*         'label_attr': {*/
/*             'class': 'sr-only'*/
/*         }*/
/*     }) }}*/
/* </div>*/
/* */
/* */
/* */
/* <div class="col-md-6 col-sm-6">*/
/*     {{ form_row(registrationForm.lastName,{*/
/*         'attr': {*/
/*             'placeholder': 'Last Name',*/
/*         },*/
/*         'label_attr': {*/
/*             'class': 'sr-only'*/
/*         }*/
/*     }) }}*/
/* </div>*/
/* */
/* */
/* */
/* <div class="col-md-6 col-sm-6">*/
/*     {{ form_row(registrationForm.phone,{*/
/*         'attr': {*/
/*             'placeholder': 'Phone No.',*/
/*         },*/
/*         'label_attr': {*/
/*             'class': 'sr-only'*/
/*         }*/
/*     }) }}*/
/* </div>*/
/* */
/* */
/* */
/* <div class="col-sm-6 ">*/
/*     {{ form_row(registrationForm.email,{*/
/*         'attr': {*/
/*             'placeholder': 'Email',*/
/*         },*/
/*         'label_attr': {*/
/*             'class': 'sr-only'*/
/*         }*/
/*     }) }}*/
/* </div>*/
/* */
/* */
/* */
/* */
/* <div class="col-sm-6 ">*/
/*     {{ form_row(registrationForm.addressLine1,{*/
/*         'attr': {*/
/*             'placeholder': 'Street Address',*/
/*         },*/
/*         'label_attr': {*/
/*             'class': 'sr-only'*/
/*         }*/
/*     }) }}*/
/* </div>*/
/* */
/* */
/* */
/* <div class="col-sm-6 ">*/
/*     {{ form_row(registrationForm.addressLine2,{*/
/*         'attr': {*/
/*             'placeholder': 'Suite / Apt',*/
/*         },*/
/*         'label_attr': {*/
/*             'class': 'sr-only'*/
/*         }*/
/*     }) }}*/
/* </div>*/
/* */
/* */
/* */
/* */
/* <div class="col-sm-6 ">*/
/*     {{ form_row(registrationForm.city,{*/
/*         'attr': {*/
/*             'placeholder': 'City',*/
/*         },*/
/*         'label_attr': {*/
/*             'class': 'sr-only'*/
/*         }*/
/*     }) }}*/
/* </div>*/
/* */
/* */
/* */
/* */
/* <div class="col-sm-3 ">*/
/*     {{ form_row(registrationForm.state,{*/
/*         'attr': {*/
/*             'placeholder': 'State',*/
/*         },*/
/*         'label_attr': {*/
/*             'class': 'sr-only'*/
/*         }*/
/*     }) }}*/
/* </div>*/
/* */
/* */
/* */
/* <div class="col-sm-3 ">*/
/*     {{ form_row(registrationForm.zip,{*/
/*         'attr': {*/
/*             'placeholder': 'Zip',*/
/*         },*/
/*         'label_attr': {*/
/*             'class': 'sr-only'*/
/*         }*/
/*     }) }}*/
/* </div>*/
/* */
/* */
/* */
/* <div class="col-sm-6 ">*/
/*     {{ form_row(registrationForm.nakedPassword.first,{*/
/*         'attr': {*/
/*             'placeholder': 'New Password',*/
/*         },*/
/*         'label_attr': {*/
/*             'class': 'sr-only'*/
/*         }*/
/*     }) }}*/
/* </div>*/
/* */
/* */
/* */
/* <div class="col-sm-6 ">*/
/*     {{ form_row(registrationForm.nakedPassword.second,{*/
/*         'attr': {*/
/*             'placeholder': 'Confirm Password',*/
/*         },*/
/*         'label_attr': {*/
/*             'class': 'sr-only'*/
/*         }*/
/*     }) }}*/
/* </div>*/
/* */
/* <div class="col-sm-6 ">*/
/* */
/* </div>*/
/* */
/* */
/* <div class="col-sm-3 col-xs-3 ">*/
/*     <button type="submit" class="btn btn-primary" formnovalidate >{{ submitBtn }}</button>*/
/* */
/* </div>*/
/* */
/* */
/*     {#<button type="submit" class="btn btn-primary" formnovalidate >{{ submitBtn }}</button>#}*/
/*     <a class="btn btn-cancel" href="{{ cancelPath }}">Cancel</a>*/
/* */
/* */
/* {{ form_end(registrationForm) }}*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* {#{{ form_row(registrationForm.firstName) }}#}*/
/* {#{{ form_row(registrationForm.lastName) }}#}*/
/* {#{{ form_row(registrationForm.phone) }}#}*/
/* {#{{ form_row(registrationForm.email) }}#}*/
/* */
/* {#{{ form_row(registrationForm.city) }}#}*/
/* {#{{ form_row(registrationForm.country) }}#}*/
/* {#{{ form_row(registrationForm.addressLine1) }}#}*/
/* {#{{ form_row(registrationForm.addressLine2) }}#}*/
/* {#{{ form_row(registrationForm.state) }}#}*/
/* {#{{ form_row(registrationForm.zip) }}#}*/
/* */
/* {#{{ form_row(registrationForm.nakedPassword.first) }}#}*/
/* {#{{ form_row(registrationForm.nakedPassword.second) }}#}*/
/* */
