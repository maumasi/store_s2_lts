<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_62d4e38d3baeaf10c6d4d76a7941a25b1207241156260375a1d7c0360d32943c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_76cab5e189c85095a12e3158b4b4435be230ab8257ff5742970dd014f3aefa1b = $this->env->getExtension("native_profiler");
        $__internal_76cab5e189c85095a12e3158b4b4435be230ab8257ff5742970dd014f3aefa1b->enter($__internal_76cab5e189c85095a12e3158b4b4435be230ab8257ff5742970dd014f3aefa1b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_76cab5e189c85095a12e3158b4b4435be230ab8257ff5742970dd014f3aefa1b->leave($__internal_76cab5e189c85095a12e3158b4b4435be230ab8257ff5742970dd014f3aefa1b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr>*/
/*     <td></td>*/
/*     <td>*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
