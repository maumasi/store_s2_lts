<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_7d21134b563adf12c0dd16b4963d0b9dc53f36303fb2307c77dafc2b98857905 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f0b0b46a85db891df1ce458c204ea978147a9ebb9862b442bd14ed28e0b04c38 = $this->env->getExtension("native_profiler");
        $__internal_f0b0b46a85db891df1ce458c204ea978147a9ebb9862b442bd14ed28e0b04c38->enter($__internal_f0b0b46a85db891df1ce458c204ea978147a9ebb9862b442bd14ed28e0b04c38_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_f0b0b46a85db891df1ce458c204ea978147a9ebb9862b442bd14ed28e0b04c38->leave($__internal_f0b0b46a85db891df1ce458c204ea978147a9ebb9862b442bd14ed28e0b04c38_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->widget($form) ?>*/
/* */
