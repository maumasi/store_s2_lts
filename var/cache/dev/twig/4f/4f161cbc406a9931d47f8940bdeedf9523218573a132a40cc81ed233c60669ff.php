<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_1577e8e93fafe4f80d6a6ebba3175e2e1597238fda4aad70bd04080f01e99a65 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7a914331a38906bf2f676b37631b33a7ae002c2761c512404a8df7a800d35d2b = $this->env->getExtension("native_profiler");
        $__internal_7a914331a38906bf2f676b37631b33a7ae002c2761c512404a8df7a800d35d2b->enter($__internal_7a914331a38906bf2f676b37631b33a7ae002c2761c512404a8df7a800d35d2b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_7a914331a38906bf2f676b37631b33a7ae002c2761c512404a8df7a800d35d2b->leave($__internal_7a914331a38906bf2f676b37631b33a7ae002c2761c512404a8df7a800d35d2b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (!isset($render_rest) || $render_rest): ?>*/
/* <?php echo $view['form']->rest($form) ?>*/
/* <?php endif ?>*/
/* </form>*/
/* */
