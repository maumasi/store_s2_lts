<?php

/* base.html.twig */
class __TwigTemplate_963171336005a5fb5979eb1df2f67d799235f003776762aca0182e5cf03fe200 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'footer' => array($this, 'block_footer'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fe48c7c63bc3f72d4d7cd0965c75617db1016a4cbbde4592bf6db9c86fdcdd68 = $this->env->getExtension("native_profiler");
        $__internal_fe48c7c63bc3f72d4d7cd0965c75617db1016a4cbbde4592bf6db9c86fdcdd68->enter($__internal_fe48c7c63bc3f72d4d7cd0965c75617db1016a4cbbde4592bf6db9c86fdcdd68_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

        ";
        // line 8
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 17
        echo "
        <!-- <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" /> -->
    </head>
    <body>
        ";
        // line 21
        $this->displayBlock('body', $context, $blocks);
        // line 25
        echo "











        ";
        // line 37
        $this->displayBlock('footer', $context, $blocks);
        // line 41
        echo "
        ";
        // line 42
        $this->displayBlock('javascripts', $context, $blocks);
        // line 54
        echo "    </body>
</html>
";
        
        $__internal_fe48c7c63bc3f72d4d7cd0965c75617db1016a4cbbde4592bf6db9c86fdcdd68->leave($__internal_fe48c7c63bc3f72d4d7cd0965c75617db1016a4cbbde4592bf6db9c86fdcdd68_prof);

    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        $__internal_a98b6172dd24c506b5e2b5361d53dd86fa164238cb896f8117563d27be7170f6 = $this->env->getExtension("native_profiler");
        $__internal_a98b6172dd24c506b5e2b5361d53dd86fa164238cb896f8117563d27be7170f6->enter($__internal_a98b6172dd24c506b5e2b5361d53dd86fa164238cb896f8117563d27be7170f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "  The Bladesmith's Workbench  ";
        
        $__internal_a98b6172dd24c506b5e2b5361d53dd86fa164238cb896f8117563d27be7170f6->leave($__internal_a98b6172dd24c506b5e2b5361d53dd86fa164238cb896f8117563d27be7170f6_prof);

    }

    // line 8
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_4f64c5b3d49ce5dd52daa3a886fde1ed184e8b680fbbb2b091d45fa92d544538 = $this->env->getExtension("native_profiler");
        $__internal_4f64c5b3d49ce5dd52daa3a886fde1ed184e8b680fbbb2b091d45fa92d544538->enter($__internal_4f64c5b3d49ce5dd52daa3a886fde1ed184e8b680fbbb2b091d45fa92d544538_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 9
        echo "        <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/reset.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("vendor/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("vendor/bootstrap/css/bootstrap-theme.min.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("vendor/fontawesome/css/font-awesome.min.css"), "html", null, true);
        echo "\" />
        <!-- <link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/styles.css"), "html", null, true);
        echo "\" /> -->
        <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/main.css"), "html", null, true);
        echo "\" />
        
        ";
        
        $__internal_4f64c5b3d49ce5dd52daa3a886fde1ed184e8b680fbbb2b091d45fa92d544538->leave($__internal_4f64c5b3d49ce5dd52daa3a886fde1ed184e8b680fbbb2b091d45fa92d544538_prof);

    }

    // line 21
    public function block_body($context, array $blocks = array())
    {
        $__internal_3f6b8ea311e64569f7f569f551b1065382c10fcfeefd62e8c6490e0e5608da1e = $this->env->getExtension("native_profiler");
        $__internal_3f6b8ea311e64569f7f569f551b1065382c10fcfeefd62e8c6490e0e5608da1e->enter($__internal_3f6b8ea311e64569f7f569f551b1065382c10fcfeefd62e8c6490e0e5608da1e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 22
        echo "            ";
        echo twig_include($this->env, $context, "store/_baseNav.html.twig");
        echo "

        ";
        
        $__internal_3f6b8ea311e64569f7f569f551b1065382c10fcfeefd62e8c6490e0e5608da1e->leave($__internal_3f6b8ea311e64569f7f569f551b1065382c10fcfeefd62e8c6490e0e5608da1e_prof);

    }

    // line 37
    public function block_footer($context, array $blocks = array())
    {
        $__internal_098b9dd7c367d7025bc8a28b9072f20c6e2d079285b9b156d15cc5b1a08a5149 = $this->env->getExtension("native_profiler");
        $__internal_098b9dd7c367d7025bc8a28b9072f20c6e2d079285b9b156d15cc5b1a08a5149->enter($__internal_098b9dd7c367d7025bc8a28b9072f20c6e2d079285b9b156d15cc5b1a08a5149_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 38
        echo "
            ";
        // line 40
        echo "        ";
        
        $__internal_098b9dd7c367d7025bc8a28b9072f20c6e2d079285b9b156d15cc5b1a08a5149->leave($__internal_098b9dd7c367d7025bc8a28b9072f20c6e2d079285b9b156d15cc5b1a08a5149_prof);

    }

    // line 42
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_f70f48394c7cf291acdd150738c9b830f7c2550a5f70d4fbc063842323e2370e = $this->env->getExtension("native_profiler");
        $__internal_f70f48394c7cf291acdd150738c9b830f7c2550a5f70d4fbc063842323e2370e->enter($__internal_f70f48394c7cf291acdd150738c9b830f7c2550a5f70d4fbc063842323e2370e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 43
        echo "       
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js\"></script>
        <script src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/dependencies/velocity.js"), "html", null, true);
        echo "\"></script>

        <script src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("vendor/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script>


        <script src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/main.js"), "html", null, true);
        echo "\"></script>

        ";
        
        $__internal_f70f48394c7cf291acdd150738c9b830f7c2550a5f70d4fbc063842323e2370e->leave($__internal_f70f48394c7cf291acdd150738c9b830f7c2550a5f70d4fbc063842323e2370e_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  186 => 51,  179 => 47,  174 => 45,  170 => 43,  164 => 42,  157 => 40,  154 => 38,  148 => 37,  137 => 22,  131 => 21,  121 => 14,  117 => 13,  113 => 12,  109 => 11,  105 => 10,  100 => 9,  94 => 8,  82 => 6,  73 => 54,  71 => 42,  68 => 41,  66 => 37,  52 => 25,  50 => 21,  44 => 18,  41 => 17,  39 => 8,  34 => 6,  27 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">*/
/*         <title>{% block title %}  The Bladesmith's Workbench  {% endblock %}</title>*/
/* */
/*         {% block stylesheets %}*/
/*         <link rel="stylesheet" href="{{ asset('css/reset.css') }}" />*/
/*         <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" />*/
/*         <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap-theme.min.css') }}" />*/
/*         <link rel="stylesheet" href="{{ asset('vendor/fontawesome/css/font-awesome.min.css') }}" />*/
/*         <!-- <link rel="stylesheet" href="{{ asset('css/styles.css') }}" /> -->*/
/*         <link rel="stylesheet" href="{{ asset('css/main.css') }}" />*/
/*         */
/*         {% endblock %}*/
/* */
/*         <!-- <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" /> -->*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}*/
/*             {{ include('store/_baseNav.html.twig') }}*/
/* */
/*         {% endblock %}*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/*         {% block footer %}*/
/* */
/*             {#{{ include('store/_footerNav.html.twig') }}#}*/
/*         {% endblock %}*/
/* */
/*         {% block javascripts %}*/
/*        */
/*         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>*/
/*         <script src="{{ asset('js/dependencies/velocity.js') }}"></script>*/
/* */
/*         <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>*/
/*             <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>*/
/* */
/* */
/*         <script src="{{ asset('js/main.js') }}"></script>*/
/* */
/*         {% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
