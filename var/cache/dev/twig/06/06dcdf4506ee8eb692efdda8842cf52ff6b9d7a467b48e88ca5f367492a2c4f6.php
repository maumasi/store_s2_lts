<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_7bd1e4b5142ab5d6a99df9e4cb2bef72f5c5865a6628e63f42d4c92a4e7e1da2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8265cdd7f83d4b30239fbf81f9c544736c9571ed0d101246af26ea2a83619032 = $this->env->getExtension("native_profiler");
        $__internal_8265cdd7f83d4b30239fbf81f9c544736c9571ed0d101246af26ea2a83619032->enter($__internal_8265cdd7f83d4b30239fbf81f9c544736c9571ed0d101246af26ea2a83619032_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_8265cdd7f83d4b30239fbf81f9c544736c9571ed0d101246af26ea2a83619032->leave($__internal_8265cdd7f83d4b30239fbf81f9c544736c9571ed0d101246af26ea2a83619032_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($expanded): ?>*/
/* <?php echo $view['form']->block($form, 'choice_widget_expanded') ?>*/
/* <?php else: ?>*/
/* <?php echo $view['form']->block($form, 'choice_widget_collapsed') ?>*/
/* <?php endif ?>*/
/* */
