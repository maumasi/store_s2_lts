<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_d062a7e43263ff5dde695bffb82bd5291f4570a502b7d4db53f70a6a4ddce98b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dbfad928f1a23b13e751959fe2dddb68141555f2ee41acf6d70fb6d16f9cd9e3 = $this->env->getExtension("native_profiler");
        $__internal_dbfad928f1a23b13e751959fe2dddb68141555f2ee41acf6d70fb6d16f9cd9e3->enter($__internal_dbfad928f1a23b13e751959fe2dddb68141555f2ee41acf6d70fb6d16f9cd9e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_dbfad928f1a23b13e751959fe2dddb68141555f2ee41acf6d70fb6d16f9cd9e3->leave($__internal_dbfad928f1a23b13e751959fe2dddb68141555f2ee41acf6d70fb6d16f9cd9e3_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_a88b0002e50fe0a6bc488c8e21f064960f1df67472621428f874f36a7012df89 = $this->env->getExtension("native_profiler");
        $__internal_a88b0002e50fe0a6bc488c8e21f064960f1df67472621428f874f36a7012df89->enter($__internal_a88b0002e50fe0a6bc488c8e21f064960f1df67472621428f874f36a7012df89_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_a88b0002e50fe0a6bc488c8e21f064960f1df67472621428f874f36a7012df89->leave($__internal_a88b0002e50fe0a6bc488c8e21f064960f1df67472621428f874f36a7012df89_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_5e029c2056fa40b2d1389e5a1fe13ac3ad926e90b920888b8133979a21612c5e = $this->env->getExtension("native_profiler");
        $__internal_5e029c2056fa40b2d1389e5a1fe13ac3ad926e90b920888b8133979a21612c5e->enter($__internal_5e029c2056fa40b2d1389e5a1fe13ac3ad926e90b920888b8133979a21612c5e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_5e029c2056fa40b2d1389e5a1fe13ac3ad926e90b920888b8133979a21612c5e->leave($__internal_5e029c2056fa40b2d1389e5a1fe13ac3ad926e90b920888b8133979a21612c5e_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_474063dbd8afde8efed2ea8fcf217084617c2c07af24db590483dfdde272f18c = $this->env->getExtension("native_profiler");
        $__internal_474063dbd8afde8efed2ea8fcf217084617c2c07af24db590483dfdde272f18c->enter($__internal_474063dbd8afde8efed2ea8fcf217084617c2c07af24db590483dfdde272f18c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_474063dbd8afde8efed2ea8fcf217084617c2c07af24db590483dfdde272f18c->leave($__internal_474063dbd8afde8efed2ea8fcf217084617c2c07af24db590483dfdde272f18c_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
