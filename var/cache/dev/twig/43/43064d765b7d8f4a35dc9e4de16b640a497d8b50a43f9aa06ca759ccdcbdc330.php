<?php

/* store/_checkout.html.twig */
class __TwigTemplate_dbfbb0b4e921aa93fc55eb8e1f6a1949e88849076316c0fcfb3ac03066c54baa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "store/_checkout.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3b9b9434a9fdb32d69dc7de9de94f652c68fe55996ab4a03cd173334799907ff = $this->env->getExtension("native_profiler");
        $__internal_3b9b9434a9fdb32d69dc7de9de94f652c68fe55996ab4a03cd173334799907ff->enter($__internal_3b9b9434a9fdb32d69dc7de9de94f652c68fe55996ab4a03cd173334799907ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "store/_checkout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3b9b9434a9fdb32d69dc7de9de94f652c68fe55996ab4a03cd173334799907ff->leave($__internal_3b9b9434a9fdb32d69dc7de9de94f652c68fe55996ab4a03cd173334799907ff_prof);

    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        $__internal_027f9274c99e6744c235d19f4d65cb648bc431214881efcd57b8531232f64c8d = $this->env->getExtension("native_profiler");
        $__internal_027f9274c99e6744c235d19f4d65cb648bc431214881efcd57b8531232f64c8d->enter($__internal_027f9274c99e6744c235d19f4d65cb648bc431214881efcd57b8531232f64c8d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 3
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo "
    - Checkout
";
        
        $__internal_027f9274c99e6744c235d19f4d65cb648bc431214881efcd57b8531232f64c8d->leave($__internal_027f9274c99e6744c235d19f4d65cb648bc431214881efcd57b8531232f64c8d_prof);

    }

    // line 7
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_d192b10ee92766d3dec91bcb06549bcb92c28479cbd01a848f0dda8630924224 = $this->env->getExtension("native_profiler");
        $__internal_d192b10ee92766d3dec91bcb06549bcb92c28479cbd01a848f0dda8630924224->enter($__internal_d192b10ee92766d3dec91bcb06549bcb92c28479cbd01a848f0dda8630924224_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 8
        echo "
    ";
        // line 9
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script type=\"text/javascript\" src=\"https://js.stripe.com/v2/\"></script>

    <script type=\"text/javascript\">
        Stripe.setPublishableKey('";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["stripe_public_key"]) ? $context["stripe_public_key"] : $this->getContext($context, "stripe_public_key")), "html", null, true);
        echo "');


        \$(function() {
            var \$form = \$('#payment-form');
            \$form.submit(function(event) {

                event.preventDefault();
                // Disable the submit button to prevent repeated clicks:
                \$form.find('.submit').prop('disabled', true);

                // Request a token from Stripe:
                Stripe.card.createToken(\$form, stripeResponseHandler);

                // Prevent the form from being submitted:
//                return false;
            });
        });


        function stripeResponseHandler(status, response) {
            // Grab the form:
            var \$form = \$('#payment-form');

            if (response.error) { // Problem!

                // Show the errors on the form:
                \$form.find('.payment-errors').text(response.error.message);
                \$form.find('.payment-errors-wrapper').removeClass('hidden');
                \$form.find('.submit').prop('disabled', false); // Re-enable submission

            } else { // Token was created!

                \$form.find('.payment-errors');
                \$form.find('.payment-errors-wrapper').addClass('hidden');

                // Get the token ID:
                var token = response.id;

                // Insert the token ID into the form so it gets submitted to the server:
                \$form.append(\$('<input type=\"hidden\" name=\"stripeToken\">').val(token));

                // Submit the form:
                \$form.get(0).submit();
            }
        };
    </script>






";
        
        $__internal_d192b10ee92766d3dec91bcb06549bcb92c28479cbd01a848f0dda8630924224->leave($__internal_d192b10ee92766d3dec91bcb06549bcb92c28479cbd01a848f0dda8630924224_prof);

    }

    // line 68
    public function block_body($context, array $blocks = array())
    {
        $__internal_1e0e56b6febf8f85375f988860de102a3226c1a43bad87eae57969b6848e3eda = $this->env->getExtension("native_profiler");
        $__internal_1e0e56b6febf8f85375f988860de102a3226c1a43bad87eae57969b6848e3eda->enter($__internal_1e0e56b6febf8f85375f988860de102a3226c1a43bad87eae57969b6848e3eda_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 69
        echo "    ";
        $this->displayParentBlock("body", $context, $blocks);
        echo "

<ol class=\"breadcrumb\">
    <li><a href=\"";
        // line 72
        echo $this->env->getExtension('routing')->getPath("store");
        echo "\">Home</a></li>
    <li class=\"active\">Checkout</li>
</ol>

<main class=\"checkout container\">

    <div class=\"row\">
        <div class=\"col-md-6 col-sm-6\">
            <table class=\"table table-striped\">
                <tr>
                    <th>Item</th>
                    <th>Qty.</th>
                    <th>Price</th>
                    <th>Remove Item</th>
                </tr>

                ";
        // line 88
        $context["count"] = 0;
        // line 89
        echo "                ";
        $context["total"] = 0;
        // line 90
        echo "
                ";
        // line 91
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["items"]) ? $context["items"] : $this->getContext($context, "items")));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 92
            echo "                    ";
            $context["count"] = ((isset($context["count"]) ? $context["count"] : $this->getContext($context, "count")) + $this->getAttribute($context["item"], "quantity", array()));
            // line 93
            echo "                    ";
            $context["total"] = ((isset($context["total"]) ? $context["total"] : $this->getContext($context, "total")) + ($this->getAttribute($this->getAttribute($context["item"], "item", array()), "price", array()) * $this->getAttribute($context["item"], "quantity", array())));
            // line 94
            echo "
                    <tr>
                        <td>
                            <a class=\"checkout-item row\" href=\"";
            // line 97
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("single_item", array("itemObj" => $this->getAttribute($this->getAttribute($context["item"], "item", array()), "id", array()))), "html", null, true);
            echo "\">
                                ";
            // line 98
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "item", array()), "itemName", array()), "html", null, true);
            echo "
                            </a>
                        </td>

                        <td>
                            <p>";
            // line 103
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "quantity", array()), "html", null, true);
            echo " x \$";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "item", array()), "price", array()), 2, ".", ","), "html", null, true);
            echo "</p>
                        </td>

                        <td>
                            <p>\$";
            // line 107
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ($this->getAttribute($this->getAttribute($context["item"], "item", array()), "price", array()) * $this->getAttribute($context["item"], "quantity", array())), 2, ".", ","), "html", null, true);
            echo "</p>
                        </td>

                        <td>
                            <form class=\"col-sm-2\" action=\"";
            // line 111
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("remove_item_at_checkout", array("itemId" => $this->getAttribute($this->getAttribute($context["item"], "item", array()), "id", array()))), "html", null, true);
            echo "\" method=\"GET\">
                                <button class=\"btn btn-danger\" type=\"submit\"><span class=\"fa fa-times-circle-o\"></span></button>
                            </form>
                        </td>


                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 119
        echo "            </table>

            <table class=\"table table-striped\">

                <tr>
                    ";
        // line 125
        echo "                    ";
        // line 126
        echo "                    ";
        // line 127
        echo "                    <td>
                        <p>Total items in cart: ";
        // line 128
        echo twig_escape_filter($this->env, (isset($context["count"]) ? $context["count"] : $this->getContext($context, "count")), "html", null, true);
        echo "</p>
                    </td>


                </tr>
                <tr>
                    ";
        // line 135
        echo "                    ";
        // line 136
        echo "                    ";
        // line 137
        echo "                    <td>
                        <p>Total: \$";
        // line 138
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["total"]) ? $context["total"] : $this->getContext($context, "total")), 2, ".", ","), "html", null, true);
        echo "</p>
                    </td>


                </tr>
            </table>
        </div>


        ";
        // line 148
        echo "
        <div class=\"col-md-4 col-md-offset-2 col-sm-6 custom-checkout\">
            ";
        // line 150
        echo twig_include($this->env, $context, "store/_customCheckoutForm.html.twig");
        echo "
        </div>


    </div>


</main>




";
        
        $__internal_1e0e56b6febf8f85375f988860de102a3226c1a43bad87eae57969b6848e3eda->leave($__internal_1e0e56b6febf8f85375f988860de102a3226c1a43bad87eae57969b6848e3eda_prof);

    }

    public function getTemplateName()
    {
        return "store/_checkout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  273 => 150,  269 => 148,  257 => 138,  254 => 137,  252 => 136,  250 => 135,  241 => 128,  238 => 127,  236 => 126,  234 => 125,  227 => 119,  213 => 111,  206 => 107,  197 => 103,  189 => 98,  185 => 97,  180 => 94,  177 => 93,  174 => 92,  170 => 91,  167 => 90,  164 => 89,  162 => 88,  143 => 72,  136 => 69,  130 => 68,  69 => 13,  62 => 9,  59 => 8,  53 => 7,  42 => 3,  36 => 2,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* {% block title %}*/
/*     {{ parent() }}*/
/*     - Checkout*/
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/* */
/*     {{ parent() }}*/
/*     <script type="text/javascript" src="https://js.stripe.com/v2/"></script>*/
/* */
/*     <script type="text/javascript">*/
/*         Stripe.setPublishableKey('{{ stripe_public_key }}');*/
/* */
/* */
/*         $(function() {*/
/*             var $form = $('#payment-form');*/
/*             $form.submit(function(event) {*/
/* */
/*                 event.preventDefault();*/
/*                 // Disable the submit button to prevent repeated clicks:*/
/*                 $form.find('.submit').prop('disabled', true);*/
/* */
/*                 // Request a token from Stripe:*/
/*                 Stripe.card.createToken($form, stripeResponseHandler);*/
/* */
/*                 // Prevent the form from being submitted:*/
/* //                return false;*/
/*             });*/
/*         });*/
/* */
/* */
/*         function stripeResponseHandler(status, response) {*/
/*             // Grab the form:*/
/*             var $form = $('#payment-form');*/
/* */
/*             if (response.error) { // Problem!*/
/* */
/*                 // Show the errors on the form:*/
/*                 $form.find('.payment-errors').text(response.error.message);*/
/*                 $form.find('.payment-errors-wrapper').removeClass('hidden');*/
/*                 $form.find('.submit').prop('disabled', false); // Re-enable submission*/
/* */
/*             } else { // Token was created!*/
/* */
/*                 $form.find('.payment-errors');*/
/*                 $form.find('.payment-errors-wrapper').addClass('hidden');*/
/* */
/*                 // Get the token ID:*/
/*                 var token = response.id;*/
/* */
/*                 // Insert the token ID into the form so it gets submitted to the server:*/
/*                 $form.append($('<input type="hidden" name="stripeToken">').val(token));*/
/* */
/*                 // Submit the form:*/
/*                 $form.get(0).submit();*/
/*             }*/
/*         };*/
/*     </script>*/
/* */
/* */
/* */
/* */
/* */
/* */
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {{ parent() }}*/
/* */
/* <ol class="breadcrumb">*/
/*     <li><a href="{{ path('store') }}">Home</a></li>*/
/*     <li class="active">Checkout</li>*/
/* </ol>*/
/* */
/* <main class="checkout container">*/
/* */
/*     <div class="row">*/
/*         <div class="col-md-6 col-sm-6">*/
/*             <table class="table table-striped">*/
/*                 <tr>*/
/*                     <th>Item</th>*/
/*                     <th>Qty.</th>*/
/*                     <th>Price</th>*/
/*                     <th>Remove Item</th>*/
/*                 </tr>*/
/* */
/*                 {% set count = 0 %}*/
/*                 {% set total = 0.00 %}*/
/* */
/*                 {% for item in items %}*/
/*                     {% set count = count + item.quantity %}*/
/*                     {% set total = total + (item.item.price * item.quantity) %}*/
/* */
/*                     <tr>*/
/*                         <td>*/
/*                             <a class="checkout-item row" href="{{ path('single_item', { 'itemObj' : item.item.id }) }}">*/
/*                                 {{ item.item.itemName }}*/
/*                             </a>*/
/*                         </td>*/
/* */
/*                         <td>*/
/*                             <p>{{item.quantity }} x ${{ item.item.price | number_format(2, '.', ',')  }}</p>*/
/*                         </td>*/
/* */
/*                         <td>*/
/*                             <p>${{ (item.item.price * item.quantity) | number_format(2, '.', ',') }}</p>*/
/*                         </td>*/
/* */
/*                         <td>*/
/*                             <form class="col-sm-2" action="{{ path('remove_item_at_checkout', {'itemId' : item.item.id}) }}" method="GET">*/
/*                                 <button class="btn btn-danger" type="submit"><span class="fa fa-times-circle-o"></span></button>*/
/*                             </form>*/
/*                         </td>*/
/* */
/* */
/*                     </tr>*/
/*                 {% endfor %}*/
/*             </table>*/
/* */
/*             <table class="table table-striped">*/
/* */
/*                 <tr>*/
/*                     {#<td></td>#}*/
/*                     {#<td></td>#}*/
/*                     {#<td></td>#}*/
/*                     <td>*/
/*                         <p>Total items in cart: {{ count }}</p>*/
/*                     </td>*/
/* */
/* */
/*                 </tr>*/
/*                 <tr>*/
/*                     {#<td></td>#}*/
/*                     {#<td></td>#}*/
/*                     {#<td></td>#}*/
/*                     <td>*/
/*                         <p>Total: ${{ total | number_format(2, '.', ',') }}</p>*/
/*                     </td>*/
/* */
/* */
/*                 </tr>*/
/*             </table>*/
/*         </div>*/
/* */
/* */
/*         {#<!-- add checkout form here -->#}*/
/* */
/*         <div class="col-md-4 col-md-offset-2 col-sm-6 custom-checkout">*/
/*             {{ include('store/_customCheckoutForm.html.twig') }}*/
/*         </div>*/
/* */
/* */
/*     </div>*/
/* */
/* */
/* </main>*/
/* */
/* */
/* */
/* */
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* {#<form action="" method="POST">#}*/
/*     {#<script#}*/
/*             {#src="https://checkout.stripe.com/checkout.js" class="stripe-button"#}*/
/*             {#data-key="{{ stripe_public_key }}"#}*/
/*             {#data-amount="{{ cart.total * 100 }}"#}*/
/*             {#data-name="Demo Site"#}*/
/*             {#data-description="Widget"#}*/
/*             {#data-image="/img/documentation/checkout/marketplace.png"#}*/
/*             {#data-locale="auto">#}*/
/*     {#</script>#}*/
/* {#</form>#}*/
