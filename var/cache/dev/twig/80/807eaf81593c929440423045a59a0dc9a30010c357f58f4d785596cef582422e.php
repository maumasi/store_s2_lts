<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_7b502d5535052e3bed461edf80045d1ad16ba39070e7959a6cb19c17257d90b0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2e304da0779d6f6bbc78b87f021fe620b8c7e2ef3201e2908b92dd639d65de7b = $this->env->getExtension("native_profiler");
        $__internal_2e304da0779d6f6bbc78b87f021fe620b8c7e2ef3201e2908b92dd639d65de7b->enter($__internal_2e304da0779d6f6bbc78b87f021fe620b8c7e2ef3201e2908b92dd639d65de7b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_2e304da0779d6f6bbc78b87f021fe620b8c7e2ef3201e2908b92dd639d65de7b->leave($__internal_2e304da0779d6f6bbc78b87f021fe620b8c7e2ef3201e2908b92dd639d65de7b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*     <?php if (!$form->parent && $errors): ?>*/
/*     <?php echo $view['form']->errors($form) ?>*/
/*     <?php endif ?>*/
/*     <?php echo $view['form']->block($form, 'form_rows') ?>*/
/*     <?php echo $view['form']->rest($form) ?>*/
/* </div>*/
/* */
