<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_cd0db9055f0ee2abcf6a82a26d863fe4f1e65cde41c716399c015102600f0bf8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c651976faa20d21e43fe36b0c58666d97b27d1c9081a43d6e3249c28fbfd2671 = $this->env->getExtension("native_profiler");
        $__internal_c651976faa20d21e43fe36b0c58666d97b27d1c9081a43d6e3249c28fbfd2671->enter($__internal_c651976faa20d21e43fe36b0c58666d97b27d1c9081a43d6e3249c28fbfd2671_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_c651976faa20d21e43fe36b0c58666d97b27d1c9081a43d6e3249c28fbfd2671->leave($__internal_c651976faa20d21e43fe36b0c58666d97b27d1c9081a43d6e3249c28fbfd2671_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'widget_container_attributes') ?>*/
/* */
