<?php

/* :vendor:editVendor.html.twig */
class __TwigTemplate_6c0493d756399ebf957b745078cb21421dad55696451b6c86b4d28dc007ec2c3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", ":vendor:editVendor.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascript' => array($this, 'block_javascript'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_83657f3a42f4a957f2e6f26f68745fbc24dfeb487bc7c67c4039182ca48f1cf8 = $this->env->getExtension("native_profiler");
        $__internal_83657f3a42f4a957f2e6f26f68745fbc24dfeb487bc7c67c4039182ca48f1cf8->enter($__internal_83657f3a42f4a957f2e6f26f68745fbc24dfeb487bc7c67c4039182ca48f1cf8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":vendor:editVendor.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_83657f3a42f4a957f2e6f26f68745fbc24dfeb487bc7c67c4039182ca48f1cf8->leave($__internal_83657f3a42f4a957f2e6f26f68745fbc24dfeb487bc7c67c4039182ca48f1cf8_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_92019a017a715dca7cd731841ad96e814314b82a244f21381c38811c2ea01dd8 = $this->env->getExtension("native_profiler");
        $__internal_92019a017a715dca7cd731841ad96e814314b82a244f21381c38811c2ea01dd8->enter($__internal_92019a017a715dca7cd731841ad96e814314b82a244f21381c38811c2ea01dd8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 5
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo "

    - Registration

";
        
        $__internal_92019a017a715dca7cd731841ad96e814314b82a244f21381c38811c2ea01dd8->leave($__internal_92019a017a715dca7cd731841ad96e814314b82a244f21381c38811c2ea01dd8_prof);

    }

    // line 10
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_bd9c2f0c1331243b88679719095b7109179bcb31735f09fe026d81d628e794a9 = $this->env->getExtension("native_profiler");
        $__internal_bd9c2f0c1331243b88679719095b7109179bcb31735f09fe026d81d628e794a9->enter($__internal_bd9c2f0c1331243b88679719095b7109179bcb31735f09fe026d81d628e794a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 11
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

";
        
        $__internal_bd9c2f0c1331243b88679719095b7109179bcb31735f09fe026d81d628e794a9->leave($__internal_bd9c2f0c1331243b88679719095b7109179bcb31735f09fe026d81d628e794a9_prof);

    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        $__internal_3f6d601e54e7ca740a196e75402a43d6b627a8ea29e213fa2c3d6f04470fa11c = $this->env->getExtension("native_profiler");
        $__internal_3f6d601e54e7ca740a196e75402a43d6b627a8ea29e213fa2c3d6f04470fa11c->enter($__internal_3f6d601e54e7ca740a196e75402a43d6b627a8ea29e213fa2c3d6f04470fa11c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 15
        echo "    ";
        $this->displayParentBlock("body", $context, $blocks);
        echo "



    <main class=\" container-fluid\">
        <div class=\"row\">
            <div class=\"col-xs-12\">
                <h1 class=\"col-lg-offset-4 col-md-offset-3 col-sm-offset-3 col-lg-4 col-md-6 col-sm-6\">Edit Vendor</h1>




                ";
        // line 27
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), 'form_start');
        echo "
                    ";
        // line 28
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), 'widget');
        echo "
                    <button type=\"submit\" class=\"btn btn-primary\" formnovalidate >Make Changes</button>
                ";
        // line 30
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), 'form_end');
        echo "
            </div>
        </div>
    </main>






";
        
        $__internal_3f6d601e54e7ca740a196e75402a43d6b627a8ea29e213fa2c3d6f04470fa11c->leave($__internal_3f6d601e54e7ca740a196e75402a43d6b627a8ea29e213fa2c3d6f04470fa11c_prof);

    }

    // line 41
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_0631eab8d1c2e5a0d86364dfd9719ceaf3dacc4d4a54066ae836c45efe6f6f88 = $this->env->getExtension("native_profiler");
        $__internal_0631eab8d1c2e5a0d86364dfd9719ceaf3dacc4d4a54066ae836c45efe6f6f88->enter($__internal_0631eab8d1c2e5a0d86364dfd9719ceaf3dacc4d4a54066ae836c45efe6f6f88_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        // line 42
        echo "    ";
        $this->displayParentBlock("javascript", $context, $blocks);
        echo "


";
        
        $__internal_0631eab8d1c2e5a0d86364dfd9719ceaf3dacc4d4a54066ae836c45efe6f6f88->leave($__internal_0631eab8d1c2e5a0d86364dfd9719ceaf3dacc4d4a54066ae836c45efe6f6f88_prof);

    }

    public function getTemplateName()
    {
        return ":vendor:editVendor.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 42,  122 => 41,  104 => 30,  99 => 28,  95 => 27,  79 => 15,  73 => 14,  62 => 11,  56 => 10,  43 => 5,  37 => 4,  11 => 2,);
    }
}
/* */
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block title %}*/
/*     {{ parent() }}*/
/* */
/*     - Registration*/
/* */
/* {% endblock %}*/
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/* */
/* {% endblock %}*/
/* {% block body %}*/
/*     {{ parent() }}*/
/* */
/* */
/* */
/*     <main class=" container-fluid">*/
/*         <div class="row">*/
/*             <div class="col-xs-12">*/
/*                 <h1 class="col-lg-offset-4 col-md-offset-3 col-sm-offset-3 col-lg-4 col-md-6 col-sm-6">Edit Vendor</h1>*/
/* */
/* */
/* */
/* */
/*                 {{ form_start(registrationForm) }}*/
/*                     {{ form_widget(registrationForm) }}*/
/*                     <button type="submit" class="btn btn-primary" formnovalidate >Make Changes</button>*/
/*                 {{ form_end(registrationForm) }}*/
/*             </div>*/
/*         </div>*/
/*     </main>*/
/* */
/* */
/* */
/* */
/* */
/* */
/* {% endblock %}*/
/* {% block javascript %}*/
/*     {{ parent() }}*/
/* */
/* */
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* */
