<?php

/* store/guestEditProfile.html.twig */
class __TwigTemplate_2260f7f78c196f518487f95f9e2329dfc835fd5c06923a171ac69f98c058c176 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "store/guestEditProfile.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascript' => array($this, 'block_javascript'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7e950715352dc5906d284f0dd0f9497535c4fc4ab93de13cd30f3fffecd394f1 = $this->env->getExtension("native_profiler");
        $__internal_7e950715352dc5906d284f0dd0f9497535c4fc4ab93de13cd30f3fffecd394f1->enter($__internal_7e950715352dc5906d284f0dd0f9497535c4fc4ab93de13cd30f3fffecd394f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "store/guestEditProfile.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7e950715352dc5906d284f0dd0f9497535c4fc4ab93de13cd30f3fffecd394f1->leave($__internal_7e950715352dc5906d284f0dd0f9497535c4fc4ab93de13cd30f3fffecd394f1_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_6bb140f740f62b38d289043bed73b319efe0886b4f1bb569f960455b4a2226f5 = $this->env->getExtension("native_profiler");
        $__internal_6bb140f740f62b38d289043bed73b319efe0886b4f1bb569f960455b4a2226f5->enter($__internal_6bb140f740f62b38d289043bed73b319efe0886b4f1bb569f960455b4a2226f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 5
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo "

    - Registration

";
        
        $__internal_6bb140f740f62b38d289043bed73b319efe0886b4f1bb569f960455b4a2226f5->leave($__internal_6bb140f740f62b38d289043bed73b319efe0886b4f1bb569f960455b4a2226f5_prof);

    }

    // line 10
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_640ee8a13da9bc10522c6d4e5cf84075dba9f3d61b94a0b04b6c314a2f80e29b = $this->env->getExtension("native_profiler");
        $__internal_640ee8a13da9bc10522c6d4e5cf84075dba9f3d61b94a0b04b6c314a2f80e29b->enter($__internal_640ee8a13da9bc10522c6d4e5cf84075dba9f3d61b94a0b04b6c314a2f80e29b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 11
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

";
        
        $__internal_640ee8a13da9bc10522c6d4e5cf84075dba9f3d61b94a0b04b6c314a2f80e29b->leave($__internal_640ee8a13da9bc10522c6d4e5cf84075dba9f3d61b94a0b04b6c314a2f80e29b_prof);

    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        $__internal_cd4ee15ab3d080b13f79a3926241a176b91de39b90cc9c35596fcc58d79b8a44 = $this->env->getExtension("native_profiler");
        $__internal_cd4ee15ab3d080b13f79a3926241a176b91de39b90cc9c35596fcc58d79b8a44->enter($__internal_cd4ee15ab3d080b13f79a3926241a176b91de39b90cc9c35596fcc58d79b8a44_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 15
        echo "    ";
        $this->displayParentBlock("body", $context, $blocks);
        echo "


    ";
        // line 19
        echo "    <ol class=\"breadcrumb\">
        <li><a href=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("store");
        echo "\">Home</a></li>
        <li class=\"active\">Edit Customer</li>
    </ol>



    <main class=\"customer-edit container-fluid\">
        <div class=\"row\">
            <div class=\"col-xs-12\">
                <h1 class=\"col-lg-offset-4 col-md-offset-3 col-sm-offset-3 col-lg-4 col-md-6 col-sm-6\">Edit ";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "firstName", array()), "html", null, true);
        echo "</h1>



                ";
        // line 33
        echo twig_include($this->env, $context, "store/_guestRegistrationForm.html.twig", array("submitBtn" => "Edit", "cancelPath" => $this->env->getExtension('routing')->getPath("store")));
        // line 36
        echo "


            </div>
        </div>
    </main>






";
        
        $__internal_cd4ee15ab3d080b13f79a3926241a176b91de39b90cc9c35596fcc58d79b8a44->leave($__internal_cd4ee15ab3d080b13f79a3926241a176b91de39b90cc9c35596fcc58d79b8a44_prof);

    }

    // line 49
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_7b6044cd9bd7e0cae3caf19a4a8fe97c832fded581d0b3d9bee4b92d42e14310 = $this->env->getExtension("native_profiler");
        $__internal_7b6044cd9bd7e0cae3caf19a4a8fe97c832fded581d0b3d9bee4b92d42e14310->enter($__internal_7b6044cd9bd7e0cae3caf19a4a8fe97c832fded581d0b3d9bee4b92d42e14310_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        // line 50
        echo "    ";
        $this->displayParentBlock("javascript", $context, $blocks);
        echo "


";
        
        $__internal_7b6044cd9bd7e0cae3caf19a4a8fe97c832fded581d0b3d9bee4b92d42e14310->leave($__internal_7b6044cd9bd7e0cae3caf19a4a8fe97c832fded581d0b3d9bee4b92d42e14310_prof);

    }

    public function getTemplateName()
    {
        return "store/guestEditProfile.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  135 => 50,  129 => 49,  110 => 36,  108 => 33,  101 => 29,  89 => 20,  86 => 19,  79 => 15,  73 => 14,  62 => 11,  56 => 10,  43 => 5,  37 => 4,  11 => 2,);
    }
}
/* */
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block title %}*/
/*     {{ parent() }}*/
/* */
/*     - Registration*/
/* */
/* {% endblock %}*/
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/* */
/* {% endblock %}*/
/* {% block body %}*/
/*     {{ parent() }}*/
/* */
/* */
/*     {# breadcrumbs #}*/
/*     <ol class="breadcrumb">*/
/*         <li><a href="{{ path('store') }}">Home</a></li>*/
/*         <li class="active">Edit Customer</li>*/
/*     </ol>*/
/* */
/* */
/* */
/*     <main class="customer-edit container-fluid">*/
/*         <div class="row">*/
/*             <div class="col-xs-12">*/
/*                 <h1 class="col-lg-offset-4 col-md-offset-3 col-sm-offset-3 col-lg-4 col-md-6 col-sm-6">Edit {{ app.user.firstName }}</h1>*/
/* */
/* */
/* */
/*                 {{ include('store/_guestRegistrationForm.html.twig',{*/
/*                     'submitBtn' : 'Edit',*/
/*                     'cancelPath' : path('store')*/
/*                 }) }}*/
/* */
/* */
/*             </div>*/
/*         </div>*/
/*     </main>*/
/* */
/* */
/* */
/* */
/* */
/* */
/* {% endblock %}*/
/* {% block javascript %}*/
/*     {{ parent() }}*/
/* */
/* */
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
