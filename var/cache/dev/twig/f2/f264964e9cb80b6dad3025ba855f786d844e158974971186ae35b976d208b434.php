<?php

/* :vendor:queItemsList.html.twig */
class __TwigTemplate_81bb12727ac57d4abfe015c756f1d3f2e9ceb1bcec59ae59ec30349bfb2daa26 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", ":vendor:queItemsList.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_22cb5abd299675a62593776b7f85805a7d164a99d1dfb62ad799deebd42f3b29 = $this->env->getExtension("native_profiler");
        $__internal_22cb5abd299675a62593776b7f85805a7d164a99d1dfb62ad799deebd42f3b29->enter($__internal_22cb5abd299675a62593776b7f85805a7d164a99d1dfb62ad799deebd42f3b29_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":vendor:queItemsList.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_22cb5abd299675a62593776b7f85805a7d164a99d1dfb62ad799deebd42f3b29->leave($__internal_22cb5abd299675a62593776b7f85805a7d164a99d1dfb62ad799deebd42f3b29_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_a54e660caabe7a4fe70d47e35d21457909202d5b4790087178a000c857b18dc9 = $this->env->getExtension("native_profiler");
        $__internal_a54e660caabe7a4fe70d47e35d21457909202d5b4790087178a000c857b18dc9->enter($__internal_a54e660caabe7a4fe70d47e35d21457909202d5b4790087178a000c857b18dc9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 5
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo "

    - Registration

";
        
        $__internal_a54e660caabe7a4fe70d47e35d21457909202d5b4790087178a000c857b18dc9->leave($__internal_a54e660caabe7a4fe70d47e35d21457909202d5b4790087178a000c857b18dc9_prof);

    }

    // line 10
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_6ff6a2d4e7b9b6ae29e3a7640c1ebd2dc5327f5b4fb281c4803395a7c2d3554c = $this->env->getExtension("native_profiler");
        $__internal_6ff6a2d4e7b9b6ae29e3a7640c1ebd2dc5327f5b4fb281c4803395a7c2d3554c->enter($__internal_6ff6a2d4e7b9b6ae29e3a7640c1ebd2dc5327f5b4fb281c4803395a7c2d3554c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 11
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

";
        
        $__internal_6ff6a2d4e7b9b6ae29e3a7640c1ebd2dc5327f5b4fb281c4803395a7c2d3554c->leave($__internal_6ff6a2d4e7b9b6ae29e3a7640c1ebd2dc5327f5b4fb281c4803395a7c2d3554c_prof);

    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        $__internal_b6e98eaed49928e357d8713ae4e12516059a6f9934e624e1e9ab5174a9335fcd = $this->env->getExtension("native_profiler");
        $__internal_b6e98eaed49928e357d8713ae4e12516059a6f9934e624e1e9ab5174a9335fcd->enter($__internal_b6e98eaed49928e357d8713ae4e12516059a6f9934e624e1e9ab5174a9335fcd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 15
        echo "    ";
        $this->displayParentBlock("body", $context, $blocks);
        echo "

    <h1>Build items list here</h1>



<main class=\"container\">
    <div class=\"row\">

        ";
        // line 24
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["queSet"]) ? $context["queSet"] : $this->getContext($context, "queSet")));
        foreach ($context['_seq'] as $context["_key"] => $context["que"]) {
            // line 25
            echo "            <div class=\" col-lg-6 col-lg-offset-3 panel panel-default\">
                <div class=\"panel-body\">
                   <p>";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["que"], "id", array()), "html", null, true);
            echo "</p>

                    <p>";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["que"], "item", array()), "itemName", array()), "html", null, true);
            echo "</p>
                    <p>";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["que"], "createdAt", array()), "format", array(0 => "M d, Y"), "method"), "html", null, true);
            echo "</p>

                    ";
            // line 33
            echo "


                        ";
            // line 37
            echo "
                    ";
            // line 39
            echo "
                    ";
            // line 41
            echo "                        ";
            // line 42
            echo "
                    ";
            // line 44
            echo "

                </div>
            </div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['que'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "



        ";
        // line 55
        echo "            ";
        // line 56
        echo "                ";
        // line 57
        echo "                    ";
        // line 58
        echo "
                    ";
        // line 60
        echo "                ";
        // line 61
        echo "            ";
        // line 62
        echo "
        ";
        // line 64
        echo "


    </div>
</main>






    ";
        
        $__internal_b6e98eaed49928e357d8713ae4e12516059a6f9934e624e1e9ab5174a9335fcd->leave($__internal_b6e98eaed49928e357d8713ae4e12516059a6f9934e624e1e9ab5174a9335fcd_prof);

    }

    public function getTemplateName()
    {
        return ":vendor:queItemsList.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  162 => 64,  159 => 62,  157 => 61,  155 => 60,  152 => 58,  150 => 57,  148 => 56,  146 => 55,  140 => 50,  129 => 44,  126 => 42,  124 => 41,  121 => 39,  118 => 37,  113 => 33,  108 => 30,  104 => 29,  99 => 27,  95 => 25,  91 => 24,  78 => 15,  72 => 14,  61 => 11,  55 => 10,  42 => 5,  36 => 4,  11 => 2,);
    }
}
/* */
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block title %}*/
/*     {{ parent() }}*/
/* */
/*     - Registration*/
/* */
/* {% endblock %}*/
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/* */
/* {% endblock %}*/
/* {% block body %}*/
/*     {{ parent() }}*/
/* */
/*     <h1>Build items list here</h1>*/
/* */
/* */
/* */
/* <main class="container">*/
/*     <div class="row">*/
/* */
/*         {% for que in queSet %}*/
/*             <div class=" col-lg-6 col-lg-offset-3 panel panel-default">*/
/*                 <div class="panel-body">*/
/*                    <p>{{ que.id }}</p>*/
/* */
/*                     <p>{{ que.item.itemName }}</p>*/
/*                     <p>{{ que.createdAt.format('M d, Y') }}</p>*/
/* */
/*                     {#{{ include('_') }}#}*/
/* */
/* */
/* */
/*                         {#<input type="checkbox" name="{{ que.id }}" >#}*/
/* */
/*                     {#{{ form_start(queForm) }}#}*/
/* */
/*                     {#<button type="submit" class="btn btn-primary">Submit</button>#}*/
/*                         {#{{ form_row(queForm.isCompleted) }}#}*/
/* */
/*                     {#{{ form_end(queForm) }}#}*/
/* */
/* */
/*                 </div>*/
/*             </div>*/
/* */
/*         {% endfor %}*/
/* */
/* */
/* */
/* */
/*         {#{% for item in items %}#}*/
/*             {#<div class=" col-lg-6 col-lg-offset-3 panel panel-default">#}*/
/*                 {#<div class="panel-body">#}*/
/*                     {#{{ item.itemName }}#}*/
/* */
/*                     {#{{  }}#}*/
/*                 {#</div>#}*/
/*             {#</div>#}*/
/* */
/*         {#{% endfor %}#}*/
/* */
/* */
/* */
/*     </div>*/
/* </main>*/
/* */
/* */
/* */
/* */
/* */
/* */
/*     {#<a href="{{ path('vendor_item_list',{'vendorId': user.id}) }}">Your Items</a>#}*/
/* {% endblock %}*/
/* */
/* */
/* */
