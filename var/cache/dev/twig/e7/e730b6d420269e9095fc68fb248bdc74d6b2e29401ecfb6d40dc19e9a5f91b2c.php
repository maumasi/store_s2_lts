<?php

/* :vendor:_vendorNav.html.twig */
class __TwigTemplate_f57e758e775074b9b21675be46683bfc5eaddc4d5e02073f1fa78e49804901b3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'vendorNav' => array($this, 'block_vendorNav'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7e71bb8cd13eab419450c92a2d4a05c595e5382d120695f96beb8071bb6ed9a7 = $this->env->getExtension("native_profiler");
        $__internal_7e71bb8cd13eab419450c92a2d4a05c595e5382d120695f96beb8071bb6ed9a7->enter($__internal_7e71bb8cd13eab419450c92a2d4a05c595e5382d120695f96beb8071bb6ed9a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":vendor:_vendorNav.html.twig"));

        // line 1
        echo "<header class=\"container-fluid\">
    <nav class=\"row\">

        ";
        // line 5
        echo "        <h1 class=\"logo col-md-4 col-xs-6 col-xs-offset-1\"> <a href=\"";
        echo $this->env->getExtension('routing')->getPath("store");
        echo "\">The Bladesmith's Workbench</a> </h1>

        <ul class=\"nav nav-pills col-md-4 col-xs-6 col-xs-offset-3\">


            ";
        // line 10
        $this->displayBlock('vendorNav', $context, $blocks);
        // line 11
        echo "             ";
        // line 12
        echo "            ";
        // line 13
        echo "

             ";
        // line 16
        echo "            ";
        // line 17
        echo "
        </ul>
    </nav>
</header>";
        
        $__internal_7e71bb8cd13eab419450c92a2d4a05c595e5382d120695f96beb8071bb6ed9a7->leave($__internal_7e71bb8cd13eab419450c92a2d4a05c595e5382d120695f96beb8071bb6ed9a7_prof);

    }

    // line 10
    public function block_vendorNav($context, array $blocks = array())
    {
        $__internal_a09cd1b45b818e021f8bf84af7a9339b7ee2717ecf229fc4f488c7fe8f645797 = $this->env->getExtension("native_profiler");
        $__internal_a09cd1b45b818e021f8bf84af7a9339b7ee2717ecf229fc4f488c7fe8f645797->enter($__internal_a09cd1b45b818e021f8bf84af7a9339b7ee2717ecf229fc4f488c7fe8f645797_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "vendorNav"));

        
        $__internal_a09cd1b45b818e021f8bf84af7a9339b7ee2717ecf229fc4f488c7fe8f645797->leave($__internal_a09cd1b45b818e021f8bf84af7a9339b7ee2717ecf229fc4f488c7fe8f645797_prof);

    }

    public function getTemplateName()
    {
        return ":vendor:_vendorNav.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 10,  49 => 17,  47 => 16,  43 => 13,  41 => 12,  39 => 11,  37 => 10,  28 => 5,  23 => 1,);
    }
}
/* <header class="container-fluid">*/
/*     <nav class="row">*/
/* */
/*         {# Link to home page #}*/
/*         <h1 class="logo col-md-4 col-xs-6 col-xs-offset-1"> <a href="{{ path('store') }}">The Bladesmith's Workbench</a> </h1>*/
/* */
/*         <ul class="nav nav-pills col-md-4 col-xs-6 col-xs-offset-3">*/
/* */
/* */
/*             {% block vendorNav %}{% endblock %}*/
/*              {#Link to a vendor registration form#}*/
/*             {#<li role="presentation"><a href="{{ path('vendor_dashboard') }}">Edit Vendor Info</a></li>#}*/
/* */
/* */
/*              {#Link to check out#}*/
/*             {#<li role="presentation" ><a href="#">View Your Products</a></li>#}*/
/* */
/*         </ul>*/
/*     </nav>*/
/* </header>*/
