<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_502acc68db0baa2f6dfb3783b5fe34b52ba74ddef7e5f4ad9b40ebcf75558771 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dcf2a2c646c05470a5e784c055a5f1651b568c7712fa8178a8135ea1f9e07095 = $this->env->getExtension("native_profiler");
        $__internal_dcf2a2c646c05470a5e784c055a5f1651b568c7712fa8178a8135ea1f9e07095->enter($__internal_dcf2a2c646c05470a5e784c055a5f1651b568c7712fa8178a8135ea1f9e07095_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_dcf2a2c646c05470a5e784c055a5f1651b568c7712fa8178a8135ea1f9e07095->leave($__internal_dcf2a2c646c05470a5e784c055a5f1651b568c7712fa8178a8135ea1f9e07095_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr style="display: none">*/
/*     <td colspan="2">*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
