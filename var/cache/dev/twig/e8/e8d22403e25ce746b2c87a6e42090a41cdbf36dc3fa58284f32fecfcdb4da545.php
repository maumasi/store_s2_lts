<?php

/* vendor/vendorItemViews/itemCreate.html.twig */
class __TwigTemplate_19677f1b1005cb4421f98399f6223c020fb784d20d3e286db7f721f90d4064f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "vendor/vendorItemViews/itemCreate.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'javascript' => array($this, 'block_javascript'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_84fb2d81ebff01d7896992b20bdef74b3e88178708e143e45a19f65f27dc1208 = $this->env->getExtension("native_profiler");
        $__internal_84fb2d81ebff01d7896992b20bdef74b3e88178708e143e45a19f65f27dc1208->enter($__internal_84fb2d81ebff01d7896992b20bdef74b3e88178708e143e45a19f65f27dc1208_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "vendor/vendorItemViews/itemCreate.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_84fb2d81ebff01d7896992b20bdef74b3e88178708e143e45a19f65f27dc1208->leave($__internal_84fb2d81ebff01d7896992b20bdef74b3e88178708e143e45a19f65f27dc1208_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_96fdef351a70c00719f94c73d5d7939a86b1904d6ed0d4af80f96bd4be39ab7d = $this->env->getExtension("native_profiler");
        $__internal_96fdef351a70c00719f94c73d5d7939a86b1904d6ed0d4af80f96bd4be39ab7d->enter($__internal_96fdef351a70c00719f94c73d5d7939a86b1904d6ed0d4af80f96bd4be39ab7d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 5
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo "

    - app.user.first_name

";
        
        $__internal_96fdef351a70c00719f94c73d5d7939a86b1904d6ed0d4af80f96bd4be39ab7d->leave($__internal_96fdef351a70c00719f94c73d5d7939a86b1904d6ed0d4af80f96bd4be39ab7d_prof);

    }

    // line 11
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_8eba567f0f6b15ae83fa4644b68dd84d7eba3ba071d4f7db1f901fb151ac6dc8 = $this->env->getExtension("native_profiler");
        $__internal_8eba567f0f6b15ae83fa4644b68dd84d7eba3ba071d4f7db1f901fb151ac6dc8->enter($__internal_8eba567f0f6b15ae83fa4644b68dd84d7eba3ba071d4f7db1f901fb151ac6dc8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        // line 12
        echo "     ";
        $this->displayParentBlock("javascript", $context, $blocks);
        echo "


 ";
        
        $__internal_8eba567f0f6b15ae83fa4644b68dd84d7eba3ba071d4f7db1f901fb151ac6dc8->leave($__internal_8eba567f0f6b15ae83fa4644b68dd84d7eba3ba071d4f7db1f901fb151ac6dc8_prof);

    }

    // line 17
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_e9cd99c954af951aab6d869e6372a4be61faa7ea55f181d1a38b105fc01831eb = $this->env->getExtension("native_profiler");
        $__internal_e9cd99c954af951aab6d869e6372a4be61faa7ea55f181d1a38b105fc01831eb->enter($__internal_e9cd99c954af951aab6d869e6372a4be61faa7ea55f181d1a38b105fc01831eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 18
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

";
        
        $__internal_e9cd99c954af951aab6d869e6372a4be61faa7ea55f181d1a38b105fc01831eb->leave($__internal_e9cd99c954af951aab6d869e6372a4be61faa7ea55f181d1a38b105fc01831eb_prof);

    }

    // line 21
    public function block_body($context, array $blocks = array())
    {
        $__internal_ac12b0b267e24d94232390753a54b61cb235a0e646194d0ae99c1676207fa032 = $this->env->getExtension("native_profiler");
        $__internal_ac12b0b267e24d94232390753a54b61cb235a0e646194d0ae99c1676207fa032->enter($__internal_ac12b0b267e24d94232390753a54b61cb235a0e646194d0ae99c1676207fa032_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 22
        echo "    ";
        $this->displayParentBlock("body", $context, $blocks);
        echo "



<main class=\"add-item container\">
    <div class=\"row\">

        <h1 class=\"col-md-offset-2 col-sm-offset-2 col-lg-4 col-md-6 col-sm-6\">Add Item</h1>


        ";
        // line 32
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["itemForm"]) ? $context["itemForm"] : $this->getContext($context, "itemForm")), 'form_start', array("attr" => array("class" => "col-md-offset-2 col-md-offset-3 col-sm-offset-3 col-lg-8 col-md-6 col-sm-6")));
        // line 36
        echo "


        <div class=\"col-md-6 col-sm-6\">
            ";
        // line 40
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["itemForm"]) ? $context["itemForm"] : $this->getContext($context, "itemForm")), "itemName", array()), 'row', array("attr" => array("placeholder" => "Item Name"), "label_attr" => array("class" => "sr-only")));
        // line 47
        echo "
        </div>


        <div class=\"col-md-6 col-sm-6\">
            ";
        // line 52
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["itemForm"]) ? $context["itemForm"] : $this->getContext($context, "itemForm")), "price", array()), 'row', array("attr" => array("placeholder" => "Price"), "label_attr" => array("class" => "sr-only")));
        // line 59
        echo "
        </div>


        ";
        // line 64
        echo "
        ";
        // line 66
        echo "





        <div class=\"col-md-6 col-sm-6\">

        </div>


        ";
        // line 78
        echo "            ";
        // line 79
        echo "        ";
        // line 80
        echo "

        <div class=\" col-sm-6\">
            ";
        // line 83
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["itemForm"]) ? $context["itemForm"] : $this->getContext($context, "itemForm")), "quantity", array()), 'row', array("attr" => array("placeholder" => "Quantity", "class" => "item-quantity"), "label_attr" => array("class" => "sr-only")));
        // line 91
        echo "
        </div>



        <div class=\"col-md-6 col-sm-6\">
            ";
        // line 97
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["itemForm"]) ? $context["itemForm"] : $this->getContext($context, "itemForm")), "itemImg", array()), 'row', array("attr" => array("class" => "btn btn-download"), "label_attr" => array("class" => "sr-only")));
        // line 104
        echo "
        </div>


        <div class=\" col-sm-12\">
            ";
        // line 109
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["itemForm"]) ? $context["itemForm"] : $this->getContext($context, "itemForm")), "description", array()), 'row', array("attr" => array("placeholder" => "Description", "class" => "item-description"), "label_attr" => array("class" => "sr-only")));
        // line 117
        echo "
        </div>
        

        <div>
            <div class=\"item-action col-xs-4 col-xs-offset-4\">
                <a href=\"";
        // line 123
        echo $this->env->getExtension('routing')->getPath("vendor_item_collection");
        echo "\">Cancel</a>
            </div>

            <div class=\"item-action col-xs-4 \">
                <button type=\"submit\" class=\"btn btn-primary col-xs-12\" formnovalidate >Add item</button>
            </div>
        </div>


        ";
        // line 132
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["itemForm"]) ? $context["itemForm"] : $this->getContext($context, "itemForm")), 'form_end');
        echo "
    </div>
</main>






























    ";
        // line 166
        echo "

    ";
        // line 169
        echo "

    ";
        // line 172
        echo "

        ";
        // line 175
        echo "        ";
        // line 176
        echo "        ";
        // line 177
        echo "        ";
        // line 178
        echo "        ";
        // line 179
        echo "            ";
        // line 180
        echo "                ";
        // line 181
        echo "            ";
        // line 182
        echo "        ";
        // line 183
        echo "


        ";
        // line 187
        echo "

    ";
        // line 190
        echo "

";
        
        $__internal_ac12b0b267e24d94232390753a54b61cb235a0e646194d0ae99c1676207fa032->leave($__internal_ac12b0b267e24d94232390753a54b61cb235a0e646194d0ae99c1676207fa032_prof);

    }

    public function getTemplateName()
    {
        return "vendor/vendorItemViews/itemCreate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  275 => 190,  271 => 187,  266 => 183,  264 => 182,  262 => 181,  260 => 180,  258 => 179,  256 => 178,  254 => 177,  252 => 176,  250 => 175,  246 => 172,  242 => 169,  238 => 166,  202 => 132,  190 => 123,  182 => 117,  180 => 109,  173 => 104,  171 => 97,  163 => 91,  161 => 83,  156 => 80,  154 => 79,  152 => 78,  139 => 66,  136 => 64,  130 => 59,  128 => 52,  121 => 47,  119 => 40,  113 => 36,  111 => 32,  97 => 22,  91 => 21,  80 => 18,  74 => 17,  62 => 12,  56 => 11,  43 => 5,  37 => 4,  11 => 2,);
    }
}
/* */
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block title %}*/
/*     {{ parent() }}*/
/* */
/*     - app.user.first_name*/
/* */
/* {% endblock %}*/
/* */
/*  {% block javascript %}*/
/*      {{ parent() }}*/
/* */
/* */
/*  {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/* */
/* {% endblock %}*/
/* {% block body %}*/
/*     {{ parent() }}*/
/* */
/* */
/* */
/* <main class="add-item container">*/
/*     <div class="row">*/
/* */
/*         <h1 class="col-md-offset-2 col-sm-offset-2 col-lg-4 col-md-6 col-sm-6">Add Item</h1>*/
/* */
/* */
/*         {{ form_start(itemForm,{*/
/*             'attr': {*/
/*                 'class': 'col-md-offset-2 col-md-offset-3 col-sm-offset-3 col-lg-8 col-md-6 col-sm-6'*/
/*             }*/
/*         }) }}*/
/* */
/* */
/*         <div class="col-md-6 col-sm-6">*/
/*             {{ form_row(itemForm.itemName,{*/
/*                 'attr': {*/
/*                     'placeholder': 'Item Name',*/
/*                 },*/
/*                 'label_attr': {*/
/*                     'class': 'sr-only'*/
/*                 }*/
/*             }) }}*/
/*         </div>*/
/* */
/* */
/*         <div class="col-md-6 col-sm-6">*/
/*             {{ form_row(itemForm.price,{*/
/*                 'attr': {*/
/*                     'placeholder': 'Price',*/
/*                 },*/
/*                 'label_attr': {*/
/*                     'class': 'sr-only'*/
/*                 }*/
/*             }) }}*/
/*         </div>*/
/* */
/* */
/*         {#<div class="col-md-6 col-sm-6">#}*/
/* */
/*         {#</div>#}*/
/* */
/* */
/* */
/* */
/* */
/* */
/*         <div class="col-md-6 col-sm-6">*/
/* */
/*         </div>*/
/* */
/* */
/*         {#<div class="col-md-6 col-sm-6">#}*/
/*             {#{{ form_row(itemForm.isInStock) }}#}*/
/*         {#</div>#}*/
/* */
/* */
/*         <div class=" col-sm-6">*/
/*             {{ form_row(itemForm.quantity,{*/
/*                 'attr': {*/
/*                     'placeholder': 'Quantity',*/
/*                     'class': 'item-quantity'*/
/*                 },*/
/*                 'label_attr': {*/
/*                     'class': 'sr-only'*/
/*                 }*/
/*             }) }}*/
/*         </div>*/
/* */
/* */
/* */
/*         <div class="col-md-6 col-sm-6">*/
/*             {{ form_row(itemForm.itemImg,{*/
/*                 'attr': {*/
/*                     'class': 'btn btn-download'*/
/*                 },*/
/*                 'label_attr': {*/
/*                     'class': 'sr-only'*/
/*                 }*/
/*             }) }}*/
/*         </div>*/
/* */
/* */
/*         <div class=" col-sm-12">*/
/*             {{ form_row(itemForm.description,{*/
/*                 'attr': {*/
/*                     'placeholder': 'Description',*/
/*                     'class': 'item-description'*/
/*                 },*/
/*                 'label_attr': {*/
/*                     'class': 'sr-only'*/
/*                 }*/
/*             }) }}*/
/*         </div>*/
/*         */
/* */
/*         <div>*/
/*             <div class="item-action col-xs-4 col-xs-offset-4">*/
/*                 <a href="{{ path('vendor_item_collection') }}">Cancel</a>*/
/*             </div>*/
/* */
/*             <div class="item-action col-xs-4 ">*/
/*                 <button type="submit" class="btn btn-primary col-xs-12" formnovalidate >Add item</button>*/
/*             </div>*/
/*         </div>*/
/* */
/* */
/*         {{ form_end(itemForm) }}*/
/*     </div>*/
/* </main>*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/*     {#<p>{{ app.user.firstName }}</p>#}*/
/* */
/* */
/*     {#<a href="{{ path('vendor_item_collection') }}" >Cancel</a>#}*/
/* */
/* */
/*     {#{{ form_start(itemForm) }}#}*/
/* */
/* */
/*         {#{{ form_row(itemForm.itemName) }}#}*/
/*         {#{{ form_row(itemForm.price) }}#}*/
/*         {#{{ form_row(itemForm.description) }}#}*/
/*         {#{{ form_row(itemForm.itemImg) }}#}*/
/*         {#{{ form_row(itemForm.isInStock,{#}*/
/*             {#'attr': {#}*/
/*                 {#'checked': 'true'#}*/
/*             {#}#}*/
/*         {#}) }}#}*/
/* */
/* */
/* */
/*         {#<button type="submit" class="btn btn-primary" formnovalidate >Create new item</button>#}*/
/* */
/* */
/*     {#{{ form_end(itemForm) }}#}*/
/* */
/* */
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* */
/* */
