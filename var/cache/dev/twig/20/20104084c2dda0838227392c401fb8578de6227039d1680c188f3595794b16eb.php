<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_66edd708813cebf0bfe11908627f076d39e5197c57a5e3c875acf659419349e0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e4507539c392c0202f1bdf761c2b7009d35899973f60b0f869e2c5411bb749be = $this->env->getExtension("native_profiler");
        $__internal_e4507539c392c0202f1bdf761c2b7009d35899973f60b0f869e2c5411bb749be->enter($__internal_e4507539c392c0202f1bdf761c2b7009d35899973f60b0f869e2c5411bb749be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_e4507539c392c0202f1bdf761c2b7009d35899973f60b0f869e2c5411bb749be->leave($__internal_e4507539c392c0202f1bdf761c2b7009d35899973f60b0f869e2c5411bb749be_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div>*/
/*     <?php echo $view['form']->label($form) ?>*/
/*     <?php echo $view['form']->errors($form) ?>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* </div>*/
/* */
