<?php

/* @Framework/Form/form_widget_simple.html.php */
class __TwigTemplate_91029f16095c972fc28ba40a235262b00843d23f74c677a0ba6dbbf0f702e5d3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_513f9bfc4f8e430040937b94315eb628876a8e67f76f92aa6dfc108442e355ae = $this->env->getExtension("native_profiler");
        $__internal_513f9bfc4f8e430040937b94315eb628876a8e67f76f92aa6dfc108442e355ae->enter($__internal_513f9bfc4f8e430040937b94315eb628876a8e67f76f92aa6dfc108442e355ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        // line 1
        echo "<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
";
        
        $__internal_513f9bfc4f8e430040937b94315eb628876a8e67f76f92aa6dfc108442e355ae->leave($__internal_513f9bfc4f8e430040937b94315eb628876a8e67f76f92aa6dfc108442e355ae_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_simple.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="<?php echo isset($type) ? $view->escape($type) : 'text' ?>" <?php echo $view['form']->block($form, 'widget_attributes') ?><?php if (!empty($value) || is_numeric($value)): ?> value="<?php echo $view->escape($value) ?>"<?php endif ?> />*/
/* */
