<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_d495310748d5461dfb761bd1204cf754a78c4dd0a9554259b0d1886d27343cdf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_77879419b666dd630f7ab5b8a724f4d23f5930855b80979c269c3f564282b3d6 = $this->env->getExtension("native_profiler");
        $__internal_77879419b666dd630f7ab5b8a724f4d23f5930855b80979c269c3f564282b3d6->enter($__internal_77879419b666dd630f7ab5b8a724f4d23f5930855b80979c269c3f564282b3d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_77879419b666dd630f7ab5b8a724f4d23f5930855b80979c269c3f564282b3d6->leave($__internal_77879419b666dd630f7ab5b8a724f4d23f5930855b80979c269c3f564282b3d6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($form->vars['multipart']): ?>enctype="multipart/form-data"<?php endif ?>*/
/* */
