<?php

/* store/item.html.twig */
class __TwigTemplate_0f6d66b7bd516a7cca23b1847a0d9609958f0ce7234876aa8e8dca2b32cd53f6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "store/item.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascript' => array($this, 'block_javascript'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4cb4f7ad415b0c427bb424de1af0205d5c178f6465ea94e67f2af630e5cd9127 = $this->env->getExtension("native_profiler");
        $__internal_4cb4f7ad415b0c427bb424de1af0205d5c178f6465ea94e67f2af630e5cd9127->enter($__internal_4cb4f7ad415b0c427bb424de1af0205d5c178f6465ea94e67f2af630e5cd9127_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "store/item.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4cb4f7ad415b0c427bb424de1af0205d5c178f6465ea94e67f2af630e5cd9127->leave($__internal_4cb4f7ad415b0c427bb424de1af0205d5c178f6465ea94e67f2af630e5cd9127_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_c6d4a8abd8c0d7f71a976257d4ae996f4836091164cbcc847ce1e0d5758fdafe = $this->env->getExtension("native_profiler");
        $__internal_c6d4a8abd8c0d7f71a976257d4ae996f4836091164cbcc847ce1e0d5758fdafe->enter($__internal_c6d4a8abd8c0d7f71a976257d4ae996f4836091164cbcc847ce1e0d5758fdafe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo "

    - Item Page

";
        
        $__internal_c6d4a8abd8c0d7f71a976257d4ae996f4836091164cbcc847ce1e0d5758fdafe->leave($__internal_c6d4a8abd8c0d7f71a976257d4ae996f4836091164cbcc847ce1e0d5758fdafe_prof);

    }

    // line 9
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_875e7843989bdbc438020ef7268dd07e7df343fb06b708055267019f051fd617 = $this->env->getExtension("native_profiler");
        $__internal_875e7843989bdbc438020ef7268dd07e7df343fb06b708055267019f051fd617->enter($__internal_875e7843989bdbc438020ef7268dd07e7df343fb06b708055267019f051fd617_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 10
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "


";
        
        $__internal_875e7843989bdbc438020ef7268dd07e7df343fb06b708055267019f051fd617->leave($__internal_875e7843989bdbc438020ef7268dd07e7df343fb06b708055267019f051fd617_prof);

    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        $__internal_51afc23fb4540656d9e5ac451df6dec30923cdc76996535ddcfb45c0ae74d1ae = $this->env->getExtension("native_profiler");
        $__internal_51afc23fb4540656d9e5ac451df6dec30923cdc76996535ddcfb45c0ae74d1ae->enter($__internal_51afc23fb4540656d9e5ac451df6dec30923cdc76996535ddcfb45c0ae74d1ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 15
        echo "    ";
        $this->displayParentBlock("body", $context, $blocks);
        echo "

    ";
        // line 18
        echo "    <ol class=\"breadcrumb\">
        <li><a href=\"";
        // line 19
        echo $this->env->getExtension('routing')->getPath("store");
        echo "\">Home</a></li>
        <li class=\"active\">";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "itemName", array()), "html", null, true);
        echo "</li>
    </ol>



    <main class=\"container item-details\">
        <div class=\"\">
            <a href=\"";
        // line 27
        echo $this->env->getExtension('routing')->getPath("store");
        echo "\"><span class=\"fa fa-angle-double-left\"></span> back to shopping</a>
            <h1>";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "itemName", array()), "html", null, true);
        echo " details</h1>
        </div>





        <div class=\"row display-item\">

            ";
        // line 38
        echo "            <div class=\"col-sm-3 col-xs-10\">
                <img class=\"img-responsive center-block item-img\" src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl(("uploads/" . $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "itemImg", array()))), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "itemName", array()), "html", null, true);
        echo "\"/>
            </div>


            ";
        // line 44
        echo "            <div class=\"col-sm-3 col-xs-10 col-sm-offset-1 col-xs-offset-1\">

                <h2>";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "itemName", array()), "html", null, true);
        echo "</h2>


                ";
        // line 49
        if ($this->getAttribute((isset($context["vendor"]) ? $context["vendor"] : $this->getContext($context, "vendor")), "companyName", array())) {
            // line 50
            echo "
                    <p class=\"vendor\">by ";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["vendor"]) ? $context["vendor"] : $this->getContext($context, "vendor")), "companyName", array()), "html", null, true);
            echo "</p>
                ";
        } else {
            // line 53
            echo "
                    <p class=\"vendor\">by ";
            // line 54
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["vendor"]) ? $context["vendor"] : $this->getContext($context, "vendor")), "firstName", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["vendor"]) ? $context["vendor"] : $this->getContext($context, "vendor")), "lastName", array()), "html", null, true);
            echo "</p>
                ";
        }
        // line 56
        echo "

                <p class=\"price\">Price: \$";
        // line 58
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "price", array()), 2, ".", ","), "html", null, true);
        echo "</p>

                ";
        // line 60
        if (($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "quantity", array()) > 0)) {
            // line 61
            echo "
                    <p class=\"in-stock\">In Stock</p>
                    <p>Quantity in stock: ";
            // line 63
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "quantity", array()), "html", null, true);
            echo "</p>
                ";
        } else {
            // line 65
            echo "
                    <p class=\"out-of-stock \">Currently Out Of Stock</p>
                ";
        }
        // line 68
        echo "

                <h3>Description</h3>
                <p class=\"description\">";
        // line 71
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "description", array()), "html", null, true);
        echo "</p>

            </div>


            ";
        // line 76
        if (($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "quantity", array()) > 0)) {
            // line 77
            echo "
            ";
            // line 79
            echo "            <div class=\"col-sm-3 col-xs-8 col-sm-offset-1 col-xs-offset-3\">

                <form class=\"add-to-cart\" action=\"";
            // line 81
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("add_to_cart", array("addItem" => $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "id", array()))), "html", null, true);
            echo "\" method=\"POST\">

                    <div class=\"input-group input-group-sm col-xs-6\">
                        <span class=\"input-group-addon\" id=\"sizing-addon3\">Quantity</span>
                        ";
            // line 86
            echo "                        <input type=\"text\" class=\"form-control\" aria-describedby=\"sizing-addon3\" size=\"20\" name=\"quantity\" value=\"1\" min=\"1\" max=\"";
            echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "quantity", array()) + 1), "html", null, true);
            echo "\">
                    </div>
                    ";
            // line 89
            echo "                    <button class=\" btn btn-md btn-success col-xs-6\" type=\"submit\">Add to Cart <span class=\"glyphicon glyphicon-shopping-cart\"></span></button>
                </form>


            </div>

            ";
        } else {
            // line 96
            echo "
            ";
            // line 98
            echo "            <div class=\"col-sm-3 col-xs-8 col-sm-offset-1 col-xs-offset-3\">

                ";
            // line 101
            echo "                    <button disabled=\"disabled\" class=\"btn btn-md btn-danger\" type=\"submit\"><span class=\"glyphicon glyphicon-exclamation-sign\"></span> Out of stock</button>
                ";
            // line 103
            echo "
            </div>


            ";
        }
        // line 108
        echo "
            ";
        // line 110
        echo "            <div class=\"col-sm-3 col-xs-10 col-sm-offset-1 col-xs-offset-1\">
                <p role=\"separator\" class=\"divider\"></p>
                ";
        // line 112
        if ((isset($context["itemsByThisVendor"]) ? $context["itemsByThisVendor"] : $this->getContext($context, "itemsByThisVendor"))) {
            // line 113
            echo "
                    ";
            // line 114
            if ($this->getAttribute((isset($context["vendor"]) ? $context["vendor"] : $this->getContext($context, "vendor")), "companyName", array())) {
                // line 115
                echo "
                        <p>More items by </p>
                        <h3 class=\"suggensted-vendor\">";
                // line 117
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["vendor"]) ? $context["vendor"] : $this->getContext($context, "vendor")), "companyName", array()), "html", null, true);
                echo "</h3>
                    ";
            } else {
                // line 119
                echo "
                        <p>More items by </p>
                        <h3 class=\"suggensted-vendor\">";
                // line 121
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["vendor"]) ? $context["vendor"] : $this->getContext($context, "vendor")), "firstName", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["vendor"]) ? $context["vendor"] : $this->getContext($context, "vendor")), "lastName", array()), "html", null, true);
                echo "</h3>
                    ";
            }
            // line 123
            echo "

                    <ul class=\"suggensted-vendor-items col-sm-12\">

                    ";
            // line 127
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["itemsByThisVendor"]) ? $context["itemsByThisVendor"] : $this->getContext($context, "itemsByThisVendor")));
            foreach ($context['_seq'] as $context["_key"] => $context["vendorItem"]) {
                // line 128
                echo "
                        ";
                // line 129
                if (($this->getAttribute($context["vendorItem"], "id", array()) != $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "id", array()))) {
                    // line 130
                    echo "
                        <li class=\"row\"><a href=\"";
                    // line 131
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("single_item", array("itemObj" => $this->getAttribute($context["vendorItem"], "id", array()))), "html", null, true);
                    echo "\">

                            <div class=\"col-sm-6 col-xs-6\">
                                <div class=\"vendorItem-img\"
                                     style=\"
                                             background: url( ";
                    // line 136
                    echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl(("uploads/" . $this->getAttribute($context["vendorItem"], "itemImg", array()))), "html", null, true);
                    echo " ) no-repeat;
                                             /*background-repeat: ;*/
                                             background-size: cover;
                                             background-position: center;
                                             background-color: #fff;
                                             border: 1px solid rgba(0,0,0,0.38);
                                            \">

                                    ";
                    // line 144
                    if (($this->getAttribute($context["vendorItem"], "quantity", array()) == 0)) {
                        // line 145
                        echo "
                                        <p class=\"out-of-stock out-of-stock-sm-thumbnail \">Currently Out Of Stock</p>
                                    ";
                    }
                    // line 148
                    echo "
                                </div>
                            </div>

                            <div class=\"suggensted-vendor-item-details col-sm-6 col-xs-6\">
                                <p>";
                    // line 153
                    echo twig_escape_filter($this->env, $this->getAttribute($context["vendorItem"], "itemName", array()), "html", null, true);
                    echo "</p>
                                <p>\$";
                    // line 154
                    echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["vendorItem"], "price", array()), 2, ".", ","), "html", null, true);
                    echo "</p>
                                ";
                    // line 156
                    echo "                            </div>


                            </a></li>

                            ";
                }
                // line 162
                echo "
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['vendorItem'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 164
            echo "
                    </ul>
                ";
        }
        // line 167
        echo "
            </div>

        </div>


    </main>

    ";
        // line 176
        echo "








";
        
        $__internal_51afc23fb4540656d9e5ac451df6dec30923cdc76996535ddcfb45c0ae74d1ae->leave($__internal_51afc23fb4540656d9e5ac451df6dec30923cdc76996535ddcfb45c0ae74d1ae_prof);

    }

    // line 186
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_c2dc27809ca7cacfeb7109d9da6a041e9ef92e6c402efb4b926c36010ba5e384 = $this->env->getExtension("native_profiler");
        $__internal_c2dc27809ca7cacfeb7109d9da6a041e9ef92e6c402efb4b926c36010ba5e384->enter($__internal_c2dc27809ca7cacfeb7109d9da6a041e9ef92e6c402efb4b926c36010ba5e384_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        // line 187
        echo "

    ";
        // line 189
        $this->displayParentBlock("javascript", $context, $blocks);
        echo "
";
        
        $__internal_c2dc27809ca7cacfeb7109d9da6a041e9ef92e6c402efb4b926c36010ba5e384->leave($__internal_c2dc27809ca7cacfeb7109d9da6a041e9ef92e6c402efb4b926c36010ba5e384_prof);

    }

    public function getTemplateName()
    {
        return "store/item.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  396 => 189,  392 => 187,  386 => 186,  370 => 176,  360 => 167,  355 => 164,  348 => 162,  340 => 156,  336 => 154,  332 => 153,  325 => 148,  320 => 145,  318 => 144,  307 => 136,  299 => 131,  296 => 130,  294 => 129,  291 => 128,  287 => 127,  281 => 123,  274 => 121,  270 => 119,  265 => 117,  261 => 115,  259 => 114,  256 => 113,  254 => 112,  250 => 110,  247 => 108,  240 => 103,  237 => 101,  233 => 98,  230 => 96,  221 => 89,  215 => 86,  208 => 81,  204 => 79,  201 => 77,  199 => 76,  191 => 71,  186 => 68,  181 => 65,  176 => 63,  172 => 61,  170 => 60,  165 => 58,  161 => 56,  154 => 54,  151 => 53,  146 => 51,  143 => 50,  141 => 49,  135 => 46,  131 => 44,  122 => 39,  119 => 38,  107 => 28,  103 => 27,  93 => 20,  89 => 19,  86 => 18,  80 => 15,  74 => 14,  62 => 10,  56 => 9,  43 => 4,  37 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block title %}*/
/*     {{ parent() }}*/
/* */
/*     - Item Page*/
/* */
/* {% endblock %}*/
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/* */
/* */
/* {% endblock %}*/
/* {% block body %}*/
/*     {{ parent() }}*/
/* */
/*     {# breadcrumbs #}*/
/*     <ol class="breadcrumb">*/
/*         <li><a href="{{ path('store') }}">Home</a></li>*/
/*         <li class="active">{{ item.itemName }}</li>*/
/*     </ol>*/
/* */
/* */
/* */
/*     <main class="container item-details">*/
/*         <div class="">*/
/*             <a href="{{ path('store') }}"><span class="fa fa-angle-double-left"></span> back to shopping</a>*/
/*             <h1>{{ item.itemName }} details</h1>*/
/*         </div>*/
/* */
/* */
/* */
/* */
/* */
/*         <div class="row display-item">*/
/* */
/*             {#display item#}*/
/*             <div class="col-sm-3 col-xs-10">*/
/*                 <img class="img-responsive center-block item-img" src="{{ asset('uploads/' ~ item.itemImg ) }}" alt="{{ item.itemName }}"/>*/
/*             </div>*/
/* */
/* */
/*             {#item details#}*/
/*             <div class="col-sm-3 col-xs-10 col-sm-offset-1 col-xs-offset-1">*/
/* */
/*                 <h2>{{ item.itemName }}</h2>*/
/* */
/* */
/*                 {% if vendor.companyName %}*/
/* */
/*                     <p class="vendor">by {{ vendor.companyName }}</p>*/
/*                 {% else %}*/
/* */
/*                     <p class="vendor">by {{ vendor.firstName }} {{ vendor.lastName }}</p>*/
/*                 {% endif %}*/
/* */
/* */
/*                 <p class="price">Price: ${{ item.price | number_format(2, '.', ',') }}</p>*/
/* */
/*                 {% if item.quantity > 0 %}*/
/* */
/*                     <p class="in-stock">In Stock</p>*/
/*                     <p>Quantity in stock: {{ item.quantity }}</p>*/
/*                 {% else %}*/
/* */
/*                     <p class="out-of-stock ">Currently Out Of Stock</p>*/
/*                 {% endif %}*/
/* */
/* */
/*                 <h3>Description</h3>*/
/*                 <p class="description">{{ item.description }}</p>*/
/* */
/*             </div>*/
/* */
/* */
/*             {% if item.quantity > 0 %}*/
/* */
/*             {#add to cart#}*/
/*             <div class="col-sm-3 col-xs-8 col-sm-offset-1 col-xs-offset-3">*/
/* */
/*                 <form class="add-to-cart" action="{{ path('add_to_cart', {'addItem': item.id}) }}" method="POST">*/
/* */
/*                     <div class="input-group input-group-sm col-xs-6">*/
/*                         <span class="input-group-addon" id="sizing-addon3">Quantity</span>*/
/*                         {#<input type="text" class="form-control" placeholder="Username" aria-describedby="sizing-addon3">#}*/
/*                         <input type="text" class="form-control" aria-describedby="sizing-addon3" size="20" name="quantity" value="1" min="1" max="{{ item.quantity + 1 }}">*/
/*                     </div>*/
/*                     {#<input type="number" name="quantity" value="1" min="1" max="{{ item.quantity + 1 }}"/>#}*/
/*                     <button class=" btn btn-md btn-success col-xs-6" type="submit">Add to Cart <span class="glyphicon glyphicon-shopping-cart"></span></button>*/
/*                 </form>*/
/* */
/* */
/*             </div>*/
/* */
/*             {% else %}*/
/* */
/*             {#add to car button disabled #}*/
/*             <div class="col-sm-3 col-xs-8 col-sm-offset-1 col-xs-offset-3">*/
/* */
/*                 {#<form class="add-to-cart" action="{{ path('add_to_cart', {'addItem': item.id}) }}" method="POST">#}*/
/*                     <button disabled="disabled" class="btn btn-md btn-danger" type="submit"><span class="glyphicon glyphicon-exclamation-sign"></span> Out of stock</button>*/
/*                 {#</form>#}*/
/* */
/*             </div>*/
/* */
/* */
/*             {% endif %}*/
/* */
/*             {#suggest more from this vendor#}*/
/*             <div class="col-sm-3 col-xs-10 col-sm-offset-1 col-xs-offset-1">*/
/*                 <p role="separator" class="divider"></p>*/
/*                 {% if itemsByThisVendor %}*/
/* */
/*                     {% if vendor.companyName %}*/
/* */
/*                         <p>More items by </p>*/
/*                         <h3 class="suggensted-vendor">{{ vendor.companyName }}</h3>*/
/*                     {% else %}*/
/* */
/*                         <p>More items by </p>*/
/*                         <h3 class="suggensted-vendor">{{ vendor.firstName }} {{ vendor.lastName }}</h3>*/
/*                     {% endif %}*/
/* */
/* */
/*                     <ul class="suggensted-vendor-items col-sm-12">*/
/* */
/*                     {% for vendorItem in itemsByThisVendor %}*/
/* */
/*                         {% if vendorItem.id != item.id %}*/
/* */
/*                         <li class="row"><a href="{{ path('single_item', { 'itemObj' : vendorItem.id }) }}">*/
/* */
/*                             <div class="col-sm-6 col-xs-6">*/
/*                                 <div class="vendorItem-img"*/
/*                                      style="*/
/*                                              background: url( {{ asset('uploads/' ~ vendorItem.itemImg) }} ) no-repeat;*/
/*                                              /*background-repeat: ;*//* */
/*                                              background-size: cover;*/
/*                                              background-position: center;*/
/*                                              background-color: #fff;*/
/*                                              border: 1px solid rgba(0,0,0,0.38);*/
/*                                             ">*/
/* */
/*                                     {% if vendorItem.quantity == 0 %}*/
/* */
/*                                         <p class="out-of-stock out-of-stock-sm-thumbnail ">Currently Out Of Stock</p>*/
/*                                     {% endif %}*/
/* */
/*                                 </div>*/
/*                             </div>*/
/* */
/*                             <div class="suggensted-vendor-item-details col-sm-6 col-xs-6">*/
/*                                 <p>{{ vendorItem.itemName }}</p>*/
/*                                 <p>${{ vendorItem.price | number_format(2, '.', ',') }}</p>*/
/*                                 {#<p>Quantity in stock: {{ item.quantity }}</p>#}*/
/*                             </div>*/
/* */
/* */
/*                             </a></li>*/
/* */
/*                             {% endif %}*/
/* */
/*                     {% endfor %}*/
/* */
/*                     </ul>*/
/*                 {% endif %}*/
/* */
/*             </div>*/
/* */
/*         </div>*/
/* */
/* */
/*     </main>*/
/* */
/*     {#add to cart button#}*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* {% endblock %}*/
/* {% block javascript %}*/
/* */
/* */
/*     {{ parent() }}*/
/* {% endblock %}*/
