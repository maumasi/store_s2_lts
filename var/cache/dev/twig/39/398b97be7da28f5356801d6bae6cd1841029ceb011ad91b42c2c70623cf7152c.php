<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_5b13a110cc5144064f207a9cf2c433960738356f747f4ba23ac348114f513b9f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_550e06d4a7eea4ad31c8ce637242d1a21b099472be982897cf9f6e182e6d03a2 = $this->env->getExtension("native_profiler");
        $__internal_550e06d4a7eea4ad31c8ce637242d1a21b099472be982897cf9f6e182e6d03a2->enter($__internal_550e06d4a7eea4ad31c8ce637242d1a21b099472be982897cf9f6e182e6d03a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_550e06d4a7eea4ad31c8ce637242d1a21b099472be982897cf9f6e182e6d03a2->leave($__internal_550e06d4a7eea4ad31c8ce637242d1a21b099472be982897cf9f6e182e6d03a2_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr>*/
/*     <td>*/
/*         <?php echo $view['form']->label($form) ?>*/
/*     </td>*/
/*     <td>*/
/*         <?php echo $view['form']->errors($form) ?>*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
