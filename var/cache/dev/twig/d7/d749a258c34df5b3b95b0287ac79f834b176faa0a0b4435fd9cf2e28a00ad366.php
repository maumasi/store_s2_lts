<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_e5a1030dafbc1a3a57ac2c7b9b2a84c3f47fbf01b9f7501ba6cd90fc0d345e6c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_113dce2c0ffa8c888f28d00e5079bf6905fc86a0790931e0e71655860f728b36 = $this->env->getExtension("native_profiler");
        $__internal_113dce2c0ffa8c888f28d00e5079bf6905fc86a0790931e0e71655860f728b36->enter($__internal_113dce2c0ffa8c888f28d00e5079bf6905fc86a0790931e0e71655860f728b36_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
        
        $__internal_113dce2c0ffa8c888f28d00e5079bf6905fc86a0790931e0e71655860f728b36->leave($__internal_113dce2c0ffa8c888f28d00e5079bf6905fc86a0790931e0e71655860f728b36_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/* */
