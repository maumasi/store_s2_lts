<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_679728cbe3ebbb4ef7c038a46773eb9de41552581a47719cf8d4fa7c7809ac24 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_07f159ff3fe4e0a64e909093bd030d2c482275e308df95043a117c8bb1efbbc7 = $this->env->getExtension("native_profiler");
        $__internal_07f159ff3fe4e0a64e909093bd030d2c482275e308df95043a117c8bb1efbbc7->enter($__internal_07f159ff3fe4e0a64e909093bd030d2c482275e308df95043a117c8bb1efbbc7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_07f159ff3fe4e0a64e909093bd030d2c482275e308df95043a117c8bb1efbbc7->leave($__internal_07f159ff3fe4e0a64e909093bd030d2c482275e308df95043a117c8bb1efbbc7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'text')) ?> %*/
/* */
