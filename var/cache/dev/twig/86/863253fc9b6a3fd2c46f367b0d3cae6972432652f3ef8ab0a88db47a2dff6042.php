<?php

/* @Framework/Form/range_widget.html.php */
class __TwigTemplate_c2cfb046fabf0a6c0afd5a7118c428154b5c9be9433da8eba862a9e66b87fa13 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_334238c36db5b40f0ef962d8792b83c61514762cbe7c2beb836943c59e6b4ae0 = $this->env->getExtension("native_profiler");
        $__internal_334238c36db5b40f0ef962d8792b83c61514762cbe7c2beb836943c59e6b4ae0->enter($__internal_334238c36db5b40f0ef962d8792b83c61514762cbe7c2beb836943c59e6b4ae0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/range_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'range'));
";
        
        $__internal_334238c36db5b40f0ef962d8792b83c61514762cbe7c2beb836943c59e6b4ae0->leave($__internal_334238c36db5b40f0ef962d8792b83c61514762cbe7c2beb836943c59e6b4ae0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/range_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'range'));*/
/* */
