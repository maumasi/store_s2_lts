<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_bd8429bed2d6521ab09e01848178e5b7a1babb928091a12c5b7f97dd7b32e5ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7b2539465852402c9273241c9c3e1d27122340928baade3903ff1e714e979ba4 = $this->env->getExtension("native_profiler");
        $__internal_7b2539465852402c9273241c9c3e1d27122340928baade3903ff1e714e979ba4->enter($__internal_7b2539465852402c9273241c9c3e1d27122340928baade3903ff1e714e979ba4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.css.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_7b2539465852402c9273241c9c3e1d27122340928baade3903ff1e714e979ba4->leave($__internal_7b2539465852402c9273241c9c3e1d27122340928baade3903ff1e714e979ba4_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
