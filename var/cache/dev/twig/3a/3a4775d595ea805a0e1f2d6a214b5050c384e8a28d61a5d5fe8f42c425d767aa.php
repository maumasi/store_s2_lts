<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_2359ccdd6038c4cdb0e7574eceed5bac1a199c6ffd6a0493130ac9d5381938be extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f14a0d26bd340045c9b6e78d769f27a9fb5bf89427a2b633e3fafd94491da32a = $this->env->getExtension("native_profiler");
        $__internal_f14a0d26bd340045c9b6e78d769f27a9fb5bf89427a2b633e3fafd94491da32a->enter($__internal_f14a0d26bd340045c9b6e78d769f27a9fb5bf89427a2b633e3fafd94491da32a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f14a0d26bd340045c9b6e78d769f27a9fb5bf89427a2b633e3fafd94491da32a->leave($__internal_f14a0d26bd340045c9b6e78d769f27a9fb5bf89427a2b633e3fafd94491da32a_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_d63492d4f03627b9a21a8d3498aa1993872396099738a0bf964626da69ce6ae4 = $this->env->getExtension("native_profiler");
        $__internal_d63492d4f03627b9a21a8d3498aa1993872396099738a0bf964626da69ce6ae4->enter($__internal_d63492d4f03627b9a21a8d3498aa1993872396099738a0bf964626da69ce6ae4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_d63492d4f03627b9a21a8d3498aa1993872396099738a0bf964626da69ce6ae4->leave($__internal_d63492d4f03627b9a21a8d3498aa1993872396099738a0bf964626da69ce6ae4_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_efac3bf1b864a064424adad2b79c6f902226788ee9024891356106d5deb16ce9 = $this->env->getExtension("native_profiler");
        $__internal_efac3bf1b864a064424adad2b79c6f902226788ee9024891356106d5deb16ce9->enter($__internal_efac3bf1b864a064424adad2b79c6f902226788ee9024891356106d5deb16ce9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_efac3bf1b864a064424adad2b79c6f902226788ee9024891356106d5deb16ce9->leave($__internal_efac3bf1b864a064424adad2b79c6f902226788ee9024891356106d5deb16ce9_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block title 'Redirection Intercepted' %}*/
/* */
/* {% block body %}*/
/*     <div class="sf-reset">*/
/*         <div class="block-exception">*/
/*             <h1>This request redirects to <a href="{{ location }}">{{ location }}</a>.</h1>*/
/* */
/*             <p>*/
/*                 <small>*/
/*                     The redirect was intercepted by the web debug toolbar to help debugging.*/
/*                     For more information, see the "intercept-redirects" option of the Profiler.*/
/*                 </small>*/
/*             </p>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
