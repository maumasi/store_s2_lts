<?php

/* vendor/vendorDashboard.html.twig */
class __TwigTemplate_a1ae62494b67336acffb263e94f487ed27c08df881f228fcec380cf28e09439b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "vendor/vendorDashboard.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cb45991b236df3fe9a274e89056813441dee8a6bd2e53daa751a82dc05398d18 = $this->env->getExtension("native_profiler");
        $__internal_cb45991b236df3fe9a274e89056813441dee8a6bd2e53daa751a82dc05398d18->enter($__internal_cb45991b236df3fe9a274e89056813441dee8a6bd2e53daa751a82dc05398d18_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "vendor/vendorDashboard.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cb45991b236df3fe9a274e89056813441dee8a6bd2e53daa751a82dc05398d18->leave($__internal_cb45991b236df3fe9a274e89056813441dee8a6bd2e53daa751a82dc05398d18_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_85c602e62351a35e18ea6b5e6ded46493b4d464f1e25b73a32548b97c1664267 = $this->env->getExtension("native_profiler");
        $__internal_85c602e62351a35e18ea6b5e6ded46493b4d464f1e25b73a32548b97c1664267->enter($__internal_85c602e62351a35e18ea6b5e6ded46493b4d464f1e25b73a32548b97c1664267_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 5
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo "

    - Registration

";
        
        $__internal_85c602e62351a35e18ea6b5e6ded46493b4d464f1e25b73a32548b97c1664267->leave($__internal_85c602e62351a35e18ea6b5e6ded46493b4d464f1e25b73a32548b97c1664267_prof);

    }

    // line 10
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_6e63565375fdb2d5ba868a4c709da28115a1a5ef61058c44270996c56715ab2c = $this->env->getExtension("native_profiler");
        $__internal_6e63565375fdb2d5ba868a4c709da28115a1a5ef61058c44270996c56715ab2c->enter($__internal_6e63565375fdb2d5ba868a4c709da28115a1a5ef61058c44270996c56715ab2c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 11
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

";
        
        $__internal_6e63565375fdb2d5ba868a4c709da28115a1a5ef61058c44270996c56715ab2c->leave($__internal_6e63565375fdb2d5ba868a4c709da28115a1a5ef61058c44270996c56715ab2c_prof);

    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        $__internal_df7c36af3d4ea6776f00d87e94e6f7c73715e6f64fd81c4054194deee2425b54 = $this->env->getExtension("native_profiler");
        $__internal_df7c36af3d4ea6776f00d87e94e6f7c73715e6f64fd81c4054194deee2425b54->enter($__internal_df7c36af3d4ea6776f00d87e94e6f7c73715e6f64fd81c4054194deee2425b54_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 15
        echo "    ";
        $this->displayParentBlock("body", $context, $blocks);
        echo "


<main class=\"dashboard container\">



    <div class=\"row\">
        <h1>Welcome to your Dashboard, ";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "firstName", array()), "html", null, true);
        echo "</h1>

        <a class=\"well well-lg col-md-offset-2 col-md-4\" href=\"";
        // line 25
        echo $this->env->getExtension('routing')->getPath("que_list");
        echo "\">Your Order Que</a>
        <a class=\"well well-lg col-md-offset-1 col-md-4\" href=\"";
        // line 26
        echo $this->env->getExtension('routing')->getPath("vendor_item_collection");
        echo "\">Your Items</a>

        ";
        // line 29
        echo "        ";
        // line 30
        echo "        ";
        // line 31
        echo "        ";
        // line 32
        echo "    </div>




    ";
        // line 38
        echo "
</main>






";
        
        $__internal_df7c36af3d4ea6776f00d87e94e6f7c73715e6f64fd81c4054194deee2425b54->leave($__internal_df7c36af3d4ea6776f00d87e94e6f7c73715e6f64fd81c4054194deee2425b54_prof);

    }

    public function getTemplateName()
    {
        return "vendor/vendorDashboard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 38,  110 => 32,  108 => 31,  106 => 30,  104 => 29,  99 => 26,  95 => 25,  90 => 23,  78 => 15,  72 => 14,  61 => 11,  55 => 10,  42 => 5,  36 => 4,  11 => 2,);
    }
}
/* */
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block title %}*/
/*     {{ parent() }}*/
/* */
/*     - Registration*/
/* */
/* {% endblock %}*/
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/* */
/* {% endblock %}*/
/* {% block body %}*/
/*     {{ parent() }}*/
/* */
/* */
/* <main class="dashboard container">*/
/* */
/* */
/* */
/*     <div class="row">*/
/*         <h1>Welcome to your Dashboard, {{ app.user.firstName }}</h1>*/
/* */
/*         <a class="well well-lg col-md-offset-2 col-md-4" href="{{ path('que_list')}}">Your Order Que</a>*/
/*         <a class="well well-lg col-md-offset-1 col-md-4" href="{{ path('vendor_item_collection')}}">Your Items</a>*/
/* */
/*         {#<div class="well well-lg col-md-offset-2 col-md-4"><a href="{{ path('que_list')}}">Your Order Que</a></div>#}*/
/*         {#<div class="well well-lg col-md-offset-1 col-md-4"><a href="{{ path('vendor_item_collection')}}">Your Items</a></div>#}*/
/*         {#<div class="well well-lg">...</div>#}*/
/*         {#<div class="well well-lg">...</div>#}*/
/*     </div>*/
/* */
/* */
/* */
/* */
/*     {#<a href="{{ path('vendor_edit')}}">edit your profile</a>#}*/
/* */
/* </main>*/
/* */
/* */
/* */
/* */
/* */
/* */
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
