<?php

/* store/_storePadgination.html.twig */
class __TwigTemplate_32eb4b190cdb42fde115d0907d28e434d1ade0c453444b2f57416b359b170f43 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0b248265b69caf179fb132759d642ec8796e544e6909e8308e28516a4c7fddc5 = $this->env->getExtension("native_profiler");
        $__internal_0b248265b69caf179fb132759d642ec8796e544e6909e8308e28516a4c7fddc5->enter($__internal_0b248265b69caf179fb132759d642ec8796e544e6909e8308e28516a4c7fddc5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "store/_storePadgination.html.twig"));

        // line 1
        echo "
<nav class=\"padgination \">


    ";
        // line 5
        if ((twig_round((isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")), 0, "floor") > 4)) {
            // line 6
            echo "
    <ul class=\"pagination pagination-sm\">

        ";
            // line 9
            if (((isset($context["currentPage"]) ? $context["currentPage"] : $this->getContext($context, "currentPage")) != 1)) {
                // line 10
                echo "
            <li class=\"\"><a href=\"";
                // line 11
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("store_page", array("pageNumber" => (((isset($context["currentPage"]) ? $context["currentPage"] : $this->getContext($context, "currentPage")) * 16) - 16))), "html", null, true);
                echo "\" aria-label=\"Previous\"><span aria-hidden=\"true\" class=\"fa fa-angle-double-left\"></span></a></li>

        ";
            } else {
                // line 14
                echo "
            <li class=\"disabled\"><a><span aria-hidden=\"true\" class=\"fa fa-angle-double-left\"></span></a></li>

        ";
            }
            // line 18
            echo "        ";
            // line 19
            echo "





        ";
            // line 25
            if ((((isset($context["currentPage"]) ? $context["currentPage"] : $this->getContext($context, "currentPage")) + 1) > 4)) {
                // line 26
                echo "
            <li class=\"\" >
                <a href=\"";
                // line 28
                echo $this->env->getExtension('routing')->getPath("store_page", array("pageNumber" => 1));
                echo "\">";
                echo 1;
                echo "<span class=\"sr-only\">(current)</span></a>

            </li>

            <li class=\"dot-dot-dot\" ><a>...</a></li>

        ";
            }
            // line 35
            echo "


        ";
            // line 39
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, twig_round((isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")), 0, "floor")));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 40
                echo "

        ";
                // line 42
                if ((((isset($context["currentPage"]) ? $context["currentPage"] : $this->getContext($context, "currentPage")) < ($context["i"] + 2)) && ($context["i"] < ((isset($context["currentPage"]) ? $context["currentPage"] : $this->getContext($context, "currentPage")) + 2)))) {
                    // line 43
                    echo "


            ";
                    // line 47
                    echo "
            ";
                    // line 48
                    if (((isset($context["currentPage"]) ? $context["currentPage"] : $this->getContext($context, "currentPage")) == $context["i"])) {
                        // line 49
                        echo "
                <li class=\"active\" >
                    <a>";
                        // line 51
                        echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                        echo "<span class=\"sr-only\">(current)</span></a>

                </li>


            ";
                    } else {
                        // line 57
                        echo "
                <li class=\"\" >
                    <a href=\"";
                        // line 59
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("store_page", array("pageNumber" => ($context["i"] * 16))), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                        echo "<span class=\"sr-only\">(current)</span></a>
                </li>


            ";
                    }
                    // line 64
                    echo "



        ";
                } else {
                    // line 69
                    echo "

        ";
                }
                // line 72
                echo "



            ";
                // line 77
                echo "
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 79
            echo "
        ";
            // line 80
            if ((((isset($context["currentPage"]) ? $context["currentPage"] : $this->getContext($context, "currentPage")) + 1) < twig_round((isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")), 0, "floor"))) {
                // line 81
                echo "
            <li class=\"dot-dot-dot\" ><a>...</a></li>

            <li class=\"\" >
                ";
                // line 86
                echo "                ";
                // line 87
                echo "                ";
                // line 88
                echo "                <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("store_page", array("pageNumber" => (twig_round((isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")), 0, "floor") * 16))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_round((isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")), 0, "floor"), "html", null, true);
                echo "<span class=\"sr-only\">(current)</span></a>

            </li>


        ";
                // line 94
                echo "
            ";
                // line 96
                echo "                ";
                // line 97
                echo "                ";
                // line 98
                echo "                ";
                // line 99
                echo "                ";
                // line 100
                echo "
            ";
                // line 102
                echo "
            ";
                // line 104
                echo "
        ";
            }
            // line 106
            echo "
        ";
            // line 107
            if (((isset($context["currentPage"]) ? $context["currentPage"] : $this->getContext($context, "currentPage")) != twig_round((isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")), 0, "floor"))) {
                // line 108
                echo "
            <li class=\"\"><a href=\"";
                // line 109
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("store_page", array("pageNumber" => (((isset($context["currentPage"]) ? $context["currentPage"] : $this->getContext($context, "currentPage")) * 16) + 16))), "html", null, true);
                echo "\" aria-label=\"Previous\"><span aria-hidden=\"true\" class=\"fa fa-angle-double-right\"></span></a></li>

        ";
            } else {
                // line 112
                echo "
            <li class=\"disabled\"><a><span aria-hidden=\"true\" class=\"fa fa-angle-double-right\"></span></a></li>

        ";
            }
            // line 116
            echo "    </ul>


    ";
        } else {
            // line 120
            echo "


";
            // line 124
            echo "    <ul class=\"pagination pagination-sm\">

        ";
            // line 126
            if (((isset($context["currentPage"]) ? $context["currentPage"] : $this->getContext($context, "currentPage")) != 1)) {
                // line 127
                echo "
            <li class=\"\"><a href=\"";
                // line 128
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("store_page", array("pageNumber" => (((isset($context["currentPage"]) ? $context["currentPage"] : $this->getContext($context, "currentPage")) * 16) - 16))), "html", null, true);
                echo "\" aria-label=\"Previous\"><span aria-hidden=\"true\" class=\"fa fa-angle-double-left\"></span></a></li>

        ";
            } else {
                // line 131
                echo "
            <li class=\"disabled\"><a><span aria-hidden=\"true\" class=\"fa fa-angle-double-left\"></span></a></li>

        ";
            }
            // line 135
            echo "        ";
            // line 136
            echo "


        ";
            // line 140
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, twig_round((isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")), 0, "floor")));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 141
                echo "
            ";
                // line 142
                if (((isset($context["currentPage"]) ? $context["currentPage"] : $this->getContext($context, "currentPage")) == $context["i"])) {
                    // line 143
                    echo "
                <li class=\"active\" >

                    <a>";
                    // line 146
                    echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                    echo "<span class=\"sr-only\">(current)</span></a>

                </li>


            ";
                } else {
                    // line 152
                    echo "
                <li class=\"\" >

                    <a href=\"";
                    // line 155
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("store_page", array("pageNumber" => ($context["i"] * 16))), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                    echo "<span class=\"sr-only\">(current)</span></a>

                </li>


            ";
                }
                // line 161
                echo "
            ";
                // line 163
                echo "
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 165
            echo "

        ";
            // line 167
            if (((isset($context["currentPage"]) ? $context["currentPage"] : $this->getContext($context, "currentPage")) != twig_round((isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")), 0, "floor"))) {
                // line 168
                echo "
            <li class=\"\"><a href=\"";
                // line 169
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("store_page", array("pageNumber" => (((isset($context["currentPage"]) ? $context["currentPage"] : $this->getContext($context, "currentPage")) * 16) + 16))), "html", null, true);
                echo "\" aria-label=\"Previous\"><span aria-hidden=\"true\" class=\"fa fa-angle-double-right\"></span></a></li>

        ";
            } else {
                // line 172
                echo "
            <li class=\"disabled\"><a><span aria-hidden=\"true\" class=\"fa fa-angle-double-right\"></span></a></li>

        ";
            }
            // line 176
            echo "    </ul>

    ";
        }
        // line 179
        echo "


    ";
        // line 183
        echo "
        ";
        // line 185
        echo "
            ";
        // line 187
        echo "
        ";
        // line 189
        echo "
            ";
        // line 191
        echo "
        ";
        // line 193
        echo "        ";
        // line 194
        echo "


    ";
        // line 198
        echo "        ";
        // line 199
        echo "
            ";
        // line 201
        echo "
                ";
        // line 203
        echo "                    ";
        // line 204
        echo "                    ";
        // line 205
        echo "                    ";
        // line 206
        echo "                    ";
        // line 207
        echo "
                ";
        // line 209
        echo "

            ";
        // line 212
        echo "
                ";
        // line 214
        echo "                    ";
        // line 215
        echo "                    ";
        // line 216
        echo "                    ";
        // line 217
        echo "                    ";
        // line 218
        echo "
                ";
        // line 220
        echo "

            ";
        // line 223
        echo "
            ";
        // line 225
        echo "
        ";
        // line 227
        echo "

        ";
        // line 230
        echo "
            ";
        // line 232
        echo "
        ";
        // line 234
        echo "
            ";
        // line 236
        echo "
        ";
        // line 238
        echo "    ";
        // line 239
        echo "</nav>";
        
        $__internal_0b248265b69caf179fb132759d642ec8796e544e6909e8308e28516a4c7fddc5->leave($__internal_0b248265b69caf179fb132759d642ec8796e544e6909e8308e28516a4c7fddc5_prof);

    }

    public function getTemplateName()
    {
        return "store/_storePadgination.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  432 => 239,  430 => 238,  427 => 236,  424 => 234,  421 => 232,  418 => 230,  414 => 227,  411 => 225,  408 => 223,  404 => 220,  401 => 218,  399 => 217,  397 => 216,  395 => 215,  393 => 214,  390 => 212,  386 => 209,  383 => 207,  381 => 206,  379 => 205,  377 => 204,  375 => 203,  372 => 201,  369 => 199,  367 => 198,  362 => 194,  360 => 193,  357 => 191,  354 => 189,  351 => 187,  348 => 185,  345 => 183,  340 => 179,  335 => 176,  329 => 172,  323 => 169,  320 => 168,  318 => 167,  314 => 165,  307 => 163,  304 => 161,  293 => 155,  288 => 152,  279 => 146,  274 => 143,  272 => 142,  269 => 141,  264 => 140,  259 => 136,  257 => 135,  251 => 131,  245 => 128,  242 => 127,  240 => 126,  236 => 124,  231 => 120,  225 => 116,  219 => 112,  213 => 109,  210 => 108,  208 => 107,  205 => 106,  201 => 104,  198 => 102,  195 => 100,  193 => 99,  191 => 98,  189 => 97,  187 => 96,  184 => 94,  173 => 88,  171 => 87,  169 => 86,  163 => 81,  161 => 80,  158 => 79,  151 => 77,  145 => 72,  140 => 69,  133 => 64,  123 => 59,  119 => 57,  110 => 51,  106 => 49,  104 => 48,  101 => 47,  96 => 43,  94 => 42,  90 => 40,  85 => 39,  80 => 35,  68 => 28,  64 => 26,  62 => 25,  54 => 19,  52 => 18,  46 => 14,  40 => 11,  37 => 10,  35 => 9,  30 => 6,  28 => 5,  22 => 1,);
    }
}
/* */
/* <nav class="padgination ">*/
/* */
/* */
/*     {% if page | round(0,'floor') > 4 %}*/
/* */
/*     <ul class="pagination pagination-sm">*/
/* */
/*         {% if currentPage != 1 %}*/
/* */
/*             <li class=""><a href="{{ path('store_page', {'pageNumber': (currentPage * 16) - 16 }) }}" aria-label="Previous"><span aria-hidden="true" class="fa fa-angle-double-left"></span></a></li>*/
/* */
/*         {% else %}*/
/* */
/*             <li class="disabled"><a><span aria-hidden="true" class="fa fa-angle-double-left"></span></a></li>*/
/* */
/*         {% endif %}*/
/*         {#<li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true" class="fa fa-angle-double-left"></span></a></li>#}*/
/* */
/* */
/* */
/* */
/* */
/* */
/*         {% if (currentPage + 1) > 4 %}*/
/* */
/*             <li class="" >*/
/*                 <a href="{{ path('store_page', {'pageNumber': 1 }) }}">{{ 1 }}<span class="sr-only">(current)</span></a>*/
/* */
/*             </li>*/
/* */
/*             <li class="dot-dot-dot" ><a>...</a></li>*/
/* */
/*         {% endif %}*/
/* */
/* */
/* */
/*         {# loop for padgination #}*/
/*         {% for i in 1..page | round(0,'floor') %}*/
/* */
/* */
/*         {% if currentPage < i + 2 and i < currentPage + 2 %}*/
/* */
/* */
/* */
/*             {#{% set page = i + 4 %}#}*/
/* */
/*             {% if currentPage == i %}*/
/* */
/*                 <li class="active" >*/
/*                     <a>{{ i }}<span class="sr-only">(current)</span></a>*/
/* */
/*                 </li>*/
/* */
/* */
/*             {% else %}*/
/* */
/*                 <li class="" >*/
/*                     <a href="{{ path('store_page', {'pageNumber': ((i) * 16)}) }}">{{ i }}<span class="sr-only">(current)</span></a>*/
/*                 </li>*/
/* */
/* */
/*             {% endif %}*/
/* */
/* */
/* */
/* */
/*         {% else  %}*/
/* */
/* */
/*         {% endif %}*/
/* */
/* */
/* */
/* */
/*             {#<li class="active"><a href="#">2 <span class="sr-only">(current)</span></a></li>#}*/
/* */
/*         {% endfor %}*/
/* */
/*         {% if (currentPage + 1 < (page | round(0,'floor'))) %}*/
/* */
/*             <li class="dot-dot-dot" ><a>...</a></li>*/
/* */
/*             <li class="" >*/
/*                 {#<form>#}*/
/*                 {#<button type="submit" >{{ i }}</button>#}*/
/*                 {#</form>#}*/
/*                 <a href="{{ path('store_page', {'pageNumber': (page | round(0,'floor') * 16)}) }}">{{ page | round(0,'floor') }}<span class="sr-only">(current)</span></a>*/
/* */
/*             </li>*/
/* */
/* */
/*         {#{% if (currentPage + 1 > (page | round(0,'floor'))) %}#}*/
/* */
/*             {#<li class="" >#}*/
/*                 {#<form>#}*/
/*                 {#<button type="submit" >{{ i }}</button>#}*/
/*                 {#</form>#}*/
/*                 {#<a href="{{ path('store_page', {'pageNumber': (page | round(0,'floor') * 16)}) }}">{{ 1 }}<span class="sr-only">(current)</span></a>#}*/
/* */
/*             {#</li>#}*/
/* */
/*             {#<li class="" ><a>...</a></li>#}*/
/* */
/*         {% endif %}*/
/* */
/*         {% if currentPage != page | round(0,'floor') %}*/
/* */
/*             <li class=""><a href="{{ path('store_page', {'pageNumber': (currentPage * 16) + 16 }) }}" aria-label="Previous"><span aria-hidden="true" class="fa fa-angle-double-right"></span></a></li>*/
/* */
/*         {% else %}*/
/* */
/*             <li class="disabled"><a><span aria-hidden="true" class="fa fa-angle-double-right"></span></a></li>*/
/* */
/*         {% endif %}*/
/*     </ul>*/
/* */
/* */
/*     {% else %}*/
/* */
/* */
/* */
/* {# regular padginatoion #}*/
/*     <ul class="pagination pagination-sm">*/
/* */
/*         {% if currentPage != 1 %}*/
/* */
/*             <li class=""><a href="{{ path('store_page', {'pageNumber': (currentPage * 16) - 16 }) }}" aria-label="Previous"><span aria-hidden="true" class="fa fa-angle-double-left"></span></a></li>*/
/* */
/*         {% else %}*/
/* */
/*             <li class="disabled"><a><span aria-hidden="true" class="fa fa-angle-double-left"></span></a></li>*/
/* */
/*         {% endif %}*/
/*         {#<li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true" class="fa fa-angle-double-left"></span></a></li>#}*/
/* */
/* */
/* */
/*         {# loop for padgination #}*/
/*         {% for i in 1..page | round(0,'floor') %}*/
/* */
/*             {% if currentPage == i %}*/
/* */
/*                 <li class="active" >*/
/* */
/*                     <a>{{ i }}<span class="sr-only">(current)</span></a>*/
/* */
/*                 </li>*/
/* */
/* */
/*             {% else %}*/
/* */
/*                 <li class="" >*/
/* */
/*                     <a href="{{ path('store_page', {'pageNumber': ((i) * 16)}) }}">{{ i }}<span class="sr-only">(current)</span></a>*/
/* */
/*                 </li>*/
/* */
/* */
/*             {% endif %}*/
/* */
/*             {#<li class="active"><a href="#">2 <span class="sr-only">(current)</span></a></li>#}*/
/* */
/*         {% endfor %}*/
/* */
/* */
/*         {% if currentPage != page | round(0,'floor') %}*/
/* */
/*             <li class=""><a href="{{ path('store_page', {'pageNumber': (currentPage * 16) + 16 }) }}" aria-label="Previous"><span aria-hidden="true" class="fa fa-angle-double-right"></span></a></li>*/
/* */
/*         {% else %}*/
/* */
/*             <li class="disabled"><a><span aria-hidden="true" class="fa fa-angle-double-right"></span></a></li>*/
/* */
/*         {% endif %}*/
/*     </ul>*/
/* */
/*     {% endif %}*/
/* */
/* */
/* */
/*     {#<ul class="pagination">#}*/
/* */
/*         {#{% if currentPage != 1 %}#}*/
/* */
/*             {#<li class=""><a href="{{ path('store_page', {'pageNumber': (currentPage * 16) - 16 }) }}" aria-label="Previous"><span aria-hidden="true" class="fa fa-angle-double-left"></span></a></li>#}*/
/* */
/*         {#{% else %}#}*/
/* */
/*             {#<li class="disabled"><a><span aria-hidden="true" class="fa fa-angle-double-left"></span></a></li>#}*/
/* */
/*         {#{% endif %}#}*/
/*         {#<li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true" class="fa fa-angle-double-left"></span></a></li>#}*/
/* */
/* */
/* */
/*     {# loop for padgination #}*/
/*         {#{% for i in 1..page | round(0,'floor') %}#}*/
/* */
/*             {#{% if currentPage == i %}#}*/
/* */
/*                 {#<li class="active" >#}*/
/*                     {#<form>#}*/
/*                     {#<button type="submit" >{{ i + 1 }}</button>#}*/
/*                     {#</form>#}*/
/*                     {#<a>{{ i }}<span class="sr-only">(current)</span></a>#}*/
/* */
/*                 {#</li>#}*/
/* */
/* */
/*             {#{% else %}#}*/
/* */
/*                 {#<li class="" >#}*/
/*                     {#<form>#}*/
/*                     {#<button type="submit" >{{ i + 1 }}</button>#}*/
/*                     {#</form>#}*/
/*                     {#<a href="{{ path('store_page', {'pageNumber': ((i) * 16)}) }}">{{ i }}<span class="sr-only">(current)</span></a>#}*/
/* */
/*                 {#</li>#}*/
/* */
/* */
/*             {#{% endif %}#}*/
/* */
/*             {#<li class="active"><a href="#">2 <span class="sr-only">(current)</span></a></li>#}*/
/* */
/*         {#{% endfor %}#}*/
/* */
/* */
/*         {#{% if currentPage != page | round(0,'floor') %}#}*/
/* */
/*             {#<li class=""><a href="{{ path('store_page', {'pageNumber': (currentPage * 16) + 16 }) }}" aria-label="Previous"><span aria-hidden="true" class="fa fa-angle-double-right"></span></a></li>#}*/
/* */
/*         {#{% else %}#}*/
/* */
/*             {#<li class="disabled"><a><span aria-hidden="true" class="fa fa-angle-double-right"></span></a></li>#}*/
/* */
/*         {#{% endif %}#}*/
/*     {#</ul>#}*/
/* </nav>*/
