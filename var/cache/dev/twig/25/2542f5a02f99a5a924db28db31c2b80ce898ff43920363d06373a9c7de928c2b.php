<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_cced95f17be0d077869492ca72ad4d51b83ebada6aeaaca43e20ee65dd118729 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_228f30488eeddaca64755cae79718b15501388269df7b99ed721d1f27c298258 = $this->env->getExtension("native_profiler");
        $__internal_228f30488eeddaca64755cae79718b15501388269df7b99ed721d1f27c298258->enter($__internal_228f30488eeddaca64755cae79718b15501388269df7b99ed721d1f27c298258_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_228f30488eeddaca64755cae79718b15501388269df7b99ed721d1f27c298258->leave($__internal_228f30488eeddaca64755cae79718b15501388269df7b99ed721d1f27c298258_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* </div>*/
/* */
