<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_56f8f612ac499f509fe74c863bb21de4f17d749bc1cd2fc6f961508b80021daa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3eddd4c14343d429c5e263b59f6019748c5c90f8e6422889ef2434295da0dc09 = $this->env->getExtension("native_profiler");
        $__internal_3eddd4c14343d429c5e263b59f6019748c5c90f8e6422889ef2434295da0dc09->enter($__internal_3eddd4c14343d429c5e263b59f6019748c5c90f8e6422889ef2434295da0dc09_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_3eddd4c14343d429c5e263b59f6019748c5c90f8e6422889ef2434295da0dc09->leave($__internal_3eddd4c14343d429c5e263b59f6019748c5c90f8e6422889ef2434295da0dc09_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->start($form) ?>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* <?php echo $view['form']->end($form) ?>*/
/* */
