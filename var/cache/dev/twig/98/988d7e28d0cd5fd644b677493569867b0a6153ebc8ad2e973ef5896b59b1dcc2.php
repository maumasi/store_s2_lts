<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_0083b856b507748ebe5a822fabc105607e7aa967ea322d907c94542a4fdf91e8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2bb15bc2d25e6ae130e220c8de96686c7375f8df4f576f57388b42421f588540 = $this->env->getExtension("native_profiler");
        $__internal_2bb15bc2d25e6ae130e220c8de96686c7375f8df4f576f57388b42421f588540->enter($__internal_2bb15bc2d25e6ae130e220c8de96686c7375f8df4f576f57388b42421f588540_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_2bb15bc2d25e6ae130e220c8de96686c7375f8df4f576f57388b42421f588540->leave($__internal_2bb15bc2d25e6ae130e220c8de96686c7375f8df4f576f57388b42421f588540_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($widget == 'single_text'): ?>*/
/*     <?php echo $view['form']->block($form, 'form_widget_simple'); ?>*/
/* <?php else: ?>*/
/*     <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*         <?php echo $view['form']->widget($form['date']).' '.$view['form']->widget($form['time']) ?>*/
/*     </div>*/
/* <?php endif ?>*/
/* */
