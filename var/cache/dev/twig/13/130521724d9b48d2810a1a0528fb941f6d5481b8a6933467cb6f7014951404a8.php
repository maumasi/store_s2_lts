<?php

/* vendor/vendorEdit.html.twig */
class __TwigTemplate_56137cb282283cb99e2895ca0dea404875c8064fcd48a155fa8689980b7615e1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "vendor/vendorEdit.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascript' => array($this, 'block_javascript'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bacc8a46b20064623e18641355f082d10b0be131eb67a2688f9274e7de2141e2 = $this->env->getExtension("native_profiler");
        $__internal_bacc8a46b20064623e18641355f082d10b0be131eb67a2688f9274e7de2141e2->enter($__internal_bacc8a46b20064623e18641355f082d10b0be131eb67a2688f9274e7de2141e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "vendor/vendorEdit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bacc8a46b20064623e18641355f082d10b0be131eb67a2688f9274e7de2141e2->leave($__internal_bacc8a46b20064623e18641355f082d10b0be131eb67a2688f9274e7de2141e2_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_e23efffa3280c1da7e4f24dd39d8e7cce58a1bddebce9c5fff0d9c7481f96b83 = $this->env->getExtension("native_profiler");
        $__internal_e23efffa3280c1da7e4f24dd39d8e7cce58a1bddebce9c5fff0d9c7481f96b83->enter($__internal_e23efffa3280c1da7e4f24dd39d8e7cce58a1bddebce9c5fff0d9c7481f96b83_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 5
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo "

    - Registration

";
        
        $__internal_e23efffa3280c1da7e4f24dd39d8e7cce58a1bddebce9c5fff0d9c7481f96b83->leave($__internal_e23efffa3280c1da7e4f24dd39d8e7cce58a1bddebce9c5fff0d9c7481f96b83_prof);

    }

    // line 10
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_cbb6dea6e9e7743290d905c2f9ba6f5423429c945483d412b6d732d460afccdf = $this->env->getExtension("native_profiler");
        $__internal_cbb6dea6e9e7743290d905c2f9ba6f5423429c945483d412b6d732d460afccdf->enter($__internal_cbb6dea6e9e7743290d905c2f9ba6f5423429c945483d412b6d732d460afccdf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 11
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

";
        
        $__internal_cbb6dea6e9e7743290d905c2f9ba6f5423429c945483d412b6d732d460afccdf->leave($__internal_cbb6dea6e9e7743290d905c2f9ba6f5423429c945483d412b6d732d460afccdf_prof);

    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        $__internal_8148118e48cb063bccd038f3a620602d3ddc36db4dbd9118ee809718872fabd7 = $this->env->getExtension("native_profiler");
        $__internal_8148118e48cb063bccd038f3a620602d3ddc36db4dbd9118ee809718872fabd7->enter($__internal_8148118e48cb063bccd038f3a620602d3ddc36db4dbd9118ee809718872fabd7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 15
        echo "    ";
        $this->displayParentBlock("body", $context, $blocks);
        echo "


    ";
        // line 19
        echo "    ";
        // line 20
        echo "        ";
        // line 21
        echo "        ";
        // line 22
        echo "    ";
        // line 23
        echo "


    <main class=\"edit-vendor container-fluid\">
        <div class=\"row\">
            <div class=\"col-xs-12\">
                <h1 class=\"col-lg-offset-4 col-md-offset-3 col-sm-offset-3 col-lg-4 col-md-6 col-sm-6\">Edit ";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "firstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "lastName", array()), "html", null, true);
        echo "</h1>



                ";
        // line 33
        echo twig_include($this->env, $context, "vendor/vendorFormTemplates/_form.html.twig", array("submitBtn" => "Edit", "cancelPath" => $this->env->getExtension('routing')->getPath("vendor_dashboard")));
        // line 36
        echo "

            </div>
        </div>
    </main>






";
        
        $__internal_8148118e48cb063bccd038f3a620602d3ddc36db4dbd9118ee809718872fabd7->leave($__internal_8148118e48cb063bccd038f3a620602d3ddc36db4dbd9118ee809718872fabd7_prof);

    }

    // line 48
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_41a703256d64c5bc680645466156c1312c70160a4068aa2dff2a0ec8ee7ebe81 = $this->env->getExtension("native_profiler");
        $__internal_41a703256d64c5bc680645466156c1312c70160a4068aa2dff2a0ec8ee7ebe81->enter($__internal_41a703256d64c5bc680645466156c1312c70160a4068aa2dff2a0ec8ee7ebe81_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        // line 49
        echo "    ";
        $this->displayParentBlock("javascript", $context, $blocks);
        echo "


";
        
        $__internal_41a703256d64c5bc680645466156c1312c70160a4068aa2dff2a0ec8ee7ebe81->leave($__internal_41a703256d64c5bc680645466156c1312c70160a4068aa2dff2a0ec8ee7ebe81_prof);

    }

    public function getTemplateName()
    {
        return "vendor/vendorEdit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 49,  131 => 48,  113 => 36,  111 => 33,  102 => 29,  94 => 23,  92 => 22,  90 => 21,  88 => 20,  86 => 19,  79 => 15,  73 => 14,  62 => 11,  56 => 10,  43 => 5,  37 => 4,  11 => 2,);
    }
}
/* */
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block title %}*/
/*     {{ parent() }}*/
/* */
/*     - Registration*/
/* */
/* {% endblock %}*/
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/* */
/* {% endblock %}*/
/* {% block body %}*/
/*     {{ parent() }}*/
/* */
/* */
/*     {# breadcrumbs #}*/
/*     {#<ol class="breadcrumb">#}*/
/*         {#<li><a href="{{ path('home') }}">Home</a></li>#}*/
/*         {#<li class="active">Registration</li>#}*/
/*     {#</ol>#}*/
/* */
/* */
/* */
/*     <main class="edit-vendor container-fluid">*/
/*         <div class="row">*/
/*             <div class="col-xs-12">*/
/*                 <h1 class="col-lg-offset-4 col-md-offset-3 col-sm-offset-3 col-lg-4 col-md-6 col-sm-6">Edit {{ app.user.firstName }} {{ app.user.lastName }}</h1>*/
/* */
/* */
/* */
/*                 {{ include('vendor/vendorFormTemplates/_form.html.twig',{*/
/*                     'submitBtn' : 'Edit',*/
/*                     'cancelPath' : path('vendor_dashboard')*/
/*                 }) }}*/
/* */
/*             </div>*/
/*         </div>*/
/*     </main>*/
/* */
/* */
/* */
/* */
/* */
/* */
/* {% endblock %}*/
/* {% block javascript %}*/
/*     {{ parent() }}*/
/* */
/* */
/* {% endblock %}*/
/* */
/* */
