<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_27a9de066715d76b30d84c4561edc1a05d5696143c0216787880f76591c3d55a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2450ecf6158480d8dd6d1b1c89b166d03a498c46d5b96b71ba07388eb7edce2c = $this->env->getExtension("native_profiler");
        $__internal_2450ecf6158480d8dd6d1b1c89b166d03a498c46d5b96b71ba07388eb7edce2c->enter($__internal_2450ecf6158480d8dd6d1b1c89b166d03a498c46d5b96b71ba07388eb7edce2c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
";
        
        $__internal_2450ecf6158480d8dd6d1b1c89b166d03a498c46d5b96b71ba07388eb7edce2c->leave($__internal_2450ecf6158480d8dd6d1b1c89b166d03a498c46d5b96b71ba07388eb7edce2c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <table <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*     <?php if (!$form->parent && $errors): ?>*/
/*     <tr>*/
/*         <td colspan="2">*/
/*             <?php echo $view['form']->errors($form) ?>*/
/*         </td>*/
/*     </tr>*/
/*     <?php endif ?>*/
/*     <?php echo $view['form']->block($form, 'form_rows') ?>*/
/*     <?php echo $view['form']->rest($form) ?>*/
/* </table>*/
/* */
