<?php

/* @Framework/Form/button_widget.html.php */
class __TwigTemplate_f959e3850dc56a827123345e58dc7720f201ebf3da0db11ed076b806ffb04165 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e35145a75b8d517a9655c57526bf266c4a27a797d178afaef2f4bb5a0792da16 = $this->env->getExtension("native_profiler");
        $__internal_e35145a75b8d517a9655c57526bf266c4a27a797d178afaef2f4bb5a0792da16->enter($__internal_e35145a75b8d517a9655c57526bf266c4a27a797d178afaef2f4bb5a0792da16_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_widget.html.php"));

        // line 1
        echo "<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<button type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'button' ?>\" <?php echo \$view['form']->block(\$form, 'button_attributes') ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></button>
";
        
        $__internal_e35145a75b8d517a9655c57526bf266c4a27a797d178afaef2f4bb5a0792da16->leave($__internal_e35145a75b8d517a9655c57526bf266c4a27a797d178afaef2f4bb5a0792da16_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (!$label) { $label = isset($label_format)*/
/*     ? strtr($label_format, array('%name%' => $name, '%id%' => $id))*/
/*     : $view['form']->humanize($name); } ?>*/
/* <button type="<?php echo isset($type) ? $view->escape($type) : 'button' ?>" <?php echo $view['form']->block($form, 'button_attributes') ?>><?php echo $view->escape(false !== $translation_domain ? $view['translator']->trans($label, array(), $translation_domain) : $label) ?></button>*/
/* */
