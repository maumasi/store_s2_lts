<?php

/* store/_customCheckoutForm.html.twig */
class __TwigTemplate_e76b66bc6b8c8203ad7fa6d3419dfa5f37ee60ed92d24c7ba5976b2634aec076 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3c7475568891b55686e3536f057805585fd9d3fbf4a532ee4fc6fcbfaa8efdc4 = $this->env->getExtension("native_profiler");
        $__internal_3c7475568891b55686e3536f057805585fd9d3fbf4a532ee4fc6fcbfaa8efdc4->enter($__internal_3c7475568891b55686e3536f057805585fd9d3fbf4a532ee4fc6fcbfaa8efdc4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "store/_customCheckoutForm.html.twig"));

        // line 1
        echo "
<form action=\"\" method=\"POST\" id=\"payment-form\">

    <div class=\" payment-errors-wrapper input-group input-group-sm alert-danger alert col-xs-11 hidden\">
        <p class=\"payment-errors \"></p>
    </div>



";
        // line 11
        echo "    <div class=\"input-group input-group-sm\">
        <span class=\"input-group-addon\" id=\"sizing-addon3\"><span class=\"fa fa-user\"></span> Full name</span>
        ";
        // line 14
        echo "        <input type=\"text\" class=\"form-control\" aria-describedby=\"sizing-addon3\" size=\"20\" data-stripe=\"name\" placeholder=\"Full name\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "firstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "lastName", array()), "html", null, true);
        echo "\">
    </div>


    <div class=\"input-group input-group-sm\">
        <span class=\"input-group-addon\" id=\"sizing-addon3\">Street address</span>
        ";
        // line 21
        echo "        <input type=\"text\" class=\"form-control\" aria-describedby=\"sizing-addon3\" size=\"20\" data-stripe=\"address_line1\" placeholder=\"Street address\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "addressLine1", array()), "html", null, true);
        echo "\">
    </div>


    <div class=\"input-group input-group-sm\">
        <span class=\"input-group-addon\" id=\"sizing-addon3\">Suite / Apt #</span>
        ";
        // line 28
        echo "        <input type=\"text\" class=\"form-control\" aria-describedby=\"sizing-addon3\" size=\"20\" data-stripe=\"address_line2\" placeholder=\"Suite / Apt #\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "addressLine2", array()), "html", null, true);
        echo "\">
    </div>


    <div class=\"input-group input-group-sm\">
        <span class=\"input-group-addon\" id=\"sizing-addon3\">City</span>
        <input type=\"text\" class=\"form-control \" aria-describedby=\"sizing-addon3\" size=\"20\" data-stripe=\"address_city\" maxlength=\"2\" placeholder=\"City\" value=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "city", array()), "html", null, true);
        echo "\">

        <span class=\"input-group-addon\" id=\"sizing-addon3\">State</span>
        <input type=\"text\" class=\"form-control\" aria-describedby=\"sizing-addon3\" size=\"20\" data-stripe=\"address_state\" maxlength=\"2\" placeholder=\"State\" value=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "state", array()), "html", null, true);
        echo "\">

        <span class=\"input-group-addon\" id=\"sizing-addon3\">Zip</span>
        <input type=\"text\" class=\"form-control\" aria-describedby=\"sizing-addon3\" size=\"20\" data-stripe=\"address_zip\" maxlength=\"2\" placeholder=\"Zip code\" value=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "zip", array()), "html", null, true);
        echo "\">

    </div>



    ";
        // line 47
        echo "    <input type=\"text\" class=\"hidden\" data-stripe=\"address_country\" value=\"US\">
    <input type=\"text\" class=\"hidden\" data-stripe=\"country\" value=\"US\">



";
        // line 53
        echo "    <div class=\"input-group input-group-sm\">
        <span class=\"input-group-addon\" id=\"sizing-addon3\"><span class=\"fa fa-credit-card\"></span></span>
        ";
        // line 56
        echo "        <input type=\"text\" class=\"form-control\" aria-describedby=\"sizing-addon3\" size=\"20\" data-stripe=\"number\" maxlength=\"16\" placeholder=\"Card Number\">
    </div>




    <div class=\"input-group input-group-sm\">
        <span class=\"input-group-addon\" id=\"sizing-addon3\">Exp Month</span>
        <input type=\"text\" class=\"form-control \" aria-describedby=\"sizing-addon3\" size=\"20\" data-stripe=\"exp_month\" maxlength=\"2\" placeholder=\"MM\">

        <span class=\"input-group-addon\" id=\"sizing-addon3\">Exp Year</span>
        <input type=\"text\" class=\"form-control\" aria-describedby=\"sizing-addon3\" size=\"20\" data-stripe=\"exp_year\" maxlength=\"2\" placeholder=\"YY\">

        <span class=\"input-group-addon\" id=\"sizing-addon3\">CVC</span>
        <input type=\"text\" class=\"form-control\" aria-describedby=\"sizing-addon3\" size=\"20\"  data-stripe=\"cvc\" maxlength=\"3\" placeholder=\"CVC\">
    </div>



    ";
        // line 76
        echo "        ";
        // line 77
        echo "        ";
        // line 78
        echo "        ";
        // line 79
        echo "    ";
        // line 80
        echo "

    ";
        // line 82
        if ((twig_length_filter($this->env, (isset($context["cartCount"]) ? $context["cartCount"] : $this->getContext($context, "cartCount"))) == 0)) {
            // line 83
            echo "
    <div class=\" send-payment input-group input-group-sm col-xs-11\">
        <input type=\"submit\" disabled=\"disabled\" class=\"submit btn btn-cancel col-xs-12\" value=\"No items in cart\">
    </div>
    ";
        } else {
            // line 88
            echo "
    <div class=\" send-payment input-group input-group-sm col-xs-11\">
        <input type=\"submit\" class=\"submit btn btn-success col-xs-12\" value=\"Submit Payment\">
    </div>
    ";
        }
        // line 93
        echo "
</form>";
        
        $__internal_3c7475568891b55686e3536f057805585fd9d3fbf4a532ee4fc6fcbfaa8efdc4->leave($__internal_3c7475568891b55686e3536f057805585fd9d3fbf4a532ee4fc6fcbfaa8efdc4_prof);

    }

    public function getTemplateName()
    {
        return "store/_customCheckoutForm.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 93,  143 => 88,  136 => 83,  134 => 82,  130 => 80,  128 => 79,  126 => 78,  124 => 77,  122 => 76,  101 => 56,  97 => 53,  90 => 47,  81 => 40,  75 => 37,  69 => 34,  59 => 28,  49 => 21,  37 => 14,  33 => 11,  22 => 1,);
    }
}
/* */
/* <form action="" method="POST" id="payment-form">*/
/* */
/*     <div class=" payment-errors-wrapper input-group input-group-sm alert-danger alert col-xs-11 hidden">*/
/*         <p class="payment-errors "></p>*/
/*     </div>*/
/* */
/* */
/* */
/* {#customer info#}*/
/*     <div class="input-group input-group-sm">*/
/*         <span class="input-group-addon" id="sizing-addon3"><span class="fa fa-user"></span> Full name</span>*/
/*         {#<input type="text" class="form-control" placeholder="Username" aria-describedby="sizing-addon3">#}*/
/*         <input type="text" class="form-control" aria-describedby="sizing-addon3" size="20" data-stripe="name" placeholder="Full name" value="{{ app.user.firstName }} {{ app.user.lastName }}">*/
/*     </div>*/
/* */
/* */
/*     <div class="input-group input-group-sm">*/
/*         <span class="input-group-addon" id="sizing-addon3">Street address</span>*/
/*         {#<input type="text" class="form-control" placeholder="Username" aria-describedby="sizing-addon3">#}*/
/*         <input type="text" class="form-control" aria-describedby="sizing-addon3" size="20" data-stripe="address_line1" placeholder="Street address" value="{{ app.user.addressLine1 }}">*/
/*     </div>*/
/* */
/* */
/*     <div class="input-group input-group-sm">*/
/*         <span class="input-group-addon" id="sizing-addon3">Suite / Apt #</span>*/
/*         {#<input type="text" class="form-control" placeholder="Username" aria-describedby="sizing-addon3">#}*/
/*         <input type="text" class="form-control" aria-describedby="sizing-addon3" size="20" data-stripe="address_line2" placeholder="Suite / Apt #" value="{{ app.user.addressLine2 }}">*/
/*     </div>*/
/* */
/* */
/*     <div class="input-group input-group-sm">*/
/*         <span class="input-group-addon" id="sizing-addon3">City</span>*/
/*         <input type="text" class="form-control " aria-describedby="sizing-addon3" size="20" data-stripe="address_city" maxlength="2" placeholder="City" value="{{ app.user.city }}">*/
/* */
/*         <span class="input-group-addon" id="sizing-addon3">State</span>*/
/*         <input type="text" class="form-control" aria-describedby="sizing-addon3" size="20" data-stripe="address_state" maxlength="2" placeholder="State" value="{{ app.user.state }}">*/
/* */
/*         <span class="input-group-addon" id="sizing-addon3">Zip</span>*/
/*         <input type="text" class="form-control" aria-describedby="sizing-addon3" size="20" data-stripe="address_zip" maxlength="2" placeholder="Zip code" value="{{ app.user.zip }}">*/
/* */
/*     </div>*/
/* */
/* */
/* */
/*     {#only customers in the USA at this time#}*/
/*     <input type="text" class="hidden" data-stripe="address_country" value="US">*/
/*     <input type="text" class="hidden" data-stripe="country" value="US">*/
/* */
/* */
/* */
/* {#payment info#}*/
/*     <div class="input-group input-group-sm">*/
/*         <span class="input-group-addon" id="sizing-addon3"><span class="fa fa-credit-card"></span></span>*/
/*         {#<input type="text" class="form-control" placeholder="Username" aria-describedby="sizing-addon3">#}*/
/*         <input type="text" class="form-control" aria-describedby="sizing-addon3" size="20" data-stripe="number" maxlength="16" placeholder="Card Number">*/
/*     </div>*/
/* */
/* */
/* */
/* */
/*     <div class="input-group input-group-sm">*/
/*         <span class="input-group-addon" id="sizing-addon3">Exp Month</span>*/
/*         <input type="text" class="form-control " aria-describedby="sizing-addon3" size="20" data-stripe="exp_month" maxlength="2" placeholder="MM">*/
/* */
/*         <span class="input-group-addon" id="sizing-addon3">Exp Year</span>*/
/*         <input type="text" class="form-control" aria-describedby="sizing-addon3" size="20" data-stripe="exp_year" maxlength="2" placeholder="YY">*/
/* */
/*         <span class="input-group-addon" id="sizing-addon3">CVC</span>*/
/*         <input type="text" class="form-control" aria-describedby="sizing-addon3" size="20"  data-stripe="cvc" maxlength="3" placeholder="CVC">*/
/*     </div>*/
/* */
/* */
/* */
/*     {#<div class="input-group input-group-sm col-sm-6">#}*/
/*         {#<span class="input-group-addon" id="sizing-addon3">CVC</span>#}*/
/*         {#<input type="text" class="form-control" aria-describedby="sizing-addon3" size="20"  data-stripe="cvc" maxlength="3" placeholder="CVC">#}*/
/*         {#<input type="text" class="form-control" aria-describedby="sizing-addon3" size="20" data-stripe="number" maxlength="16" placeholder="Card Number">#}*/
/*     {#</div>#}*/
/* */
/* */
/*     {% if cartCount | length == 0 %}*/
/* */
/*     <div class=" send-payment input-group input-group-sm col-xs-11">*/
/*         <input type="submit" disabled="disabled" class="submit btn btn-cancel col-xs-12" value="No items in cart">*/
/*     </div>*/
/*     {% else %}*/
/* */
/*     <div class=" send-payment input-group input-group-sm col-xs-11">*/
/*         <input type="submit" class="submit btn btn-success col-xs-12" value="Submit Payment">*/
/*     </div>*/
/*     {% endif %}*/
/* */
/* </form>*/
