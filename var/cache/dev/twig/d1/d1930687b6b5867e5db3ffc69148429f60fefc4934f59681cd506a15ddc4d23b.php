<?php

/* vendor/vendorSignup.html.twig */
class __TwigTemplate_a302f0f6323713c52caf391882961a8456eedde978cdd475e404ba52c41a3999 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "vendor/vendorSignup.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascript' => array($this, 'block_javascript'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f14967af98a2f6dbfa19e9f08dccd9979a0d43d548a5e3e8d9d58f087d216047 = $this->env->getExtension("native_profiler");
        $__internal_f14967af98a2f6dbfa19e9f08dccd9979a0d43d548a5e3e8d9d58f087d216047->enter($__internal_f14967af98a2f6dbfa19e9f08dccd9979a0d43d548a5e3e8d9d58f087d216047_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "vendor/vendorSignup.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f14967af98a2f6dbfa19e9f08dccd9979a0d43d548a5e3e8d9d58f087d216047->leave($__internal_f14967af98a2f6dbfa19e9f08dccd9979a0d43d548a5e3e8d9d58f087d216047_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_596c407f3a4817189e2a326777cf26aba7e10c77812f9a512692ac3d99a30441 = $this->env->getExtension("native_profiler");
        $__internal_596c407f3a4817189e2a326777cf26aba7e10c77812f9a512692ac3d99a30441->enter($__internal_596c407f3a4817189e2a326777cf26aba7e10c77812f9a512692ac3d99a30441_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 5
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo "

    - Registration

";
        
        $__internal_596c407f3a4817189e2a326777cf26aba7e10c77812f9a512692ac3d99a30441->leave($__internal_596c407f3a4817189e2a326777cf26aba7e10c77812f9a512692ac3d99a30441_prof);

    }

    // line 10
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_fd063d0c154dd9dd19a2a444bf17e85aab2bf06592378950913356771352ddfa = $this->env->getExtension("native_profiler");
        $__internal_fd063d0c154dd9dd19a2a444bf17e85aab2bf06592378950913356771352ddfa->enter($__internal_fd063d0c154dd9dd19a2a444bf17e85aab2bf06592378950913356771352ddfa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 11
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

";
        
        $__internal_fd063d0c154dd9dd19a2a444bf17e85aab2bf06592378950913356771352ddfa->leave($__internal_fd063d0c154dd9dd19a2a444bf17e85aab2bf06592378950913356771352ddfa_prof);

    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        $__internal_59945bdcd76617a8f1bc6362d8d0bf8c1ab412624241b8641d78649d2c6cce32 = $this->env->getExtension("native_profiler");
        $__internal_59945bdcd76617a8f1bc6362d8d0bf8c1ab412624241b8641d78649d2c6cce32->enter($__internal_59945bdcd76617a8f1bc6362d8d0bf8c1ab412624241b8641d78649d2c6cce32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 15
        echo "    ";
        $this->displayParentBlock("body", $context, $blocks);
        echo "


    ";
        // line 19
        echo "    <ol class=\"breadcrumb\">
        <li><a href=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("store");
        echo "\">Home</a></li>
        <li class=\"active\">Registration</li>
    </ol>



    <main class=\"container-fluid vendor-sign-up\">
        <div class=\"row\">
            <div class=\"col-xs-12\">
                <h1 class=\"col-lg-offset-4 col-md-offset-3 col-sm-offset-3 col-lg-4 col-md-6 col-sm-6\">Vendor Registration</h1>



                ";
        // line 33
        echo twig_include($this->env, $context, "vendor/vendorFormTemplates/_form.html.twig", array("submitBtn" => "Register", "cancelPath" => $this->env->getExtension('routing')->getPath("store")));
        // line 36
        echo "


                ";
        // line 40
        echo "                    ";
        // line 41
        echo "                    ";
        // line 42
        echo "                ";
        // line 43
        echo "            </div>
        </div>
    </main>






";
        
        $__internal_59945bdcd76617a8f1bc6362d8d0bf8c1ab412624241b8641d78649d2c6cce32->leave($__internal_59945bdcd76617a8f1bc6362d8d0bf8c1ab412624241b8641d78649d2c6cce32_prof);

    }

    // line 53
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_1bb71bb54f27715c54d5f264810a5fe9ae2a638a73b6f7e314b2020768d381e4 = $this->env->getExtension("native_profiler");
        $__internal_1bb71bb54f27715c54d5f264810a5fe9ae2a638a73b6f7e314b2020768d381e4->enter($__internal_1bb71bb54f27715c54d5f264810a5fe9ae2a638a73b6f7e314b2020768d381e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        // line 54
        echo "    ";
        $this->displayParentBlock("javascript", $context, $blocks);
        echo "


";
        
        $__internal_1bb71bb54f27715c54d5f264810a5fe9ae2a638a73b6f7e314b2020768d381e4->leave($__internal_1bb71bb54f27715c54d5f264810a5fe9ae2a638a73b6f7e314b2020768d381e4_prof);

    }

    public function getTemplateName()
    {
        return "vendor/vendorSignup.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  140 => 54,  134 => 53,  118 => 43,  116 => 42,  114 => 41,  112 => 40,  107 => 36,  105 => 33,  89 => 20,  86 => 19,  79 => 15,  73 => 14,  62 => 11,  56 => 10,  43 => 5,  37 => 4,  11 => 2,);
    }
}
/* */
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block title %}*/
/*     {{ parent() }}*/
/* */
/*     - Registration*/
/* */
/* {% endblock %}*/
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/* */
/* {% endblock %}*/
/* {% block body %}*/
/*     {{ parent() }}*/
/* */
/* */
/*     {# breadcrumbs #}*/
/*     <ol class="breadcrumb">*/
/*         <li><a href="{{ path('store') }}">Home</a></li>*/
/*         <li class="active">Registration</li>*/
/*     </ol>*/
/* */
/* */
/* */
/*     <main class="container-fluid vendor-sign-up">*/
/*         <div class="row">*/
/*             <div class="col-xs-12">*/
/*                 <h1 class="col-lg-offset-4 col-md-offset-3 col-sm-offset-3 col-lg-4 col-md-6 col-sm-6">Vendor Registration</h1>*/
/* */
/* */
/* */
/*                 {{ include('vendor/vendorFormTemplates/_form.html.twig',{*/
/*                     'submitBtn' : 'Register',*/
/*                     'cancelPath' : path('store')*/
/*                 }) }}*/
/* */
/* */
/*                 {#{{ form_start(registrationForm) }}#}*/
/*                     {#{{ form_widget(registrationForm) }}#}*/
/*                     {#<button type="submit" class="btn btn-primary" formnovalidate >Register</button>#}*/
/*                 {#{{ form_end(registrationForm) }}#}*/
/*             </div>*/
/*         </div>*/
/*     </main>*/
/* */
/* */
/* */
/* */
/* */
/* */
/* {% endblock %}*/
/* {% block javascript %}*/
/*     {{ parent() }}*/
/* */
/* */
/* {% endblock %}*/
/* */
/* */
