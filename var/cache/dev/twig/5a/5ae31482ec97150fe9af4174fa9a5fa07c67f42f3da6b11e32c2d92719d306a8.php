<?php

/* vendor/vendorFormTemplates/_form.html.twig */
class __TwigTemplate_107edacf4abf741d6d07aacbbcdbd8575bcf2d06c6d14df7d7305e3615ecf910 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_93f19b3dba7fc315ed3c4215437cc92de813311c48f9bffd4dcd5066f28bbd0d = $this->env->getExtension("native_profiler");
        $__internal_93f19b3dba7fc315ed3c4215437cc92de813311c48f9bffd4dcd5066f28bbd0d->enter($__internal_93f19b3dba7fc315ed3c4215437cc92de813311c48f9bffd4dcd5066f28bbd0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "vendor/vendorFormTemplates/_form.html.twig"));

        // line 1
        echo "


";
        // line 4
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), 'form_start', array("attr" => array("class" => "col-lg-offset-4 col-md-offset-3 col-sm-offset-3 col-lg-4 col-md-6 col-sm-6")));
        // line 8
        echo "

    ";
        // line 11
        echo "
<div class=\"col-md-6 col-sm-6\">
    ";
        // line 13
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "firstName", array()), 'row', array("attr" => array("placeholder" => "First Name"), "label_attr" => array("class" => "sr-only")));
        // line 20
        echo "
</div>



<div class=\"col-md-6 col-sm-6\">
    ";
        // line 26
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "lastName", array()), 'row', array("attr" => array("placeholder" => "Last Name"), "label_attr" => array("class" => "sr-only")));
        // line 33
        echo "
</div>




<div class=\"col-md-6 col-sm-6\">
    ";
        // line 40
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "companyName", array()), 'row', array("attr" => array("placeholder" => "Company Name"), "label_attr" => array("class" => "sr-only")));
        // line 47
        echo "
</div>



<div class=\"col-md-6 col-sm-6\">
    ";
        // line 53
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "phone", array()), 'row', array("attr" => array("placeholder" => "Phone No."), "label_attr" => array("class" => "sr-only")));
        // line 60
        echo "
</div>



<div class=\"col-md-6 col-sm-6\">
    ";
        // line 66
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "email", array()), 'row', array("attr" => array("placeholder" => "Email"), "label_attr" => array("class" => "sr-only")));
        // line 73
        echo "
</div>




<div class=\"col-sm-6 \">
    ";
        // line 80
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "addressLine1", array()), 'row', array("attr" => array("placeholder" => "Street Address"), "label_attr" => array("class" => "sr-only")));
        // line 87
        echo "
</div>



<div class=\"col-sm-6 \">
    ";
        // line 93
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "addressLine2", array()), 'row', array("attr" => array("placeholder" => "Suite / Apt"), "label_attr" => array("class" => "sr-only")));
        // line 100
        echo "
</div>




<div class=\"col-sm-6 \">
    ";
        // line 107
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "city", array()), 'row', array("attr" => array("placeholder" => "City"), "label_attr" => array("class" => "sr-only")));
        // line 114
        echo "
</div>




<div class=\"col-sm-6 \">
    ";
        // line 121
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "state", array()), 'row', array("attr" => array("placeholder" => "State"), "label_attr" => array("class" => "sr-only")));
        // line 128
        echo "
</div>



<div class=\"col-sm-6 \">
    ";
        // line 134
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "zip", array()), 'row', array("attr" => array("placeholder" => "Zip"), "label_attr" => array("class" => "sr-only")));
        // line 141
        echo "
</div>



<div class=\"col-md-6 col-sm-6\">
    ";
        // line 147
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "nakedPassword", array()), "first", array()), 'row', array("attr" => array("placeholder" => "New Password"), "label_attr" => array("class" => "sr-only")));
        // line 154
        echo "
</div>



<div class=\"col-md-6 col-sm-6\">
    ";
        // line 160
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "nakedPassword", array()), "second", array()), 'row', array("attr" => array("placeholder" => "Confirm Password"), "label_attr" => array("class" => "sr-only")));
        // line 167
        echo "
</div>





<div class=\"col-sm-6 \">

</div>


<div class=\"col-sm-3 col-xs-3 \">
    <button type=\"submit\" class=\"btn btn-primary\" formnovalidate >";
        // line 180
        echo twig_escape_filter($this->env, (isset($context["submitBtn"]) ? $context["submitBtn"] : $this->getContext($context, "submitBtn")), "html", null, true);
        echo "</button>

</div>


";
        // line 186
        echo "<a class=\"btn btn-cancel\" href=\"";
        echo twig_escape_filter($this->env, (isset($context["cancelPath"]) ? $context["cancelPath"] : $this->getContext($context, "cancelPath")), "html", null, true);
        echo "\">Cancel</a>


";
        // line 189
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), 'form_end');
        echo "




















";
        // line 211
        echo "
";
        // line 213
        echo "
";
        // line 220
        echo "

";
        // line 224
        echo "

";
        
        $__internal_93f19b3dba7fc315ed3c4215437cc92de813311c48f9bffd4dcd5066f28bbd0d->leave($__internal_93f19b3dba7fc315ed3c4215437cc92de813311c48f9bffd4dcd5066f28bbd0d_prof);

    }

    public function getTemplateName()
    {
        return "vendor/vendorFormTemplates/_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  217 => 224,  213 => 220,  210 => 213,  207 => 211,  183 => 189,  176 => 186,  168 => 180,  153 => 167,  151 => 160,  143 => 154,  141 => 147,  133 => 141,  131 => 134,  123 => 128,  121 => 121,  112 => 114,  110 => 107,  101 => 100,  99 => 93,  91 => 87,  89 => 80,  80 => 73,  78 => 66,  70 => 60,  68 => 53,  60 => 47,  58 => 40,  49 => 33,  47 => 26,  39 => 20,  37 => 13,  33 => 11,  29 => 8,  27 => 4,  22 => 1,);
    }
}
/* */
/* */
/* */
/* {{ form_start(registrationForm,{*/
/*     'attr': {*/
/*         'class': 'col-lg-offset-4 col-md-offset-3 col-sm-offset-3 col-lg-4 col-md-6 col-sm-6'*/
/*     }*/
/* }) }}*/
/* */
/*     {#{{ form_widget(registrationForm) }}#}*/
/* */
/* <div class="col-md-6 col-sm-6">*/
/*     {{ form_row(registrationForm.firstName,{*/
/*         'attr': {*/
/*             'placeholder': 'First Name',*/
/*         },*/
/*         'label_attr': {*/
/*             'class': 'sr-only'*/
/*         }*/
/*     }) }}*/
/* </div>*/
/* */
/* */
/* */
/* <div class="col-md-6 col-sm-6">*/
/*     {{ form_row(registrationForm.lastName,{*/
/*         'attr': {*/
/*             'placeholder': 'Last Name',*/
/*         },*/
/*         'label_attr': {*/
/*             'class': 'sr-only'*/
/*         }*/
/*     }) }}*/
/* </div>*/
/* */
/* */
/* */
/* */
/* <div class="col-md-6 col-sm-6">*/
/*     {{ form_row(registrationForm.companyName,{*/
/*         'attr': {*/
/*             'placeholder': 'Company Name',*/
/*         },*/
/*         'label_attr': {*/
/*             'class': 'sr-only'*/
/*         }*/
/*     }) }}*/
/* </div>*/
/* */
/* */
/* */
/* <div class="col-md-6 col-sm-6">*/
/*     {{ form_row(registrationForm.phone,{*/
/*         'attr': {*/
/*             'placeholder': 'Phone No.',*/
/*         },*/
/*         'label_attr': {*/
/*             'class': 'sr-only'*/
/*         }*/
/*     }) }}*/
/* </div>*/
/* */
/* */
/* */
/* <div class="col-md-6 col-sm-6">*/
/*     {{ form_row(registrationForm.email,{*/
/*         'attr': {*/
/*             'placeholder': 'Email',*/
/*         },*/
/*         'label_attr': {*/
/*             'class': 'sr-only'*/
/*         }*/
/*     }) }}*/
/* </div>*/
/* */
/* */
/* */
/* */
/* <div class="col-sm-6 ">*/
/*     {{ form_row(registrationForm.addressLine1,{*/
/*         'attr': {*/
/*             'placeholder': 'Street Address',*/
/*         },*/
/*         'label_attr': {*/
/*             'class': 'sr-only'*/
/*         }*/
/*     }) }}*/
/* </div>*/
/* */
/* */
/* */
/* <div class="col-sm-6 ">*/
/*     {{ form_row(registrationForm.addressLine2,{*/
/*         'attr': {*/
/*             'placeholder': 'Suite / Apt',*/
/*         },*/
/*         'label_attr': {*/
/*             'class': 'sr-only'*/
/*         }*/
/*     }) }}*/
/* </div>*/
/* */
/* */
/* */
/* */
/* <div class="col-sm-6 ">*/
/*     {{ form_row(registrationForm.city,{*/
/*         'attr': {*/
/*             'placeholder': 'City',*/
/*         },*/
/*         'label_attr': {*/
/*             'class': 'sr-only'*/
/*         }*/
/*     }) }}*/
/* </div>*/
/* */
/* */
/* */
/* */
/* <div class="col-sm-6 ">*/
/*     {{ form_row(registrationForm.state,{*/
/*         'attr': {*/
/*             'placeholder': 'State',*/
/*         },*/
/*         'label_attr': {*/
/*             'class': 'sr-only'*/
/*         }*/
/*     }) }}*/
/* </div>*/
/* */
/* */
/* */
/* <div class="col-sm-6 ">*/
/*     {{ form_row(registrationForm.zip,{*/
/*         'attr': {*/
/*             'placeholder': 'Zip',*/
/*         },*/
/*         'label_attr': {*/
/*             'class': 'sr-only'*/
/*         }*/
/*     }) }}*/
/* </div>*/
/* */
/* */
/* */
/* <div class="col-md-6 col-sm-6">*/
/*     {{ form_row(registrationForm.nakedPassword.first,{*/
/*         'attr': {*/
/*             'placeholder': 'New Password',*/
/*         },*/
/*         'label_attr': {*/
/*             'class': 'sr-only'*/
/*         }*/
/*     }) }}*/
/* </div>*/
/* */
/* */
/* */
/* <div class="col-md-6 col-sm-6">*/
/*     {{ form_row(registrationForm.nakedPassword.second,{*/
/*         'attr': {*/
/*             'placeholder': 'Confirm Password',*/
/*         },*/
/*         'label_attr': {*/
/*             'class': 'sr-only'*/
/*         }*/
/*     }) }}*/
/* </div>*/
/* */
/* */
/* */
/* */
/* */
/* <div class="col-sm-6 ">*/
/* */
/* </div>*/
/* */
/* */
/* <div class="col-sm-3 col-xs-3 ">*/
/*     <button type="submit" class="btn btn-primary" formnovalidate >{{ submitBtn }}</button>*/
/* */
/* </div>*/
/* */
/* */
/* {#<button type="submit" class="btn btn-primary" formnovalidate >{{ submitBtn }}</button>#}*/
/* <a class="btn btn-cancel" href="{{ cancelPath }}">Cancel</a>*/
/* */
/* */
/* {{ form_end(registrationForm) }}*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* {#{{ form_start(registrationForm) }}#}*/
/* */
/* {#{{ form_widget(registrationForm) }}#}*/
/* */
/* {#{{ form_row(registrationForm.firstName) }}#}*/
/* {#{{ form_row(registrationForm.lastName) }}#}*/
/* {#{{ form_row(registrationForm.companyName) }}#}*/
/* {#{{ form_row(registrationForm.email) }}#}*/
/* {#{{ form_row(registrationForm.nakedPassword.first) }}#}*/
/* {#{{ form_row(registrationForm.nakedPassword.second) }}#}*/
/* */
/* */
/* {#<button type="submit" class="btn btn-primary" formnovalidate >{{ submitBtn }}</button>#}*/
/* {#<a class="btn btn-primary" href="{{ cancelPath }}">Cancel</a>#}*/
/* */
/* */
/* {#{{ form_end(registrationForm) }}#}*/
