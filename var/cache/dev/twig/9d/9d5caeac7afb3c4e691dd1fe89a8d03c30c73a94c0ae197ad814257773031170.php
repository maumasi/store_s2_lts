<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_e44fec5369842817c16d2c766212ca703c609057214e9bf94b0b10b1f068fd98 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7f5d3dc29ccdb050c0f1cdb75a6f1fb02d39722c4bac556bb2d0f2d35516f23f = $this->env->getExtension("native_profiler");
        $__internal_7f5d3dc29ccdb050c0f1cdb75a6f1fb02d39722c4bac556bb2d0f2d35516f23f->enter($__internal_7f5d3dc29ccdb050c0f1cdb75a6f1fb02d39722c4bac556bb2d0f2d35516f23f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, (isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_7f5d3dc29ccdb050c0f1cdb75a6f1fb02d39722c4bac556bb2d0f2d35516f23f->leave($__internal_7f5d3dc29ccdb050c0f1cdb75a6f1fb02d39722c4bac556bb2d0f2d35516f23f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo str_replace('{{ widget }}', $view['form']->block($form, 'form_widget_simple'), $money_pattern) ?>*/
/* */
