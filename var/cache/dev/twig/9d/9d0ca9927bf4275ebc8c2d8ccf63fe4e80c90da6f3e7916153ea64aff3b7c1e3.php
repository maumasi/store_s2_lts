<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_4e2ce755048be68b24593a4f4ad1cbaadd9f9643144d8217e2eac34190d73101 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_299ea8da51c320d1d6f30fc678c40852cdb360766373638d1b09585c8c51db2e = $this->env->getExtension("native_profiler");
        $__internal_299ea8da51c320d1d6f30fc678c40852cdb360766373638d1b09585c8c51db2e->enter($__internal_299ea8da51c320d1d6f30fc678c40852cdb360766373638d1b09585c8c51db2e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.js.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_299ea8da51c320d1d6f30fc678c40852cdb360766373638d1b09585c8c51db2e->leave($__internal_299ea8da51c320d1d6f30fc678c40852cdb360766373638d1b09585c8c51db2e_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
