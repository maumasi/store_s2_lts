<?php

/* store/guestSignup.html.twig */
class __TwigTemplate_0aaeaa668db45d1ada2b222338b666db51403366ad15f6c835a3ee0d4c2ada74 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "store/guestSignup.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascript' => array($this, 'block_javascript'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6ef7a82c885875f820e3bd746dd4270e37c9440a9398c8a4504b849c90207fd6 = $this->env->getExtension("native_profiler");
        $__internal_6ef7a82c885875f820e3bd746dd4270e37c9440a9398c8a4504b849c90207fd6->enter($__internal_6ef7a82c885875f820e3bd746dd4270e37c9440a9398c8a4504b849c90207fd6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "store/guestSignup.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6ef7a82c885875f820e3bd746dd4270e37c9440a9398c8a4504b849c90207fd6->leave($__internal_6ef7a82c885875f820e3bd746dd4270e37c9440a9398c8a4504b849c90207fd6_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_d3d748a8c17c10e99094d605bfdcf73de2d1dbed95621b4f1582f5bd77af840a = $this->env->getExtension("native_profiler");
        $__internal_d3d748a8c17c10e99094d605bfdcf73de2d1dbed95621b4f1582f5bd77af840a->enter($__internal_d3d748a8c17c10e99094d605bfdcf73de2d1dbed95621b4f1582f5bd77af840a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 5
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo "

    - Registration

";
        
        $__internal_d3d748a8c17c10e99094d605bfdcf73de2d1dbed95621b4f1582f5bd77af840a->leave($__internal_d3d748a8c17c10e99094d605bfdcf73de2d1dbed95621b4f1582f5bd77af840a_prof);

    }

    // line 10
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_7ee1faf7896a22c0318eee2d3129b9358e48537ec12c64d936ae1b772a2e3ab8 = $this->env->getExtension("native_profiler");
        $__internal_7ee1faf7896a22c0318eee2d3129b9358e48537ec12c64d936ae1b772a2e3ab8->enter($__internal_7ee1faf7896a22c0318eee2d3129b9358e48537ec12c64d936ae1b772a2e3ab8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 11
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

";
        
        $__internal_7ee1faf7896a22c0318eee2d3129b9358e48537ec12c64d936ae1b772a2e3ab8->leave($__internal_7ee1faf7896a22c0318eee2d3129b9358e48537ec12c64d936ae1b772a2e3ab8_prof);

    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        $__internal_b0d275a6418c478bbe9180ffb79cb190f062f22077babdc12620c40eda30965f = $this->env->getExtension("native_profiler");
        $__internal_b0d275a6418c478bbe9180ffb79cb190f062f22077babdc12620c40eda30965f->enter($__internal_b0d275a6418c478bbe9180ffb79cb190f062f22077babdc12620c40eda30965f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 15
        echo "    ";
        $this->displayParentBlock("body", $context, $blocks);
        echo "


    ";
        // line 19
        echo "    <ol class=\"breadcrumb\">
        <li><a href=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("store");
        echo "\">Home</a></li>
        <li class=\"active\">Customer Registration</li>
    </ol>



    <main class=\"container-fluid customer-reg\">
        <div class=\"row\">
            ";
        // line 29
        echo "                <h1 class=\"col-lg-offset-4 col-md-offset-3 col-sm-offset-3 col-lg-4 col-md-6 col-sm-6\">Customer Registration</h1>



                ";
        // line 33
        echo twig_include($this->env, $context, "store/_guestRegistrationForm.html.twig", array("submitBtn" => "Register", "cancelPath" => $this->env->getExtension('routing')->getPath("store")));
        // line 36
        echo "


                ";
        // line 40
        echo "                    ";
        // line 41
        echo "                    ";
        // line 42
        echo "                ";
        // line 43
        echo "            ";
        // line 44
        echo "        </div>
    </main>






";
        
        $__internal_b0d275a6418c478bbe9180ffb79cb190f062f22077babdc12620c40eda30965f->leave($__internal_b0d275a6418c478bbe9180ffb79cb190f062f22077babdc12620c40eda30965f_prof);

    }

    // line 53
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_b6a168f84fc24011924e3b2aad4254db42c274c7f15f12e7232b4322283be284 = $this->env->getExtension("native_profiler");
        $__internal_b6a168f84fc24011924e3b2aad4254db42c274c7f15f12e7232b4322283be284->enter($__internal_b6a168f84fc24011924e3b2aad4254db42c274c7f15f12e7232b4322283be284_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        // line 54
        echo "    ";
        $this->displayParentBlock("javascript", $context, $blocks);
        echo "


";
        
        $__internal_b6a168f84fc24011924e3b2aad4254db42c274c7f15f12e7232b4322283be284->leave($__internal_b6a168f84fc24011924e3b2aad4254db42c274c7f15f12e7232b4322283be284_prof);

    }

    public function getTemplateName()
    {
        return "store/guestSignup.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 54,  136 => 53,  121 => 44,  119 => 43,  117 => 42,  115 => 41,  113 => 40,  108 => 36,  106 => 33,  100 => 29,  89 => 20,  86 => 19,  79 => 15,  73 => 14,  62 => 11,  56 => 10,  43 => 5,  37 => 4,  11 => 2,);
    }
}
/* */
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block title %}*/
/*     {{ parent() }}*/
/* */
/*     - Registration*/
/* */
/* {% endblock %}*/
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/* */
/* {% endblock %}*/
/* {% block body %}*/
/*     {{ parent() }}*/
/* */
/* */
/*     {# breadcrumbs #}*/
/*     <ol class="breadcrumb">*/
/*         <li><a href="{{ path('store') }}">Home</a></li>*/
/*         <li class="active">Customer Registration</li>*/
/*     </ol>*/
/* */
/* */
/* */
/*     <main class="container-fluid customer-reg">*/
/*         <div class="row">*/
/*             {#<div class="col-xs-12">#}*/
/*                 <h1 class="col-lg-offset-4 col-md-offset-3 col-sm-offset-3 col-lg-4 col-md-6 col-sm-6">Customer Registration</h1>*/
/* */
/* */
/* */
/*                 {{ include('store/_guestRegistrationForm.html.twig',{*/
/*                     'submitBtn' : 'Register',*/
/*                     'cancelPath' : path('store')*/
/*                 }) }}*/
/* */
/* */
/*                 {#{{ form_start(registrationForm) }}#}*/
/*                     {#{{ form_widget(registrationForm) }}#}*/
/*                     {#<button type="submit" class="btn btn-primary" formnovalidate >Register</button>#}*/
/*                 {#{{ form_end(registrationForm) }}#}*/
/*             {#</div>#}*/
/*         </div>*/
/*     </main>*/
/* */
/* */
/* */
/* */
/* */
/* */
/* {% endblock %}*/
/* {% block javascript %}*/
/*     {{ parent() }}*/
/* */
/* */
/* {% endblock %}*/
/* */
/* */
