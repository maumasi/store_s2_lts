<?php

/* :vendor/vendorFormTemplates:_queStatus.html.twig */
class __TwigTemplate_c3ae34c7e74af6b64c6beafdddbd1b4ab590a8ad1467608f0558d2247bdff582 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9118bd23e5bb7c61ec6756de10e2cc0b6b1be22fcc5fef8a5a9fdb484a97df65 = $this->env->getExtension("native_profiler");
        $__internal_9118bd23e5bb7c61ec6756de10e2cc0b6b1be22fcc5fef8a5a9fdb484a97df65->enter($__internal_9118bd23e5bb7c61ec6756de10e2cc0b6b1be22fcc5fef8a5a9fdb484a97df65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":vendor/vendorFormTemplates:_queStatus.html.twig"));

        // line 1
        echo "
";
        // line 2
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["queForm"]) ? $context["queForm"] : $this->getContext($context, "queForm")), 'form_start');
        echo "

    ";
        // line 4
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["queForm"]) ? $context["queForm"] : $this->getContext($context, "queForm")), "isCompleted", array()), 'row');
        echo "

    <button type=\"submit\" class=\"btn btn-primary\">Submit</button>

";
        // line 8
        echo         $this->env->getExtension('form')->renderer->renderBlock();
        echo "



";
        
        $__internal_9118bd23e5bb7c61ec6756de10e2cc0b6b1be22fcc5fef8a5a9fdb484a97df65->leave($__internal_9118bd23e5bb7c61ec6756de10e2cc0b6b1be22fcc5fef8a5a9fdb484a97df65_prof);

    }

    public function getTemplateName()
    {
        return ":vendor/vendorFormTemplates:_queStatus.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 8,  30 => 4,  25 => 2,  22 => 1,);
    }
}
/* */
/* {{ form_start(queForm) }}*/
/* */
/*     {{ form_row(queForm.isCompleted) }}*/
/* */
/*     <button type="submit" class="btn btn-primary">Submit</button>*/
/* */
/* {{ form_end() }}*/
/* */
/* */
/* */
/* */
