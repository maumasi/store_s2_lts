<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_64984a19f1f5bc6b12ed3ec7d57aeaf4179ff1d03daed9bb72cec86446b53d36 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9b22fb9648d20c085c9d1ca784ca82c686342d5b670e919590a8c0ddf58510ee = $this->env->getExtension("native_profiler");
        $__internal_9b22fb9648d20c085c9d1ca784ca82c686342d5b670e919590a8c0ddf58510ee->enter($__internal_9b22fb9648d20c085c9d1ca784ca82c686342d5b670e919590a8c0ddf58510ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_9b22fb9648d20c085c9d1ca784ca82c686342d5b670e919590a8c0ddf58510ee->leave($__internal_9b22fb9648d20c085c9d1ca784ca82c686342d5b670e919590a8c0ddf58510ee_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (count($errors) > 0): ?>*/
/*     <ul>*/
/*         <?php foreach ($errors as $error): ?>*/
/*             <li><?php echo $error->getMessage() ?></li>*/
/*         <?php endforeach; ?>*/
/*     </ul>*/
/* <?php endif ?>*/
/* */
