<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_81459cb5236c47efbedafe3f2a54a7889fcda11c92de6d5f52dbee4cde1b9e26 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_690e8c5ec0985509ed4dec2826f9d05f2dc49d078b3c7b21858099ba392c1e10 = $this->env->getExtension("native_profiler");
        $__internal_690e8c5ec0985509ed4dec2826f9d05f2dc49d078b3c7b21858099ba392c1e10->enter($__internal_690e8c5ec0985509ed4dec2826f9d05f2dc49d078b3c7b21858099ba392c1e10_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_690e8c5ec0985509ed4dec2826f9d05f2dc49d078b3c7b21858099ba392c1e10->leave($__internal_690e8c5ec0985509ed4dec2826f9d05f2dc49d078b3c7b21858099ba392c1e10_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($compound): ?>*/
/* <?php echo $view['form']->block($form, 'form_widget_compound')?>*/
/* <?php else: ?>*/
/* <?php echo $view['form']->block($form, 'form_widget_simple')?>*/
/* <?php endif ?>*/
/* */
