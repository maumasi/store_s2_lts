<?php

/* TwigBundle:Exception:exception.json.twig */
class __TwigTemplate_dee285154b3789ca198fcf8df6a3d2b9bb324735ffa364299b5989994865f352 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_512a8239f77b0dbe7fb318612d3a5b61453f136503118689ddb74ddbc56f4f2e = $this->env->getExtension("native_profiler");
        $__internal_512a8239f77b0dbe7fb318612d3a5b61453f136503118689ddb74ddbc56f4f2e->enter($__internal_512a8239f77b0dbe7fb318612d3a5b61453f136503118689ddb74ddbc56f4f2e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "exception" => $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "toarray", array()))));
        echo "
";
        
        $__internal_512a8239f77b0dbe7fb318612d3a5b61453f136503118689ddb74ddbc56f4f2e->leave($__internal_512a8239f77b0dbe7fb318612d3a5b61453f136503118689ddb74ddbc56f4f2e_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}*/
/* */
