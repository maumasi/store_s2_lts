<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_e6760c9f163d6fe4244f0b60c21f9fd33468124b4e68ce512bda1659753ec5ed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2ec51238c88d5dc5dae9fc1f0a5fd074397a8f49d1be62ff071e845a0fb1af72 = $this->env->getExtension("native_profiler");
        $__internal_2ec51238c88d5dc5dae9fc1f0a5fd074397a8f49d1be62ff071e845a0fb1af72->enter($__internal_2ec51238c88d5dc5dae9fc1f0a5fd074397a8f49d1be62ff071e845a0fb1af72_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_2ec51238c88d5dc5dae9fc1f0a5fd074397a8f49d1be62ff071e845a0fb1af72->leave($__internal_2ec51238c88d5dc5dae9fc1f0a5fd074397a8f49d1be62ff071e845a0fb1af72_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (isset($prototype)): ?>*/
/*     <?php $attr['data-prototype'] = $view->escape($view['form']->row($prototype)) ?>*/
/* <?php endif ?>*/
/* <?php echo $view['form']->widget($form, array('attr' => $attr)) ?>*/
/* */
