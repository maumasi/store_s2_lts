<?php

/* vendor/vendorItemViews/itemEdit.html.twig */
class __TwigTemplate_a46be56e49fc4fc2e60cc523a144b7e13e53044eda2ffbd40dfeef2bc71db30c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "vendor/vendorItemViews/itemEdit.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'javascript' => array($this, 'block_javascript'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dd288ce9107a7d1dfa504738b80ccc22cd01721361d0840fbd44f14df7b71378 = $this->env->getExtension("native_profiler");
        $__internal_dd288ce9107a7d1dfa504738b80ccc22cd01721361d0840fbd44f14df7b71378->enter($__internal_dd288ce9107a7d1dfa504738b80ccc22cd01721361d0840fbd44f14df7b71378_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "vendor/vendorItemViews/itemEdit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_dd288ce9107a7d1dfa504738b80ccc22cd01721361d0840fbd44f14df7b71378->leave($__internal_dd288ce9107a7d1dfa504738b80ccc22cd01721361d0840fbd44f14df7b71378_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_3dcb2c5974954fb8bf7d380e5f6cfde0af8fd65d0760da93b88ec1dcb358956f = $this->env->getExtension("native_profiler");
        $__internal_3dcb2c5974954fb8bf7d380e5f6cfde0af8fd65d0760da93b88ec1dcb358956f->enter($__internal_3dcb2c5974954fb8bf7d380e5f6cfde0af8fd65d0760da93b88ec1dcb358956f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 5
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo "

    - app.user.first_name

";
        
        $__internal_3dcb2c5974954fb8bf7d380e5f6cfde0af8fd65d0760da93b88ec1dcb358956f->leave($__internal_3dcb2c5974954fb8bf7d380e5f6cfde0af8fd65d0760da93b88ec1dcb358956f_prof);

    }

    // line 11
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_53ad3b1f78666dd47025d0a73e2ee134f5234b42585722c4d1b97710f17c0e17 = $this->env->getExtension("native_profiler");
        $__internal_53ad3b1f78666dd47025d0a73e2ee134f5234b42585722c4d1b97710f17c0e17->enter($__internal_53ad3b1f78666dd47025d0a73e2ee134f5234b42585722c4d1b97710f17c0e17_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        // line 12
        echo "     ";
        $this->displayParentBlock("javascript", $context, $blocks);
        echo "


 ";
        
        $__internal_53ad3b1f78666dd47025d0a73e2ee134f5234b42585722c4d1b97710f17c0e17->leave($__internal_53ad3b1f78666dd47025d0a73e2ee134f5234b42585722c4d1b97710f17c0e17_prof);

    }

    // line 17
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_0ce49a32de6831489b0ef364d11ae15ff9ae83ef3b49dc4fa3a022eb87d2bb06 = $this->env->getExtension("native_profiler");
        $__internal_0ce49a32de6831489b0ef364d11ae15ff9ae83ef3b49dc4fa3a022eb87d2bb06->enter($__internal_0ce49a32de6831489b0ef364d11ae15ff9ae83ef3b49dc4fa3a022eb87d2bb06_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 18
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

";
        
        $__internal_0ce49a32de6831489b0ef364d11ae15ff9ae83ef3b49dc4fa3a022eb87d2bb06->leave($__internal_0ce49a32de6831489b0ef364d11ae15ff9ae83ef3b49dc4fa3a022eb87d2bb06_prof);

    }

    // line 21
    public function block_body($context, array $blocks = array())
    {
        $__internal_8a58d95c25945a985fc325e07dffcd3134361e35e81709a15fde589be019976a = $this->env->getExtension("native_profiler");
        $__internal_8a58d95c25945a985fc325e07dffcd3134361e35e81709a15fde589be019976a->enter($__internal_8a58d95c25945a985fc325e07dffcd3134361e35e81709a15fde589be019976a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 22
        echo "    ";
        echo twig_include($this->env, $context, "store/_baseNav.html.twig");
        echo "

<main class=\"edit-item container\">



    <div class=\"row\">

        <h1 class=\"col-md-offset-2 col-sm-offset-2 col-lg-4 col-md-6 col-sm-6\">Edit Item</h1>


        ";
        // line 33
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["itemForm"]) ? $context["itemForm"] : $this->getContext($context, "itemForm")), 'form_start', array("attr" => array("class" => "col-md-offset-2 col-md-offset-3 col-sm-offset-3 col-lg-8 col-md-6 col-sm-6")));
        // line 37
        echo "


        <div class=\"col-md-6 col-sm-6\">
            ";
        // line 41
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["itemForm"]) ? $context["itemForm"] : $this->getContext($context, "itemForm")), "itemName", array()), 'row', array("attr" => array("placeholder" => "Item Name"), "label_attr" => array("class" => "sr-only")));
        // line 48
        echo "
        </div>


        <div class=\"col-md-6 col-sm-6\">
            ";
        // line 53
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["itemForm"]) ? $context["itemForm"] : $this->getContext($context, "itemForm")), "price", array()), 'row', array("attr" => array("placeholder" => "Price"), "label_attr" => array("class" => "sr-only")));
        // line 60
        echo "
        </div>



        <div class=\"col-md-6 col-sm-6\">

        </div>


        <div class=\"col-md-6 col-sm-6\">
            ";
        // line 71
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["itemForm"]) ? $context["itemForm"] : $this->getContext($context, "itemForm")), "quantity", array()), 'row', array("attr" => array("placeholder" => "Quantity in stock"), "label_attr" => array("class" => "sr-only")));
        // line 78
        echo "
        </div>





        <div class=\"col-md-6 col-sm-6\">
            ";
        // line 86
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["itemForm"]) ? $context["itemForm"] : $this->getContext($context, "itemForm")), "isRemovedFromStore", array()), 'row');
        echo "
        </div>

        <div class=\"col-md-6 col-sm-6\">

        </div>


        <div class=\"col-md-6 col-sm-6\">
            ";
        // line 95
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["itemForm"]) ? $context["itemForm"] : $this->getContext($context, "itemForm")), "itemImg", array()), 'row', array("attr" => array("class" => "btn btn-download"), "label_attr" => array("class" => "sr-only")));
        // line 102
        echo "
        </div>


        <div class=\" col-sm-12\">
            ";
        // line 107
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["itemForm"]) ? $context["itemForm"] : $this->getContext($context, "itemForm")), "description", array()), 'row', array("attr" => array("placeholder" => "Description", "class" => "item-description"), "label_attr" => array("class" => "sr-only")));
        // line 115
        echo "
        </div>

        ";
        // line 119
        echo "            ";
        // line 120
        echo "        ";
        // line 121
        echo "
        ";
        // line 123
        echo "            ";
        // line 124
        echo "        ";
        // line 125
        echo "

        <div>
            <div class=\"item-action col-xs-4 col-xs-offset-4\">
                <a href=\"";
        // line 129
        echo $this->env->getExtension('routing')->getPath("vendor_item_collection");
        echo "\">Cancel</a>
            </div>

            <div class=\"item-action col-xs-4 \">
                <button type=\"submit\" class=\"btn btn-primary col-xs-12\" formnovalidate >Edit item</button>
            </div>
        </div>


        ";
        // line 138
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["itemForm"]) ? $context["itemForm"] : $this->getContext($context, "itemForm")), 'form_end');
        echo "
    </div>





</main>








    ";
        // line 155
        echo "
        ";
        // line 157
        echo "        ";
        // line 158
        echo "        ";
        // line 159
        echo "        ";
        // line 160
        echo "        ";
        // line 161
        echo "        ";
        // line 162
        echo "
        ";
        // line 164
        echo "
    ";
        // line 166
        echo "

";
        
        $__internal_8a58d95c25945a985fc325e07dffcd3134361e35e81709a15fde589be019976a->leave($__internal_8a58d95c25945a985fc325e07dffcd3134361e35e81709a15fde589be019976a_prof);

    }

    public function getTemplateName()
    {
        return "vendor/vendorItemViews/itemEdit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  251 => 166,  248 => 164,  245 => 162,  243 => 161,  241 => 160,  239 => 159,  237 => 158,  235 => 157,  232 => 155,  213 => 138,  201 => 129,  195 => 125,  193 => 124,  191 => 123,  188 => 121,  186 => 120,  184 => 119,  179 => 115,  177 => 107,  170 => 102,  168 => 95,  156 => 86,  146 => 78,  144 => 71,  131 => 60,  129 => 53,  122 => 48,  120 => 41,  114 => 37,  112 => 33,  97 => 22,  91 => 21,  80 => 18,  74 => 17,  62 => 12,  56 => 11,  43 => 5,  37 => 4,  11 => 2,);
    }
}
/* */
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block title %}*/
/*     {{ parent() }}*/
/* */
/*     - app.user.first_name*/
/* */
/* {% endblock %}*/
/* */
/*  {% block javascript %}*/
/*      {{ parent() }}*/
/* */
/* */
/*  {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/* */
/* {% endblock %}*/
/* {% block body %}*/
/*     {{ include('store/_baseNav.html.twig') }}*/
/* */
/* <main class="edit-item container">*/
/* */
/* */
/* */
/*     <div class="row">*/
/* */
/*         <h1 class="col-md-offset-2 col-sm-offset-2 col-lg-4 col-md-6 col-sm-6">Edit Item</h1>*/
/* */
/* */
/*         {{ form_start(itemForm,{*/
/*             'attr': {*/
/*                 'class': 'col-md-offset-2 col-md-offset-3 col-sm-offset-3 col-lg-8 col-md-6 col-sm-6'*/
/*             }*/
/*         }) }}*/
/* */
/* */
/*         <div class="col-md-6 col-sm-6">*/
/*             {{ form_row(itemForm.itemName,{*/
/*                 'attr': {*/
/*                     'placeholder': 'Item Name',*/
/*                 },*/
/*                 'label_attr': {*/
/*                     'class': 'sr-only'*/
/*                 }*/
/*             }) }}*/
/*         </div>*/
/* */
/* */
/*         <div class="col-md-6 col-sm-6">*/
/*             {{ form_row(itemForm.price,{*/
/*                 'attr': {*/
/*                     'placeholder': 'Price',*/
/*                 },*/
/*                 'label_attr': {*/
/*                     'class': 'sr-only'*/
/*                 }*/
/*             }) }}*/
/*         </div>*/
/* */
/* */
/* */
/*         <div class="col-md-6 col-sm-6">*/
/* */
/*         </div>*/
/* */
/* */
/*         <div class="col-md-6 col-sm-6">*/
/*             {{ form_row(itemForm.quantity,{*/
/*                 'attr': {*/
/*                     'placeholder': 'Quantity in stock',*/
/*                 },*/
/*                 'label_attr': {*/
/*                     'class': 'sr-only'*/
/*                 }*/
/*             }) }}*/
/*         </div>*/
/* */
/* */
/* */
/* */
/* */
/*         <div class="col-md-6 col-sm-6">*/
/*             {{ form_row(itemForm.isRemovedFromStore) }}*/
/*         </div>*/
/* */
/*         <div class="col-md-6 col-sm-6">*/
/* */
/*         </div>*/
/* */
/* */
/*         <div class="col-md-6 col-sm-6">*/
/*             {{ form_row(itemForm.itemImg,{*/
/*                 'attr': {*/
/*                   'class': 'btn btn-download'*/
/*                 },*/
/*                 'label_attr': {*/
/*                     'class': 'sr-only'*/
/*                 }*/
/*             }) }}*/
/*         </div>*/
/* */
/* */
/*         <div class=" col-sm-12">*/
/*             {{ form_row(itemForm.description,{*/
/*                 'attr': {*/
/*                     'placeholder': 'Description',*/
/*                     'class': 'item-description'*/
/*                 },*/
/*                 'label_attr': {*/
/*                     'class': 'sr-only'*/
/*                 }*/
/*             }) }}*/
/*         </div>*/
/* */
/*         {#<div class="col-md-6 col-sm-6 ">#}*/
/*             {#<a href="{{ path('vendor_item_collection') }}">Cancel</a>#}*/
/*         {#</div>#}*/
/* */
/*         {#<div class="col-md-6 col-sm-6  ">#}*/
/*             {#<button type="submit" class="btn btn-primary col-xs-12" formnovalidate >Edit item</button>#}*/
/*         {#</div>#}*/
/* */
/* */
/*         <div>*/
/*             <div class="item-action col-xs-4 col-xs-offset-4">*/
/*                 <a href="{{ path('vendor_item_collection') }}">Cancel</a>*/
/*             </div>*/
/* */
/*             <div class="item-action col-xs-4 ">*/
/*                 <button type="submit" class="btn btn-primary col-xs-12" formnovalidate >Edit item</button>*/
/*             </div>*/
/*         </div>*/
/* */
/* */
/*         {{ form_end(itemForm) }}*/
/*     </div>*/
/* */
/* */
/* */
/* */
/* */
/* </main>*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/*     {#{{ form_start(itemForm) }}#}*/
/* */
/*         {#{{ form_row(itemForm.itemName) }}#}*/
/*         {#{{ form_row(itemForm.price) }}#}*/
/*         {#{{ form_row(itemForm.description) }}#}*/
/*         {#{{ form_row(itemForm.itemImg) }}#}*/
/*         {#{{ form_row(itemForm.isInStock) }}#}*/
/*         {#{{ form_row(itemForm.isRemovedFromStore) }}#}*/
/* */
/*         {#<button type="submit" class="btn btn-primary" formnovalidate >Edit item</button>#}*/
/* */
/*     {#{{ form_end(itemForm) }}#}*/
/* */
/* */
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* */
/* */
