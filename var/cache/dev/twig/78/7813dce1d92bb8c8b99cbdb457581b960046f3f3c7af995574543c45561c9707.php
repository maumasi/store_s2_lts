<?php

/* security/vendorLogin.html.twig */
class __TwigTemplate_b098ed96e2650935abb88d41200ad0644414970ff2073ae74a8e9ee544d4dc83 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 5
        $this->parent = $this->loadTemplate("base.html.twig", "security/vendorLogin.html.twig", 5);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b9b6de72d8e933d990ecb6ea4e0a61ae5bd89b10f4468f842f925fbd9ba2916c = $this->env->getExtension("native_profiler");
        $__internal_b9b6de72d8e933d990ecb6ea4e0a61ae5bd89b10f4468f842f925fbd9ba2916c->enter($__internal_b9b6de72d8e933d990ecb6ea4e0a61ae5bd89b10f4468f842f925fbd9ba2916c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "security/vendorLogin.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b9b6de72d8e933d990ecb6ea4e0a61ae5bd89b10f4468f842f925fbd9ba2916c->leave($__internal_b9b6de72d8e933d990ecb6ea4e0a61ae5bd89b10f4468f842f925fbd9ba2916c_prof);

    }

    // line 8
    public function block_title($context, array $blocks = array())
    {
        $__internal_18dd03c3abe6b64252e3360b5c33cc378d0b76843e265a2326bf29797f5cbd62 = $this->env->getExtension("native_profiler");
        $__internal_18dd03c3abe6b64252e3360b5c33cc378d0b76843e265a2326bf29797f5cbd62->enter($__internal_18dd03c3abe6b64252e3360b5c33cc378d0b76843e265a2326bf29797f5cbd62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 9
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo "

    - Login
";
        
        $__internal_18dd03c3abe6b64252e3360b5c33cc378d0b76843e265a2326bf29797f5cbd62->leave($__internal_18dd03c3abe6b64252e3360b5c33cc378d0b76843e265a2326bf29797f5cbd62_prof);

    }

    // line 15
    public function block_body($context, array $blocks = array())
    {
        $__internal_0c3b8a4bf1d8fcf12d9c43cfcfd19fc5b370b92b8fa1b81fbd259b9da7250e3a = $this->env->getExtension("native_profiler");
        $__internal_0c3b8a4bf1d8fcf12d9c43cfcfd19fc5b370b92b8fa1b81fbd259b9da7250e3a->enter($__internal_0c3b8a4bf1d8fcf12d9c43cfcfd19fc5b370b92b8fa1b81fbd259b9da7250e3a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 16
        $this->displayParentBlock("body", $context, $blocks);
        echo "







<main class=\"container login-form\">
    <div class=\"row login\">

        <h1 class=\"col-lg-8\">Login</h1>



        ";
        // line 31
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 32
            echo "            <div class=\"alert alert-danger\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</div>
        ";
        }
        // line 34
        echo "
        ";
        // line 35
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("class" => "form-inline col-lg-8 col-sm-10")));
        // line 39
        echo "

        <div class=\"col-md-3 col-sm-4 \">
            ";
        // line 42
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "_email", array()), 'row', array("attr" => array("placeholder" => "Email"), "label_attr" => array("class" => "sr-only")));
        // line 49
        echo "
        </div>

        <div class=\"col-md-3 col-sm-4 \">
            ";
        // line 53
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "_password", array()), 'row', array("attr" => array("placeholder" => "Password"), "label_attr" => array("class" => "sr-only")));
        // line 60
        echo "
        </div>

        <div class=\"col-md-4 col-sm-4 \">
            <button type=\"submit\" class=\"btn btn-primary col-xs-12\" formnovalidate>login</button>

        </div>
        ";
        // line 67
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

        <p class=\"col-md-12\">Not registered vendor?<a href=\"";
        // line 69
        echo $this->env->getExtension('routing')->getPath("vendor_register");
        echo "\" class=\"\"> Easy, register here.</a></p>
    </div>












";
        // line 85
        echo "    ";
        // line 86
        echo "        ";
        // line 87
        echo "




        ";
        // line 93
        echo "            ";
        // line 94
        echo "        ";
        // line 95
        echo "
        ";
        // line 97
        echo "            ";
        // line 98
        echo "            ";
        // line 99
        echo "        ";
        // line 100
        echo "        ";
        // line 101
        echo "        ";
        // line 102
        echo "
    ";
        
        $__internal_0c3b8a4bf1d8fcf12d9c43cfcfd19fc5b370b92b8fa1b81fbd259b9da7250e3a->leave($__internal_0c3b8a4bf1d8fcf12d9c43cfcfd19fc5b370b92b8fa1b81fbd259b9da7250e3a_prof);

    }

    public function getTemplateName()
    {
        return "security/vendorLogin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  164 => 102,  162 => 101,  160 => 100,  158 => 99,  156 => 98,  154 => 97,  151 => 95,  149 => 94,  147 => 93,  140 => 87,  138 => 86,  136 => 85,  119 => 69,  114 => 67,  105 => 60,  103 => 53,  97 => 49,  95 => 42,  90 => 39,  88 => 35,  85 => 34,  79 => 32,  77 => 31,  59 => 16,  53 => 15,  41 => 9,  35 => 8,  11 => 5,);
    }
}
/* */
/* {#<?php#}*/
/* */
/* */
/* {% extends 'base.html.twig' %}*/
/* */
/* */
/* {% block title %}*/
/*     {{ parent() }}*/
/* */
/*     - Login*/
/* {% endblock %}*/
/* */
/* */
/* {% block body %}*/
/* {{ parent() }}*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* <main class="container login-form">*/
/*     <div class="row login">*/
/* */
/*         <h1 class="col-lg-8">Login</h1>*/
/* */
/* */
/* */
/*         {% if error %}*/
/*             <div class="alert alert-danger">{{ error.messageKey|trans(error.messageData, 'security') }}</div>*/
/*         {% endif %}*/
/* */
/*         {{ form_start(form,{*/
/*             'attr': {*/
/*                 'class': 'form-inline col-lg-8 col-sm-10'*/
/*             }*/
/*         }) }}*/
/* */
/*         <div class="col-md-3 col-sm-4 ">*/
/*             {{ form_row(form._email,{*/
/*                 'attr': {*/
/*                     'placeholder': 'Email',*/
/*                 },*/
/*                 'label_attr': {*/
/*                     'class': 'sr-only'*/
/*                 }*/
/*             }) }}*/
/*         </div>*/
/* */
/*         <div class="col-md-3 col-sm-4 ">*/
/*             {{ form_row(form._password,{*/
/*                 'attr': {*/
/*                     'placeholder': 'Password',*/
/*                 },*/
/*                 'label_attr': {*/
/*                     'class': 'sr-only'*/
/*                 }*/
/*             }) }}*/
/*         </div>*/
/* */
/*         <div class="col-md-4 col-sm-4 ">*/
/*             <button type="submit" class="btn btn-primary col-xs-12" formnovalidate>login</button>*/
/* */
/*         </div>*/
/*         {{ form_end(form) }}*/
/* */
/*         <p class="col-md-12">Not registered vendor?<a href="{{ path('vendor_register') }}" class=""> Easy, register here.</a></p>*/
/*     </div>*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* {#<div class="container">#}*/
/* {#<div class="row">#}*/
/*     {#<div class="col-xs-12">#}*/
/*         {#<h1>Vendor Login</h1>#}*/
/* */
/* */
/* */
/* */
/* */
/*         {#{% if error %}#}*/
/*             {#<div class="alert alert-danger">{{ error.messageKey|trans(error.messageData, 'security') }}</div>#}*/
/*         {#{% endif %}#}*/
/* */
/*         {#{{ form_start(form) }}#}*/
/*             {#{{ form_row(form._email) }}#}*/
/*             {#{{ form_row(form._password) }}#}*/
/*         {#<button type="submit" class="btn btn-primary" formnovalidate>login</button>#}*/
/*         {#{{ form_end(form) }}#}*/
/*         {##}*/
/* */
/*     {#</div>#}*/
/* {#</div>#}*/
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
