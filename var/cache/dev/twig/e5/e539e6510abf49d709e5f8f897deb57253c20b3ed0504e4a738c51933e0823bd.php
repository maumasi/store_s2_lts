<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_d207823cb54fd3a5ab0694cdfe9d342dcc4cb625bd4fccf27fe030832908bb0c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a2b88e916c510b51164de26f956f65d052bcb0d93b01fcad9210197f2fe33e88 = $this->env->getExtension("native_profiler");
        $__internal_a2b88e916c510b51164de26f956f65d052bcb0d93b01fcad9210197f2fe33e88->enter($__internal_a2b88e916c510b51164de26f956f65d052bcb0d93b01fcad9210197f2fe33e88_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_a2b88e916c510b51164de26f956f65d052bcb0d93b01fcad9210197f2fe33e88->leave($__internal_a2b88e916c510b51164de26f956f65d052bcb0d93b01fcad9210197f2fe33e88_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'button_widget',  array('type' => isset($type) ? $type : 'submit')) ?>*/
/* */
