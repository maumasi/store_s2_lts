<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_98fb199e7fdde76723347c725b99b19126cef2786a8588e4289bf23fad87b60b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_372c83b5671442528bfa6e07317800f6c21ca952c56d7d13aaeeda1d2d5f88a4 = $this->env->getExtension("native_profiler");
        $__internal_372c83b5671442528bfa6e07317800f6c21ca952c56d7d13aaeeda1d2d5f88a4->enter($__internal_372c83b5671442528bfa6e07317800f6c21ca952c56d7d13aaeeda1d2d5f88a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_372c83b5671442528bfa6e07317800f6c21ca952c56d7d13aaeeda1d2d5f88a4->leave($__internal_372c83b5671442528bfa6e07317800f6c21ca952c56d7d13aaeeda1d2d5f88a4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php foreach ($form as $child): ?>*/
/*     <?php if (!$child->isRendered()): ?>*/
/*         <?php echo $view['form']->row($child) ?>*/
/*     <?php endif; ?>*/
/* <?php endforeach; ?>*/
/* */
