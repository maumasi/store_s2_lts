<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_b6537c8f4e05a60f049cc65416cd0225e12167bd408f6ad5a549e7dd9e594a32 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bc349d12893f3344f3656fb631f9366396f84059b61b50a82ead2b7a7911bca4 = $this->env->getExtension("native_profiler");
        $__internal_bc349d12893f3344f3656fb631f9366396f84059b61b50a82ead2b7a7911bca4->enter($__internal_bc349d12893f3344f3656fb631f9366396f84059b61b50a82ead2b7a7911bca4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_bc349d12893f3344f3656fb631f9366396f84059b61b50a82ead2b7a7911bca4->leave($__internal_bc349d12893f3344f3656fb631f9366396f84059b61b50a82ead2b7a7911bca4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'hidden')) ?>*/
/* */
