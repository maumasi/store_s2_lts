<?php

/* :vendor:_form.html.twig */
class __TwigTemplate_b4f0601f7d37d46d8af6799dcf7bcf98e33257003236dfbfbd548015dce1e4c7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a4cdf4eb51c680a2341053187c00e563b4844929234c1a1d39657767e0a9b6f2 = $this->env->getExtension("native_profiler");
        $__internal_a4cdf4eb51c680a2341053187c00e563b4844929234c1a1d39657767e0a9b6f2->enter($__internal_a4cdf4eb51c680a2341053187c00e563b4844929234c1a1d39657767e0a9b6f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":vendor:_form.html.twig"));

        // line 1
        echo "
";
        // line 2
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), 'form_start');
        echo "

    ";
        // line 4
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), 'widget');
        echo "

    ";
        // line 6
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "firstName", array()), 'row');
        echo "
    ";
        // line 7
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "lastName", array()), 'row');
        echo "
    ";
        // line 8
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "companyName", array()), 'row');
        echo "
    ";
        // line 9
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "email", array()), 'row');
        echo "
    ";
        // line 10
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "nakedPassword", array()), "first", array()), 'row');
        echo "
    ";
        // line 11
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "nakedPassword", array()), "second", array()), 'row');
        echo "


    <button type=\"submit\" class=\"btn btn-primary\" formnovalidate >";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["submitBtn"]) ? $context["submitBtn"] : $this->getContext($context, "submitBtn")), "html", null, true);
        echo "</button>
    <a class=\"btn btn-primary\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["cancelPath"]) ? $context["cancelPath"] : $this->getContext($context, "cancelPath")), "html", null, true);
        echo "\">Cancel</a>


";
        // line 18
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), 'form_end');
        
        $__internal_a4cdf4eb51c680a2341053187c00e563b4844929234c1a1d39657767e0a9b6f2->leave($__internal_a4cdf4eb51c680a2341053187c00e563b4844929234c1a1d39657767e0a9b6f2_prof);

    }

    public function getTemplateName()
    {
        return ":vendor:_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 18,  65 => 15,  61 => 14,  55 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  35 => 6,  30 => 4,  25 => 2,  22 => 1,);
    }
}
/* */
/* {{ form_start(registrationForm) }}*/
/* */
/*     {{ form_widget(registrationForm) }}*/
/* */
/*     {{ form_row(registrationForm.firstName) }}*/
/*     {{ form_row(registrationForm.lastName) }}*/
/*     {{ form_row(registrationForm.companyName) }}*/
/*     {{ form_row(registrationForm.email) }}*/
/*     {{ form_row(registrationForm.nakedPassword.first) }}*/
/*     {{ form_row(registrationForm.nakedPassword.second) }}*/
/* */
/* */
/*     <button type="submit" class="btn btn-primary" formnovalidate >{{ submitBtn }}</button>*/
/*     <a class="btn btn-primary" href="{{ cancelPath }}">Cancel</a>*/
/* */
/* */
/* {{ form_end(registrationForm) }}*/
