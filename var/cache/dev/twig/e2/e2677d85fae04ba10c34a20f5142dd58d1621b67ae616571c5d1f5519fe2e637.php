<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_d384c51e2b8bf79a286c55bb5870cf2466dbc437f541074673c103544b87afaf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bc24c1dbb0cd3b01498e9107259799706ef88fd427c960d3d79cac35071c6eea = $this->env->getExtension("native_profiler");
        $__internal_bc24c1dbb0cd3b01498e9107259799706ef88fd427c960d3d79cac35071c6eea->enter($__internal_bc24c1dbb0cd3b01498e9107259799706ef88fd427c960d3d79cac35071c6eea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_bc24c1dbb0cd3b01498e9107259799706ef88fd427c960d3d79cac35071c6eea->leave($__internal_bc24c1dbb0cd3b01498e9107259799706ef88fd427c960d3d79cac35071c6eea_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/* <?php foreach ($form as $child): ?>*/
/*     <?php echo $view['form']->widget($child) ?>*/
/*     <?php echo $view['form']->label($child, null, array('translation_domain' => $choice_translation_domain)) ?>*/
/* <?php endforeach ?>*/
/* </div>*/
/* */
