<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_090ee2039e51331432aa0fe1bc34bac1e681413be932f368a82ef8978bf8e527 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2f4ebca2cc73f4ce55156efe2da583220643370f57718cdac3af1159b86bb22a = $this->env->getExtension("native_profiler");
        $__internal_2f4ebca2cc73f4ce55156efe2da583220643370f57718cdac3af1159b86bb22a->enter($__internal_2f4ebca2cc73f4ce55156efe2da583220643370f57718cdac3af1159b86bb22a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2f4ebca2cc73f4ce55156efe2da583220643370f57718cdac3af1159b86bb22a->leave($__internal_2f4ebca2cc73f4ce55156efe2da583220643370f57718cdac3af1159b86bb22a_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_4aebf68844b44411e41c29a424908bf668ddc5aab882511a682d2db477e829db = $this->env->getExtension("native_profiler");
        $__internal_4aebf68844b44411e41c29a424908bf668ddc5aab882511a682d2db477e829db->enter($__internal_4aebf68844b44411e41c29a424908bf668ddc5aab882511a682d2db477e829db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_4aebf68844b44411e41c29a424908bf668ddc5aab882511a682d2db477e829db->leave($__internal_4aebf68844b44411e41c29a424908bf668ddc5aab882511a682d2db477e829db_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_79cf88554e2e1d258b42234a614e136380be87e1fd0d9ac2c0d4a110347904a5 = $this->env->getExtension("native_profiler");
        $__internal_79cf88554e2e1d258b42234a614e136380be87e1fd0d9ac2c0d4a110347904a5->enter($__internal_79cf88554e2e1d258b42234a614e136380be87e1fd0d9ac2c0d4a110347904a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_79cf88554e2e1d258b42234a614e136380be87e1fd0d9ac2c0d4a110347904a5->leave($__internal_79cf88554e2e1d258b42234a614e136380be87e1fd0d9ac2c0d4a110347904a5_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_fcdccea34e99fa8f69922f5db257994d6ca1a25dda6e6e43a8ca7f3ae131f4da = $this->env->getExtension("native_profiler");
        $__internal_fcdccea34e99fa8f69922f5db257994d6ca1a25dda6e6e43a8ca7f3ae131f4da->enter($__internal_fcdccea34e99fa8f69922f5db257994d6ca1a25dda6e6e43a8ca7f3ae131f4da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_fcdccea34e99fa8f69922f5db257994d6ca1a25dda6e6e43a8ca7f3ae131f4da->leave($__internal_fcdccea34e99fa8f69922f5db257994d6ca1a25dda6e6e43a8ca7f3ae131f4da_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
