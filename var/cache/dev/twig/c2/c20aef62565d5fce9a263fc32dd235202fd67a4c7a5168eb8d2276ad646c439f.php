<?php

/* store/_cartTablePreLogin.html.twig */
class __TwigTemplate_ed1d9b6cb49c4b6f1b76fcaed80aabce682917622dc611e229df6331e67a244d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_13fc06f0e09e229a4fe58b654c20309ef795f0d057c1cbc2e357cddb6a46f7a0 = $this->env->getExtension("native_profiler");
        $__internal_13fc06f0e09e229a4fe58b654c20309ef795f0d057c1cbc2e357cddb6a46f7a0->enter($__internal_13fc06f0e09e229a4fe58b654c20309ef795f0d057c1cbc2e357cddb6a46f7a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "store/_cartTablePreLogin.html.twig"));

        // line 1
        echo "
<table class=\"table\" id=\"cartList\">

    <tr>
        <th>Item</th>
        <th>Price</th>
        ";
        // line 8
        echo "    </tr>


    ";
        // line 11
        $context["total"] = 0;
        // line 12
        echo "    ";
        $context["count"] = 0;
        // line 13
        echo "
    ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["cartCount"]) ? $context["cartCount"] : $this->getContext($context, "cartCount")));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 15
            echo "
        <tr>
            <td>
                <a class=\"checkout-item row\" href=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("single_item", array("itemObj" => $this->getAttribute($context["item"], "id", array()))), "html", null, true);
            echo "\">
                    ";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "itemName", array()), "html", null, true);
            echo "
                </a>
            </td>

            <td>
                <p>\$";
            // line 24
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["item"], "price", array()), 2, ".", ","), "html", null, true);
            echo "</p>
            </td>


            ";
            // line 28
            $context["total"] = ((isset($context["total"]) ? $context["total"] : $this->getContext($context, "total")) + $this->getAttribute($context["item"], "price", array()));
            // line 29
            echo "            ";
            $context["count"] = ((isset($context["count"]) ? $context["count"] : $this->getContext($context, "count")) + 1);
            // line 30
            echo "            ";
            // line 31
            echo "            ";
            // line 32
            echo "            ";
            // line 33
            echo "            ";
            // line 34
            echo "            ";
            // line 35
            echo "        </tr>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "

    <tr class=\"cartStatus\">
        <td>
            Items in cart:
        </td>

        <td>
            ";
        // line 46
        echo twig_escape_filter($this->env, (isset($context["count"]) ? $context["count"] : $this->getContext($context, "count")), "html", null, true);
        echo "
        </td>
    </tr>

    <tr class=\"cartStatus\">
        <td>
            Total:
        </td>

        <td>
            \$";
        // line 56
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (twig_round(((isset($context["total"]) ? $context["total"] : $this->getContext($context, "total")) * 100), 0, "floor") / 100), 2, ".", ","), "html", null, true);
        echo "
        </td>
    </tr>


</table>";
        
        $__internal_13fc06f0e09e229a4fe58b654c20309ef795f0d057c1cbc2e357cddb6a46f7a0->leave($__internal_13fc06f0e09e229a4fe58b654c20309ef795f0d057c1cbc2e357cddb6a46f7a0_prof);

    }

    public function getTemplateName()
    {
        return "store/_cartTablePreLogin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 56,  104 => 46,  94 => 38,  86 => 35,  84 => 34,  82 => 33,  80 => 32,  78 => 31,  76 => 30,  73 => 29,  71 => 28,  64 => 24,  56 => 19,  52 => 18,  47 => 15,  43 => 14,  40 => 13,  37 => 12,  35 => 11,  30 => 8,  22 => 1,);
    }
}
/* */
/* <table class="table" id="cartList">*/
/* */
/*     <tr>*/
/*         <th>Item</th>*/
/*         <th>Price</th>*/
/*         {#<th>Remove Item</th>#}*/
/*     </tr>*/
/* */
/* */
/*     {% set total = 0.00 %}*/
/*     {% set count = 0 %}*/
/* */
/*     {% for item in cartCount %}*/
/* */
/*         <tr>*/
/*             <td>*/
/*                 <a class="checkout-item row" href="{{ path('single_item', { 'itemObj' : item.id }) }}">*/
/*                     {{ item.itemName }}*/
/*                 </a>*/
/*             </td>*/
/* */
/*             <td>*/
/*                 <p>${{ item.price | number_format(2, '.', ',')}}</p>*/
/*             </td>*/
/* */
/* */
/*             {% set total = total + item.price %}*/
/*             {% set count = count + 1  %}*/
/*             {#<td>#}*/
/*             {#<form class="col-sm-2" action="{{ path('remove_item_at_checkout', {'itemId' : item.id}) }}" method="GET">#}*/
/*             {#<button class="btn btn-danger" type="submit"><span class="fa fa-times-circle-o"></span></button>#}*/
/*             {#</form>#}*/
/*             {#</td>#}*/
/*         </tr>*/
/* */
/*     {% endfor %}*/
/* */
/* */
/*     <tr class="cartStatus">*/
/*         <td>*/
/*             Items in cart:*/
/*         </td>*/
/* */
/*         <td>*/
/*             {{ count }}*/
/*         </td>*/
/*     </tr>*/
/* */
/*     <tr class="cartStatus">*/
/*         <td>*/
/*             Total:*/
/*         </td>*/
/* */
/*         <td>*/
/*             ${{ (((total * 100) | round(0, 'floor')) / 100) | number_format(2, '.', ',') }}*/
/*         </td>*/
/*     </tr>*/
/* */
/* */
/* </table>*/
