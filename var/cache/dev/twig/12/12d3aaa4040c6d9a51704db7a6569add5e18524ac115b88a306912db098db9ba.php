<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_1b308c00725fe44242af33c175766220e543eccad3b640395188463b3bb2f19b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6f41772b4351a6a3e4dd4e6168820330bb58feed7f30cc6570db98f1013f7b75 = $this->env->getExtension("native_profiler");
        $__internal_6f41772b4351a6a3e4dd4e6168820330bb58feed7f30cc6570db98f1013f7b75->enter($__internal_6f41772b4351a6a3e4dd4e6168820330bb58feed7f30cc6570db98f1013f7b75_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_6f41772b4351a6a3e4dd4e6168820330bb58feed7f30cc6570db98f1013f7b75->leave($__internal_6f41772b4351a6a3e4dd4e6168820330bb58feed7f30cc6570db98f1013f7b75_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <textarea <?php echo $view['form']->block($form, 'widget_attributes') ?>><?php echo $view->escape($value) ?></textarea>*/
/* */
