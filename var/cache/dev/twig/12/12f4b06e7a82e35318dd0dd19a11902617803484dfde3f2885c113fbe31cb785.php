<?php

/* store/_printableReceipt.html.twig */
class __TwigTemplate_c2ad3b1eee3ba1b2de484c6363ad708d8d9a73198c6a722f40558439d9c149c2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 4
        $this->parent = $this->loadTemplate("base.html.twig", "store/_printableReceipt.html.twig", 4);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascript' => array($this, 'block_javascript'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b000ef0028088822434cc413e7e70586039f3c4be163a28d370c6c5516fd7d69 = $this->env->getExtension("native_profiler");
        $__internal_b000ef0028088822434cc413e7e70586039f3c4be163a28d370c6c5516fd7d69->enter($__internal_b000ef0028088822434cc413e7e70586039f3c4be163a28d370c6c5516fd7d69_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "store/_printableReceipt.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b000ef0028088822434cc413e7e70586039f3c4be163a28d370c6c5516fd7d69->leave($__internal_b000ef0028088822434cc413e7e70586039f3c4be163a28d370c6c5516fd7d69_prof);

    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        $__internal_f597314d5582a4b5e231242feb0035168205bc04fa5916e09eb76fe3237d108f = $this->env->getExtension("native_profiler");
        $__internal_f597314d5582a4b5e231242feb0035168205bc04fa5916e09eb76fe3237d108f->enter($__internal_f597314d5582a4b5e231242feb0035168205bc04fa5916e09eb76fe3237d108f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 7
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo "

    - Receipt

";
        
        $__internal_f597314d5582a4b5e231242feb0035168205bc04fa5916e09eb76fe3237d108f->leave($__internal_f597314d5582a4b5e231242feb0035168205bc04fa5916e09eb76fe3237d108f_prof);

    }

    // line 12
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_ae85d8c8e1cc40443d26df53a9ab585198d12d78931f395abc33e7beeb3c3472 = $this->env->getExtension("native_profiler");
        $__internal_ae85d8c8e1cc40443d26df53a9ab585198d12d78931f395abc33e7beeb3c3472->enter($__internal_ae85d8c8e1cc40443d26df53a9ab585198d12d78931f395abc33e7beeb3c3472_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 13
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

";
        
        $__internal_ae85d8c8e1cc40443d26df53a9ab585198d12d78931f395abc33e7beeb3c3472->leave($__internal_ae85d8c8e1cc40443d26df53a9ab585198d12d78931f395abc33e7beeb3c3472_prof);

    }

    // line 16
    public function block_body($context, array $blocks = array())
    {
        $__internal_bea7392002c1b0f44fd64c3aefa812f7a4630db71cb9e890267bcdbc18c94cdd = $this->env->getExtension("native_profiler");
        $__internal_bea7392002c1b0f44fd64c3aefa812f7a4630db71cb9e890267bcdbc18c94cdd->enter($__internal_bea7392002c1b0f44fd64c3aefa812f7a4630db71cb9e890267bcdbc18c94cdd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 17
        echo "    ";
        $this->displayParentBlock("body", $context, $blocks);
        echo "


";
        // line 21
        echo "<ol class=\"breadcrumb\">
    <li><a href=\"";
        // line 22
        echo $this->env->getExtension('routing')->getPath("store");
        echo "\">Home</a></li>
    <li class=\"active\">Receipt</li>
</ol>



<main class=\"reciept container-fluid\">

    <div class=\"row\">
        <div class=\"col-md-6 col-sm-6 col-sm-offset-3\">



            <button class=\"btn btn-info\" onclick=\"window.print()\">Print for your records</button>

            <div class=\"page-header\">
                <h1>Receipt</h1>
                <small>Print and keep for your records</small>
            </div>

            <table class=\"table table-striped\">
                <tr>
                    <th>Item</th>
                    <th>Item ID#</th>
                    <th>Qty.</th>
                    <th>Price</th>
                </tr>

                ";
        // line 50
        $context["count"] = 0;
        // line 51
        echo "                ";
        $context["total"] = 0;
        // line 52
        echo "
                ";
        // line 53
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["items"]) ? $context["items"] : $this->getContext($context, "items")));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 54
            echo "                    ";
            $context["count"] = ((isset($context["count"]) ? $context["count"] : $this->getContext($context, "count")) + $this->getAttribute($context["item"], "quantity", array()));
            // line 55
            echo "                    ";
            $context["total"] = ((isset($context["total"]) ? $context["total"] : $this->getContext($context, "total")) + ($this->getAttribute($this->getAttribute($context["item"], "item", array()), "price", array()) * $this->getAttribute($context["item"], "quantity", array())));
            // line 56
            echo "
                    <tr>
                        <td>
                            <p >
                                ";
            // line 60
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "item", array()), "itemName", array()), "html", null, true);
            echo "
                            </p>
                        </td>

                        <td>
                            <p>";
            // line 65
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "item", array()), "id", array()), "html", null, true);
            echo "</p>
                        </td>

                        <td>
                            <p>";
            // line 69
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "quantity", array()), "html", null, true);
            echo " x \$";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "item", array()), "price", array()), 2, ".", ","), "html", null, true);
            echo "</p>
                        </td>

                        <td>
                            <p>\$";
            // line 73
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ($this->getAttribute($this->getAttribute($context["item"], "item", array()), "price", array()) * $this->getAttribute($context["item"], "quantity", array())), 2, ".", ","), "html", null, true);
            echo "</p>
                        </td>

                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 78
        echo "            </table>

            <table class=\"table table-striped\">
                <tr>
                    <td>
                        <p>Total items bought: ";
        // line 83
        echo twig_escape_filter($this->env, (isset($context["count"]) ? $context["count"] : $this->getContext($context, "count")), "html", null, true);
        echo "</p>
                    </td>

                    <td>
                        <p>Total: \$";
        // line 87
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["total"]) ? $context["total"] : $this->getContext($context, "total")), 2, ".", ","), "html", null, true);
        echo "</p>
                    </td>

                </tr>
                <tr>
                    <td>
                        <p>Invoice Receipt ID: <span class=\"text-danger\">";
        // line 93
        echo twig_escape_filter($this->env, (isset($context["invoiceId"]) ? $context["invoiceId"] : $this->getContext($context, "invoiceId")), "html", null, true);
        echo "</span></p>
                    </td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>

</main>






";
        
        $__internal_bea7392002c1b0f44fd64c3aefa812f7a4630db71cb9e890267bcdbc18c94cdd->leave($__internal_bea7392002c1b0f44fd64c3aefa812f7a4630db71cb9e890267bcdbc18c94cdd_prof);

    }

    // line 109
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_a6bca655d0bd85fa3a04a23ca08ea1ff407887b3566e01bd6ef16380ef42872a = $this->env->getExtension("native_profiler");
        $__internal_a6bca655d0bd85fa3a04a23ca08ea1ff407887b3566e01bd6ef16380ef42872a->enter($__internal_a6bca655d0bd85fa3a04a23ca08ea1ff407887b3566e01bd6ef16380ef42872a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        // line 110
        echo "    ";
        $this->displayParentBlock("javascript", $context, $blocks);
        echo "

";
        
        $__internal_a6bca655d0bd85fa3a04a23ca08ea1ff407887b3566e01bd6ef16380ef42872a->leave($__internal_a6bca655d0bd85fa3a04a23ca08ea1ff407887b3566e01bd6ef16380ef42872a_prof);

    }

    public function getTemplateName()
    {
        return "store/_printableReceipt.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  231 => 110,  225 => 109,  202 => 93,  193 => 87,  186 => 83,  179 => 78,  168 => 73,  159 => 69,  152 => 65,  144 => 60,  138 => 56,  135 => 55,  132 => 54,  128 => 53,  125 => 52,  122 => 51,  120 => 50,  89 => 22,  86 => 21,  79 => 17,  73 => 16,  62 => 13,  56 => 12,  43 => 7,  37 => 6,  11 => 4,);
    }
}
/* */
/* */
/* */
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block title %}*/
/*     {{ parent() }}*/
/* */
/*     - Receipt*/
/* */
/* {% endblock %}*/
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/* */
/* {% endblock %}*/
/* {% block body %}*/
/*     {{ parent() }}*/
/* */
/* */
/* {# breadcrumbs #}*/
/* <ol class="breadcrumb">*/
/*     <li><a href="{{ path('store') }}">Home</a></li>*/
/*     <li class="active">Receipt</li>*/
/* </ol>*/
/* */
/* */
/* */
/* <main class="reciept container-fluid">*/
/* */
/*     <div class="row">*/
/*         <div class="col-md-6 col-sm-6 col-sm-offset-3">*/
/* */
/* */
/* */
/*             <button class="btn btn-info" onclick="window.print()">Print for your records</button>*/
/* */
/*             <div class="page-header">*/
/*                 <h1>Receipt</h1>*/
/*                 <small>Print and keep for your records</small>*/
/*             </div>*/
/* */
/*             <table class="table table-striped">*/
/*                 <tr>*/
/*                     <th>Item</th>*/
/*                     <th>Item ID#</th>*/
/*                     <th>Qty.</th>*/
/*                     <th>Price</th>*/
/*                 </tr>*/
/* */
/*                 {% set count = 0 %}*/
/*                 {% set total = 0.00 %}*/
/* */
/*                 {% for item in items %}*/
/*                     {% set count = count + item.quantity %}*/
/*                     {% set total = total + (item.item.price * item.quantity) %}*/
/* */
/*                     <tr>*/
/*                         <td>*/
/*                             <p >*/
/*                                 {{ item.item.itemName }}*/
/*                             </p>*/
/*                         </td>*/
/* */
/*                         <td>*/
/*                             <p>{{ item.item.id }}</p>*/
/*                         </td>*/
/* */
/*                         <td>*/
/*                             <p>{{item.quantity }} x ${{ item.item.price | number_format(2, '.', ',')  }}</p>*/
/*                         </td>*/
/* */
/*                         <td>*/
/*                             <p>${{ (item.item.price * item.quantity) | number_format(2, '.', ',') }}</p>*/
/*                         </td>*/
/* */
/*                     </tr>*/
/*                 {% endfor %}*/
/*             </table>*/
/* */
/*             <table class="table table-striped">*/
/*                 <tr>*/
/*                     <td>*/
/*                         <p>Total items bought: {{ count }}</p>*/
/*                     </td>*/
/* */
/*                     <td>*/
/*                         <p>Total: ${{ total | number_format(2, '.', ',') }}</p>*/
/*                     </td>*/
/* */
/*                 </tr>*/
/*                 <tr>*/
/*                     <td>*/
/*                         <p>Invoice Receipt ID: <span class="text-danger">{{ invoiceId }}</span></p>*/
/*                     </td>*/
/*                     <td></td>*/
/*                 </tr>*/
/*             </table>*/
/*         </div>*/
/*     </div>*/
/* */
/* </main>*/
/* */
/* */
/* */
/* */
/* */
/* */
/* {% endblock %}*/
/* {% block javascript %}*/
/*     {{ parent() }}*/
/* */
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
