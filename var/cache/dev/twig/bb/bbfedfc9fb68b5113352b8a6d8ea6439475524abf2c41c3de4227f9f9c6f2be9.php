<?php

/* vendor/vendorQue/queList.html.twig */
class __TwigTemplate_f571aabb4cd4690ddc9c3cbafb5cb874a2363c3d4ccd142dc8e7991f8a729df5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "vendor/vendorQue/queList.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascript' => array($this, 'block_javascript'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d1a31d7c73d49c64c8f450139242149d3b15e3d2601e17fc111d5c33f4fe0a04 = $this->env->getExtension("native_profiler");
        $__internal_d1a31d7c73d49c64c8f450139242149d3b15e3d2601e17fc111d5c33f4fe0a04->enter($__internal_d1a31d7c73d49c64c8f450139242149d3b15e3d2601e17fc111d5c33f4fe0a04_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "vendor/vendorQue/queList.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d1a31d7c73d49c64c8f450139242149d3b15e3d2601e17fc111d5c33f4fe0a04->leave($__internal_d1a31d7c73d49c64c8f450139242149d3b15e3d2601e17fc111d5c33f4fe0a04_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_124f31712856349564369f46b5b05054e45d3eb09dd51dca2b82f250503e1587 = $this->env->getExtension("native_profiler");
        $__internal_124f31712856349564369f46b5b05054e45d3eb09dd51dca2b82f250503e1587->enter($__internal_124f31712856349564369f46b5b05054e45d3eb09dd51dca2b82f250503e1587_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo "
    - Que
";
        
        $__internal_124f31712856349564369f46b5b05054e45d3eb09dd51dca2b82f250503e1587->leave($__internal_124f31712856349564369f46b5b05054e45d3eb09dd51dca2b82f250503e1587_prof);

    }

    // line 8
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_511eb041ef6a2c10c94b36ae7aef914c36dd08eaf4b5f08c5cc582ad56255a44 = $this->env->getExtension("native_profiler");
        $__internal_511eb041ef6a2c10c94b36ae7aef914c36dd08eaf4b5f08c5cc582ad56255a44->enter($__internal_511eb041ef6a2c10c94b36ae7aef914c36dd08eaf4b5f08c5cc582ad56255a44_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 9
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
        
        $__internal_511eb041ef6a2c10c94b36ae7aef914c36dd08eaf4b5f08c5cc582ad56255a44->leave($__internal_511eb041ef6a2c10c94b36ae7aef914c36dd08eaf4b5f08c5cc582ad56255a44_prof);

    }

    // line 12
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_7d1fdba2e4196d38f18c5ee9017f28f4ea550807b8b88137a10e2e43659e964a = $this->env->getExtension("native_profiler");
        $__internal_7d1fdba2e4196d38f18c5ee9017f28f4ea550807b8b88137a10e2e43659e964a->enter($__internal_7d1fdba2e4196d38f18c5ee9017f28f4ea550807b8b88137a10e2e43659e964a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        // line 13
        echo "    ";
        $this->displayParentBlock("javascript", $context, $blocks);
        echo "
";
        
        $__internal_7d1fdba2e4196d38f18c5ee9017f28f4ea550807b8b88137a10e2e43659e964a->leave($__internal_7d1fdba2e4196d38f18c5ee9017f28f4ea550807b8b88137a10e2e43659e964a_prof);

    }

    // line 21
    public function block_body($context, array $blocks = array())
    {
        $__internal_707a3af70dadb2ea89369d3145041f901976e27fbb602f247928e081b22aab12 = $this->env->getExtension("native_profiler");
        $__internal_707a3af70dadb2ea89369d3145041f901976e27fbb602f247928e081b22aab12->enter($__internal_707a3af70dadb2ea89369d3145041f901976e27fbb602f247928e081b22aab12_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 22
        echo "    ";
        $this->displayParentBlock("body", $context, $blocks);
        echo "

    ";
        // line 25
        echo "    ";
        // line 26
        echo "        ";
        // line 27
        echo "        ";
        // line 28
        echo "    ";
        // line 29
        echo "
<main class=\"container que\">





    <div class=\"row\">

        <h1 class=\"col-lg-offset-4 col-md-offset-3 col-sm-offset-3 col-lg-4 col-md-6 col-sm-6\">Your order que</h1>

        <ul class=\"list-group\">
            ";
        // line 41
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["queList"]) ? $context["queList"] : $this->getContext($context, "queList")));
        foreach ($context['_seq'] as $context["_key"] => $context["queOrder"]) {
            // line 42
            echo "
                <li class=\"list-group-item col-xs-6 col-xs-offset-3\">

                    <span class=\"badge\">";
            // line 45
            echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute($context["queOrder"], "order", array())), "html", null, true);
            echo "</span>

                    Customer: <a href=\"";
            // line 47
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("que_order", array("customerId" => $this->getAttribute($this->getAttribute($context["queOrder"], "customer", array()), "id", array()))), "html", null, true);
            echo "\">
                        <p>";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["queOrder"], "customer", array()), "firstName", array()), "html", null, true);
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["queOrder"], "customer", array()), "lastName", array()), "html", null, true);
            echo "</p>
                    </a>

                </li>

                ";
            // line 54
            echo "
                        ";
            // line 56
            echo "
                    ";
            // line 58
            echo "
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['queOrder'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 60
        echo "        </ul>
    </div>










    ";
        // line 73
        echo "        ";
        // line 74
        echo "
            ";
        // line 76
        echo "
                ";
        // line 78
        echo "
            ";
        // line 80
        echo "
        ";
        // line 82
        echo "    ";
        // line 83
        echo "












</main>
";
        
        $__internal_707a3af70dadb2ea89369d3145041f901976e27fbb602f247928e081b22aab12->leave($__internal_707a3af70dadb2ea89369d3145041f901976e27fbb602f247928e081b22aab12_prof);

    }

    public function getTemplateName()
    {
        return "vendor/vendorQue/queList.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 83,  189 => 82,  186 => 80,  183 => 78,  180 => 76,  177 => 74,  175 => 73,  161 => 60,  154 => 58,  151 => 56,  148 => 54,  138 => 48,  134 => 47,  129 => 45,  124 => 42,  120 => 41,  106 => 29,  104 => 28,  102 => 27,  100 => 26,  98 => 25,  92 => 22,  86 => 21,  76 => 13,  70 => 12,  60 => 9,  54 => 8,  43 => 4,  37 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block title %}*/
/*     {{ parent() }}*/
/*     - Que*/
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/* {% endblock %}*/
/* */
/* {% block javascript %}*/
/*     {{ parent() }}*/
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* */
/* */
/* {% block body %}*/
/*     {{ parent() }}*/
/* */
/*     {# breadcrumbs #}*/
/*     {#<ol class="breadcrumb">#}*/
/*         {#<li><a href="{{ path('home') }}">Home</a></li>#}*/
/*         {#<li class="active">Registration</li>#}*/
/*     {#</ol>#}*/
/* */
/* <main class="container que">*/
/* */
/* */
/* */
/* */
/* */
/*     <div class="row">*/
/* */
/*         <h1 class="col-lg-offset-4 col-md-offset-3 col-sm-offset-3 col-lg-4 col-md-6 col-sm-6">Your order que</h1>*/
/* */
/*         <ul class="list-group">*/
/*             {% for queOrder in queList %}*/
/* */
/*                 <li class="list-group-item col-xs-6 col-xs-offset-3">*/
/* */
/*                     <span class="badge">{{ queOrder.order | length }}</span>*/
/* */
/*                     Customer: <a href="{{ path('que_order',{'customerId' : queOrder.customer.id}) }}">*/
/*                         <p>{{ queOrder.customer.firstName }}  {{ queOrder.customer.lastName }}</p>*/
/*                     </a>*/
/* */
/*                 </li>*/
/* */
/*                 {#<li><a href="{{ path('que_order',{'customerId' : queOrder.customer.id}) }}">#}*/
/* */
/*                         {#<p>{{ queOrder.customer.firstName }}</p>#}*/
/* */
/*                     {#</a> </li>#}*/
/* */
/*             {% endfor %}*/
/*         </ul>*/
/*     </div>*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/*     {#<ul>#}*/
/*         {#{% for que in queList %}#}*/
/* */
/*             {#<li><a href="{{ path('que_order',{'customerId' : que.customer.id}) }}">#}*/
/* */
/*                 {#<p>{{ que.customer.firstName }}</p>#}*/
/* */
/*             {#</a> </li>#}*/
/* */
/*         {#{% endfor %}#}*/
/*     {#</ul>#}*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* </main>*/
/* {% endblock %}*/
/* */
/* */
/* */
