<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_cdaca7d96b7c0a90e87c8415d78916d869c11154a3d5935ef7e59446f8eae247 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_853901b70afcd1677deb9efdc07badc333442fe7553e230265e9f4357585578e = $this->env->getExtension("native_profiler");
        $__internal_853901b70afcd1677deb9efdc07badc333442fe7553e230265e9f4357585578e->enter($__internal_853901b70afcd1677deb9efdc07badc333442fe7553e230265e9f4357585578e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_853901b70afcd1677deb9efdc07badc333442fe7553e230265e9f4357585578e->leave($__internal_853901b70afcd1677deb9efdc07badc333442fe7553e230265e9f4357585578e_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_a4974d3c6fdf41bde5d78a4fac23fd036876ea44da67fc5835bb65a84d4ff06c = $this->env->getExtension("native_profiler");
        $__internal_a4974d3c6fdf41bde5d78a4fac23fd036876ea44da67fc5835bb65a84d4ff06c->enter($__internal_a4974d3c6fdf41bde5d78a4fac23fd036876ea44da67fc5835bb65a84d4ff06c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_a4974d3c6fdf41bde5d78a4fac23fd036876ea44da67fc5835bb65a84d4ff06c->leave($__internal_a4974d3c6fdf41bde5d78a4fac23fd036876ea44da67fc5835bb65a84d4ff06c_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }
}
/* {% block panel '' %}*/
/* */
