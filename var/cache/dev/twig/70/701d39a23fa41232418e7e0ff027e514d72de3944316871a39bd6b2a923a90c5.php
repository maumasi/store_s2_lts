<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_bc964fedfba0014f123d9c389155bdd14973af6a4037852fd0285d08b230eda0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_185c94767ab70e803b73a6a52c06aa2c49b2b45b8c550f09829c2cb3af1af299 = $this->env->getExtension("native_profiler");
        $__internal_185c94767ab70e803b73a6a52c06aa2c49b2b45b8c550f09829c2cb3af1af299->enter($__internal_185c94767ab70e803b73a6a52c06aa2c49b2b45b8c550f09829c2cb3af1af299_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_185c94767ab70e803b73a6a52c06aa2c49b2b45b8c550f09829c2cb3af1af299->leave($__internal_185c94767ab70e803b73a6a52c06aa2c49b2b45b8c550f09829c2cb3af1af299_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php foreach ($form as $child) : ?>*/
/*     <?php echo $view['form']->row($child) ?>*/
/* <?php endforeach; ?>*/
/* */
