<?php

/* :store:home.html.twig */
class __TwigTemplate_86d7719534573b15b72a68d8cf4a09108cd2de4b64fbce1570cb25ba80287257 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":store:home.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a5f4cdd20b87750b54a8cebc0b20a3f087471d62616c41f48c7f81e65b62e787 = $this->env->getExtension("native_profiler");
        $__internal_a5f4cdd20b87750b54a8cebc0b20a3f087471d62616c41f48c7f81e65b62e787->enter($__internal_a5f4cdd20b87750b54a8cebc0b20a3f087471d62616c41f48c7f81e65b62e787_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":store:home.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a5f4cdd20b87750b54a8cebc0b20a3f087471d62616c41f48c7f81e65b62e787->leave($__internal_a5f4cdd20b87750b54a8cebc0b20a3f087471d62616c41f48c7f81e65b62e787_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_7e57ff3574a7a70dc401ec1a0ed5e1b1383000db933ced67d3eb9ec9bc9ffcdb = $this->env->getExtension("native_profiler");
        $__internal_7e57ff3574a7a70dc401ec1a0ed5e1b1383000db933ced67d3eb9ec9bc9ffcdb->enter($__internal_7e57ff3574a7a70dc401ec1a0ed5e1b1383000db933ced67d3eb9ec9bc9ffcdb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo "

    - Home Page

";
        
        $__internal_7e57ff3574a7a70dc401ec1a0ed5e1b1383000db933ced67d3eb9ec9bc9ffcdb->leave($__internal_7e57ff3574a7a70dc401ec1a0ed5e1b1383000db933ced67d3eb9ec9bc9ffcdb_prof);

    }

    // line 9
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_a0048daf38eea7b33d61f0e3b4c908ab231a0c768948a2d709afb90e7968c996 = $this->env->getExtension("native_profiler");
        $__internal_a0048daf38eea7b33d61f0e3b4c908ab231a0c768948a2d709afb90e7968c996->enter($__internal_a0048daf38eea7b33d61f0e3b4c908ab231a0c768948a2d709afb90e7968c996_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 10
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "


";
        
        $__internal_a0048daf38eea7b33d61f0e3b4c908ab231a0c768948a2d709afb90e7968c996->leave($__internal_a0048daf38eea7b33d61f0e3b4c908ab231a0c768948a2d709afb90e7968c996_prof);

    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        $__internal_08d324be730db3f268c7a8b53b10705eccfa5555008bb1fe254ebbb350842aff = $this->env->getExtension("native_profiler");
        $__internal_08d324be730db3f268c7a8b53b10705eccfa5555008bb1fe254ebbb350842aff->enter($__internal_08d324be730db3f268c7a8b53b10705eccfa5555008bb1fe254ebbb350842aff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 15
        echo "    ";
        $this->displayParentBlock("body", $context, $blocks);
        echo "





    ";
        // line 22
        echo "
        ";
        // line 24
        echo "
    ";
        // line 26
        echo "


    <main class=\"container-fluid\">

    </main>







    <div>
        <ul>
            <li>
                <a href=\"#\">
                    About The Bladesmith's Workbench
                </a>
            </li>

            <li>
                <a href=\"";
        // line 48
        echo $this->env->getExtension('routing')->getPath("store");
        echo "\">
                    Knife Gallery
                </a>
            </li>

            <li>
                <a href=\"";
        // line 54
        echo $this->env->getExtension('routing')->getPath("vendor_register");
        echo "\">
                    Regiser To Become A Vendor
                </a>
            </li>
        </ul>
    </div>





";
        // line 66
        echo "    ";
        // line 67
        echo "
";
        // line 69
        echo "


";
        
        $__internal_08d324be730db3f268c7a8b53b10705eccfa5555008bb1fe254ebbb350842aff->leave($__internal_08d324be730db3f268c7a8b53b10705eccfa5555008bb1fe254ebbb350842aff_prof);

    }

    public function getTemplateName()
    {
        return ":store:home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  147 => 69,  144 => 67,  142 => 66,  128 => 54,  119 => 48,  95 => 26,  92 => 24,  89 => 22,  79 => 15,  73 => 14,  61 => 10,  55 => 9,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block title %}*/
/*     {{ parent() }}*/
/* */
/*     - Home Page*/
/* */
/* {% endblock %}*/
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/* */
/* */
/* {% endblock %}*/
/* {% block body %}*/
/*     {{ parent() }}*/
/* */
/* */
/* */
/* */
/* */
/*     {#<div class="row">#}*/
/* */
/*         {#<p class="col-md-6 col-md-offset-4"> Are you a bladesmith? <a class="btn btn-primary btn-xs" href="#" role="button">Register to become a vendor</a></p>#}*/
/* */
/*     {#</div>#}*/
/* */
/* */
/* */
/*     <main class="container-fluid">*/
/* */
/*     </main>*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/*     <div>*/
/*         <ul>*/
/*             <li>*/
/*                 <a href="#">*/
/*                     About The Bladesmith's Workbench*/
/*                 </a>*/
/*             </li>*/
/* */
/*             <li>*/
/*                 <a href="{{ path('store') }}">*/
/*                     Knife Gallery*/
/*                 </a>*/
/*             </li>*/
/* */
/*             <li>*/
/*                 <a href="{{ path('vendor_register') }}">*/
/*                     Regiser To Become A Vendor*/
/*                 </a>*/
/*             </li>*/
/*         </ul>*/
/*     </div>*/
/* */
/* */
/* */
/* */
/* */
/* {#{% block footer %}#}*/
/*     {#{{ parent() }}#}*/
/* */
/* {#{% endblock %}#}*/
/* */
/* */
/* */
/* {% endblock %}*/
/* {#{% block javascript %}#}*/
/* */
/* */
/*     {#{{ parent() }}#}*/
/* {#{% endblock %}#}*/
