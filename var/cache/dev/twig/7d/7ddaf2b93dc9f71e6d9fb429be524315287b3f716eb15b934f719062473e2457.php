<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_cf3c2336ee1c372ff8236975bbeaca435b42b80ed93ea97f2567448bea02d944 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dc751a20639181d19578bb78dd24a4a6c84a937b3c42f006282eba7631b2b828 = $this->env->getExtension("native_profiler");
        $__internal_dc751a20639181d19578bb78dd24a4a6c84a937b3c42f006282eba7631b2b828->enter($__internal_dc751a20639181d19578bb78dd24a4a6c84a937b3c42f006282eba7631b2b828_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_dc751a20639181d19578bb78dd24a4a6c84a937b3c42f006282eba7631b2b828->leave($__internal_dc751a20639181d19578bb78dd24a4a6c84a937b3c42f006282eba7631b2b828_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'search')) ?>*/
/* */
