<?php

/* vendor/vendorItemViews/itemDeleteConfirm.html.twig */
class __TwigTemplate_1f323015ce8982163c27949f4855dd7f380ecccbb5af12cfd702a728e2b6d4f9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "vendor/vendorItemViews/itemDeleteConfirm.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'javascript' => array($this, 'block_javascript'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dcae83a4f1a031362ef7800e33f5fd2aea24d6390ef7eea86d0ae6aa431e718d = $this->env->getExtension("native_profiler");
        $__internal_dcae83a4f1a031362ef7800e33f5fd2aea24d6390ef7eea86d0ae6aa431e718d->enter($__internal_dcae83a4f1a031362ef7800e33f5fd2aea24d6390ef7eea86d0ae6aa431e718d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "vendor/vendorItemViews/itemDeleteConfirm.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_dcae83a4f1a031362ef7800e33f5fd2aea24d6390ef7eea86d0ae6aa431e718d->leave($__internal_dcae83a4f1a031362ef7800e33f5fd2aea24d6390ef7eea86d0ae6aa431e718d_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_5d654d95ec5416f851b4954441329a68ca6e28b531ae543b8f8b92b56429ea3c = $this->env->getExtension("native_profiler");
        $__internal_5d654d95ec5416f851b4954441329a68ca6e28b531ae543b8f8b92b56429ea3c->enter($__internal_5d654d95ec5416f851b4954441329a68ca6e28b531ae543b8f8b92b56429ea3c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 5
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo "

    - app.user.first_name

";
        
        $__internal_5d654d95ec5416f851b4954441329a68ca6e28b531ae543b8f8b92b56429ea3c->leave($__internal_5d654d95ec5416f851b4954441329a68ca6e28b531ae543b8f8b92b56429ea3c_prof);

    }

    // line 11
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_25fa992d1ca71a1c09edf8af2384c4d1c5f09d8948894d9f6ec70ff41e2287d3 = $this->env->getExtension("native_profiler");
        $__internal_25fa992d1ca71a1c09edf8af2384c4d1c5f09d8948894d9f6ec70ff41e2287d3->enter($__internal_25fa992d1ca71a1c09edf8af2384c4d1c5f09d8948894d9f6ec70ff41e2287d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        // line 12
        echo "     ";
        $this->displayParentBlock("javascript", $context, $blocks);
        echo "


 ";
        
        $__internal_25fa992d1ca71a1c09edf8af2384c4d1c5f09d8948894d9f6ec70ff41e2287d3->leave($__internal_25fa992d1ca71a1c09edf8af2384c4d1c5f09d8948894d9f6ec70ff41e2287d3_prof);

    }

    // line 17
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_6303d72dc2df7fe35a4aade6bddb82c9f54404cb884fb3f299fcb2be9f9d9264 = $this->env->getExtension("native_profiler");
        $__internal_6303d72dc2df7fe35a4aade6bddb82c9f54404cb884fb3f299fcb2be9f9d9264->enter($__internal_6303d72dc2df7fe35a4aade6bddb82c9f54404cb884fb3f299fcb2be9f9d9264_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 18
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

";
        
        $__internal_6303d72dc2df7fe35a4aade6bddb82c9f54404cb884fb3f299fcb2be9f9d9264->leave($__internal_6303d72dc2df7fe35a4aade6bddb82c9f54404cb884fb3f299fcb2be9f9d9264_prof);

    }

    // line 21
    public function block_body($context, array $blocks = array())
    {
        $__internal_705a96c3eb1181204aa6ef6227a553320cf51b0f91991f001cfcae31ee4a35b7 = $this->env->getExtension("native_profiler");
        $__internal_705a96c3eb1181204aa6ef6227a553320cf51b0f91991f001cfcae31ee4a35b7->enter($__internal_705a96c3eb1181204aa6ef6227a553320cf51b0f91991f001cfcae31ee4a35b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 22
        echo "    ";
        $this->displayParentBlock("body", $context, $blocks);
        echo "

<main class=\"delete-item-review container\">

    ";
        // line 26
        if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "has", array(0 => "stillHasOrders"), "method")) {
            // line 27
            echo "
        <div class=\"alert alert-danger\">

            ";
            // line 30
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "stillHasOrders"), "method"));
            foreach ($context['_seq'] as $context["_key"] => $context["msg"]) {
                // line 31
                echo "            <p>";
                echo twig_escape_filter($this->env, $context["msg"], "html", null, true);
                echo "</p>

            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['msg'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 34
            echo "
        </div>
    ";
        }
        // line 37
        echo "


    <div class=\"vendor-delete-item \">

        <a class=\"row \" href=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("single_item", array("itemObj" => $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "id", array()))), "html", null, true);
        echo "\">

            ";
        // line 45
        echo "            <div class=\"item-img col-md-2 col-sm-4 col-xs-6 col-xs-offset-2\"
                 style=\"
                         background: url( ";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl(("uploads/" . $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "itemImg", array()))), "html", null, true);
        echo " ) no-repeat;
                         /*background-repeat: ;*/
                         background-size: cover;
                         background-position: center;
                         background-color: #fff;
                         border: 1px solid rgba(0,0,0,0.38);


                         \">

                ";
        // line 57
        if (($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "quantity", array()) == 0)) {
            // line 58
            echo "
                    <p class=\"out-of-stock \">Currently Out Of Stock</p>
                ";
        }
        // line 61
        echo "

            </div>

            <div class=\"item-name col-xs-2\">
                <p>";
        // line 66
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "itemName", array()), "html", null, true);
        echo "</p>
                <p>\$";
        // line 67
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "price", array()), "html", null, true);
        echo "</p>
                ";
        // line 69
        echo "            </div>
        </a>

        <div class=\"row\">

            <div class=\"item-action col-xs-2 col-xs-offset-2\">
                <a href=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("vendor_item_delete", array("id" => $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "id", array()))), "html", null, true);
        echo "\" class=\"btn btn-danger\" >Delete</a>
            </div>

            <div class=\"item-action col-xs-3\">
                <a href=\"";
        // line 79
        echo $this->env->getExtension('routing')->getPath("vendor_item_collection");
        echo "\" class=\"btn cancel-delete\" >Cancel</a>
            </div>

        </div>



    </div>











    ";
        // line 99
        echo "
    ";
        // line 101
        echo "




</main>

";
        
        $__internal_705a96c3eb1181204aa6ef6227a553320cf51b0f91991f001cfcae31ee4a35b7->leave($__internal_705a96c3eb1181204aa6ef6227a553320cf51b0f91991f001cfcae31ee4a35b7_prof);

    }

    public function getTemplateName()
    {
        return "vendor/vendorItemViews/itemDeleteConfirm.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  222 => 101,  219 => 99,  197 => 79,  190 => 75,  182 => 69,  178 => 67,  174 => 66,  167 => 61,  162 => 58,  160 => 57,  147 => 47,  143 => 45,  138 => 42,  131 => 37,  126 => 34,  116 => 31,  112 => 30,  107 => 27,  105 => 26,  97 => 22,  91 => 21,  80 => 18,  74 => 17,  62 => 12,  56 => 11,  43 => 5,  37 => 4,  11 => 2,);
    }
}
/* */
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block title %}*/
/*     {{ parent() }}*/
/* */
/*     - app.user.first_name*/
/* */
/* {% endblock %}*/
/* */
/*  {% block javascript %}*/
/*      {{ parent() }}*/
/* */
/* */
/*  {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/* */
/* {% endblock %}*/
/* {% block body %}*/
/*     {{ parent() }}*/
/* */
/* <main class="delete-item-review container">*/
/* */
/*     {% if app.session.flashbag.has('stillHasOrders') %}*/
/* */
/*         <div class="alert alert-danger">*/
/* */
/*             {% for msg in app.session.flashbag.get('stillHasOrders') %}*/
/*             <p>{{ msg }}</p>*/
/* */
/*             {% endfor %}*/
/* */
/*         </div>*/
/*     {% endif %}*/
/* */
/* */
/* */
/*     <div class="vendor-delete-item ">*/
/* */
/*         <a class="row " href="{{ path('single_item', { 'itemObj' : item.id }) }}">*/
/* */
/*             {#<img class="img-responsive center-block item-img" align="middle" src="{{ asset('uploads/' ~ item.itemImg ) }}" alt="{{ item.itemName }}"/>#}*/
/*             <div class="item-img col-md-2 col-sm-4 col-xs-6 col-xs-offset-2"*/
/*                  style="*/
/*                          background: url( {{ asset('uploads/' ~ item.itemImg) }} ) no-repeat;*/
/*                          /*background-repeat: ;*//* */
/*                          background-size: cover;*/
/*                          background-position: center;*/
/*                          background-color: #fff;*/
/*                          border: 1px solid rgba(0,0,0,0.38);*/
/* */
/* */
/*                          ">*/
/* */
/*                 {% if item.quantity == 0 %}*/
/* */
/*                     <p class="out-of-stock ">Currently Out Of Stock</p>*/
/*                 {% endif %}*/
/* */
/* */
/*             </div>*/
/* */
/*             <div class="item-name col-xs-2">*/
/*                 <p>{{ item.itemName }}</p>*/
/*                 <p>${{ item.price }}</p>*/
/*                 {#<p>{{ asset("uploads/" ~ item.itemImg ) }}</p>#}*/
/*             </div>*/
/*         </a>*/
/* */
/*         <div class="row">*/
/* */
/*             <div class="item-action col-xs-2 col-xs-offset-2">*/
/*                 <a href="{{ path('vendor_item_delete', {'id' : item.id}) }}" class="btn btn-danger" >Delete</a>*/
/*             </div>*/
/* */
/*             <div class="item-action col-xs-3">*/
/*                 <a href="{{ path('vendor_item_collection') }}" class="btn cancel-delete" >Cancel</a>*/
/*             </div>*/
/* */
/*         </div>*/
/* */
/* */
/* */
/*     </div>*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/*     {#<p>{{ app.user.firstName }}</p>#}*/
/* */
/*     {#<p>{{ item.itemName }}</p>#}*/
/* */
/* */
/* */
/* */
/* */
/* </main>*/
/* */
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* */
/* */
