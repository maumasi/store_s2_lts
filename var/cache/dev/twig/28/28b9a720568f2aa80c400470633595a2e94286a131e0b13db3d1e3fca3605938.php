<?php

/* store/_baseNav.html.twig */
class __TwigTemplate_2fe4f47f0c07a111353b5aebfafa074b7216e630bdc07e219ca3e7c7225dc2ec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_07bac2dee6b37d89b4d509e5c2f74abd376b079bd8c70cfec26aef7744c78ea3 = $this->env->getExtension("native_profiler");
        $__internal_07bac2dee6b37d89b4d509e5c2f74abd376b079bd8c70cfec26aef7744c78ea3->enter($__internal_07bac2dee6b37d89b4d509e5c2f74abd376b079bd8c70cfec26aef7744c78ea3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "store/_baseNav.html.twig"));

        // line 1
        echo "
<header>
    <nav class=\"navbar navbar-inverse\">
        <div class=\"container-fluid\">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"";
        // line 14
        echo $this->env->getExtension('routing')->getPath("store");
        echo "\">The Bladesmith's Workbench</a>
            </div>


            ";
        // line 19
        echo "                ";
        // line 20
        echo "            ";
        // line 21
        echo "

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">




                <ul class=\"nav navbar-nav main-nav\">


                    ";
        // line 32
        if ($this->env->getExtension('security')->isGranted("ROLE_VENDOR")) {
            // line 33
            echo "
                        <li class=\"dropdown\">
                            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">Vendor: ";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "firstName", array()), "html", null, true);
            echo " <span class=\"caret\"></span></a>
                            <ul class=\"dropdown-menu\">

                                <li role=\"presentation\"><a href=\"";
            // line 38
            echo $this->env->getExtension('routing')->getPath("que_list");
            echo "\">Your Order Que</a></li>

                                ";
            // line 41
            echo "                                ";
            // line 42
            echo "
                                <li role=\"separator\" class=\"divider\"></li>
                                <li role=\"presentation\"><a href=\"";
            // line 44
            echo $this->env->getExtension('routing')->getPath("vendor_item_collection");
            echo "\">Your Items</a></li>

                                <li role=\"separator\" class=\"divider\"></li>
                                <li role=\"presentation\"><a href=\"";
            // line 47
            echo $this->env->getExtension('routing')->getPath("vendor_dashboard");
            echo "\">Vendor Dashboard</a></li>

                                <li role=\"separator\" class=\"divider\"></li>
                                <li role=\"presentation\"><a href=\"";
            // line 50
            echo $this->env->getExtension('routing')->getPath("vendor_edit");
            echo "\">Edit Vendor Info</a></li>

                                <li role=\"separator\" class=\"divider\"></li>
                                <li role=\"presentation\"><a href=\"";
            // line 53
            echo $this->env->getExtension('routing')->getPath("secure_logout");
            echo "\">Logout</a></li>


                            </ul>
                        </li>
                    ";
        } else {
            // line 59
            echo "                        ";
            // line 60
            echo "
                    ";
        }
        // line 62
        echo "



                    ";
        // line 66
        if ($this->env->getExtension('security')->isGranted("ROLE_CUSTOMER")) {
            // line 67
            echo "
                        <li class=\"dropdown\">
                            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">Welcome ";
            // line 69
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "firstName", array()), "html", null, true);
            echo " <span class=\"caret\"></span></a>
                            <ul class=\"dropdown-menu\">

                                <li role=\"presentation\"><a href=\"";
            // line 72
            echo $this->env->getExtension('routing')->getPath("guest_edit");
            echo "\">Edit Customer Info</a></li>
                                <li role=\"separator\" class=\"divider\"></li>
                                <li role=\"presentation\"><a href=\"";
            // line 74
            echo $this->env->getExtension('routing')->getPath("secure_logout");
            echo "\">Logout</a></li>
                            </ul>
                        </li>
                    ";
        } elseif (($this->env->getExtension('security')->isGranted("ROLE_VENDOR") == false)) {
            // line 78
            echo "
                        <li role=\"presentation\"><a href=\"";
            // line 79
            echo $this->env->getExtension('routing')->getPath("vendor_register");
            echo "\">Become a vendor</a></li>
                        <li role=\"presentation\"><a href=\"";
            // line 80
            echo $this->env->getExtension('routing')->getPath("guest_register");
            echo "\">Customer Registration</a></li>
                        <li role=\"presentation\"><a class=\"\" href=\"";
            // line 81
            echo $this->env->getExtension('routing')->getPath("secure_login");
            echo "\"><span class=\"label label-success\">Login</span></a></li>
                    ";
        }
        // line 83
        echo "
                    ";
        // line 84
        $context["count"] = 0;
        // line 85
        echo "                    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["cartCount"]) ? $context["cartCount"] : $this->getContext($context, "cartCount")));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 86
            echo "                        ";
            $context["count"] = ((isset($context["count"]) ? $context["count"] : $this->getContext($context, "count")) + $this->getAttribute($context["item"], "quantity", array()));
            // line 87
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 88
        echo "
                    <ul class=\"nav navbar-nav\">
                        <li class=\"cart customers-cart\"><a class=\"cart\" href=\"";
        // line 90
        echo $this->env->getExtension('routing')->getPath("store_checkout");
        echo "\">Cart <span class=\"glyphicon glyphicon-shopping-cart\"><span class=\"cart-count\">";
        echo twig_escape_filter($this->env, (isset($context["count"]) ? $context["count"] : $this->getContext($context, "count")), "html", null, true);
        echo "</span></span></a></li>
                    </ul>

                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
        ";
        // line 96
        echo twig_include($this->env, $context, "store/_cartList.html.twig");
        echo "
    </nav>
</header>























































";
        // line 155
        echo "        ";
        // line 156
        echo "
            ";
        // line 158
        echo "                ";
        // line 159
        echo "                    ";
        // line 160
        echo "                            ";
        // line 161
        echo "                            ";
        // line 162
        echo "                            ";
        // line 163
        echo "                            ";
        // line 164
        echo "
                        ";
        // line 166
        echo "                        ";
        // line 167
        echo "                        ";
        // line 168
        echo "                        ";
        // line 169
        echo "                    ";
        // line 170
        echo "
                    ";
        // line 172
        echo "                    ";
        // line 173
        echo "
                ";
        // line 175
        echo "

                ";
        // line 178
        echo "
                    ";
        // line 180
        echo "
                        ";
        // line 182
        echo "
                        ";
        // line 184
        echo "                            ";
        // line 185
        echo "                            ";
        // line 186
        echo "

                        ";
        // line 189
        echo "                            ";
        // line 190
        echo "                            ";
        // line 191
        echo "
                        ";
        // line 193
        echo "


                        ";
        // line 197
        echo "                        ";
        // line 198
        echo "                        ";
        // line 199
        echo "
                        ";
        // line 201
        echo "
                    ";
        // line 203
        echo "
                ";
        // line 205
        echo "


            ";
        // line 209
        echo "






        ";
        
        $__internal_07bac2dee6b37d89b4d509e5c2f74abd376b079bd8c70cfec26aef7744c78ea3->leave($__internal_07bac2dee6b37d89b4d509e5c2f74abd376b079bd8c70cfec26aef7744c78ea3_prof);

    }

    public function getTemplateName()
    {
        return "store/_baseNav.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  345 => 209,  340 => 205,  337 => 203,  334 => 201,  331 => 199,  329 => 198,  327 => 197,  322 => 193,  319 => 191,  317 => 190,  315 => 189,  311 => 186,  309 => 185,  307 => 184,  304 => 182,  301 => 180,  298 => 178,  294 => 175,  291 => 173,  289 => 172,  286 => 170,  284 => 169,  282 => 168,  280 => 167,  278 => 166,  275 => 164,  273 => 163,  271 => 162,  269 => 161,  267 => 160,  265 => 159,  263 => 158,  260 => 156,  258 => 155,  197 => 96,  186 => 90,  182 => 88,  176 => 87,  173 => 86,  168 => 85,  166 => 84,  163 => 83,  158 => 81,  154 => 80,  150 => 79,  147 => 78,  140 => 74,  135 => 72,  129 => 69,  125 => 67,  123 => 66,  117 => 62,  113 => 60,  111 => 59,  102 => 53,  96 => 50,  90 => 47,  84 => 44,  80 => 42,  78 => 41,  73 => 38,  67 => 35,  63 => 33,  61 => 32,  48 => 21,  46 => 20,  44 => 19,  37 => 14,  22 => 1,);
    }
}
/* */
/* <header>*/
/*     <nav class="navbar navbar-inverse">*/
/*         <div class="container-fluid">*/
/* */
/*             <!-- Brand and toggle get grouped for better mobile display -->*/
/*             <div class="navbar-header">*/
/*                 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">*/
/*                     <span class="sr-only">Toggle navigation</span>*/
/*                     <span class="icon-bar"></span>*/
/*                     <span class="icon-bar"></span>*/
/*                     <span class="icon-bar"></span>*/
/*                 </button>*/
/*                 <a class="navbar-brand" href="{{ path('store') }}">The Bladesmith's Workbench</a>*/
/*             </div>*/
/* */
/* */
/*             {#<ul class="nav navbar-nav">#}*/
/*                 {#<li class="cart"><a class="cart" href="#">Cart <span class="glyphicon glyphicon-shopping-cart"><span class="cart-count">6</span></span></a></li>#}*/
/*             {#</ul>#}*/
/* */
/* */
/*             <!-- Collect the nav links, forms, and other content for toggling -->*/
/*             <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">*/
/* */
/* */
/* */
/* */
/*                 <ul class="nav navbar-nav main-nav">*/
/* */
/* */
/*                     {% if is_granted('ROLE_VENDOR') %}*/
/* */
/*                         <li class="dropdown">*/
/*                             <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Vendor: {{ app.user.firstName }} <span class="caret"></span></a>*/
/*                             <ul class="dropdown-menu">*/
/* */
/*                                 <li role="presentation"><a href="{{ path('que_list')}}">Your Order Que</a></li>*/
/* */
/*                                 {#<li role="separator" class="divider"></li>#}*/
/*                                 {#<li role="presentation"><a href="{{ path('vendor_edit')}}">edit your profile</a></li>#}*/
/* */
/*                                 <li role="separator" class="divider"></li>*/
/*                                 <li role="presentation"><a href="{{ path('vendor_item_collection')}}">Your Items</a></li>*/
/* */
/*                                 <li role="separator" class="divider"></li>*/
/*                                 <li role="presentation"><a href="{{ path('vendor_dashboard') }}">Vendor Dashboard</a></li>*/
/* */
/*                                 <li role="separator" class="divider"></li>*/
/*                                 <li role="presentation"><a href="{{ path('vendor_edit') }}">Edit Vendor Info</a></li>*/
/* */
/*                                 <li role="separator" class="divider"></li>*/
/*                                 <li role="presentation"><a href="{{ path('secure_logout') }}">Logout</a></li>*/
/* */
/* */
/*                             </ul>*/
/*                         </li>*/
/*                     {% else %}*/
/*                         {#<li role="presentation"><a href="{{ path('secure_logout') }}">Logout</a></li>#}*/
/* */
/*                     {% endif %}*/
/* */
/* */
/* */
/* */
/*                     {% if is_granted('ROLE_CUSTOMER') %}*/
/* */
/*                         <li class="dropdown">*/
/*                             <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Welcome {{ app.user.firstName }} <span class="caret"></span></a>*/
/*                             <ul class="dropdown-menu">*/
/* */
/*                                 <li role="presentation"><a href="{{ path('guest_edit') }}">Edit Customer Info</a></li>*/
/*                                 <li role="separator" class="divider"></li>*/
/*                                 <li role="presentation"><a href="{{ path('secure_logout') }}">Logout</a></li>*/
/*                             </ul>*/
/*                         </li>*/
/*                     {% elseif is_granted('ROLE_VENDOR') == false %}*/
/* */
/*                         <li role="presentation"><a href="{{ path('vendor_register') }}">Become a vendor</a></li>*/
/*                         <li role="presentation"><a href="{{ path('guest_register') }}">Customer Registration</a></li>*/
/*                         <li role="presentation"><a class="" href="{{ path('secure_login') }}"><span class="label label-success">Login</span></a></li>*/
/*                     {% endif %}*/
/* */
/*                     {% set count = 0 %}*/
/*                     {% for item in cartCount %}*/
/*                         {% set count = count + item.quantity %}*/
/*                     {% endfor %}*/
/* */
/*                     <ul class="nav navbar-nav">*/
/*                         <li class="cart customers-cart"><a class="cart" href="{{ path('store_checkout') }}">Cart <span class="glyphicon glyphicon-shopping-cart"><span class="cart-count">{{ count }}</span></span></a></li>*/
/*                     </ul>*/
/* */
/*                 </ul>*/
/*             </div><!-- /.navbar-collapse -->*/
/*         </div><!-- /.container-fluid -->*/
/*         {{ include('store/_cartList.html.twig') }}*/
/*     </nav>*/
/* </header>*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* {#<header>#}*/
/*         {#<nav class="navbar navbar-inverse">#}*/
/* */
/*             {#<div class="container">#}*/
/*                 {#<div class="navbar-header">#}*/
/*                     {#<button type="button"#}*/
/*                             {#class="navbar-toggle collapse"#}*/
/*                             {#data-toggle="collapse"#}*/
/*                             {#data-target="#collapsemenu"#}*/
/*                             {#aria-expanded="false">#}*/
/* */
/*                         {#<span class="sr-only">Toggle nav</span>#}*/
/*                         {#<span class="icon-bar"></span>#}*/
/*                         {#<span class="icon-bar"></span>#}*/
/*                         {#<span class="icon-bar"></span>#}*/
/*                     {#</button>#}*/
/* */
/*                     {#logo#}*/
/*                     {#<h1 class="navbar-brand"> <a href="{{ path('home') }}">The Bladesmith's Workbench</a> </h1>#}*/
/* */
/*                 {#</div>#}*/
/* */
/* */
/*                 {#<div class="collapse navbar-collapse" id="collapsemenu">#}*/
/* */
/*                     {#<ul class="nav navbar-nav">#}*/
/* */
/*                         {# Link to a vendor login / logout #}*/
/* */
/*                         {#{% if is_granted('ROLE_CUSTOMER') || is_granted('ROLE_VENDOR') %}#}*/
/*                             {#<li role="presentation"><a href="{{ path('secure_logout') }}">Logout</a></li>#}*/
/*                             {#<li role="presentation"><a href="{{ path('vendor_dashboard') }}">Dashboard</a></li>#}*/
/* */
/* */
/*                         {#{% else %}#}*/
/*                             {#<li role="presentation"><a href="{{ path('secure_guest_login') }}">Guest Login</a></li>#}*/
/*                             {#<li role="presentation"><a href="{{ path('guest_register') }}">Guest Register</a></li>#}*/
/* */
/*                         {#{% endif %}#}*/
/* */
/* */
/* */
/*                         {#{% if is_granted('ROLE_VENDOR') %}#}*/
/*                         {# Link to check out #}*/
/*                         {#<li role="presentation" ><a href="{{ path('store_checkout') }}">Cart</a></li>#}*/
/* */
/*                         {#{% endif %}#}*/
/* */
/*                     {#</ul>#}*/
/* */
/*                 {#</div>#}*/
/* */
/* */
/* */
/*             {#</div>#}*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/*         {#</nav>#}*/
/* {#</header>#}*/
/* */
