<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_812966a77edae0457d200168a16cdd36892b13536717fdee9b2e4e459e62aecf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3ae6c47fcd77079d3fa49676be641492d0cb7d9ca64c8c0cf73314d2ec17d327 = $this->env->getExtension("native_profiler");
        $__internal_3ae6c47fcd77079d3fa49676be641492d0cb7d9ca64c8c0cf73314d2ec17d327->enter($__internal_3ae6c47fcd77079d3fa49676be641492d0cb7d9ca64c8c0cf73314d2ec17d327_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_3ae6c47fcd77079d3fa49676be641492d0cb7d9ca64c8c0cf73314d2ec17d327->leave($__internal_3ae6c47fcd77079d3fa49676be641492d0cb7d9ca64c8c0cf73314d2ec17d327_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
