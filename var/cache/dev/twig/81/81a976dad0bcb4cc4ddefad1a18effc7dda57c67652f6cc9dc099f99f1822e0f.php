<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_f37bd192c25458079f91c4fd484f1b1b088efaa57a0b1539b73e15efe62ee45e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4f1e7f44e7f9e928fb194c8d91c1e9dccd42fec64ed122d63ae6e02b93b56e9a = $this->env->getExtension("native_profiler");
        $__internal_4f1e7f44e7f9e928fb194c8d91c1e9dccd42fec64ed122d63ae6e02b93b56e9a->enter($__internal_4f1e7f44e7f9e928fb194c8d91c1e9dccd42fec64ed122d63ae6e02b93b56e9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_4f1e7f44e7f9e928fb194c8d91c1e9dccd42fec64ed122d63ae6e02b93b56e9a->leave($__internal_4f1e7f44e7f9e928fb194c8d91c1e9dccd42fec64ed122d63ae6e02b93b56e9a_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
