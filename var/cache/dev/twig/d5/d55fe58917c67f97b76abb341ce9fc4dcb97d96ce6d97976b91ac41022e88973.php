<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_29f8c29eaad39859b0815464606e5d8d4674957bee60c8ed6fce676dd385a300 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2c5c5a28c8d267446a2d6cb23e7b6d1715cf00bc8a768326e61de4d65966a421 = $this->env->getExtension("native_profiler");
        $__internal_2c5c5a28c8d267446a2d6cb23e7b6d1715cf00bc8a768326e61de4d65966a421->enter($__internal_2c5c5a28c8d267446a2d6cb23e7b6d1715cf00bc8a768326e61de4d65966a421_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_2c5c5a28c8d267446a2d6cb23e7b6d1715cf00bc8a768326e61de4d65966a421->leave($__internal_2c5c5a28c8d267446a2d6cb23e7b6d1715cf00bc8a768326e61de4d65966a421_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="radio"*/
/*     <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/*     value="<?php echo $view->escape($value) ?>"*/
/*     <?php if ($checked): ?> checked="checked"<?php endif ?>*/
/* />*/
/* */
