<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_d9f3b3ff5bdb30d842d17f1f68237096984cc4785c5a82306702984204dfc034 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a30c7023bed1d35993b8f89aca39ce10f76c5cb768e167313ebe0b81468fb767 = $this->env->getExtension("native_profiler");
        $__internal_a30c7023bed1d35993b8f89aca39ce10f76c5cb768e167313ebe0b81468fb767->enter($__internal_a30c7023bed1d35993b8f89aca39ce10f76c5cb768e167313ebe0b81468fb767_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_a30c7023bed1d35993b8f89aca39ce10f76c5cb768e167313ebe0b81468fb767->leave($__internal_a30c7023bed1d35993b8f89aca39ce10f76c5cb768e167313ebe0b81468fb767_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'text')) ?>*/
/* */
