<?php

/* vendor/vendorItemViews/itemCollection.html.twig */
class __TwigTemplate_f4f325f41e661f8675f16a3c4fe6f062105cadfacbcb8a029de9213858ec0eba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "vendor/vendorItemViews/itemCollection.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'javascript' => array($this, 'block_javascript'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_512177101319d38ce694abe2192c911abea2cdf56e21c0757acfe7d3adea413a = $this->env->getExtension("native_profiler");
        $__internal_512177101319d38ce694abe2192c911abea2cdf56e21c0757acfe7d3adea413a->enter($__internal_512177101319d38ce694abe2192c911abea2cdf56e21c0757acfe7d3adea413a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "vendor/vendorItemViews/itemCollection.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_512177101319d38ce694abe2192c911abea2cdf56e21c0757acfe7d3adea413a->leave($__internal_512177101319d38ce694abe2192c911abea2cdf56e21c0757acfe7d3adea413a_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_3a600e866dcfe5fdf67cff90a70225b32f9104ded07f0f556752aa68c622e43e = $this->env->getExtension("native_profiler");
        $__internal_3a600e866dcfe5fdf67cff90a70225b32f9104ded07f0f556752aa68c622e43e->enter($__internal_3a600e866dcfe5fdf67cff90a70225b32f9104ded07f0f556752aa68c622e43e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 5
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo "

    - app.user.first_name

";
        
        $__internal_3a600e866dcfe5fdf67cff90a70225b32f9104ded07f0f556752aa68c622e43e->leave($__internal_3a600e866dcfe5fdf67cff90a70225b32f9104ded07f0f556752aa68c622e43e_prof);

    }

    // line 11
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_fbda95279f1f3b4c9736d1f492c20c181a1cc10cfa79334c4e444d38556b6bc6 = $this->env->getExtension("native_profiler");
        $__internal_fbda95279f1f3b4c9736d1f492c20c181a1cc10cfa79334c4e444d38556b6bc6->enter($__internal_fbda95279f1f3b4c9736d1f492c20c181a1cc10cfa79334c4e444d38556b6bc6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        // line 12
        echo "     ";
        $this->displayParentBlock("javascript", $context, $blocks);
        echo "


 ";
        
        $__internal_fbda95279f1f3b4c9736d1f492c20c181a1cc10cfa79334c4e444d38556b6bc6->leave($__internal_fbda95279f1f3b4c9736d1f492c20c181a1cc10cfa79334c4e444d38556b6bc6_prof);

    }

    // line 17
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_8cacbd3addb81bea405f9725bb9d6af5f79818ea57e23f3ce121592fa69fe61a = $this->env->getExtension("native_profiler");
        $__internal_8cacbd3addb81bea405f9725bb9d6af5f79818ea57e23f3ce121592fa69fe61a->enter($__internal_8cacbd3addb81bea405f9725bb9d6af5f79818ea57e23f3ce121592fa69fe61a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 18
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

";
        
        $__internal_8cacbd3addb81bea405f9725bb9d6af5f79818ea57e23f3ce121592fa69fe61a->leave($__internal_8cacbd3addb81bea405f9725bb9d6af5f79818ea57e23f3ce121592fa69fe61a_prof);

    }

    // line 21
    public function block_body($context, array $blocks = array())
    {
        $__internal_48daef2508b9f051d69dcd889e30c3cfc11ceed38b4af5ac483e25452909fd2e = $this->env->getExtension("native_profiler");
        $__internal_48daef2508b9f051d69dcd889e30c3cfc11ceed38b4af5ac483e25452909fd2e->enter($__internal_48daef2508b9f051d69dcd889e30c3cfc11ceed38b4af5ac483e25452909fd2e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 22
        echo "    ";
        $this->displayParentBlock("body", $context, $blocks);
        echo "


<main class=\"container\">
    <div class=\"row items\">

            <a href=\"";
        // line 28
        echo $this->env->getExtension('routing')->getPath("vendor_item_create");
        echo "\" class=\"add-item-btn col-xs-offset-1 btn btn-warning btn-lg \" >Add a new item</a>


        <ul class=\"list-inline\">
            ";
        // line 32
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["items"]) ? $context["items"] : $this->getContext($context, "items")));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 33
            echo "                <li class=\"vendor-item-review col-md-2 col-sm-4 col-xs-8 col-sm-offset-1 col-xs-offset-2  store-item\">

                    <a class=\"\" href=\"";
            // line 35
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("single_item", array("itemObj" => $this->getAttribute($context["item"], "id", array()))), "html", null, true);
            echo "\">

                        ";
            // line 38
            echo "                        <div class=\"item-img\"
                             style=\"
                                     background: url( ";
            // line 40
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl(("uploads/" . $this->getAttribute($context["item"], "itemImg", array()))), "html", null, true);
            echo " ) no-repeat;
                                     /*background-repeat: ;*/
                                     background-size: cover;
                                     background-position: center;
                                     background-color: #fff;
                                     border: 1px solid rgba(0,0,0,0.38);


                                     \">

                            ";
            // line 50
            if (($this->getAttribute($context["item"], "quantity", array()) == 0)) {
                // line 51
                echo "
                                <p class=\"out-of-stock \">Currently Out Of Stock</p>
                            ";
            } elseif (($this->getAttribute(            // line 53
$context["item"], "isRemovedFromStore", array()) == true)) {
                // line 54
                echo "
                                <p class=\"removed-from-store alert alert-warning\">This item is currently removed from the store</p>
                            ";
            }
            // line 57
            echo "
                            <div class=\"item-name \">
                                <p>";
            // line 59
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "itemName", array()), "html", null, true);
            echo "</p>
                                <p>\$";
            // line 60
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "price", array()), "html", null, true);
            echo "</p>
                                ";
            // line 62
            echo "                            </div>
                        </div>
                    </a>
                    <div class=\"item-action col-xs-6\">
                        <a class=\" btn btn-success\" href=\"";
            // line 66
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("vendor_item_edit", array("id" => $this->getAttribute($context["item"], "id", array()))), "html", null, true);
            echo "\">Edit</a>
                    </div>
                    <div class=\"item-action col-xs-6\">
                        <a class=\" btn btn-danger\"href=\"";
            // line 69
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("vendor_item_confirm_delete", array("id" => $this->getAttribute($context["item"], "id", array()))), "html", null, true);
            echo "\">Delete</a>
                    </div>
                </li>


            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 75
        echo "        </ul>
    </div>
</main>




















";
        // line 100
        echo "
";
        // line 102
        echo "
    ";
        // line 104
        echo "
    ";
        // line 106
        echo "        ";
        // line 107
        echo "        ";
        // line 108
        echo "        ";
        // line 109
        echo "        ";
        // line 110
        echo "
    ";
        // line 112
        echo "
    ";
        // line 114
        echo "
";
        // line 116
        echo "

";
        
        $__internal_48daef2508b9f051d69dcd889e30c3cfc11ceed38b4af5ac483e25452909fd2e->leave($__internal_48daef2508b9f051d69dcd889e30c3cfc11ceed38b4af5ac483e25452909fd2e_prof);

    }

    public function getTemplateName()
    {
        return "vendor/vendorItemViews/itemCollection.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  244 => 116,  241 => 114,  238 => 112,  235 => 110,  233 => 109,  231 => 108,  229 => 107,  227 => 106,  224 => 104,  221 => 102,  218 => 100,  193 => 75,  181 => 69,  175 => 66,  169 => 62,  165 => 60,  161 => 59,  157 => 57,  152 => 54,  150 => 53,  146 => 51,  144 => 50,  131 => 40,  127 => 38,  122 => 35,  118 => 33,  114 => 32,  107 => 28,  97 => 22,  91 => 21,  80 => 18,  74 => 17,  62 => 12,  56 => 11,  43 => 5,  37 => 4,  11 => 2,);
    }
}
/* */
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block title %}*/
/*     {{ parent() }}*/
/* */
/*     - app.user.first_name*/
/* */
/* {% endblock %}*/
/* */
/*  {% block javascript %}*/
/*      {{ parent() }}*/
/* */
/* */
/*  {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/* */
/* {% endblock %}*/
/* {% block body %}*/
/*     {{ parent() }}*/
/* */
/* */
/* <main class="container">*/
/*     <div class="row items">*/
/* */
/*             <a href="{{ path('vendor_item_create') }}" class="add-item-btn col-xs-offset-1 btn btn-warning btn-lg " >Add a new item</a>*/
/* */
/* */
/*         <ul class="list-inline">*/
/*             {% for item in items %}*/
/*                 <li class="vendor-item-review col-md-2 col-sm-4 col-xs-8 col-sm-offset-1 col-xs-offset-2  store-item">*/
/* */
/*                     <a class="" href="{{ path('single_item', { 'itemObj' : item.id }) }}">*/
/* */
/*                         {#<img class="img-responsive center-block item-img" align="middle" src="{{ asset('uploads/' ~ item.itemImg ) }}" alt="{{ item.itemName }}"/>#}*/
/*                         <div class="item-img"*/
/*                              style="*/
/*                                      background: url( {{ asset('uploads/' ~ item.itemImg) }} ) no-repeat;*/
/*                                      /*background-repeat: ;*//* */
/*                                      background-size: cover;*/
/*                                      background-position: center;*/
/*                                      background-color: #fff;*/
/*                                      border: 1px solid rgba(0,0,0,0.38);*/
/* */
/* */
/*                                      ">*/
/* */
/*                             {% if item.quantity == 0 %}*/
/* */
/*                                 <p class="out-of-stock ">Currently Out Of Stock</p>*/
/*                             {% elseif item.isRemovedFromStore == true %}*/
/* */
/*                                 <p class="removed-from-store alert alert-warning">This item is currently removed from the store</p>*/
/*                             {% endif %}*/
/* */
/*                             <div class="item-name ">*/
/*                                 <p>{{ item.itemName }}</p>*/
/*                                 <p>${{ item.price }}</p>*/
/*                                 {#<p>{{ asset("uploads/" ~ item.itemImg ) }}</p>#}*/
/*                             </div>*/
/*                         </div>*/
/*                     </a>*/
/*                     <div class="item-action col-xs-6">*/
/*                         <a class=" btn btn-success" href="{{ path('vendor_item_edit', {'id' : item.id}) }}">Edit</a>*/
/*                     </div>*/
/*                     <div class="item-action col-xs-6">*/
/*                         <a class=" btn btn-danger"href="{{ path('vendor_item_confirm_delete', {'id' : item.id}) }}">Delete</a>*/
/*                     </div>*/
/*                 </li>*/
/* */
/* */
/*             {% endfor %}*/
/*         </ul>*/
/*     </div>*/
/* </main>*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* {#<a href="{{ path('vendor_item_create') }}" class="" >add a new item</a>#}*/
/* {#<p>{{ app.user.firstName }}</p>#}*/
/* */
/* {#<ul>#}*/
/* */
/*     {#{% for item in items %}#}*/
/* */
/*     {#<li>#}*/
/*         {#<img src="{{ asset('uploads/' ~ item.itemImg ) }}" alt="{{ item.itemName }}"/>#}*/
/*         {#<p>{{ item.itemName }}</p>#}*/
/*         {#<a href="{{ path('vendor_item_edit', {'id' : item.id}) }}">Edit</a>#}*/
/*         {#<a href="{{ path('vendor_item_confirm_delete', {'id' : item.id}) }}">Delete</a>#}*/
/* */
/*     {#</li>#}*/
/* */
/*     {#{% endfor %}#}*/
/* */
/* {#</ul>#}*/
/* */
/* */
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* */
/* */
