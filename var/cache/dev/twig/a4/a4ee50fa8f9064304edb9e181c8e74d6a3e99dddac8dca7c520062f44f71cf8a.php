<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_a9c8901571ebd2b2fdc5f83d570d0d71967f98775dd32c997212cccbc59ff5c7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bb153008bef623a91426af2cc8e02d6ccda05c177166237d44aca8c8cee33b54 = $this->env->getExtension("native_profiler");
        $__internal_bb153008bef623a91426af2cc8e02d6ccda05c177166237d44aca8c8cee33b54->enter($__internal_bb153008bef623a91426af2cc8e02d6ccda05c177166237d44aca8c8cee33b54_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_bb153008bef623a91426af2cc8e02d6ccda05c177166237d44aca8c8cee33b54->leave($__internal_bb153008bef623a91426af2cc8e02d6ccda05c177166237d44aca8c8cee33b54_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'url')) ?>*/
/* */
