<?php

/* :store:_footerNav.html.twig */
class __TwigTemplate_f0c82d61546023c99286f8b53a3ce2e99576195262a360139684f465ba58ce42 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cdd5d05b2f08519785f7b1dea69d71b7c4d12e6841ff80f4d672443a9e40f79b = $this->env->getExtension("native_profiler");
        $__internal_cdd5d05b2f08519785f7b1dea69d71b7c4d12e6841ff80f4d672443a9e40f79b->enter($__internal_cdd5d05b2f08519785f7b1dea69d71b7c4d12e6841ff80f4d672443a9e40f79b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":store:_footerNav.html.twig"));

        // line 1
        echo "




<footer class=\"footer site-footer nav navbar-fixed-bottom navbar-inverse\">

    <nav class=\"container\">

         ";
        // line 11
        echo "        ";
        // line 12
        echo "
        <ul class=\"nav navbar-nav\">

             ";
        // line 16
        echo "
            ";
        // line 17
        if ($this->env->getExtension('security')->isGranted("ROLE_VENDOR")) {
            // line 18
            echo "            <li  role=\"presentation\" ><a href=\"";
            echo $this->env->getExtension('routing')->getPath("secure_logout");
            echo "\">Vendor Logout</a></li>
            <li role=\"presentation\"><a href=\"";
            // line 19
            echo $this->env->getExtension('routing')->getPath("vendor_dashboard");
            echo "\">Dashboard</a></li>

            ";
        } else {
            // line 22
            echo "            <li role=\"presentation\"><a class=\"text-muted\" href=\"";
            echo $this->env->getExtension('routing')->getPath("secure_login");
            echo "\">Vendor Login</a></li>
            <li role=\"presentation\"><a href=\"";
            // line 23
            echo $this->env->getExtension('routing')->getPath("vendor_register");
            echo "\">Become a vendor</a></li>

            ";
        }
        // line 26
        echo "
        </ul>
    </nav>

</footer>

";
        
        $__internal_cdd5d05b2f08519785f7b1dea69d71b7c4d12e6841ff80f4d672443a9e40f79b->leave($__internal_cdd5d05b2f08519785f7b1dea69d71b7c4d12e6841ff80f4d672443a9e40f79b_prof);

    }

    public function getTemplateName()
    {
        return ":store:_footerNav.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 26,  61 => 23,  56 => 22,  50 => 19,  45 => 18,  43 => 17,  40 => 16,  35 => 12,  33 => 11,  22 => 1,);
    }
}
/* */
/* */
/* */
/* */
/* */
/* <footer class="footer site-footer nav navbar-fixed-bottom navbar-inverse">*/
/* */
/*     <nav class="container">*/
/* */
/*          {#Link to home page#}*/
/*         {#<h1 class="logo col-md-4 col-xs-6 col-xs-offset-1"> <a href="{{ path('store') }}">The Bladesmith's Workbench</a> </h1>#}*/
/* */
/*         <ul class="nav navbar-nav">*/
/* */
/*              {#Link to a vendor login / logout#}*/
/* */
/*             {% if is_granted('ROLE_VENDOR') %}*/
/*             <li  role="presentation" ><a href="{{ path('secure_logout') }}">Vendor Logout</a></li>*/
/*             <li role="presentation"><a href="{{ path('vendor_dashboard') }}">Dashboard</a></li>*/
/* */
/*             {% else %}*/
/*             <li role="presentation"><a class="text-muted" href="{{ path('secure_login') }}">Vendor Login</a></li>*/
/*             <li role="presentation"><a href="{{ path('vendor_register') }}">Become a vendor</a></li>*/
/* */
/*             {% endif %}*/
/* */
/*         </ul>*/
/*     </nav>*/
/* */
/* </footer>*/
/* */
/* */
