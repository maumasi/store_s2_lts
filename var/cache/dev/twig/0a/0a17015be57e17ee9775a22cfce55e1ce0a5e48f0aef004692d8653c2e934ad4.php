<?php

/* :security:guestLogin.html.twig */
class __TwigTemplate_a460690dca94c7a3727c9d7cbf8592931164bd3373061968605e05c3d15ca92c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 5
        $this->parent = $this->loadTemplate("base.html.twig", ":security:guestLogin.html.twig", 5);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bac31fddaedf7a40bf2a1188628fc69503f59cf6dadc8010860714eca9d097a8 = $this->env->getExtension("native_profiler");
        $__internal_bac31fddaedf7a40bf2a1188628fc69503f59cf6dadc8010860714eca9d097a8->enter($__internal_bac31fddaedf7a40bf2a1188628fc69503f59cf6dadc8010860714eca9d097a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":security:guestLogin.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bac31fddaedf7a40bf2a1188628fc69503f59cf6dadc8010860714eca9d097a8->leave($__internal_bac31fddaedf7a40bf2a1188628fc69503f59cf6dadc8010860714eca9d097a8_prof);

    }

    // line 8
    public function block_title($context, array $blocks = array())
    {
        $__internal_bffcc6f3e1f9ac984d8de4f27268d2876695f6beb27634dc4154ff5055788ac1 = $this->env->getExtension("native_profiler");
        $__internal_bffcc6f3e1f9ac984d8de4f27268d2876695f6beb27634dc4154ff5055788ac1->enter($__internal_bffcc6f3e1f9ac984d8de4f27268d2876695f6beb27634dc4154ff5055788ac1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 9
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo "

    - Login
";
        
        $__internal_bffcc6f3e1f9ac984d8de4f27268d2876695f6beb27634dc4154ff5055788ac1->leave($__internal_bffcc6f3e1f9ac984d8de4f27268d2876695f6beb27634dc4154ff5055788ac1_prof);

    }

    // line 14
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_f2a10267fd82624d1ccb4a5d124462b4f61acb1bbf719b0956ce23dd49463609 = $this->env->getExtension("native_profiler");
        $__internal_f2a10267fd82624d1ccb4a5d124462b4f61acb1bbf719b0956ce23dd49463609->enter($__internal_f2a10267fd82624d1ccb4a5d124462b4f61acb1bbf719b0956ce23dd49463609_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 15
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
        
        $__internal_f2a10267fd82624d1ccb4a5d124462b4f61acb1bbf719b0956ce23dd49463609->leave($__internal_f2a10267fd82624d1ccb4a5d124462b4f61acb1bbf719b0956ce23dd49463609_prof);

    }

    // line 19
    public function block_body($context, array $blocks = array())
    {
        $__internal_7e9e3176bce953fcd964c0125468692f8082edfd29a6d1a9107715b2130abaed = $this->env->getExtension("native_profiler");
        $__internal_7e9e3176bce953fcd964c0125468692f8082edfd29a6d1a9107715b2130abaed->enter($__internal_7e9e3176bce953fcd964c0125468692f8082edfd29a6d1a9107715b2130abaed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 20
        $this->displayParentBlock("body", $context, $blocks);
        echo "

<main class=\"container login-form\">
    <div class=\"row login\">

        <h1 class=\"col-lg-8\">Customer Login</h1>



        ";
        // line 29
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 30
            echo "            <div class=\"alert alert-danger\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</div>
        ";
        }
        // line 32
        echo "
            ";
        // line 33
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("class" => "form-inline col-lg-8 col-sm-10")));
        // line 37
        echo "

            <div class=\"col-md-3 col-sm-4 \">
                ";
        // line 40
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "_email", array()), 'row', array("attr" => array("placeholder" => "Email"), "label_attr" => array("class" => "sr-only")));
        // line 47
        echo "
            </div>

            <div class=\"col-md-3 col-sm-4 \">
                ";
        // line 51
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "_password", array()), 'row', array("attr" => array("placeholder" => "Password"), "label_attr" => array("class" => "sr-only")));
        // line 58
        echo "
            </div>

            <div class=\"col-md-4 col-sm-4 \">
                <button type=\"submit\" class=\"btn btn-primary col-xs-12\" formnovalidate>login</button>

            </div>
            ";
        // line 65
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

        <p class=\"col-md-12\">Not registered customer?<a href=\"";
        // line 67
        echo $this->env->getExtension('routing')->getPath("guest_register");
        echo "\" class=\"\"> Easy, register here.</a></p>
    </div>


</main>
";
        
        $__internal_7e9e3176bce953fcd964c0125468692f8082edfd29a6d1a9107715b2130abaed->leave($__internal_7e9e3176bce953fcd964c0125468692f8082edfd29a6d1a9107715b2130abaed_prof);

    }

    public function getTemplateName()
    {
        return ":security:guestLogin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  129 => 67,  124 => 65,  115 => 58,  113 => 51,  107 => 47,  105 => 40,  100 => 37,  98 => 33,  95 => 32,  89 => 30,  87 => 29,  75 => 20,  69 => 19,  60 => 15,  54 => 14,  42 => 9,  36 => 8,  11 => 5,);
    }
}
/* */
/* {#<?php#}*/
/* */
/* */
/* {% extends 'base.html.twig' %}*/
/* */
/* */
/* {% block title %}*/
/*     {{ parent() }}*/
/* */
/*     - Login*/
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/* {{ parent() }}*/
/* {% endblock %}*/
/* */
/* */
/* {% block body %}*/
/* {{ parent() }}*/
/* */
/* <main class="container login-form">*/
/*     <div class="row login">*/
/* */
/*         <h1 class="col-lg-8">Customer Login</h1>*/
/* */
/* */
/* */
/*         {% if error %}*/
/*             <div class="alert alert-danger">{{ error.messageKey|trans(error.messageData, 'security') }}</div>*/
/*         {% endif %}*/
/* */
/*             {{ form_start(form,{*/
/*                 'attr': {*/
/*                     'class': 'form-inline col-lg-8 col-sm-10'*/
/*                 }*/
/*             }) }}*/
/* */
/*             <div class="col-md-3 col-sm-4 ">*/
/*                 {{ form_row(form._email,{*/
/*                     'attr': {*/
/*                         'placeholder': 'Email',*/
/*                     },*/
/*                     'label_attr': {*/
/*                         'class': 'sr-only'*/
/*                     }*/
/*                 }) }}*/
/*             </div>*/
/* */
/*             <div class="col-md-3 col-sm-4 ">*/
/*                 {{ form_row(form._password,{*/
/*                     'attr': {*/
/*                         'placeholder': 'Password',*/
/*                     },*/
/*                     'label_attr': {*/
/*                         'class': 'sr-only'*/
/*                     }*/
/*                 }) }}*/
/*             </div>*/
/* */
/*             <div class="col-md-4 col-sm-4 ">*/
/*                 <button type="submit" class="btn btn-primary col-xs-12" formnovalidate>login</button>*/
/* */
/*             </div>*/
/*             {{ form_end(form) }}*/
/* */
/*         <p class="col-md-12">Not registered customer?<a href="{{ path('guest_register') }}" class=""> Easy, register here.</a></p>*/
/*     </div>*/
/* */
/* */
/* </main>*/
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
