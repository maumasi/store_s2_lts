<?php

/* TwigBundle:Exception:error.atom.twig */
class __TwigTemplate_f710951e51974576e7b1b7efd1f047f748e573244a734bfd16dde6fb42dcfd9a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9e0ef4f121393156a44da400fa0b1257e06bbcf8ff13dc0d8018e73e31535d9f = $this->env->getExtension("native_profiler");
        $__internal_9e0ef4f121393156a44da400fa0b1257e06bbcf8ff13dc0d8018e73e31535d9f->enter($__internal_9e0ef4f121393156a44da400fa0b1257e06bbcf8ff13dc0d8018e73e31535d9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "TwigBundle:Exception:error.atom.twig", 1)->display($context);
        
        $__internal_9e0ef4f121393156a44da400fa0b1257e06bbcf8ff13dc0d8018e73e31535d9f->leave($__internal_9e0ef4f121393156a44da400fa0b1257e06bbcf8ff13dc0d8018e73e31535d9f_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.atom.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/error.xml.twig' %}*/
/* */
