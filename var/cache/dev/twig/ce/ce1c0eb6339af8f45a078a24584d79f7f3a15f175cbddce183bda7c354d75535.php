<?php

/* :vendor/vendorFormTemplates:_edit.html.twig */
class __TwigTemplate_fd25e95cb71d38466df4df507dde45a9362f1042998cb41d552ee57f00e57127 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_77ecb3e2a009e1ec261c92f00dbdbcdd2a1b1245f8bd9396ab1214b096258099 = $this->env->getExtension("native_profiler");
        $__internal_77ecb3e2a009e1ec261c92f00dbdbcdd2a1b1245f8bd9396ab1214b096258099->enter($__internal_77ecb3e2a009e1ec261c92f00dbdbcdd2a1b1245f8bd9396ab1214b096258099_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":vendor/vendorFormTemplates:_edit.html.twig"));

        // line 1
        echo "

";
        // line 3
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), 'form_start');
        echo "

    ";
        // line 6
        echo "
    ";
        // line 7
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "firstName", array()), 'row');
        echo "
    ";
        // line 8
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "lastName", array()), 'row');
        echo "
    ";
        // line 9
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "companyName", array()), 'row');
        echo "
    ";
        // line 10
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "email", array()), 'row');
        echo "
    ";
        // line 11
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "newPassword", array()), "first", array()), 'row');
        echo "
    ";
        // line 12
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), "newPassword", array()), "second", array()), 'row');
        echo "


<button type=\"submit\" class=\"btn btn-primary\" formnovalidate >";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["submitBtn"]) ? $context["submitBtn"] : $this->getContext($context, "submitBtn")), "html", null, true);
        echo "</button>
<a class=\"btn btn-primary\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, (isset($context["cancelPath"]) ? $context["cancelPath"] : $this->getContext($context, "cancelPath")), "html", null, true);
        echo "\">Cancel</a>


";
        // line 19
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["registrationForm"]) ? $context["registrationForm"] : $this->getContext($context, "registrationForm")), 'form_end');
        
        $__internal_77ecb3e2a009e1ec261c92f00dbdbcdd2a1b1245f8bd9396ab1214b096258099->leave($__internal_77ecb3e2a009e1ec261c92f00dbdbcdd2a1b1245f8bd9396ab1214b096258099_prof);

    }

    public function getTemplateName()
    {
        return ":vendor/vendorFormTemplates:_edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 19,  64 => 16,  60 => 15,  54 => 12,  50 => 11,  46 => 10,  42 => 9,  38 => 8,  34 => 7,  31 => 6,  26 => 3,  22 => 1,);
    }
}
/* */
/* */
/* {{ form_start(registrationForm) }}*/
/* */
/*     {#{{ form_widget(registrationForm) }}#}*/
/* */
/*     {{ form_row(registrationForm.firstName) }}*/
/*     {{ form_row(registrationForm.lastName) }}*/
/*     {{ form_row(registrationForm.companyName) }}*/
/*     {{ form_row(registrationForm.email) }}*/
/*     {{ form_row(registrationForm.newPassword.first) }}*/
/*     {{ form_row(registrationForm.newPassword.second) }}*/
/* */
/* */
/* <button type="submit" class="btn btn-primary" formnovalidate >{{ submitBtn }}</button>*/
/* <a class="btn btn-primary" href="{{ cancelPath }}">Cancel</a>*/
/* */
/* */
/* {{ form_end(registrationForm) }}*/
