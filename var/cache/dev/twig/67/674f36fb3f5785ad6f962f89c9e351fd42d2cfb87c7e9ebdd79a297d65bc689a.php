<?php

/* TwigBundle:Exception:error.js.twig */
class __TwigTemplate_2da6e9ce20da629c14b5396b2ff2c2f9a8677bca0f120f42451eba087e0b0330 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7f187dc80198662bab5156fddc01ca3ec247f9f154c0f55c39b38e6465232ebe = $this->env->getExtension("native_profiler");
        $__internal_7f187dc80198662bab5156fddc01ca3ec247f9f154c0f55c39b38e6465232ebe->enter($__internal_7f187dc80198662bab5156fddc01ca3ec247f9f154c0f55c39b38e6465232ebe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_7f187dc80198662bab5156fddc01ca3ec247f9f154c0f55c39b38e6465232ebe->leave($__internal_7f187dc80198662bab5156fddc01ca3ec247f9f154c0f55c39b38e6465232ebe_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
