<?php

/* store/_cartTablePostLogin.html.twig */
class __TwigTemplate_7cd70c8b1bb537ef8758452708055dc46c7f82c7b49d393cd3cd234f923a8351 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d61e514213d8b9cbf9e2a1b1c38da331217d96eea1b5848d9683d4e2347ce938 = $this->env->getExtension("native_profiler");
        $__internal_d61e514213d8b9cbf9e2a1b1c38da331217d96eea1b5848d9683d4e2347ce938->enter($__internal_d61e514213d8b9cbf9e2a1b1c38da331217d96eea1b5848d9683d4e2347ce938_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "store/_cartTablePostLogin.html.twig"));

        // line 1
        echo "

<table class=\"table\" id=\"cartList\">

    <tr>
        <th>Item</th>
        <th>Qty.</th>
        <th>Price</th>
        ";
        // line 10
        echo "    </tr>


    ";
        // line 13
        $context["total"] = 0;
        // line 14
        echo "    ";
        $context["count"] = 0;
        // line 15
        echo "
    ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["cartCount"]) ? $context["cartCount"] : $this->getContext($context, "cartCount")));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 17
            echo "
        <tr>
            <td>
                <a class=\"checkout-item row\" href=\"";
            // line 20
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("single_item", array("itemObj" => $this->getAttribute($this->getAttribute($context["item"], "item", array()), "id", array()))), "html", null, true);
            echo "\">
                    ";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "item", array()), "itemName", array()), "html", null, true);
            echo "
                </a>
            </td>

            <td>
                <p>";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "quantity", array()), "html", null, true);
            echo " x \$";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($this->getAttribute($context["item"], "item", array()), "price", array()), 2, ".", ","), "html", null, true);
            echo " each</p>
            </td>

            <td>
                <p>\$";
            // line 30
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ($this->getAttribute($this->getAttribute($context["item"], "item", array()), "price", array()) * $this->getAttribute($context["item"], "quantity", array())), 2, ".", ","), "html", null, true);
            echo "</p>
            </td>


            ";
            // line 34
            $context["total"] = ((isset($context["total"]) ? $context["total"] : $this->getContext($context, "total")) + ($this->getAttribute($this->getAttribute($context["item"], "item", array()), "price", array()) * $this->getAttribute($context["item"], "quantity", array())));
            // line 35
            echo "            ";
            $context["count"] = ((isset($context["count"]) ? $context["count"] : $this->getContext($context, "count")) + $this->getAttribute($context["item"], "quantity", array()));
            // line 36
            echo "            ";
            // line 37
            echo "            ";
            // line 38
            echo "            ";
            // line 39
            echo "            ";
            // line 40
            echo "            ";
            // line 41
            echo "        </tr>

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "

    <tr class=\"cartStatus\">
        <td>
            Items in cart:
        </td>

        <td>
            ";
        // line 52
        echo twig_escape_filter($this->env, (isset($context["count"]) ? $context["count"] : $this->getContext($context, "count")), "html", null, true);
        echo "
        </td>
    </tr>

    <tr class=\"cartStatus\">
        <td>
            Total:
        </td>

        <td>
            \$";
        // line 62
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (twig_round(((isset($context["total"]) ? $context["total"] : $this->getContext($context, "total")) * 100), 0, "floor") / 100), 2, ".", ","), "html", null, true);
        echo "
        </td>
    </tr>


</table>";
        
        $__internal_d61e514213d8b9cbf9e2a1b1c38da331217d96eea1b5848d9683d4e2347ce938->leave($__internal_d61e514213d8b9cbf9e2a1b1c38da331217d96eea1b5848d9683d4e2347ce938_prof);

    }

    public function getTemplateName()
    {
        return "store/_cartTablePostLogin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 62,  115 => 52,  105 => 44,  97 => 41,  95 => 40,  93 => 39,  91 => 38,  89 => 37,  87 => 36,  84 => 35,  82 => 34,  75 => 30,  66 => 26,  58 => 21,  54 => 20,  49 => 17,  45 => 16,  42 => 15,  39 => 14,  37 => 13,  32 => 10,  22 => 1,);
    }
}
/* */
/* */
/* <table class="table" id="cartList">*/
/* */
/*     <tr>*/
/*         <th>Item</th>*/
/*         <th>Qty.</th>*/
/*         <th>Price</th>*/
/*         {#<th>Remove Item</th>#}*/
/*     </tr>*/
/* */
/* */
/*     {% set total = 0.00 %}*/
/*     {% set count = 0 %}*/
/* */
/*     {% for item in cartCount %}*/
/* */
/*         <tr>*/
/*             <td>*/
/*                 <a class="checkout-item row" href="{{ path('single_item', { 'itemObj' : item.item.id }) }}">*/
/*                     {{ item.item.itemName }}*/
/*                 </a>*/
/*             </td>*/
/* */
/*             <td>*/
/*                 <p>{{ item.quantity }} x ${{ item.item.price | number_format(2, '.', ',')}} each</p>*/
/*             </td>*/
/* */
/*             <td>*/
/*                 <p>${{ (item.item.price * item.quantity) | number_format(2, '.', ',')}}</p>*/
/*             </td>*/
/* */
/* */
/*             {% set total = total + item.item.price * item.quantity %}*/
/*             {% set count = count + item.quantity  %}*/
/*             {#<td>#}*/
/*             {#<form class="col-sm-2" action="{{ path('remove_item_at_checkout', {'itemId' : item.id}) }}" method="GET">#}*/
/*             {#<button class="btn btn-danger" type="submit"><span class="fa fa-times-circle-o"></span></button>#}*/
/*             {#</form>#}*/
/*             {#</td>#}*/
/*         </tr>*/
/* */
/*     {% endfor %}*/
/* */
/* */
/*     <tr class="cartStatus">*/
/*         <td>*/
/*             Items in cart:*/
/*         </td>*/
/* */
/*         <td>*/
/*             {{ count }}*/
/*         </td>*/
/*     </tr>*/
/* */
/*     <tr class="cartStatus">*/
/*         <td>*/
/*             Total:*/
/*         </td>*/
/* */
/*         <td>*/
/*             ${{ (((total * 100) | round(0, 'floor')) / 100) | number_format(2, '.', ',') }}*/
/*         </td>*/
/*     </tr>*/
/* */
/* */
/* </table>*/
