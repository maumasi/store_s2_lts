<?php

/* store/_cartList.html.twig */
class __TwigTemplate_ec9d3f551f9fd4009957bbd8330655da2c661072a694ed0614391351b6c87641 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0acab5c94f545459e401655061e49ffe1c27e59f9742918b9dcbbf9dae32ae59 = $this->env->getExtension("native_profiler");
        $__internal_0acab5c94f545459e401655061e49ffe1c27e59f9742918b9dcbbf9dae32ae59->enter($__internal_0acab5c94f545459e401655061e49ffe1c27e59f9742918b9dcbbf9dae32ae59_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "store/_cartList.html.twig"));

        // line 1
        echo "

<div class=\"cartList col-xs-12\" >
    <div class=\"panel panel-default pull-right\">

        <!-- Default panel contents -->
        <div class=\"panel-heading\"><a href=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("store_checkout");
        echo "\">Click to Checkout <span class=\"fa fa-angle-double-right\"></span></a></div>

        <!-- Table -->
        ";
        // line 10
        if (($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()) != null)) {
            // line 11
            echo "
            ";
            // line 12
            echo twig_include($this->env, $context, "store/_cartTablePostLogin.html.twig");
            echo "

        ";
        } else {
            // line 15
            echo "
            ";
            // line 16
            echo twig_include($this->env, $context, "store/_cartTablePreLogin.html.twig");
            echo "

        ";
        }
        // line 19
        echo "
        <div class=\"panel-heading\"><a href=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("store_checkout");
        echo "\">Click to Checkout <span class=\"fa fa-angle-double-right\"></span></a></div>

        <span type=\"button\" class=\"close-cart btn btn-cancel col-xs-offset-4\">close</span>
    </div>
</div>


";
        
        $__internal_0acab5c94f545459e401655061e49ffe1c27e59f9742918b9dcbbf9dae32ae59->leave($__internal_0acab5c94f545459e401655061e49ffe1c27e59f9742918b9dcbbf9dae32ae59_prof);

    }

    public function getTemplateName()
    {
        return "store/_cartList.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 20,  56 => 19,  50 => 16,  47 => 15,  41 => 12,  38 => 11,  36 => 10,  30 => 7,  22 => 1,);
    }
}
/* */
/* */
/* <div class="cartList col-xs-12" >*/
/*     <div class="panel panel-default pull-right">*/
/* */
/*         <!-- Default panel contents -->*/
/*         <div class="panel-heading"><a href="{{ path('store_checkout') }}">Click to Checkout <span class="fa fa-angle-double-right"></span></a></div>*/
/* */
/*         <!-- Table -->*/
/*         {% if app.user != null %}*/
/* */
/*             {{ include('store/_cartTablePostLogin.html.twig') }}*/
/* */
/*         {% else %}*/
/* */
/*             {{ include('store/_cartTablePreLogin.html.twig') }}*/
/* */
/*         {% endif %}*/
/* */
/*         <div class="panel-heading"><a href="{{ path('store_checkout') }}">Click to Checkout <span class="fa fa-angle-double-right"></span></a></div>*/
/* */
/*         <span type="button" class="close-cart btn btn-cancel col-xs-offset-4">close</span>*/
/*     </div>*/
/* </div>*/
/* */
/* */
/* {#<script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>#}*/
/* {#<script src="{{ asset('js/main.js') }}"></script>#}*/
