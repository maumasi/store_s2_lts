<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_d7f644c80341c53b7c1fa7d018effe541e54c3ab7bfabaac5bd20d5789476b9f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ef0cd42be2346dd12f45ea50aee8d97494804498dc158a7670f36cb8fc697b7c = $this->env->getExtension("native_profiler");
        $__internal_ef0cd42be2346dd12f45ea50aee8d97494804498dc158a7670f36cb8fc697b7c->enter($__internal_ef0cd42be2346dd12f45ea50aee8d97494804498dc158a7670f36cb8fc697b7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_ef0cd42be2346dd12f45ea50aee8d97494804498dc158a7670f36cb8fc697b7c->leave($__internal_ef0cd42be2346dd12f45ea50aee8d97494804498dc158a7670f36cb8fc697b7c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'button_widget',  array('type' => isset($type) ? $type : 'reset')) ?>*/
/* */
