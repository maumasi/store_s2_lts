<?php

/* store/gallery.html.twig */
class __TwigTemplate_9566752d67f178aa786a6fcb8f61aac6e6eef8c7071cc4dab6188509bbf73357 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "store/gallery.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascript' => array($this, 'block_javascript'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_49341877d6daf302bb34647350fe0839e0e6b278785af6c5f8a895b5ea9888a4 = $this->env->getExtension("native_profiler");
        $__internal_49341877d6daf302bb34647350fe0839e0e6b278785af6c5f8a895b5ea9888a4->enter($__internal_49341877d6daf302bb34647350fe0839e0e6b278785af6c5f8a895b5ea9888a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "store/gallery.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_49341877d6daf302bb34647350fe0839e0e6b278785af6c5f8a895b5ea9888a4->leave($__internal_49341877d6daf302bb34647350fe0839e0e6b278785af6c5f8a895b5ea9888a4_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_d0c56d93242d22c4b747539feb32d7097102adeb3136995c30b39a4772ff6a2f = $this->env->getExtension("native_profiler");
        $__internal_d0c56d93242d22c4b747539feb32d7097102adeb3136995c30b39a4772ff6a2f->enter($__internal_d0c56d93242d22c4b747539feb32d7097102adeb3136995c30b39a4772ff6a2f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo "

    - Home Page

";
        
        $__internal_d0c56d93242d22c4b747539feb32d7097102adeb3136995c30b39a4772ff6a2f->leave($__internal_d0c56d93242d22c4b747539feb32d7097102adeb3136995c30b39a4772ff6a2f_prof);

    }

    // line 9
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_3de1943bbfc09e944a8861f3e648fd56ac90d67b75fb3548a5234b0f3484e766 = $this->env->getExtension("native_profiler");
        $__internal_3de1943bbfc09e944a8861f3e648fd56ac90d67b75fb3548a5234b0f3484e766->enter($__internal_3de1943bbfc09e944a8861f3e648fd56ac90d67b75fb3548a5234b0f3484e766_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 10
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "


";
        
        $__internal_3de1943bbfc09e944a8861f3e648fd56ac90d67b75fb3548a5234b0f3484e766->leave($__internal_3de1943bbfc09e944a8861f3e648fd56ac90d67b75fb3548a5234b0f3484e766_prof);

    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        $__internal_9a2db0c6129931fe861514a7903bb2239aa5fc0aae1cc78ec9aa39e79892e959 = $this->env->getExtension("native_profiler");
        $__internal_9a2db0c6129931fe861514a7903bb2239aa5fc0aae1cc78ec9aa39e79892e959->enter($__internal_9a2db0c6129931fe861514a7903bb2239aa5fc0aae1cc78ec9aa39e79892e959_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 15
        echo "    ";
        $this->displayParentBlock("body", $context, $blocks);
        echo "


    <div class=\"hero jumbotron\">
        ";
        // line 20
        echo "        ";
        // line 21
        echo "            ";
        // line 22
        echo "            ";
        // line 23
        echo "        ";
        // line 24
        echo "        <h1 class=\"store-h1\">The Bladesmith's Workbench</h1>


    </div>



    ";
        // line 32
        echo "


    <main class=\"gallery container\">

        <div class=\"row\">
            <h2 class=\"col-xs-offset-1\">Knife Gallery</h2>

        ";
        // line 41
        echo "            <div class=\"top-pagdgination col-lg-offset-9 col-md-offset-8 col-sm-offset-6 col-xs-offset-2\">
                ";
        // line 42
        echo twig_include($this->env, $context, "store/_storePadgination.html.twig");
        echo "
            </div>


        </div>



    ";
        // line 51
        echo "        <div class=\"row items\">
            <ul class=\"list-inline\">

                ";
        // line 54
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["items"]) ? $context["items"] : $this->getContext($context, "items")));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 55
            echo "                    ";
            if (($this->getAttribute($context["item"], "isRemovedFromStore", array()) == false)) {
                // line 56
                echo "                    <li class=\"col-md-2 col-sm-4 col-xs-8 col-sm-offset-1 col-xs-offset-2  store-item\">
                        <a href=\"";
                // line 57
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("single_item", array("itemObj" => $this->getAttribute($context["item"], "id", array()))), "html", null, true);
                echo "\">

                            ";
                // line 60
                echo "                            <div class=\"item-img\"
                                 style=\"
                                     background: url( ";
                // line 62
                echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl(("uploads/" . $this->getAttribute($context["item"], "itemImg", array()))), "html", null, true);
                echo " ) no-repeat;
                                     /*background-repeat: ;*/
                                     background-size: cover;
                                     background-position: center;
                                     background-color: #fff;
                                     border: 1px solid rgba(0,0,0,0.38);


                                        \">

                                ";
                // line 72
                if (($this->getAttribute($context["item"], "quantity", array()) == 0)) {
                    // line 73
                    echo "
                                    <p class=\"out-of-stock \">Currently Out Of Stock</p>
                                ";
                }
                // line 76
                echo "
                            <div class=\"item-name \">
                                <p>";
                // line 78
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "itemName", array()), "html", null, true);
                echo "</p>
                                <p>\$";
                // line 79
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "price", array()), "html", null, true);
                echo "</p>
                                ";
                // line 81
                echo "                            </div>


                            </div>

                        </a>
                    </li>
                    ";
            }
            // line 89
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 90
        echo "            </ul>
        </div>


";
        // line 95
        echo "        <div class=\"bottom-pagdgination col-lg-offset-9 col-md-offset-8 col-sm-offset-6 col-xs-offset-2\">
            ";
        // line 96
        echo twig_include($this->env, $context, "store/_storePadgination.html.twig");
        echo "
        </div>



    </main>





";
        
        $__internal_9a2db0c6129931fe861514a7903bb2239aa5fc0aae1cc78ec9aa39e79892e959->leave($__internal_9a2db0c6129931fe861514a7903bb2239aa5fc0aae1cc78ec9aa39e79892e959_prof);

    }

    // line 108
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_82aedd96fc648d30a8c72b95cba8308ec821a11dbbcada451e81ca6013805499 = $this->env->getExtension("native_profiler");
        $__internal_82aedd96fc648d30a8c72b95cba8308ec821a11dbbcada451e81ca6013805499->enter($__internal_82aedd96fc648d30a8c72b95cba8308ec821a11dbbcada451e81ca6013805499_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        // line 109
        echo "

    ";
        // line 111
        $this->displayParentBlock("javascript", $context, $blocks);
        echo "
";
        
        $__internal_82aedd96fc648d30a8c72b95cba8308ec821a11dbbcada451e81ca6013805499->leave($__internal_82aedd96fc648d30a8c72b95cba8308ec821a11dbbcada451e81ca6013805499_prof);

    }

    public function getTemplateName()
    {
        return "store/gallery.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  239 => 111,  235 => 109,  229 => 108,  210 => 96,  207 => 95,  201 => 90,  195 => 89,  185 => 81,  181 => 79,  177 => 78,  173 => 76,  168 => 73,  166 => 72,  153 => 62,  149 => 60,  144 => 57,  141 => 56,  138 => 55,  134 => 54,  129 => 51,  118 => 42,  115 => 41,  105 => 32,  96 => 24,  94 => 23,  92 => 22,  90 => 21,  88 => 20,  80 => 15,  74 => 14,  62 => 10,  56 => 9,  43 => 4,  37 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block title %}*/
/*     {{ parent() }}*/
/* */
/*     - Home Page*/
/* */
/* {% endblock %}*/
/* {% block stylesheets %}*/
/*     {{ parent() }}*/
/* */
/* */
/* {% endblock %}*/
/* {% block body %}*/
/*     {{ parent() }}*/
/* */
/* */
/*     <div class="hero jumbotron">*/
/*         {# breadcrumbs #}*/
/*         {#<ol class="breadcrumb">#}*/
/*             {#<li><a href="{{ path('home') }}">Home</a></li>#}*/
/*             {#<li class="active">Home</li>#}*/
/*         {#</ol>#}*/
/*         <h1 class="store-h1">The Bladesmith's Workbench</h1>*/
/* */
/* */
/*     </div>*/
/* */
/* */
/* */
/*     {#<p>{{ t }}</p> *** this was just for testing *** #}*/
/* */
/* */
/* */
/*     <main class="gallery container">*/
/* */
/*         <div class="row">*/
/*             <h2 class="col-xs-offset-1">Knife Gallery</h2>*/
/* */
/*         {#top padgination#}*/
/*             <div class="top-pagdgination col-lg-offset-9 col-md-offset-8 col-sm-offset-6 col-xs-offset-2">*/
/*                 {{ include('store/_storePadgination.html.twig') }}*/
/*             </div>*/
/* */
/* */
/*         </div>*/
/* */
/* */
/* */
/*     {#list out items#}*/
/*         <div class="row items">*/
/*             <ul class="list-inline">*/
/* */
/*                 {% for item in items %}*/
/*                     {% if item.isRemovedFromStore == false %}*/
/*                     <li class="col-md-2 col-sm-4 col-xs-8 col-sm-offset-1 col-xs-offset-2  store-item">*/
/*                         <a href="{{ path('single_item', { 'itemObj' : item.id }) }}">*/
/* */
/*                             {#<img class="img-responsive center-block item-img" align="middle" src="{{ asset('uploads/' ~ item.itemImg ) }}" alt="{{ item.itemName }}"/>#}*/
/*                             <div class="item-img"*/
/*                                  style="*/
/*                                      background: url( {{ asset('uploads/' ~ item.itemImg) }} ) no-repeat;*/
/*                                      /*background-repeat: ;*//* */
/*                                      background-size: cover;*/
/*                                      background-position: center;*/
/*                                      background-color: #fff;*/
/*                                      border: 1px solid rgba(0,0,0,0.38);*/
/* */
/* */
/*                                         ">*/
/* */
/*                                 {% if item.quantity == 0 %}*/
/* */
/*                                     <p class="out-of-stock ">Currently Out Of Stock</p>*/
/*                                 {% endif %}*/
/* */
/*                             <div class="item-name ">*/
/*                                 <p>{{ item.itemName }}</p>*/
/*                                 <p>${{ item.price }}</p>*/
/*                                 {#<p>{{ asset("uploads/" ~ item.itemImg ) }}</p>#}*/
/*                             </div>*/
/* */
/* */
/*                             </div>*/
/* */
/*                         </a>*/
/*                     </li>*/
/*                     {% endif %}*/
/*                 {% endfor %}*/
/*             </ul>*/
/*         </div>*/
/* */
/* */
/* {#bottom padgination#}*/
/*         <div class="bottom-pagdgination col-lg-offset-9 col-md-offset-8 col-sm-offset-6 col-xs-offset-2">*/
/*             {{ include('store/_storePadgination.html.twig') }}*/
/*         </div>*/
/* */
/* */
/* */
/*     </main>*/
/* */
/* */
/* */
/* */
/* */
/* {% endblock %}*/
/* {% block javascript %}*/
/* */
/* */
/*     {{ parent() }}*/
/* {% endblock %}*/
